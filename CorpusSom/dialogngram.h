#ifndef DIALOGNGRAM_H
#define DIALOGNGRAM_H
#include <QtWidgets>
#include <../Interface/Interface/BDD/bdd.h>
#include"modelsom.h"

class DialogNgram : public QDialog
{
    Q_OBJECT
public:
    DialogNgram(int, int, ModelSom *, QWidget *parent=0) ;
    void createNgramView() ;
    void showNgramTable() ;
    int IdRow ;
    int IdCol ;
    ModelSom * Model ;
private:
    QPushButton     *   OkButton ;
    QTableWidget    *   NgramView ;
};

#endif // DIALOGNGRAM_H
