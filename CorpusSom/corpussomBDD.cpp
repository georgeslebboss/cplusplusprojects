#include "corpussomBDD.h"
using namespace std;

CorpusSOMBDD::CorpusSOMBDD()
{
}

/*********************les requets sql utilises par AlgoSOM et CorpuSom**********************/
QString CorpusSOMBDD::sqlGetNomById(idd id_ngram)
{
    QSqlQuery reqn(*BDD::DatabaseConnection);
    QString gram ="";
    if (nameapp=="CorpusChar")
        reqn.prepare("select ngram from ngramme where id_ngram = ?");
    else if (nameapp=="CorpusWord")
        reqn.prepare("select form from \"WORD_form\" where id = ?");
    else if (nameapp=="SurveyWord2Vec")
        reqn.prepare("select word_litteral from \"W2V_WORDSSKIP\" where id_word = ?");
    else if (nameapp=="SurveyGlove")
        reqn.prepare("select word_litteral from \"GLOVE_WORDS\" where id_word = ?");
    reqn.addBindValue(id_ngram);
    if(not reqn.exec())
    {
        qDebug() << reqn.lastError().text()<< endl;
        return "";
    }
    while(reqn.next()) gram = "\'" + reqn.value(0).toString() + "\'";
    return gram;

}
void CorpusSOMBDD::setNameapp(QString napp)
{
    qDebug() << "Name APP: " << napp;
    nameapp=napp;
}
