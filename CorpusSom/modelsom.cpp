#include "modelsom.h"
#include "customviewdialog.h"
ModelSom::ModelSom()
{
    VisuSOMBDD=new CorpusSOMBDD();
}

QVariant ModelSom::data(const QModelIndex & index, int role) const
{
    if (not index.isValid()) return QVariant() ;
    if (role == Qt::TextAlignmentRole) return Qt::AlignHCenter ;
    if (role == Qt::DisplayRole) return cellVisibleContent(index.row(), index.column()) ;
    if (role == Qt::BackgroundRole)
    {
        // la view s'actualise à chaque fois si un des role redifini est détecté,
       // j'ai changé le comportement de la view dans  la fonction WindowSom::customViewCarte quand on clique sur finished
        //qDebug() << "role background";
        QBrush redBackground(Qt::blue);
        return QVariant(redBackground);
    }
    return QVariant();
}

// le nombre maximal de ngrammes par cellule dépend du nombre de lignes (30 = 1 ngramme, 15 = 2 ngrammes, etc)
// retourne la chaîne constituée des ngrammes visibles séparés par un RC
QString ModelSom::cellVisibleContent(int i, int j) const
{
    QString str ="";
    int compteur = 0;
    QMapIterator<idd, double> it(CarteTopo[i][j]);
    while (it.hasNext() and compteur++ < ( RowCarte * 0.3))// and compteur++ < (30 / RowCarte))
    {
        it.next();
        str += "\n" +sqlGetNomById(it.key()) ;
    }
    return str;
}

Qt::ItemFlags ModelSom::flags(const QModelIndex &index) const
{
    if (not index.isValid()) return Qt::ItemIsEnabled ;
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable ;
}

bool ModelSom::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int row = index.row() ;
    int col = index.column() ;
    if (not index.isValid()) return false ;
    if (role == Qt::TextColorRole || role==Qt::EditRole)
    {
        qDebug() << index.data().toString() + "(" + QString::number(row) +","+ QString::number(col) + ")" ;
        emit dataChanged(index,index);
        return true ;
    }
    return false;
}


int ModelSom::offset(int row) const
{
    if (row%2 == 0)
        return 0 ;
    return 1 ;
}


void ModelSom::createCarte(int lig, int col)
{
    CarteTopo = new classe * [lig] ;
    for (int i = 0 ; i < lig ; i++) CarteTopo[i] = new classe[col] ;
}
QString ModelSom::sqlGetNomById(idd id_ngram) const
{
    return VisuSOMBDD->sqlGetNomById(id_ngram);
}


