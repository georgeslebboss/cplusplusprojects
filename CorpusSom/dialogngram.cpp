#include "dialogngram.h"

DialogNgram::DialogNgram(int idrow, int idcol, ModelSom * model, QWidget *parent)
  : QDialog(parent), IdRow(idrow), IdCol(idcol), Model(model)
{
    QGridLayout * mainlayout = new QGridLayout;
    mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));
    createNgramView();
    mainlayout->addWidget(NgramView, 0, 0);
    OkButton = new QPushButton("&Ok");
    connect(OkButton, SIGNAL(clicked()), this, SLOT(close()));
    mainlayout->addWidget(OkButton, 1, 0);
    showNgramTable();
    setLayout(mainlayout);
    setWindowTitle("Classe") ;
}

void DialogNgram::createNgramView()
{
    NgramView = new QTableWidget(0, 2) ;
    NgramView->setSelectionBehavior(QAbstractItemView::SelectRows) ;
    NgramView->setHorizontalHeaderLabels(QStringList() << tr("Word") << tr("Distance")) ;
    NgramView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
    // NgramView->setSortingEnabled(true);
    NgramView->verticalHeader()->hide() ;
    NgramView->setShowGrid(false) ;
    NgramView->setContextMenuPolicy(Qt::CustomContextMenu) ;
}

void DialogNgram::showNgramTable()
{
    QMapIterator<idd, double> it(Model->CarteTopo[IdRow][IdCol]);
    while (it.hasNext())
    {
        it.next() ;
        //QTableWidgetItem * ngram = new QTableWidgetItem(Model->sqlGetWord(it.key())) ;
        QTableWidgetItem * ngram = new QTableWidgetItem(Model->sqlGetNomById(it.key())) ;
        ngram->setFlags(ngram->flags() ^ Qt::ItemIsEditable) ;
        QTableWidgetItem * distance = new QTableWidgetItem(QString::number(double(it.value()))) ;
        distance->setTextAlignment(Qt::AlignLeft);
        distance->setFlags(distance->flags() ^ Qt::ItemIsEditable);
        int row = NgramView->rowCount();
        NgramView->insertRow(row);
        NgramView->setItem(row, 0, ngram);
        NgramView->setItem(row, 1, distance);
    }
    NgramView->setSortingEnabled(true);
}
