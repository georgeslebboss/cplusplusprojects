#ifndef CORPUSSOMBDD_H
#define CORPUSSOMBDD_H

#include <../Interface/Interface/BDD/bdd.h>
typedef uint idd ;


class CorpusSOMBDD : public BDD
{
    Q_OBJECT
/********************************************************************************************/
public:
      CorpusSOMBDD();
      /*les requetes SQL utilises par CorpusSOM*/
      QString sqlGetNomById(idd );

private:
      QString nameapp;
public slots:
    void setNameapp(QString);
};
#endif // CORPUSSOMBDD_H
