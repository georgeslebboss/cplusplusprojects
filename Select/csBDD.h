#ifndef CORPUSSELECTBDD_H
#define CORPUSSELECTBDD_H

#include <../fromInterface.h>


class CorpusSelectBDD : public BDD
{
    Q_OBJECT
/********************************************************************************************/
public:
      CorpusSelectBDD();
      /*les requetes SQL utilises par CorpusSelect*/
      QSqlQueryModel * sqlloadCorpus();
      int sqlGetCorpusId(QString, QString, QString);
      int sqlGetUrlId(QString );
      int sqlGetFormatId(QString );
      int sqlGetEncodingId(QString );
      QString sqlWriteDocument(const QVariantList &);
};
#endif // CORPUSSELECTBDD_H
