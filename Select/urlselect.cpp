﻿#include "urlselect.h"


// ================================================================================================ FENETRAGE
//========================================================================= CONSTRUCTEURS

// Constructeur d'une adresse locale
// Pour les symlink, on se contente d'enregistrer la cible
// qui n'est peut être pas valide
// qui va contrôler et créer une Url ?

Url::Url(QUrl url, QFileInfo fi)
{ Url1 = url ;
  Scheme = Url1.scheme() ;
  
  QFile file(fi.absoluteFilePath()) ;
  
  if (not fi.symLinkTarget().isEmpty())
  { Url2 = QUrl(fi.symLinkTarget()) ;
    return ; }
    
  Size = fi.size() ;  
  Type = shellCommand(QString("file -b --mime-type \"%1\"").arg(fi.absoluteFilePath())).trimmed() ;
  Code = shellCommand(QString("file -b --mime-encoding \"%1\"").arg(fi.absoluteFilePath())).trimmed() ;
  
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  { Checksum = qChecksum(file.readAll(), fi.size()) ;
    file.close() ; }
  //  else
  //    WindowSelect::answerMessage(QObject::tr("File %1 could not be opened in readonly mode").arg(fi.absoluteFilePath())) ;
}

// variables de classe
QString Url::TempPath = "temp" ;
int Url::GlobalTempId = 0 ;

// Il faudra remplacer shell par quelque chose de plus général (indépendant du système)
// Pour le mimetype, on a QMimeDatabase::mimeTypeForFile(fi) ;
// Pour le codage...

// Construit pour un fichier distant
Url::Url(QUrl url, QNetworkReply * reply)
  : TempFileId(GlobalTempId++)
{ Url1 = url ;
  Scheme = Url1.scheme() ;
  
  QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute) ;
  if (not redirectionTarget.isNull())
  { Url2 = url.resolved(redirectionTarget.toUrl()) ;
    return ; }
  
  TempFile = QString("File") + QString::number(getTempFileId()) ;
  TempFile += url.fileName().section('.', -1, -1) ;
  
  QFile * file = downloadTempFile(TempFile, reply) ;
  if (not file) return ;

  QFileInfo fi(* file) ;
  Size = fi.size() ; 
  Type = shellCommand(QString("file -b --mime-type \"%1\"").arg(fi.absoluteFilePath())).trimmed() ;
  Code = shellCommand(QString("file -b --mime-encoding \"%1\"").arg(fi.absoluteFilePath())).trimmed() ;
  
  if (file->open(QIODevice::ReadOnly | QIODevice::Text))
  { Checksum = qChecksum(file->readAll(), fi.size()) ;
    file->close() ; }
//  else
//    WindowSelect::answerMessage(QObject::tr("File %1 could not be opened in readonly mode").arg(fi.absoluteFilePath())) ;
}

// AUXILIAIRES DU CONSTRUCTEUR

QFile * Url::downloadTempFile(QString filename, QNetworkReply * reply)
{ QDir dir(QDir::current()) ;
  if (not dir.mkpath(TempPath))
  { qDebug() << QString("impossible créer %1").arg(dir.absolutePath() + "/" + TempPath) ;
    return NULL ; }
  QFile * file = new QFile(dir.absoluteFilePath(TempPath) + "/" + filename) ;
  if (not file->open(QIODevice::WriteOnly))
  { qDebug() << QString("impossible écrire %1").arg(file->fileName()) ;
    return NULL ; }
  file->write(reply->readAll());
  file->flush() ;
  file->close() ;
  return file ;
}

/* Récupéré au cas où on préfèrerait
 * /// When this function returns true, you can be certain that the file contains exactly "foo bar".
 * bool writeFooBar() {
 *  QSaveFile file(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
 *  if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
 *    file.write("foo bar");
 *    file.commit();
 *    return true;
 *  }
 *  return false;
 * }
 */

QString Url::shellCommand(const QString & command)
{
  #if (defined (Q_OS_LINUX) || defined (Q_OS_MAC))
  return shell(command) ;
  #endif
}

// utilisée par shellCommand
QString Url::shell (const QString & cmd)
{ QProcess process;
  process.start("bash", QStringList() << "-c" << cmd);
  process.waitForFinished ();
  return (process.exitCode () == 0) ? QString (process.readAllStandardOutput()) : QString();
}
