#include "csBDD.h"
using namespace std;

CorpusSelectBDD::CorpusSelectBDD()
{
}

/********************************les requetes SQL utilises par CorpusSelect**************/
QSqlQueryModel * CorpusSelectBDD::sqlloadCorpus()
{
    qDebug() << "LoadCorpus de BDD: " << BDD::DatabaseConnection;
    QSqlQueryModel * model = new QSqlQueryModel();
    model->setQuery("SELECT nom_corpus FROM corpus",*BDD::DatabaseConnection);
    return model;
}
int CorpusSelectBDD::sqlGetCorpusId(QString name, QString langage, QString script)
{
    qDebug() << "Corpus Id de BDD : " << BDD::DatabaseConnection << " pour " << name;
    QSqlQuery req(*BDD::DatabaseConnection);
    req.exec(QString("select id from corpus where nom_corpus = '%1'").arg(name)) ;
    if (req.next()) return req.value(0).toInt() ;
    req.exec(QString("insert into corpus (nom_corpus, language_corpus, script_corpus) values ('%1', '%2', '%3')").arg(name, langage, script)) ;
    return req.lastInsertId().toInt() ;
}
int CorpusSelectBDD::sqlGetUrlId(QString url)
{
    QSqlQuery req(*BDD::DatabaseConnection);
    req.exec(QString("select id from url where depot_url = '%1'").arg(url)) ;
    if(req.next()) return req.value(0).toInt() ;
    req.exec(QString("insert into url (depot_url) values ('%1')").arg(url)) ;
    return req.lastInsertId().toInt();
}

int CorpusSelectBDD::sqlGetFormatId(QString format)
{
    QSqlQuery req(*BDD::DatabaseConnection);
    req.exec(QString("select id from format where extension = '%1'").arg(format)) ;
    if(req.next()) return req.value(0).toInt() ;
    req.exec(QString("insert into format (extension) values ('%1')").arg(format)) ;
    return req.lastInsertId().toInt() ;
}

int CorpusSelectBDD::sqlGetEncodingId(QString encoding)
{
    QSqlQuery req(*BDD::DatabaseConnection);
    req.exec(QString("select id from codage where nom_codage = '%1'").arg(encoding)) ;
    if(req.next()) return req.value(0).toInt() ;
    req.exec(QString("insert into codage (nom_codage) values ('%1')").arg(encoding)) ;
    return req.lastInsertId().toInt() ;
}
QString CorpusSelectBDD::sqlWriteDocument(const QVariantList &args)
{
    QSqlQuery request(*BDD::DatabaseConnection);
    request.prepare("insert into document(depot_url_copy, date_de_creation, fk_id_corpus, fk_id_url, fk_id_format, fk_id_codage, size, checksum) values (:url, :date, :idcorpus, :idUrl, :idFormat, :idCodage, :size, :checksum)") ;
    request.bindValue(":url", args[0]) ;
    request.bindValue(":date", args[1]) ;
    request.bindValue(":idcorpus", args[2]) ;
    request.bindValue(":idUrl", args[3]) ;
    request.bindValue(":idFormat", args[4]) ;
    request.bindValue(":idCodage", args[5]) ;
    request.bindValue(":size", args[6]) ;
    request.bindValue(":checksum", args[7]) ;
    if (request.execBatch()==false)
        return request.lastError().text() ;
    return "";
}

