#ifndef CHECKED_H
#define CHECKED_H
 
#include <QHeaderView>
#include <QPainter>
#include <QMouseEvent>
 
class CheckedHeader : public QHeaderView
{ Q_OBJECT
   
   public:
     explicit CheckedHeader(Qt::Orientation, Qt::CheckState, QWidget * = 0);
   
     void setChecked(bool);
   
   signals:
     void toggled(bool);
   
 protected:
   void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const;
   void mousePressEvent(QMouseEvent *event);
   
 private:
   QRect checkBoxRect(const QRect &sourceRect) const;
   bool m_isOn;
 };
 
 #endif // CHECKED_H
 