﻿#include "cs_tab.h"
#include <QMessageBox>

// ================================================================================================ FENETRAGE
// Hook
bool WindowSelect::convertibleFile(QString type)
{ if (not Recursivity->isChecked()) return false ;
  notImplementedMessage(tr("Convertibility of files")) ;
  return false ;
}
/*    
 *    // utilise le convertisseur d'open-office
 *    int WindowSelect::convertibleFilesFormat(const QString &file, const QString &format)
 *    { QProcess process;
 *      QString temp;
 *      temp = QString(" unoconv -f \"%1\" \"%2\" ").arg(format).arg(file) ;
 *      process.start("bash", QStringList() << "-c" << temp);
 *      process.waitForFinished ();
 *      return  (process.exitCode() == 0) ? 0 : 1 ;
 *    }
 * 
 * // tester si le fichier est convertible à un encodage donné
 * // futur bouton lié à la case à cocher
 * int WindowSelect::convertibleFilesEncoding(const QString &file, const QString &encoding)
 * { QProcess process;
 *  QString temp;
 *  temp = QString("iconv -f \"%1\" -t \"%2\" \"%3\" > temp").arg(shellCommand(QString(tr("file -b --mime \"%1\" | cut -d '=' -f 2").arg(file)))).arg(encoding).arg(file) ;
 *  process.start("bash", QStringList() << "-c" << temp);
 *  process.waitForFinished ();
 *  return  (process.exitCode() == 0) ? 0 : 1;
 * }
 */

// à utiliser lors d'un save corpus, pour enregistrer la checksum
// et lors d'un load corpus, pour comparer l'ancienne et la nouvelle version
// pour l'instant inutilisé
//  QCryptographicHash hash(cryptoAlgorithm); = ?

QByteArray checksum(const QString & url)
{ QCryptographicHash cryptoAlgorithm(QCryptographicHash::Sha512)  ;
  QFile file(url) ;
  if (file.open(QFile::ReadOnly) and (cryptoAlgorithm.addData(& file))) return cryptoAlgorithm.result() ;
  return QByteArray() ;
}

//==================================================================================================
WindowSelect::WindowSelect()
  : FileUrls(), PreceedingFileUrls(), InitialCheckState(Qt::Checked), Quick(false)
// Si on veut changer le comportement de la checkbox de l'url, basculer Checked / Unchecked
{
  SelectBDD=new CorpusSelectBDD();
  QGridLayout * mainlayout = new QGridLayout ;
  mainlayout->setSizeConstraint(QLayout::SetNoConstraint) ;
  
  //====================================================== PARTIE HAUTE (CRITÈRES)
  mainlayout->addWidget(new QLabel("Corpus"), 1, 0) ;
  CorpusName = loadCorpus() ;
  mainlayout->addWidget(CorpusName, 1, 1) ;

  mainlayout->addWidget(new QLabel("Language & Script"), 1, 2);
  Languagebox = new QComboBox();
  Languagebox->addItems({"Arabic", "French", "English"});
  mainlayout->addWidget(Languagebox, 1, 3);
  
  Scriptbox = new QComboBox();
  Scriptbox->addItems({"Arabic", "Latin"});
  mainlayout->addWidget(Scriptbox, 1, 4);
  
  // URL
  mainlayout->addWidget(new QLabel(tr("Url")), 2, 0) ;
  UrlBox = createComboBox() ;
  mainlayout->addWidget(UrlBox, 2, 1, 1, 2) ;
  mainlayout->addWidget(UrlBox, 1, 1, 1, 2) ;

  // Browse + sa checkbox
  Directory = new QCheckBox(tr("Select a directory")) ;
  Directory->setChecked(false) ;
  mainlayout->addWidget(Directory, 2, 4) ;
  BrowseButton = createBrowseButton(tr("&Browse files"), SLOT(browse())) ;
  mainlayout->addWidget(BrowseButton, 2, 3) ;

  // Format
  mainlayout->addWidget(new QLabel(tr("Format")), 3, 0);
  FormatComboBox = loadFileFormatComboBox(":/files/format.txt");
  mainlayout->addWidget(FormatComboBox, 3, 1);

  // Encoding
  mainlayout->addWidget(new QLabel(tr("Encoding")), 3, 2) ;
  EncodingComboBox = loadFileEncodingComboBox(":/files/encodage.txt") ;
  mainlayout->addWidget(EncodingComboBox, 3, 3) ;

  // Criteria
  // Il faudra retravailler ça dans la version suivante
  // Pour avoir plusieurs critères
  // Pour l'instant inutilisable

  CriteriaComboBox = createComboBox() ;
  mainlayout->addWidget(new QLabel(tr("Other criteria")), 4, 0) ;
  mainlayout->addWidget(CriteriaComboBox, 4, 1, 1, 3);

  // Récursivité
  Recursivity = new QCheckBox(tr("Recursivity"));
  Recursivity->setChecked(false);
  mainlayout->addWidget(Recursivity, 5, 0);
// si Recursivity est coché, il faut mettre Quick à false, car il faudra accéder aux données
    
  // Convertibilité
  CbConvertible = new QCheckBox(tr("Convertible files")) ;
  CbConvertible->setChecked(false) ;
  mainlayout->addWidget(CbConvertible, 5, 1) ;

  // Fini critères, lancer (commit)
  mainlayout->addWidget(createButton(tr("&Add to corpus"), SLOT(addToCorpus())), 5, 4) ;

  //======================================================== PARTIE BASSE GAUCHE (FICHIERS)

  createFilesTable() ;
  mainlayout->addWidget(FilesTable, 6, 0, 1, 4) ;

  AnswerZone = new QLabel ;
  mainlayout->addWidget(AnswerZone, 7, 0, 1, 4) ;

  //======================================================== PARTIE BASSE DROITE (OPÉRATIONS)

  QGroupBox * operationsGroupBox = new QGroupBox(tr("")) ;
  mainlayout->addWidget(operationsGroupBox, 6, 4) ;
  QGridLayout * operationsLayout= new QGridLayout ;

  // Save files in another location
  SaveFilesButton = createButton(tr("&Save files"), SLOT(saveToAnotherUrl())) ;
  operationsLayout->addWidget(SaveFilesButton, 1, 0) ;

  Reset = createButton(tr("&Reset"), SLOT(reset())) ;
  operationsLayout->addWidget(Reset, 2, 0) ;

  SaveCorpusButton = createButton("Save &corpus", SLOT(saveToDB())) ;
  operationsLayout->addWidget(SaveCorpusButton, 4, 0) ;

  HistoryButton = createButton("&History", SLOT(historyOfCorpus())) ;
  operationsLayout->addWidget(HistoryButton, 5, 0) ;

  //================================================================== DIALOQUE DE PROGRESSION
//  ProgressDialog = new QProgressDialog(this);
//  connect(ProgressDialog, SIGNAL(canceled()), this, SLOT(cancelDownload()));

  // possibilité de mettre des étiquettes sur les fichiers
  //labellingButton = createButton(tr("&Labelling"), SLOT(labelling()));

  // prévoir un bouton Reset Corpus pour remettre tout à zéro (critères, url, urls précédentes, etc)
  // avec un message avertissant l'utilisateur que l'ensemble des fichiers va être supprimé,
  // s'il veut simplement supprimer la dernière opération c'est au choix.

  operationsGroupBox->setLayout(operationsLayout);
  setLayout(mainlayout);
  setWindowTitle(tr("CharProps"));
}

//===================================================================DESTRUCTEUR
WindowSelect::~WindowSelect()
{
    deleteTmpDir(Url::TempPath);
}
// détruire les sous-répertoires pourra être utile avec ftp

void WindowSelect::deleteTmpDir(QString temp)
{ QDir dir(QDir::current().absolutePath() + "/" + temp) ;
  QFileInfoList entries = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files) ;
    int count = entries.size() ;
    
    for (int i = 0; i < count; ++i)
    {   QFileInfo entryInfo = entries[i];
      QString path = entryInfo.absoluteFilePath();
//      if (entryInfo.isDir()) deleteTmpDir(QDir (path)) ;
      QFile file(path) ;
      file.remove() ;
    }
    dir.rmdir(dir.absolutePath());
  }
//==================================================================== AUXILIAIRES DU CONSTRUCTEUR

// Pour construire CorpusName
// à partir de la base de données
// la connexion est faite dans le main
// elle devrait être faite ici par appel de connectionDialog

QComboBox * WindowSelect::loadCorpus()
{
    QSqlQueryModel * model = SelectBDD->sqlloadCorpus();
    if (model->rowCount()<0) QMessageBox::information(this,"ROW COUNT COMBO","rrr");
    QComboBox * combobox = new QComboBox();
    combobox->setEditable(true);
    combobox->setModel(model);
    return combobox;
}

void WindowSelect::createFilesTable()
{ FilesTable = new QTableWidget(0, 6) ;
  FilesTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
  
  FilesTable->setHorizontalHeaderLabels(QStringList() << tr("Url") << tr("Format | Encoding") << tr("Convertible") << tr("Checksum") << tr("Present in DB") << tr("Size")) ;
  CheckedHeader * header = new CheckedHeader(Qt::Horizontal, InitialCheckState, FilesTable) ;
  FilesTable->setHorizontalHeader(header) ;
  
  FilesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents) ;
  FilesTable->horizontalHeader()->setMinimumSectionSize(40) ;
  FilesTable->horizontalHeader()->setStretchLastSection(true) ;
//  FilesTable->setSortingEnabled(true) ; ne fonctionne pas correctement
// deux problèmes : conflit avec la checkbox et infos qui ne s'affichent plus
  
  connect(header, SIGNAL(toggled(bool)), this, SLOT(checkedHeader(bool)));
  
  FilesTable->verticalHeader()->setVisible(false) ;
  FilesTable->setShowGrid(false) ;
  FilesTable->setContextMenuPolicy(Qt::CustomContextMenu);

  connect(FilesTable, SIGNAL(cellActivated(int, int)), this, SLOT(openTableFile(int, int))) ;
}

QComboBox * WindowSelect::createComboBox(const QString & text)
{ QComboBox * combobox = new QComboBox ;
  combobox->setEditable(true) ;
  combobox->addItem(text) ;
  combobox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred) ;
  return combobox ;
}

QPushButton * WindowSelect::createButton(const QString & text, const char * member)
{ QPushButton * button = new QPushButton(text) ;
  connect(button, SIGNAL(clicked()), this, member) ;
  return button ;
}

// pour Browse
QPushButton * WindowSelect::createBrowseButton(const QString & text, const char * member)
{ QPushButton * button = new QPushButton(text) ;
  connect(button, SIGNAL(clicked()), this, member) ;
  connect(Directory, SIGNAL(stateChanged(int)), this, SLOT(changeCaption(int))) ;
  return button ;
}

// load format combobox from txt file
// Le format devrait être lu comme la norme (téléchargée et éventuellement mise à jour à partir du site officiel)
// une sélection des types : application / text (message ?)
// pour application et pour text, les sous-types les plus connus.
// il faudra mettre les formats dans la base
// et format.txt ne devrait pas contenir les wildcards, insérés dans le programme ou choisis par l'utilisateur
// *, xxx/*, */xxx

QComboBox * WindowSelect::loadFileFormatComboBox(const QString & formatname)
{ QComboBox * combobox = new QComboBox() ;
  FormatModel = new QStandardItemModel() ;
  combobox->setModel(FormatModel) ;
  
  pairlist textFormat = readFileAsStrings(formatname) ;

  while (not textFormat.isEmpty())
  { QStandardItem * item = new QStandardItem() ; // make every line of combobox be checked
    item->setText(textFormat.first().first) ;
    item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled) ;
    item->setData(textFormat.takeFirst().second, Qt::ToolTipRole) ;
    item->setData(Qt::Unchecked, Qt::CheckStateRole) ;
    FormatModel->appendRow(item) ;
  }
  
  QModelIndex index = FormatModel->index(0, 0) ;
  FormatModel->setData(index, Qt::Checked, Qt::CheckStateRole) ;
  FormatItems << "*" ;
  FormatModel->setHorizontalHeaderLabels(QStringList() << "Select...") ;
  
  FormatView = new QTreeView() ;
  FormatView->setModel(FormatModel) ;
  combobox->setView(FormatView) ;
  connect(FormatView, SIGNAL(clicked(QModelIndex)), this, SLOT(clickedItemFormat(QModelIndex)));
  return combobox ;
}

// il faudra mettre les codages dans la base
// pas de wildcard ici sauf peut-être *

QComboBox * WindowSelect::loadFileEncodingComboBox(const QString & encodingname)
{ QComboBox * combobox = new QComboBox();
  EncodingModel = new QStandardItemModel();
  combobox->setModel(EncodingModel);

  pairlist textEncoding = readFileAsStrings(encodingname) ;
  while (not textEncoding.isEmpty())
  { QStandardItem * item = new QStandardItem() ; // make every line of combobox be checked
    item->setText(textEncoding.first().first) ;
    item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled) ;
    item->setData(textEncoding.takeFirst().second, Qt::ToolTipRole) ;
    item->setData(Qt::Unchecked, Qt::CheckStateRole) ;
    EncodingModel->appendRow(item) ;
  }
  
  QModelIndex index = EncodingModel->index(0,0);
  EncodingModel->setData(index, Qt::Checked, Qt::CheckStateRole);
  EncodingItems << "utf-8" ;
  EncodingModel->setHorizontalHeaderLabels( QStringList() << "Select..." );
  
  EncodingView = new QTreeView() ;
  EncodingView->setModel(EncodingModel) ;
  combobox->setView(EncodingView) ;
  connect(EncodingView, SIGNAL(clicked(QModelIndex)), this, SLOT(clickedItemEncoding(QModelIndex))) ;
  return combobox;
}

// Pour lire les fichiers de données contenant les formats, codages, etc.
// fonction qui lit un fichier et met les lignes dans une liste de paires
// en supprimant les doublons (en gardant le premier)
// le premier élément de la clé est forcé en minuscules

pairlist WindowSelect::readFileAsStrings(const QString & filename)
{ QFile file(filename) ;
  if (not file.open(QIODevice::ReadOnly | QIODevice::Text))
  { qDebug() << "impossible to open the file " << filename ;
    exit(1) ; }
  QTextStream in(& file) ;
  //in.setCodec("UTF-8");

  pairlist result ;
  while (not in.atEnd())
  { QStringList line = in.readLine().split("#") ;
    if (line.isEmpty() or line[0].isEmpty()) continue ;
    if (memberPairlist(QPair<QString, QString>(line[0].toLower(), line[1]), result)) continue ;
    result << QPair<QString, QString>(line[0].toLower(), line[1]) ;
  }
  file.close() ;
  return result ;
}

// Utilisé par readFileAsStrings
bool WindowSelect::memberPairlist(QPair<QString, QString> pair, pairlist list)
{ while (not list.isEmpty())
    if (pair.first == list.takeFirst().first) return true ;
  return false ;
}

//==================================================================================== SLOTS HORS GO
//===================================== BOUTONS

// bouton Browse
// Dès que la case Directory est cochée, le bouton change de texte et de comportement
// Bug ? : quand on ajoute une url par copier, la case Directory devrait se décocher ?
void WindowSelect::browse()
{ QUrl url ;
  url = (Directory->isChecked())
    ? QFileDialog::getExistingDirectoryUrl(this, tr("Find directory"), QUrl(QDir::currentPath()))
    : QFileDialog::getOpenFileUrl(this, tr("Find file"), QUrl(QDir::currentPath())) ;

  // items mémorisés
  if (not url.isEmpty())
  { if (UrlBox->findText(url.toString()) == -1) UrlBox->addItem(url.toString()) ;
    UrlBox->setCurrentIndex(UrlBox->findText(url.toString())); }
}

// slot de BrowseButton
void WindowSelect::changeCaption(int state)
{ if (Directory->isChecked()) this->BrowseButton->setText(tr("Browse directories")) ;
  else this->BrowseButton->setText(tr("Browse files")) ;
  UrlBox->clear() ;
}

// bouton + dans UrlBox et bouton SaveFile
// mais dans ce deuxième cas, où va s'ouvrir l'url ???
// va probablement disparaître sauf si SaveFile en a besoin

void WindowSelect::saveToAnotherUrl()
{ notImplementedMessage(tr("Saving files in another location")) ;
}

void WindowSelect::historyOfCorpus()
{ notImplementedMessage(tr("History of corpus constitution")) ;
}

void WindowSelect::saveToDB()
{ if (sqlWriteDocument())   answerMessage(tr("All document coordinates have been successfully stored in database")) ;
}

// idsFormat et idsEncoding devraient être initialisés dans loadFileEncodingComboBox et loadFileFormatComboBox

bool WindowSelect::sqlWriteDocument()
{
    if (not (FilesTable->rowCount() > 0))
        return false ;

  static QVariantList urls ;
  static QVariantList dates ;
  static QVariantList corpora ;
  static QVariantList sizes ;
  static QVariantList checksums ;
  
  static QVariantList idsurl ;
  static QVariantList idsformat ;
  static QVariantList idsencoding ;
  int corpus = sqlGetCorpusId() ;  
  QString date = QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss.zzz") ;
  
  for (int i = 0 ; i < Urls.size() ; ++i )
  {
    const QString url = Urls[i].getUrl1().toString().trimmed() ;
    corpora   << corpus ;
    dates     << date ;
    urls      << url ;
    sizes     << Urls[i].Size ;
    checksums << Urls[i].Checksum ;
    idsformat   << sqlGetFormatId(Urls[i].Type) ;
    idsencoding << sqlGetEncodingId(Urls[i].Code) ;
    idsurl      << sqlGetUrlId(url) ;
    answerMessage(tr("Url %1 document coordinates will be stored in database").arg(url)) ;
  }
  QString execresult=SelectBDD->sqlWriteDocument({urls, dates,corpora, idsurl,idsformat,idsencoding,sizes,checksums});
  if (execresult!="")
  {
      answerMessage(tr("Url document coordinates will not be stored in database because of %1").arg(execresult)) ;
      return false;
  }
    answerMessage(tr("Url document coordinates are successfully stored in database"));
  return true ;
}

// utilisée par sqlWriteDocument()
// recupérer l'id du corpus de la base de données,
// si le corpus n'existe pas ajouter à la base de données

int WindowSelect::sqlGetCorpusId()
{
    return SelectBDD->sqlGetCorpusId(CorpusName->currentText().trimmed(), Languagebox->itemText(Languagebox->currentIndex()), Scriptbox->itemText(Scriptbox->currentIndex()));
}

int WindowSelect::sqlGetUrlId(QString url)
{
    return SelectBDD->sqlGetUrlId(url);
}

int WindowSelect::sqlGetFormatId(QString format)
{
    return SelectBDD->sqlGetFormatId(format);
}  

int WindowSelect::sqlGetEncodingId(QString encoding)
{
    return SelectBDD->sqlGetEncodingId(encoding);
}  

// provisoire
// il faudra donner à l'utilisateur le choix entre revenir en arrière d'un coup ou tout effacer

void WindowSelect::reset()
{
  FileUrls.clear() ;
  FilesTable->setRowCount(0) ;
  Recursivity->setChecked(false) ;
  UrlBox->clear() ;
  //remise à zéro ou à l'état précédent
}

//===================================== COCHES

// quand on coche la case de la colonne
// met tous les fichiers à la même valeur
// quand on change la valeur puis on charge les fichiers, ce n'est plus cohérent
// à changer

void WindowSelect::checkedHeader(bool state)
{ int rows = FilesTable->rowCount() ;
  int i = 0 ;
  while (i < rows)
    FilesTable->item(i++, 0)->setCheckState(state ? Qt::Checked : Qt::Unchecked) ;
}

// les deux slots suivants peuvent être unifiés en utilisant sender() et qt_cast

// cocher une case de format
// il faudra s'arranger pour que cliquer sur quelque chose ça décoche "*"

QStringList WindowSelect::clickedItemFormat(const QModelIndex &index)
{ QStandardItem * itemm = FormatModel->itemFromIndex(index);
  if (itemm->checkState() == Qt::Checked)
    FormatItems.append(itemm->text()) ;
  else
  { for (int i=0; i < FormatItems.size(); i++)
      if (FormatItems.at(i).contains(itemm->text()))
        FormatItems.removeAt(i) ;
  }
  return FormatItems;  
}

// cocher une case d'encodage

QStringList WindowSelect::clickedItemEncoding(const QModelIndex &index)
{ QStandardItem * itemm = EncodingModel->itemFromIndex(index);
  if (itemm->checkState() == Qt::Checked)
    EncodingItems.append(itemm->text()) ;
  else
  { for (int i=0; i< EncodingItems.size(); i++)
      if (EncodingItems.at(i).contains(itemm->text()))
        EncodingItems.removeAt(i) ;
  }
  return EncodingItems;
}

// ======================================================================================== ACTIONS DE GO
//============================================================ PARTIE HAUTE (CRITÈRES)


/*
Quand les critères sont définis, on doit remplir la table des fichiers

Basculer | Filtrer | Récurser | Chercher



1. On commence par regarder l'url courante ;
il peut s'agir d'une url http, https, ftp, ou file
En fonction du cas, on activera la fonction correspondante



A. HTTP: ou HTTPS:
=> suivre la redirection index... et éventuellement deuxième redirection
Si récursivité, ramasser les liens de la page et récursiver
éviter les boucles

renvoyer la liste des url des fichiers trouvés

B. File:
=> simplement afficher le fichier de l'url ou si c'est un répertoire,
renvoyer la liste des fichiers

C. FTP:
=> pareil que file:

Ensuite on filtre les fichiers à l'aide des critères

Si format ou encodage interdit, on peut se retrouver sans fichier ; il faut un message


currenturl contient le répertoire UrlBox mis à jour au moment où on tape addToCorpus (et seulement à ce moment là, sauf si on demande une remise à zéro (bouton à prévoir)
FileUrls contient les urls précédentes, encore valides au moment où on tape addToCorpus

Chaque fois qu'on tape addToCorpus, on incrémente le corpus.


*/

// Slot du bouton addToCorpus
void WindowSelect::addToCorpus()
{ urllist urls = getCurrentUrl() ;
  PreviousUrls.clear() ;
  PreviousUrls << Urls ;
  Urls.append(urls) ;
  //ProgressDialog->setWindowTitle(tr("Download files"));
  //ProgressDialog->setLabelText(tr("Downloading %1.").arg(fileName));
  answerMessage(tr("%1 file(s) found").arg(Urls.size()) + tr(" (Click / Double click on a file to open it)")) ;
}

// Fonction qui ramène une liste d'url
// avec récursion le cas échéant
// au départ l'url est simplement une QString ; on en fera une Url plus tard
// Les url qui sont déjà dans la table sont éliminées plus loin
// il faudrait un test plus complexe, avec checksum. Que faire si le checksum est différent ?
// Si on a fait une sauvegarde, la proposer.

// ce sont les sous-fonctions qui construisent l'Url

urllist WindowSelect::getCurrentUrl()
{ QString currenturl = (Directory->isChecked()) ? UrlBox->currentText().append("/") : UrlBox->currentText() ;
  QUrl url(currenturl) ;
  //qDebug() << "getCurrentUrl : " << url.toString() ;
//  updateBox(UrlBox) ; // réfléchir quand on l'utilise
  if (url.scheme().contains("http")) return getCurrentHttpUrl(url) ;
  if (url.scheme() == "ftp") return getCurrentFtpUrl(url) ;
  return getCurrentFileUrl(url) ;
}

// ajoute des items dans la mémoire des urls
// vérifie s'il n'y est pas déjà
// devrait vérifier sur les vraies adresses : x/ et x sont considérés comme différents
// mais ça n'est pas très important
// des messages devraient être affichés en bas de la fenêtre

void WindowSelect::updateBox(QComboBox * combobox)
{ int i = combobox->count() ;
  while (i)
    if (combobox->findText(combobox->itemText(i--)) > 0) return ;
  combobox->addItem(combobox->currentText()) ;
}

// Hook
urllist WindowSelect::getCurrentFtpUrl(QUrl url)
{ urllist urls ;
  notImplementedMessage(tr("Processing of FTP protocol")) ;
  return urls ;
}

// fonction qui ramène une liste d'url de fichiers
// s'il y a récursivité, récupérer les sous-répertoires ;
// pas de symlink donc pas de circuits
// mais attention au cas où le répertoire de départ est un symlink ; il faudra tester

urllist WindowSelect::getCurrentFileUrl(QUrl qurl)
{ urllist urls ;
  if (qurl.fileName().isEmpty()) return getCurrentFiles(qurl.path()) ;
  Url * url = createLocalUrl(qurl) ;
  if (not url)
  { answerMessage(tr("File %1 does not exist").arg(qurl.toLocalFile())) ;
    return urls ; }
  if (memberUrlList(* url, Urls)) return urls ;
  /**Eliminer par Georges dans CorpusWord le if et else de filter file afin de sauvegarder les fichiers de n'importe quel format dans bdd**/
  //if (filterFile(* url))
  { setFileRow(* url) ;
    urls << * url ; }
 // else
 //   answerMessage(tr("File %1 does not fulfill criteria").arg(qurl.toLocalFile())) ;
  return urls ;
}

// récupérer les fichiers en local
// Je ne sais pas pourquoi même QUrl(dir.fileInfo().absoluteFilePath()).toString()) ne restitue pas le scheme.
urllist WindowSelect::getCurrentFiles(QString path)
{ urllist files ;
  QDirIterator::IteratorFlag flag = (Recursivity->isChecked()) ? QDirIterator::Subdirectories : QDirIterator::NoIteratorFlags ;
  QDirIterator dir(path, QStringList() << "*", QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot, flag) ;
  
  while (dir.hasNext())
  { dir.next() ;
    QString file(dir.fileInfo().absoluteFilePath()) ;
    Url * url = createLocalUrl(QUrl(QString("file://" + file))) ;
    if (not url)
    { answerMessage(tr("File %1 does not exist").arg(file)) ;  // ne devrait pas arriver, les noms sont automatiquement tirés du répertoire
      continue ; }
    if (memberUrlList(* url, Urls)) continue ;
    /**Eliminer par Georges dans CorpusWord le if et else de filter file afin de sauvegarder les fichiers de n'importe quel format dans bdd**/
    //if (filterFile(* url))
    { setFileRow(* url) ;
      files << * url ; }
    //else
      //answerMessage(tr("File %1 does not fulfill criteria").arg(file)) ;
  }
  return files ;
}


Url * WindowSelect::createLocalUrl(QUrl qurl)
{ QFileInfo fi(qurl.toLocalFile()) ;
  if (not fi.exists()) return NULL ;
  Url * url = new Url(qurl, fi) ;
  return url ;
}

// tester si l'url figure dans le corpus
// Différents cas à mieux traiter ; pour l'instant ils sont juste différenciés
// pb d'affichage : si est appelée par une url distante, la méthode toLocalFile ne convient pas

bool WindowSelect::memberUrlList(Url url, urllist urls)
{ while (not urls.isEmpty())
  { if ((url.getUrl1() == urls.first().getUrl1()) and (url.Checksum == urls.first().Checksum))
    { answerMessage(tr("File %1 already figures in your corpus").arg(url.getUrl1().toLocalFile())) ;
      return true ; }
    if (url.getUrl1() == urls.first().getUrl1())
    { answerMessage(tr("File %1 already figures in your corpus with a different checksum").arg(url.getUrl1().toLocalFile())) ;
      return true ; }
    if (url.getUrl1() == urls.first().getUrl2())
    { answerMessage(tr("File %1 is the same as the symbolic linc target of %2").arg(url.getUrl1().toLocalFile()).arg(urls.first().getUrl1().toLocalFile())) ;
      return true ; }
    urls.removeFirst() ;
  }
  return false ;
}


// fonction qui ramène une liste d'url http valides
// il faudrait virer de l'adresse les # and co ?
// C'est une liste s'il y a récursivité
// => on devrait récupérer tous les liens de la page
// attention aux circuits
/* Traitement des redirections

Cas 1. url existait comme redirection mais n'est plus une redirection
Cas 2. url existait comme redirection mais est une redirection sur une autre page
Cas 3. pas de changement
Cas 4. url n'existait pas comme redirection
Mais il faudrait coupler ça avec la checksum (si la page a changé, la checksum n'est plus la même, sauf dans le cas 3

  Autres solutions : faire d'url une classe
  nécessite réécriture de beaucoup de fonctions
  mais il faudra le faire de toutes façons.

Je propose donc qu'en attendant, on ne traite pas la redirection.

Ancien code :
QVariant redirectionTarget = Reply->attribute(QNetworkRequest::RedirectionTargetAttribute) ;
if (Reply->error())
{ QMessageBox::information(this, tr("HTTP File"), tr("Download failed: %1.").arg(Reply->errorString()));
return ; }
else if (!redirectionTarget.isNull())
{ QUrl newUrl = HttpUrl.resolved(redirectionTarget.toUrl()) ;
HttpUrl = newUrl ;uru

La liste FileUrls contiendra les Urls redirigeantes et une autre pour les targets ?
On est dans un cas particulier de getCurrentLinks

Pour l'attente de la réponse, il y a plus propre (mais avec QThread et sockets), voir :
http://www.codeproject.com/Articles/484905/Use-QNetworkAccessManager-for-synchronous-download
*/

urllist WindowSelect::getCurrentHttpUrl(QUrl qurl)
{ urllist urls ;
  if (not qurl.isValid()) return urls ;
  Url * url = createDistantUrl(qurl) ; 
  if (not url)
  { answerMessage(tr("Url %1 could not be downloaded").arg(qurl.toLocalFile())) ;
    return urls ; }
  if (memberUrlList(* url, Urls)) return urls ;
  if (filterFile(* url))
  { setFileRow(* url) ;
    urls << * url ; }
  else
    answerMessage(tr("File %1 does not fulfill criteria").arg(qurl.toLocalFile())) ;
  if (Recursivity->isChecked()) return urls << getCurrentLinks(* url) ;
  return urls ;
}

// Hook
// Cette fonction appellera récursivement getCurrentHttpUrl sur chaque url
// elle devra gérer les circuits et s'arrêter avant d'avoir épuisé le web
// attention, elle ne va pas forcément fonctionner avec le QEventLoop

urllist WindowSelect::getCurrentLinks(Url url)
{ urllist urls ;
  notImplementedMessage(tr("Processing recursively for page links")) ;
  return urls ;
}

Url * WindowSelect::createDistantUrl(QUrl qurl)
{ if (not qurl.isValid())
  { answerMessage(tr("Url %1 not valid").arg(qurl.toString())) ;
    return NULL ; }
  Reply = Qnam.get(QNetworkRequest(qurl)) ;
  QEventLoop loop ;
  connect(Reply, SIGNAL(finished()), & loop, SLOT(quit())) ;
  QTimer time ;
  connect(&time, SIGNAL(timeout()), & loop, SLOT(quit())) ;
  time.start(5000) ;
  loop.exec() ;
  if (Reply->error())
  { answerMessage(tr("Download file %1 failed: %2").arg(qurl.toString()).arg(Reply->errorString())) ;
    return NULL ; }
  Url * url = new Url(qurl, Reply) ;
  return url ;
}

// Version rapide (sans télécharger), basée sur le header
// transmis par Reply
// Le flag indique si on prend la méthode rapide ou la méthode plus sûre (avec downloadFile)

bool WindowSelect::filterFile(Url url)
{ if (not (FormatItems.contains("*") or FormatItems.contains(url.Type))) return false ;
  if (not (EncodingItems.contains(url.Code) or convertibleFile(url.Code))) return false ;
  return true ;
}

// Fonction qui insère le fichier dans la table
void WindowSelect::setFileRow(Url url)
{ QTableWidgetItem * urlcell = new QTableWidgetItem(url.getUrl1().toString()) ;
  urlcell->setFlags(urlcell->flags() ^ Qt::ItemIsEditable);
  urlcell->data(Qt::ItemIsTristate);
  urlcell->setCheckState(InitialCheckState) ;
  
  QTableWidgetItem * infocell = new QTableWidgetItem(url.Type + " | " + url.Code) ;
  infocell->setFlags(infocell->flags() ^ Qt::ItemIsEditable) ;
  
  QTableWidgetItem * sizecell = new QTableWidgetItem(tr("%1 KB").arg(int((url.Size + 1023) / 1024))); // size column
  sizecell->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  sizecell->setFlags(sizecell->flags() ^ Qt::ItemIsEditable);
  
  int row = FilesTable->rowCount() ;
  FilesTable->insertRow(row) ;
  FilesTable->setItem(row, 0, urlcell) ;
  FilesTable->setItem(row, 1, infocell) ;
  FilesTable->setItem(row, 5, sizecell) ;
}

// ======================================================================================== ACTIONS DE LA TABLE

// clic sur fichier dans la table
// slot installé par createFilesTable

void WindowSelect::openTableFile(int row, int /* column */)
{ QTableWidgetItem *item = FilesTable->item(row, 0);
  QDesktopServices::openUrl
    ( QUrl(item->text()).isValid() ? (item->text()) : QUrl::fromLocalFile(QDir::current().absoluteFilePath(item->text())) ) ;
}

// suppr sur fichier dans la table
void WindowSelect::keyPressEvent(QKeyEvent *event)
{ int i = FilesTable->currentRow();
  if (not (event->key()==Qt::Key_Delete and FilesTable->currentIndex().isValid())) return ;
  if
    ( QMessageBox::question
      ( this,
        tr("Delete File"),
        tr("Do you want to delete the file %1 from the current corpus?").arg(FilesTable->item(i,0)->text()),
        QMessageBox::Yes|QMessageBox::No, QMessageBox::No )
      == QMessageBox::Yes ){
    FilesTable->removeRow(i) ;
    FileUrls.removeAt(i);} // FilesTable contient la même liste que FileUrls ?

  answerMessage(tr("%1 file(s) found").arg(FilesTable->rowCount()) + (" (Double click on a file or folder to open it)"));
}

// ftp http : load http address
// à utiliser quand on clique sur le fichier
/*
void WindowSelect::httpWeb()
{  HttpUrl = UrlBox->currentText(); 
   startRequest(HttpUrl);
}
*/

// =============================================================== inutilisés

void WindowSelect::downloadReadProgress(quint64 bytesRead, quint64 totalBytes)
{   /*.progressBar..*/ }


//===================================================================================================== BAS NIVEAU

void WindowSelect::notImplementedMessage(QString text)
{ answerMessage(text.append(tr(" is not yet implemented"))) ;
}

void WindowSelect::answerMessage(QString text)
{ AnswerZone->setText(text) ;
  AnswerZone->setWordWrap(true) ;
  AnswerZone->repaint() ;
  qDebug() << text ;
}
/*
 * checkbox dans chaque line de table widget : ok
 * les operations de conversion de format libiconv (convertible files) : ok
 * recherche des urls suivant le contenu de formatItems : ok
 * recherche des urls   "      "   "     "  encodageItems       : ok
 * verifier et eliminer les doublons de FilesTable : ok
 * convertion en txt/xml et en unicode :
 * recherche dans le web:
 * Optimisation :
 * data base :
 */
