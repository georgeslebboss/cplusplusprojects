#ifndef WINDOWSELECT_H
#define WINDOWSELECT_H

#include "checked.h"
#include "urlselect.h"
#include "csBDD.h"

#define QT5
#include <QtWidgets>
#include <QtNetwork>
#include <QCryptographicHash>

#include "../fromInterface.h"

//#include <QtFtp>

typedef QList <QPair <QString, QString> > pairlist ;

class WindowSelect : public Tab
{ Q_OBJECT

  public:
    WindowSelect() ;
    CorpusSelectBDD *SelectBDD;
    ~WindowSelect() ;
    
  protected slots:
    void            downloadReadProgress(quint64, quint64); // à garder ?
    void            browse() ;
    void            addToCorpus() ;
    void            saveToAnotherUrl() ;
    void            openTableFile(int, int) ;
    
    void            saveToDB();
    void            historyOfCorpus();
    void            reset();
    void            changeCaption(int state) ;
    
    QStringList     clickedItemFormat(const QModelIndex &index) ;
    QStringList     clickedItemEncoding(const QModelIndex &index) ;
    void            checkedHeader(bool) ;

  protected:

    bool            filterFile(Url) ;
    bool            convertibleFile(QString) ;

    QByteArray      checksum(const QString &); // inutilisé mais à garder en attendant de voir si qchecksum convient

//    int             convertibleFilesEncoding(const QString &, const QString &);
//    int             convertibleFilesFormat(const QString &, const QString &);
    void            setFileRow(Url) ;
    
    QComboBox    *  CriteriaComboBox;
    QLabel       *  criteriaLabel;

    QComboBox    *  Languagebox;
    QComboBox    *  Scriptbox;


    // L'intérêt des noms de boutons
    // c'est si on a besoin de les inhiber à un moment donné
    
    QPushButton  *  SaveFilesButton;

    
    QPushButton  *  HistoryButton;
    
    QPushButton  *  Reset;
    
    // sauvegarde dans la base
    
    QComboBox    *  CorpusName ;
    QComboBox    *  loadCorpus() ;
    QPushButton  *  SaveCorpusButton ;
    bool            sqlWriteDocument() ;
    int             sqlGetCorpusId() ;
    int             sqlGetUrlId(QString) ;
    int             sqlGetFormatId(QString) ;
    int             sqlGetEncodingId(QString) ;
    
    // Browse url
    QPushButton  *  BrowseButton;
    QComboBox    *  UrlBox ;
    QCheckBox    *  Directory ;
    QPushButton  *  createBrowseButton(const QString &, const char *);
    void            updateBox(QComboBox *) ;
    
    QCheckBox    *  CbConvertible;
    QCheckBox    *  Recursivity;
   
    // Table
    QTableWidget *  FilesTable;

    
    QStringList     FileUrls ;
    
    urllist         Urls ;
    Url *   createLocalUrl(QUrl) ;
    Url *   createDistantUrl(QUrl) ;
    
    QStringList     PreceedingFileUrls ;
    
    
    urllist         PreviousUrls ;
    
    Qt::CheckState  InitialCheckState ;

    // Formats
    QComboBox             * FormatComboBox;
    QStandardItemModel    * FormatModel;
    QTreeView             * FormatView;
    QStringList             FormatItems;

    // Charsets
    QComboBox             * EncodingComboBox;
    QStandardItemModel    * EncodingModel;
    QTreeView             * EncodingView;
    QStringList             EncodingItems;

    QComboBox    *  loadFileEncodingComboBox(const QString &);
    QComboBox    *  loadFileFormatComboBox(const QString &);
    pairlist        readFileAsStrings(const QString &) ;
    bool            memberPairlist(QPair<QString, QString>, pairlist) ;
 
    QProgressDialog* ProgressDialog;

    QNetworkAccessManager Qnam;
    QNetworkReply * Reply;

    

    // fenêtrage
    void  createFilesTable() ;
    void keyPressEvent(QKeyEvent *) ;
    
    // addToCorpus methods
    
    urllist getCurrentUrl() ;
    urllist getCurrentFtpUrl(QUrl) ;
    urllist getCurrentFileUrl(QUrl) ;
    urllist getCurrentFiles(QString) ;

    bool    memberUrlList(Url, urllist) ;
  
    urllist getCurrentHttpUrl(QUrl) ;
    urllist getCurrentLinks(Url) ;
    bool        Quick ; // pour chercher dans le header plutôt que télécharger le fichier

    // écriture dans le bas de la fenêtre
    QLabel       *  AnswerZone ;
    void            notImplementedMessage(QString) ;
    void            answerMessage(QString) ;

    // bas niveau
    QPushButton  *  createButton(const QString &, const char *);
    QComboBox    *  createComboBox(const QString & = QString());
    
    // pour le destructeur
    void            deleteTmpDir(QString) ;


} ;
#endif // WINDOWSELECT_H







