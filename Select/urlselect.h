#ifndef URLSELECT_H
#define URLSELECT_H

#define QT5
//#include <QtWidgets>
#include <QtNetwork>
#include <QCryptographicHash>

//#include <QtFtp>

typedef QList <QPair <QString, QString> > pairlist ;

class Url
{
private:
    QUrl            Url1 ;          // url principale
    QUrl            Url2 ;          // url de redirection ou de symlink
    QString         Scheme ;        // protocole de communication
    QString         TempFile ;      // path du fichier téléchargé
    int             TempFileId ;    // pré-initialisé dans le constructeur
    
  public:
    Url(QUrl, QNetworkReply *) ;
    Url(QUrl, QFileInfo) ;
    
    QUrl        getUrl1() { return Url1 ; }
    QUrl        getUrl2() { return Url2 ; }
    quint16     setChecksum(const QString &) ;
    quint16     getChecksum() ;
     
    static QString  TempPath ;
    static int      GlobalTempId ;
    
    QString         Type ;          // file type (application/html)
    QString         Code ;          // file charset
    quint16         Checksum ;
    qint64          Size ;
    QList<Url>      SaveFiles ;

protected:
    QString         shell (const QString &) ;
    QString         shellCommand(const QString &) ;
    int             getTempFileId() const { return TempFileId ; }
    QFile    *      downloadTempFile(QString, QNetworkReply *) ;
  
} ;

typedef QList<Url> urllist ;




#endif // URLSELECT_H







