#include "som.h"
using namespace std;
Som::Som(float alphai, float sigmai, float sigmaf)
    : Alphai(alphai), Sigmai(sigmai), Sigmaf(sigmaf), QCI(0), QCE(0)
{
    /*intialiser AlgoSOMBDD*/
    SOMBDD=new AlgoSOMBDD();
    SOM = NULL;
}

// le constructeur installe la table
// initialise la mémoire des vecteurs et les paramètres du som
void Som::setparametersSOM(int nlig, int ncol)
{
    Lig=nlig;
    Col=ncol;
    if ((Lig > 0) && (Col > 0))
      { // Creation du SOM
        SOM = new Neurone * [Lig];
        for (int i=0; i < Lig; i++)
            SOM[i] = new Neurone[Col];
      }
    else
        qDebug() << "Erreur: les parametres sont invalides!";
   //qDebug() << "Initialisation matrice : " << time.elapsed() / (qint64) 1000 << " secondes" ;
}

Som::~Som()
{
    SOM_clean();
    DataMatrix.clear();
}

//Initialisation de vecteurs mémoires à partir du DataMatrix
void Som::initialiseSOM()
{
    if ((SOM != NULL) && (DataMatrix.size() != 0))
    {
        qDebug() << "Initialisation des vecteurs mémoires en cours..." ;
        for (int i(0); i < Lig; i++)
            for (int j(0); j < Col; j++)
                initialiseVM(SOM[i][j].w);
    }
}

// On va initialiser chaque Vecteur Mémoire 
void Som::initialiseVM(maLigne & Mem)
{
    int k;
    int i;
    for (k = 0; k < Nb_vect/4; k++)
    {
        i = entier_aleatoire_a_b(0,Nb_vect);
        QMapIterator<id, double > it(DataMatrix[i]->mligne);
        while ( it.hasNext())
        {
           it.next();
           Mem[it.key()] = Mem.value(it.key()) + it.value() ;
        }
   }
   QMapIterator<id, double > im(Mem) ;
   while ( im.hasNext())
   {
       im.next();
       Mem[im.key()] = Mem.value(im.key())*4/Nb_vect ;
   }
}
/*************************************************************************************************/
/**************************************** version Euclidiènne ***********************************/
// distance Euclidienne
float Som::distanceEuclidienne(const maLigne & X, const maLigne & Mem)
{
    float result = 0 ; 	    // résultat du calcul
    QMapIterator<id, double> ix(X);
    while (ix.hasNext())
    {
        ix.next( ) ;
        result += (Mem.value(ix.key()) - ix.value()) * (Mem.value(ix.key()) - ix.value());
    }
    QMapIterator<id, double> im(Mem);
    while (im.hasNext())
    {
        im.next( ) ;
        if (not X.contains(im.key()))
            result += (im.value()) * (im.value());
    }
    return sqrt(result) ;
}

// MultiThread de distance: on partage le calcul sur deux processus en même temps
void Som::distanceEcth1(const maLigne & X, const maLigne & Mem, float & result)
{
    QMapIterator<id, double> ix(X);
    while (ix.hasNext())
    {
        ix.next( ) ;
        result += (Mem.value(ix.key()) - ix.value()) * (Mem.value(ix.key()) - ix.value());
    }
}

void Som::distanceEcth2(const maLigne & X, const maLigne & Mem, float & result)
{
    QMapIterator<id, double> im(Mem);
    while (im.hasNext())
    {
        im.next( ) ;
        if (not X.contains(im.key()))
            result += (im.value()) * (im.value());
    }
}

float Som::distanceEuclidienneMth(const maLigne & X, const maLigne & Mem)
{
  float result1 = 0 ;
  float result2 = 0 ; 	    
  std::thread th1(&Som::distanceEcth1, this, std::ref(X), std::ref(Mem), std::ref(result1)) ;
  std::thread th2(&Som::distanceEcth2, this, std::ref(X), std::ref(Mem), std::ref(result2)) ;
  th1.join() ;
  th2.join() ;

  return sqrt(result1 + result2) ;
}

// Calul la norme d'un vecteur 

float Som::normeEc(const maLigne & X)
{
  float result = 0 ; 	         // résultat du calcul
  QMapIterator<id, double> i(X);
  while (i.hasNext())
  {
      i.next( ) ;
      result += i.value() * i.value() ;
  }
  return sqrt(result) ;
}

// Normalise un vecteur

void Som::normaliseEc(maLigne & X)
{
  float norm = normeEc(X) ;
  if ( norm != 0)
  {
      QMapIterator<id, double> i(X);
      while (i.hasNext())
      {
          i.next( ) ;
          X[i.key()] = i.value()/norm ;
      }
   }
}

// On va normaliser tous les vecteurs d'entrées
void Som::normaliseEcTousVecteurs()
{
  int i;
  for (i = 0; i < DataMatrix.size(); i++)
      normaliseEc((maLigne&)DataMatrix[i]->mligne);
}

/***************************************  version Manhattan ***************************************/

// distance Manhattan
float Som::distanceManhattan(const maLigne & X, const maLigne & Mem)
{
  float result = 0 ; 	    // résultat du calcul
  QMapIterator<id, double> ix(X);
  while (ix.hasNext())
  {
      ix.next( ) ;
      result += fabs(Mem.value(ix.key()) - ix.value());
  }
  QMapIterator<id, double> im(Mem);
  while (im.hasNext())
  {
    im.next( ) ;
    if (not X.contains(im.key()))
        result += im.value();
  }
 return result ;
}

// MultiThread de distance: on partage le calcul sur deux processus en même temps
void Som::distanceMtth1(const maLigne & X, const maLigne & Mem, float & result)
{
    QMapIterator<id, double> ix(X);
    while (ix.hasNext())
    {
        ix.next( ) ;
        result += fabs(Mem.value(ix.key()) - ix.value());
      }
}

void Som::distanceMtth2(const maLigne & X, const maLigne & Mem, float & result)
{
    QMapIterator<id, double> im(Mem);
    while (im.hasNext())
    {
        im.next( ) ;
        if (not X.contains(im.key()))
        result += im.value();
  }
}

float Som::distanceManhattanMth(const maLigne & X, const maLigne & Mem)
{
    float result1 = 0 ;
    float result2 = 0 ;
    std::thread th1(&Som::distanceMtth1, this, std::ref(X), std::ref(Mem), std::ref(result1)) ;
    std::thread th2(&Som::distanceMtth2, this, std::ref(X), std::ref(Mem), std::ref(result2)) ;
    th1.join() ;
    th2.join() ;

    return result1 + result2 ;
}

// norme 1 Manhanttan
float Som::normeMt(const maLigne & X)
{
    float result = 0 ; 	         // résultat du calcul
    QMapIterator<id, double> i(X);
    while (i.hasNext())
    {
        i.next( ) ;
        result += i.value();  // on n'a pas utilisé fabs(i.value()) parce que toutes les valeurs sont >= 0
    }
    return result ;
}

void Som::normaliseMt(maLigne & X)
{
    float norm = normeMt(X) ;
    if ( norm != 0 )
    {
        QMapIterator<id, double> i(X);
        while (i.hasNext())
        {
            i.next( ) ;
            X[i.key()] = i.value()/norm ;
        }
    }
}

void Som::normaliseMtTousVecteurs()
{
    int i;
    for (i = 0; i < DataMatrix.size(); i++)
        normaliseMt((maLigne&)DataMatrix[i]->mligne);
}

/**************************************************************************************************************/
// produit scalaire
float Som::produitScalaire(const maLigne & X, const maLigne & Mem) // X vecteur d'entrée et Mem pour vecteur mémoire d'un neurone
{
    float result = 0 ; 	   // résultat du calcul
    QMapIterator<id, double> i(X);
    while (i.hasNext())
    {
        i.next( ) ;
        result += (i.value()) * Mem.value(i.key()) ;
    }
    return result ;
}

float Som::cosTita(const maLigne& X, const maLigne& Mem) // X vecteur d'entrée et Mem pour vecteur mémoire d'un neurone
{
    float norme_vm = normeEc(Mem) ;
    if (norme_vm != 0)
        return produitScalaire(X,Mem)/norme_vm ;
    else
        return 0 ;
}

float Som::distanceCosinus(const maLigne& X, const maLigne& Mem) // X vecteur d'entrée et Mem pour vecteur mémoire d'un neurone
{
    float norme_vm = normeEc(Mem) ;
    if (norme_vm != 0)
        return (1 - produitScalaire(X,Mem)/norme_vm) ;
    else
        return 0 ;
}

/*              determiner le neurone gagnant Nstar                          */
/****************************************************************************/

// On utilise ici consinus d'angle pour calculer la similarité entre vecteurs X et Mem
// trouver le neurone gagnant  avec  cosinus entre le vecteur d'entré et le vecteur mémoire

void Som::getNStar(int iid, int& xStar, int& yStar, float& dStar, DISTANCE distance)  // Ici on récupère les coordonnées du Nstar et distance
{
    int i,j;
    int X, Y;
    float dist;
    float dist_min;
    dist_min = (this->*distance)(DataMatrix[iid]->mligne, SOM[0][0].w);
    X = 0;
    Y = 0;

    for ( i = 0; i < Lig; i++ )
        for ( j = 0; j < Col; j++ )
        {
            dist = (this->*distance)(DataMatrix[iid]->mligne, SOM[i][j].w);
            if (dist < dist_min)
            {
                dist_min = dist;
                X = i;
                Y = j;
            }
        }
     xStar = X;
     yStar = Y;
     dStar = dist_min;
}
  
/*********************************************************************************/
bool Som::voisin(int i, int j)
{
   if (i<0 || i >= Lig || j<0 || j>=Col)
        return 0;
   else
        return 1;
}

int Som::max(int a, int b)
{
    if ( a < b)
        return b ;
    else
        return a ;
}

/*        la mise a jour des vecteurs memoire     */
/* ******************************************************** */

void Som::majVM(int &x, int &y, int &i, int &j, float & segma, float  &alpha, int &iid) // MAJ  vecteur pour neurone(x,y) voisin de (i,j)
{ float fctvoisin,dmodecare;
  dmodecare = pow( max( abs(i-x) , abs(j-y) ) , 2 ); 
  fctvoisin = exp((-1.0)*dmodecare/(2*pow(segma,2)));
  QMapIterator<id, double > it(DataMatrix[iid]->mligne);
  while ( it.hasNext())
  {it.next();
    SOM[i][j].w[it.key()] = SOM[i][j].w.value(it.key())
		          + alpha*fctvoisin*(it.value() - SOM[i][j].w.value(it.key())); // MAJ de vecteurs codes
  }
  QMapIterator<id, double > im(SOM[i][j].w);
  while ( im.hasNext())
  {im.next();
    if (not DataMatrix[iid]->mligne.contains(im.key()))
      SOM[i][j].w[im.key()] = im.value() - alpha*fctvoisin * im.value(); // MAJ de vecteurs codes
  }
}

void Som::majVect4(int x, int y, int r, int iid, int temps, int Tmax)
{ int i,j;
  float alpha,segma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  segma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  float dist;
   for(i= x-r; i<= x+r; i++)
     for(j= y-r; j<= y+r; j++)
       {if (voisin(i,j))
          { dist = pow((i-x),2) + pow((j-y),2);
               if (dist <= (r*r))
                 majVM(x, y, i, j, segma, alpha, iid) ;
          }
        }
}

void Som::majVect6(int x, int y, int r, int iid, int temps, int Tmax)
{ int i,j;
  float alpha,segma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  segma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  float dist;
  float y6, j6;

    if (x%2 == 0)
        y6 = y + 0.5;
    else
        y6 = y;

    for(i= x-r; i<= x+r; i++)
     for(j= y-r; j<= y+r; j++)
       {if (voisin(i,j))
          { if (i%2 == 0) j6 = j + 0.5;
            else  j6 = j;
            dist = pow((i-x),2) + pow((j6-y6),2);
             if (dist <= (1.25)*r*r)
                majVM(x, y, i, j, segma, alpha, iid) ;
          }
        }
}

void Som::majVect8(int x, int y, int r, int iid, int temps, int Tmax)
{ int i,j;
  float alpha,segma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  segma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  float dist;
   for(i= x-r; i<= x+r; i++)
     for(j= y-r; j<= y+r; j++)
       {if (voisin(i,j))
          { dist = pow((i-x),2) + pow((j-y),2);
               if (dist <= (2*r*r))
                 majVM(x, y, i, j, segma, alpha, iid) ;
          }
        }
}


/**********************************************************************/

/*         Apprentissage                     */
/********************************************/

bool Som:: apprentissage(int max, QString topo, DISTANCE distance, int nb_thread, QElapsedTimer time)
{
    if ((SOM != NULL) && (DataMatrix.size() != 0))
    {
        int t, iid;
        int rayonInit = Col/4 ;
        int rayon ;
        int xStar, yStar;
        float dStar ;
        
        // choix de la topologie de SOM
        typedef void(Som::*MAJVM)(int, int, int, id, int, int);
        MAJVM majVectMem;

        if (topo == "hexagonal") { majVectMem = &Som::majVect6MultiThread ; Top = hexagonal ;} // majVect6MultiThread or majVect6
            else if (topo == "huit_voisins") { majVectMem = &Som::majVect8 ; Top = huit_voisins ;}
                else if (topo == "carre") { majVectMem = &Som::majVect4 ; Top = carre ;}
   
        etatChanged("Apprentissage en cours......."); 	// envoie de message à l'interface
        qDebug() <<"Apprentissage en cours.......";
        t = 0;
        while (t < max)
        {
            rayon = rayonInit - (rayonInit+1) * t / max ;
            // choisir aleatoire un vecteur
            iid = entier_aleatoire_a_b(0,Nb_vect);
            // determiner le neurone gagnant
            getNStarMultiThread(iid, xStar, yStar, dStar, nb_thread, distance);
            // mettre à jour vecteurs mémoires de voisinage
            (this->*majVectMem)(xStar, yStar, rayon, iid, t, max);
           
            t++;
            fprintf(stderr, "\033[0GProcessed %d / %d itérations ", t ,max);
     
            // afficher dans l'interface graphique l'évolution de processus
            //etatSOM =  "Apprentissage en cours..." + QString::number(t) + "/"+  QString::number(max);
            //etatChanged(etatSOM); 	// envoie de message à l'interface
        }
        outVM("vm10x10GDB.txt");
        qDebug() << "\nApprentissage est terminé avec succes";
        qDebug() << "Apprentissage : " << time.elapsed() / (qint64) 60000 << " minutes" ;
        return true;
    }
    else
    {
        qDebug() << "Impossible de faire apprentissage";
        return false;
    }
}

// Imprimer les classes dans un fichier texte:
// Prochainement on va inserer les classes dans la bdd dans une table classe
void Som::imprimeClasses(QString f)
{
  //QSqlQuery req(*dbConnection) ;
  //QString Synset;
  if (SOM != NULL)
  { int i,j ;
    QFile file(f);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream flux(&file);
    for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     {
         flux<<"N["<<i<<"]["<<j<<"]:"<<"  "<<SOM[i][j].classe.size()<<endl;
         //Synset="N["+QString::number(i)+"]["+QString::number(j)+"]";
         if (SOM[i][j].classe.size() != 0)
         {
           QMapIterator<id, float> it(SOM[i][j].classe);
           while (it.hasNext())
           {
               it.next();
               // flux<< "\""<<sqlGetWord(it.key()) <<"\" ";
               flux<< "\""<<sqlGetNomById(it.key()) <<"\" ";
               //req.exec(QString("insert into \"WORD_synsets\" (synset,id_word) values('%1',%2)").arg(Synset).arg(it.key())) ;
          }
         
          flux<<"\n\n";
       }
     }
    file.close();
  }
}



double Som::reel_aleatoire_a_b(double a, double b)  // generateur des réels aleatoires entre a et b
{
    return ( rand()/(double)RAND_MAX ) * (b-a) + a;    
}

int Som::entier_aleatoire_a_b(int a,int b)    // generateur des entiers aleatoires entre a et b
{
   return (rand() % (b - a )) + a;
}

void Som::SOM_clean()
{ int i,j ;
  if (SOM != NULL)
  { for (i = 0; i < Lig; i++)
     for(j = 0; j < Col; j++) SOM[i][j].w.clear();
     for(i = 0; i < Lig; i++) delete [] SOM[i] ;
     delete [] SOM;
     SOM = NULL   ;
     qDebug() << "SOM suprime" ;
  }
}

/*****************************************************************
         Multithreading
********************************************************************/
// imprime un fichier texte: id i j distance
void Som::imprimeIdIJDist(QString f, int seuil,  int iterations, QString dist, bool initial)
{ if (SOM != NULL)			
  { int i,j ;
    QFile file(f);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
    QTextStream flux(&file);
    flux.setCodec("iso-8859-1");
    // en tête du fichier
    flux << "# Base_de_données " << BDD::getdbStatus() <<endl ;
    flux << "# Seuil " << seuil <<endl ;
    flux << "# SOM " << Lig <<"x"<< Col <<endl ;
    flux << "# Nbre_itérations " << iterations <<endl ;
    flux << "# Distance_utilisée " << dist <<endl ;
    flux << "# Nbre_de_ngrammes " << Nb_vect <<endl ;
    flux << "# Initialise_SOM_au_centre " << initial << endl;
    flux << "# QCI (HI) = " << QCI << endl ;
    flux << "# QCE (HO) = " << QCE << endl ;
        
    /*********************************************************************/
    flux << Lig <<" "<< Col <<endl ;
    for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { if (SOM[i][j].classe.size() != 0)
       {  QMapIterator<id, float> it(SOM[i][j].classe) ;
          while (it.hasNext())
          {it.next();
           flux<< it.key() <<" "<< i <<" "<< j <<" "<< it.value() <<endl ;
          }
       
       }
     }
    file.close();
  }
}
 

/**************************************************************************************************
       la distance en paramètre
************************************************************************************************************/

void Som::getNStarThread(int debut, int fin, int iid, float& dist_min, int& X, int& Y, DISTANCE distance) 
{ int i,j;							
  float dist;
  for ( i = debut; i < fin; i++ )
   for ( j = 0; j < Col; j++ )
      { dist = (this->*distance)(DataMatrix[iid]->mligne, SOM[i][j].w);
        if (dist < dist_min)
        { dist_min = dist;
          X = i;
          Y = j;
        }
      }
}

void Som::getNStarMultiThread(int iid, int& xStar, int& yStar, float& dStar, int nb_thread, DISTANCE distance )
{ // nouvelle version création dynamique des threads
   int i;
   QVector <Nstar*> VGs;
   for (i = 0; i < nb_thread; i++)
   { VGs.append(new Nstar);
     VGs[i]->X = 0;
     VGs[i]->Y = 0;
     VGs[i]->dist = (this->*distance)(DataMatrix[iid]->mligne, SOM[0][0].w) ;
   }

   //Créer des threads 
    int debut, fin = 0;
    QVector <std::thread*> threads(nb_thread) ;
    for (i = 0; i < nb_thread-1; i++)
    { debut = fin;
      fin +=Lig/nb_thread ; 
      threads[i] = new std::thread(&Som::getNStarThread,this,debut,fin,iid,
					std::ref(VGs[i]->dist), std::ref(VGs[i]->X), std::ref(VGs[i]->Y), distance);
    }
    threads[i] = new std::thread(&Som::getNStarThread,this,fin,Lig,iid,
					    std::ref(VGs[i]->dist), std::ref(VGs[i]->X), std::ref(VGs[i]->Y), distance); // la dernière partie
    for (i = 0; i < nb_thread; i++) threads[i]->join(); // started thread

    xStar = VGs[0]->X ;
    yStar = VGs[0]->Y ;
    dStar = VGs[0]->dist ;
    for (i = 1; i < nb_thread; i++)
    { if (VGs[i]->dist < dStar)
       { dStar = VGs[i]->dist ;
         xStar = VGs[i]->X ;
         yStar = VGs[i]->Y ;         
       }
     }
}

// pour la maj on utilise deux threads: 1 prend une partie des voisins et l'autre il prend le reste
void Som::majVect6MultiThread(int x, int y, int r, int iid, int temps, int Tmax)
{ float alpha,sigma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  sigma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  // création des threads
  std::thread th1(&Som::majVect6Thread,this,x-r,x,alpha,sigma,x,y,r,iid) ;
  std::thread th2(&Som::majVect6Thread,this,x,x+r+1,alpha,sigma,x,y,r,iid) ;
  // started 
  th1.join() ;
  th2.join() ;

}

void Som::majVect6Thread(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid)
{ int i,j;
  float dist;
  float y6, j6;
  if (x%2 == 0) y6 = y + 0.5;
  else  y6 = y;
  for(i= debut; i< fin; i++)
   for(j= y-r; j<= y+r; j++)
    {if (voisin(i,j))
      { if (i%2 == 0) j6 = j + 0.5;
        else  j6 = j;
        dist = pow((i-x),2) + pow((j6-y6),2);
        if (dist <= (1.25)*r*r) 
         majVM(x, y, i, j, sigma, alpha, iid) ; 
           
       }
     }
}


void  Som::classifierThread(int debut, int fin, DISTANCE distance, int nb_thread_ap)
{ int xStar, yStar ;
  float dStar ;
  QString str;
  qDebug() << "Running..." ;

  for (int i(debut); i < fin; i++)
    {  getNStarMultiThread(i, xStar, yStar, dStar, nb_thread_ap, distance);
       SOM[ xStar ][ yStar ].classe.insert(DataMatrix[i]->id_vect, dStar);
       fprintf(stderr, "\033[0G... %d", i);
    }
}

void  Som::classificationMultiThread(DISTANCE distance, int nb_thread, int nb_thread_ap, QElapsedTimer time)
{
  if ((SOM != NULL) && (DataMatrix.size() != 0))
  {
    etatChanged("Classification en cours...");
    qDebug() <<"Classification en cours.......\n";
     
    //Créer des threads et partager le KeyDataMatrix sur les threads
    int debut, fin = 0;
    QVector <std::thread*> threads(nb_thread) ;
    for (int i(0); i < nb_thread-1; i++)
    { debut = fin;
      fin +=Nb_vect/nb_thread ; 
      threads[i] = new std::thread(&Som::classifierThread,this, debut, fin, distance, nb_thread_ap); // std::ref(flux)
    }
    threads[nb_thread-1] = new std::thread(&Som::classifierThread,this, fin, Nb_vect, distance, nb_thread_ap); // la dernière partie
    for (int i(0); i < nb_thread; i++) threads[i]->join(); // started thread

   etatChanged("Classification terminée avec succée");
   QCI = getQCI() ;
   QCE = getQCE() ;
   qDebug() << "\nQualité de la classification (QC) = "<< QCI << "QCE = " << QCE ;
   qDebug() << "Note : On cherche à minimiser QC, (QC --> 0 <=> bonne classification)" ;
  }
   else
    qDebug() << "ERREUR: Impossible de faire l'Apprentissage.";
   qDebug() << "Classification : " << time.elapsed() / (qint64) 60000 << " minutes" ;
}
// calcul de QCI et QCE

double Som::getQCI()
{ int i, j ;
  int Nb_classes = 0 ;
  double inertie_intraClasse ;
  double inertie_interClasses = 0 ;
  for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { if (SOM[i][j].classe.size() != 0)
       {  inertie_intraClasse = 0 ;
	  Nb_classes ++ ;
          QMapIterator<id, float> it(SOM[i][j].classe) ;
          while (it.hasNext())
          {it.next();
           inertie_intraClasse += it.value()*it.value() ;
          }
	  inertie_intraClasse /= SOM[i][j].classe.size() ;
        }
	inertie_interClasses += inertie_intraClasse ;
      }
   return inertie_interClasses/Nb_classes ;
}
  
double Som::getQCE()
{ double distance = 0 ;
  double dist ;
  int M = Lig * Col ;
  int i, j, k, n ;
  for (i=0; i < Lig; i++)
   for (j=0; j < Col; j++)
    for (k=0; k < Lig; k++)
     for (n=0; n < Col; n++)
      { if ((k*Lig + n) > (i*Lig +j))
 	 { dist = distanceEuclidienneMth(SOM[i][j].w, SOM[k][n].w);
	   distance += dist*dist ;
         }
       }
   return distance/((M*M-M)/2) ;
}   


/******************************************************** getVM() , getDataMatrix(), outVM(), outDataMatrix()*********************/


void Som::outVM(QString vmFile)  // imprimer les vecteurs mémoire dans un fichier texte, afin de les utiliser ultirieurement
{ QFile file(vmFile);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
  QTextStream flux;
  flux.setDevice(& file);
  qDebug() << "Imprission de VM en cours..." ;
  flux << Lig <<" "<< Col <<endl;
  int i,j ;
  for (i=0;  i < Lig; i++)
   for (j=0; j < Col; j++)
    { QMapIterator<id, double> it(SOM[i][j].w);
      flux << SOM[i][j].w.size() <<" " ;
      while (it.hasNext())
      {it.next();
        flux << it.key() <<" "<< it.value() <<" ";
      }
      flux <<"\n" ;
     }
  file.close();
}

void Som::getVM(QString vmFile)  // charger les vecteurs mémoire à partir d'un fichier texte.
{ QFile file(vmFile);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))  return;
  QTextStream flux;
  flux.setDevice(& file);
  qDebug() << "getVM en cours..." ;
  int i, j, k ;
  int fLig, fCol ;
  long long v_size ;  // la taille du VM
  id col ;    	     // position sur la ligne
  double valeur ;   // la valeur correspond
  flux >> fLig >> fCol ;
  if ((fLig == Lig) && (fCol == Col))
   { for (i = 0;  i < Lig; i++)
     for (j = 0; j < Col; j++)
      { flux >> v_size ;
        for (k = 0; k < v_size; k++)
         { flux >> col >> valeur ;
           SOM[i][j].w[col] = valeur ;
         }
       }
    }
   else
    { qDebug() << "Erreur: le fichier pour getVM ne correspond pas au SOM" ;
      SOM_clean();
    }
      
   file.close();
}


void Som::outDataMatrix(QString veFile)  // imprimer la matrice de données dans un fichier texte, afin de les utiliser ultirieurement
{ QFile file(veFile);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
  QTextStream flux;
  flux.setDevice(& file);
  qDebug() << "Imprission de VE en cours..." ;
  int i;
  flux << DataMatrix.size() <<endl;
  for ( i = 0; i < DataMatrix.size(); i++) 
  { flux << DataMatrix[i]->id_vect <<" "<< DataMatrix[i]->mligne.size() <<endl;
     QMapIterator<id, double> ix(DataMatrix[i]->mligne);
     while (ix.hasNext())
     { ix.next();
       flux << ix.key() <<" "<< ix.value() <<" ";
     }
      flux <<"\n" ;
    }
  file.close();
}

void Som::getDataMatrix(QString veFile)  // charger la matrice d'entrée à partir d'un fichier texte.
{
  QFile file(veFile);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))  return;
  QTextStream flux;
  flux.setDevice(& file);
  qDebug() << "getDataMatrix en cours..." ;
  int i , j ;
  int k = 0;
  id ligne ;
  long long v_size ;  // la taille du VE
  id col ;    	     // position sur la ligne
  double valeur ;   // la valeur qui correspond
  flux >> Nb_vect ; 
  for(j = 0; j < Nb_vect; j++)
  {  flux >> ligne >> v_size ;
     DataMatrix.append(new Ligne);
     DataMatrix[k]->id_vect = ligne ;
     for (i = 0;  i < v_size; i++)
     { flux >> col >> valeur ;
       DataMatrix[k]->mligne.insert(col, valeur); // insérer dans la ligne courante
     }
     k++ ;
    
  }
  file.close();
  qDebug() <<"Nb_vect = "<< Nb_vect ;
}



/************************* dessus les fonctions utilisées par SOM *************************************/

/***********************************************************************************************************/
/********************************* Accée à la bdd  *********************************************************/
/**********************************************************************************************************/
int Som::sqlGetTailleVecteur()
{
    return SOMBDD->sqlGetTailleVecteur();
}
// à partir des tables matrice, ngramme, on construit une matrice de donnée ==> SOM
// on colle dans DataMatrix les valeurs de la matrice
// il serait peut-être plus rapide de trier par ligne et d'affecter ligne par ligne ?
bool Som::sqlGetDataMatrix()
{
    Taille = sqlGetTailleVecteur();
    qDebug() << "+++++++++++++++Taille des vecteurs: " << Taille;
    etatChanged("Construction de la matrice en cours...");   // envoie de message d'état à l'interface
    qDebug() << "Construction de la matrice en cours"<<endl;
    maMatrice sqlDataMatrix=SOMBDD->sqlGetDataMatrix(Taille);
    if (sqlDataMatrix.size()==0)
    {
        qDebug() << "++++++++++++DataMatrix size: " << sqlDataMatrix.size();
        return false;
    }
    maMatrice::iterator it;
    int i=0;
    int nb_val = 0; // nombre de valeurs non null
    for(it=sqlDataMatrix.begin();it!=sqlDataMatrix.end();++it)
    {
        DataMatrix.append(new Ligne);
        DataMatrix[i]->id_vect = it.key() ;
        maLigne::iterator itcol;
        i++;
        for(itcol=sqlDataMatrix[it.key()]->begin();itcol!=sqlDataMatrix[it.key()]->end();++itcol)
       {
            DataMatrix[i-1]->mligne.insert(itcol.key(),itcol.value());
            nb_val++;
        }
    }
    //DataMatrix=
    Nb_vect = DataMatrix.size() ;
    qDebug() <<"Nb_vect = "<<Nb_vect;
    qDebug() <<"Nb de valeurs non null = "<< nb_val <<" / " << Nb_vect*Nb_vect << "===>" <<(double)nb_val/(Nb_vect*Nb_vect) <<"%" ;
    return true;
}
QString Som::sqlGetNomById(id id_ngram)
{
    return SOMBDD->sqlGetNomById(id_ngram);
}

// Inserer les synsets dans la base de donnees
void Som::insertSynsets()
{
    QString Synset;
    if (SOM != NULL)
    {
        int i,j ;
        for (i=0;  i < Lig; i++)
            for (j=0; j < Col; j++)
            {
                Synset="N["+QString::number(i)+"]["+QString::number(j)+"]";
                if (SOM[i][j].classe.size() != 0)
                {
                    QMapIterator<id, float> it(SOM[i][j].classe);
                    while (it.hasNext())
                    {
                        it.next();
                        sqlInsertSynsets( Synset, it.key(),it.value());
                    }
                }
     }
  }
    else
        qDebug() << "SOM is NULL";
}
bool Som::sqlInsertSynsets(QString syn, id idw, qreal val)
{
    return SOMBDD->sqlInsertSynsets(syn,idw,val);
}

// test calcul de distance
void Som::euclidienneTest()
{ QMap<id, double> vect1, vect2 ;
  vect1.insert(2,2);
  vect1.insert(3,5);
  vect1.insert(6,7);
  vect2.insert(1,1);
  vect2.insert(2,3);
  vect2.insert(4,2);
  vect2.insert(6,5);

  qDebug() << "Un exemple pour tester les distance avec et sans multithreading" ;
  qDebug() <<"vect1 = (0,2,5,0,0,6)" ;
  qDebug() <<"vect2 = (1,3,0,2,0,5)" ;
  qDebug() <<"euclidienne multithread entre vect1 et vect2: "<< distanceEuclidienneMth(vect1,vect2) ;
  qDebug() <<"euclidienne entre vect1 et vect2: "<< distanceEuclidienne(vect1,vect2) ;
  qDebug() <<"manhattan multithread entre vect1 et vect2: "<< distanceManhattanMth(vect1,vect2) ;
  qDebug() <<"manhattan entre vect1 et vect2: "<< distanceManhattan(vect1,vect2) ;
  
  normaliseEc(vect1);
  normaliseEc(vect2);
  qDebug() <<"costita entre vect1 et vect2: "<< cosTita(vect1,vect2) ;
}

/********************************************************************/

