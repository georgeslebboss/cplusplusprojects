#ifndef SOM_H_INCLUDED
#define SOM_H_INCLUDED

#include <iostream>
#include <thread>
#include <QtWidgets>
#include "algosomBDD.h"

class Som;
typedef float(Som::*DISTANCE)(const maLigne &, const maLigne &);

class Som : public QWidget
{
    int Lig;      // lig x col = nombre de neurones de la carte
    int Col;
    int Taille;   // taille du vecteur mémoire
    float Alphai; // paramètres de la fct d'Apprentissage, une fonction Gaussiènne
    float Sigmai;
    float Sigmaf;

    //int Nb_vect;         // nombre de vecteurs d'entrées = dataMatrix.size()
    int Nb_vect;

    QString etatSOM;		// pour afficher l'évolution de SOM dans l'interface graphique

    struct Neurone
    {   maLigne w;		  // vecteur mémoire
        QMap<id, float> classe;	 // classe contient tous les mots associer à ce neurone 
    } ;
    
    Neurone **SOM ; 		// la carte topologique SOM
   
    //maMatrice DataMatrix; 
    struct Ligne
    {  id id_vect ;
       maLigne mligne ;
    };
    QVector <Ligne*> DataMatrix ; // la matrice de données

    //QVector <id> KeyDataMatrix; // pour stocker tous les key de dataMatrix , utilisé par classificationMultiThread()
 
    enum Topologie {carre = 4, hexagonal = 6, huit_voisins = 8 };
    Topologie Top;
    enum Distance {costita, Euclidienne, Manhattan};
    Distance Dis;

    struct Nstar  // used by Multithreading
    { int X; 
      int Y;
      float dist;
    } ;  

    double QCI ; // qualité de classification, tous les threads en ont accés 
    double QCE ;

Q_OBJECT

    
  public:

    Som(float = 0.5, float = 0.5, float = 0.2);
    //Som(QString, int, int);
    void setparametersSOM(int, int);//set les parametres ligne et colonne
    /*Acces aux requetes AlgoSOM*/
    AlgoSOMBDD *SOMBDD;
    ~Som();
        
    bool  apprentissage(int, QString, DISTANCE, int, QElapsedTimer);
    void  initialiseSOM();
    void  getVM(QString vmFile) ; // charger les vecteurs mémoire à partir d'un fichier texte.
    void  outDataMatrix(QString veFile) ; // imprimer la matrice de données dans un fichier texte, afin de les utiliser ultirieurement
    void  getDataMatrix(QString veFile) ; // charger la matrice d'entrée à partir d'un fichier texte.

   

    // MultiThread
    void  classificationMultiThread(DISTANCE distance, int nb_thread, int nb_th_ap, QElapsedTimer);
    void  classifierThread(int, int, DISTANCE distance, int nb_thread_ap);
    
    void imprimeClasses(QString);
    void insertSynsets();

    void imprimeIdIJDist(QString f, int seuil,  int iterations, QString dist, bool) ;  // imprime un fichier texte: id i j distance
    int getnb_vect() { return Nb_vect; }
    bool sqlGetDataMatrix() ;
    int sqlGetTailleVecteur();

    void initialiseVM(maLigne & Mem);
    void  normaliseMtTousVecteurs();
    void  normaliseEcTousVecteurs();
    QString sqlGetNomById(id);

    void euclidienneTest();

    float distanceEuclidienneMth(const maLigne & X, const maLigne & Mem);
    float distanceManhattanMth(const maLigne & X, const maLigne & Mem);
    float distanceCosinus(const maLigne& X, const maLigne& Mem);
   

    signals:
     void etatChanged(QString);

  private:
    // distance Euclidiènne entre deux vecteurs
    
    //version cosinus tita
    float produitScalaire(const maLigne& X, const maLigne& Mem); // X vecteur d'entrée et Mem pour vecteur mémoire d'un neurone
    float cosTita(const maLigne& X, const maLigne& Mem);
        
    // version Manhattan
    float distanceManhattan(const maLigne&  X, const maLigne& Mem);
    float normeMt(const maLigne& X);   		
    void  normaliseMt(maLigne & X);	
    
    /* MultiThread*/
    void  distanceMtth1(const maLigne & X, const maLigne & Mem, float & result);
    void  distanceMtth2(const maLigne & X, const maLigne & Mem, float & result);
    
 
    // version Euclidiènne
    float distanceEuclidienne(const maLigne& X, const maLigne& Mem);
    float normeEc(const maLigne& X);     //used by cosTita	
    void  normaliseEc(maLigne & X);	
    
    //******
     void getNStar(int iid, int& xStar, int& yStar, float& dStar, DISTANCE distance);

    /* MultiThread*/
    void  distanceEcth1(const maLigne & X, const maLigne & Mem, float & result);
    void  distanceEcth2(const maLigne & X, const maLigne & Mem, float & result);
    

    // MAJ des vecteurs mémoires
    void majVect4(int x, int y, int r, int, int temps, int Tmax); // topologie en carre, 4 voisins
    void majVect6(int x, int y, int r, int, int temps, int Tmax); // topologie en hexagonal, 6 voisins
    void majVect8(int x, int y, int r, int, int temps, int Tmax); // topologie en  8 voisins
    void majVM(int &x, int &y, int &i, int &j, float & segma, float  &alpha, int &iid); // used by majVect4 ,6 ,8
    
    bool voisin(int i,int j); // = true si (i,j) dans la carte SOM sinon false
    int max(int,int);
    double reel_aleatoire_a_b(double a, double b);   // réel aleatoire entre a et b
    int entier_aleatoire_a_b(int a,int b);		// entier aleatoire entre a et b
    void SOM_clean();          // pour suprimer SOM du mémoire
    void outVM(QString vmFile); // imprimer les vecteurs mémoire dans un fichier texte, afin de les utiliser ultirieurement
    

    // MultiThread
    /* hexagonal */
    void  majVect6MultiThread(int x, int y, int r, int iid, int temps, int Tmax);
    void  majVect6Thread(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid);
    

    /** ***/
    void getNStarThread(int debut, int fin, int iid, float& dist_min, int& X, int& Y, DISTANCE distance);
    void getNStarMultiThread(int iid, int& xStar, int& yStar, float& dStar, int nb_thread, DISTANCE distance );

   // calcul de QCI et QCE
    double getQCI() ;
    double getQCE() ;
    double getQCE_R() ;
    bool sqlInsertSynsets(QString , id, qreal );
	
};

#endif // SOM_H_INCLUDED
