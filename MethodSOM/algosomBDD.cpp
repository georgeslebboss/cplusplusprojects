#include "algosomBDD.h"
using namespace std;

AlgoSOMBDD::AlgoSOMBDD()
{
}

/*********************les requets sql utilises par AlgoSOM et CorpuSom**********************/
QString AlgoSOMBDD::sqlGetNomById(id id_ngram)
{
    QSqlQuery reqn(*BDD::DatabaseConnection);
    QString gram ="";
    if (nameapp=="CorpusChar")
        reqn.prepare("select ngram from ngramme where id_ngram = ?");
    else if (nameapp=="CorpusWord")
        //reqn.prepare("select form from \"WORD_form\" where id = ?");
        reqn.prepare("select preprocform from \"WORD_preprocform\" where idformpreproc = ?");
    else if (nameapp=="SurveyWord2Vec")
        reqn.prepare("select word_litteral from \"W2V_WORDSSKIP\" where id_word = ?");
    else if (nameapp=="SurveyGlove")
        reqn.prepare("select word_litteral from \"GLOVE_WORDS\" where id_word = ?");
    reqn.addBindValue(id_ngram);
    if(not reqn.exec())
    {
        qDebug() << reqn.lastError().text()<< endl;
        return "";
    }
    while(reqn.next()) gram = reqn.value(0).toString();
    return gram;

}
bool AlgoSOMBDD::sqlInsertSynsets(QString Synset, id idw, qreal valr)
{
    QSqlQuery req(*BDD::DatabaseConnection);
    bool res=true;
    if (nameapp== "CorpusWord")
        res=req.exec(QString("insert into \"WORD_synsets_value\" (synset,id_word,val) values('%1',%2,%3)").arg(Synset).arg(idw).arg(valr));
    if(not res)
        qDebug() << req.lastError().text()<< endl;
    return res;
}
int AlgoSOMBDD::sqlGetTailleVecteur()
{
    QSqlQuery reqn(*BDD::DatabaseConnection);
    int nbN ;
    QString reqsql;
    if (nameapp=="CorpusChar")
        reqsql="select count (ngram) from  ngramme where stoplist = 'f'";
    else if (nameapp=="CorpusWord")
        //reqsql="select count(form) from  \"WORD_form\" ";
        reqsql="select count(*) from  \"WORD_preprocform\" ";
    else if (nameapp=="SurveyWord2Vec")
        reqsql="select count(id_word) from  \"W2V_WORDSSKIP\"";
    else if (nameapp=="SurveyGlove")
        reqsql="select count(id_word) from \"GLOVE_WORDS\" ";
    if(not reqn.exec(reqsql))
    {
        qDebug() << reqn.lastError().text()<< endl;
        return -1;
    }
    while(reqn.next())
        nbN = reqn.value(0).toLongLong();
    return nbN;
}
maMatrice AlgoSOMBDD::sqlGetDataMatrix(int taille)
{
    maMatrice DataMatrix;
    if (nameapp=="CorpusChar")
        DataMatrix=this->sqlGetDataMatrixXYVal("select fk_i,fk_j,val from matrice where stoplist = 'f' order by fk_i,fk_j",taille);
    else if (nameapp=="CorpusWord")
        DataMatrix=this->sqlGetDataMatrixXYVal("select fk_form,fk_pattern,sum(val) as val from \"WORD_matrix\" group by fk_form,fk_pattern order by fk_form,fk_pattern",taille);
    else if (nameapp=="SurveyWord2Vec")
        DataMatrix=this->sqlGetDataMatrixXYArray("select id_word,word_vector from \"W2V_WORDSSKIP\"",taille);
    return DataMatrix;
}

maMatrice AlgoSOMBDD::sqlGetDataMatrixXYVal(QString reqsql, int taille)
{
    maMatrice DataMatrix;
    QSqlQuery reqm(*BDD::DatabaseConnection) ;
    id ligne = 0 ; 	    // ligne corresponde à id_ngram
    id col ;
    int i = 0 ;
    double valeur;

    qDebug() << "Construction de la matrice en cours" << *BDD::DatabaseConnection;
    qDebug() <<reqsql;
    if(not reqm.exec(reqsql)) // On récupère i, j, valeur
    {
        qDebug() << "ERREUR: " <<reqm.lastError().text()<< endl;
        return {};
    }
   while(reqm.next())
   {
       if (ligne != reqm.value(0).toLongLong()) // if condition, On passe à ligne suivante dans la DataMatrix
       {
           ligne = reqm.value(0).toLongLong() ;
           //qDebug() << "ligne :" <<ligne;
           DataMatrix.insert(ligne, new maLigne);
           i++;
       }
       col = reqm.value(1).toLongLong() ;
       valeur =(double)reqm.value(2).toLongLong() ;
       DataMatrix[ligne]->insert(col, valeur); // insérer dans la ligne courante
       //nb_val ++ ;
       // afficher dans l'interface graphique l'évolution de processus
       //qDebug() <<"+++Construction de la matrice en cours..." + QString::number(ligne ) + "/"+  QString::number(taille);
       qDebug() <<"+++Construction de la matrice en cours..." << ligne  << "/"+  QString::number(taille);
   }
   //sauvegarder la matrice dans un fichier sous format "id_word id_patt:value "
   QFile outFile("matrixtoclassify.txt");
   if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
       qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
   QTextStream outsql(&outFile);
   outsql.setCodec("UTF-8");
   QMap<id,maLigne*>::iterator it;
   QMap<id,double>::iterator itl;
   for(it=DataMatrix.begin();it!=DataMatrix.end();++it)
   {
       outsql << it.key() << " ";
       for(itl=it.value()->begin();itl!=it.value()->end();++itl)
           outsql << itl.key() <<":"<<itl.value()<<" ";
       outsql<< "\n";
   }
   outFile.close();
   return DataMatrix;
}
void AlgoSOMBDD::sqlGetDataMatrixFile(QString matrixfile)
{
    QString reqsql="select fk_form,fk_pattern,sum(val) as val from \"WORD_matrix\" group by fk_form,fk_pattern order by fk_form,fk_pattern";
    maMatrice DataMatrix;
    QSqlQuery reqm(*BDD::DatabaseConnection) ;
    id ligne = 0 ; 	    // ligne corresponde à id_ngram
    id col ;
    int i = 0 ;
    double valeur;

    qDebug() << "Construction de la matrice en cours" << *BDD::DatabaseConnection;
    qDebug() <<reqsql;
    if(not reqm.exec(reqsql)) // On récupère i, j, valeur
    {
        qDebug() << "ERREUR: " <<reqm.lastError().text()<< endl;
        exit(0);
    }
   while(reqm.next())
   {
       if (ligne != reqm.value(0).toLongLong()) // if condition, On passe à ligne suivante dans la DataMatrix
       {
           ligne = reqm.value(0).toLongLong() ;
           //qDebug() << "ligne :" <<ligne;
           DataMatrix.insert(ligne, new maLigne);
           i++;
       }
       col = reqm.value(1).toLongLong() ;
       valeur =(double)reqm.value(2).toLongLong() ;
       DataMatrix[ligne]->insert(col, valeur); // insérer dans la ligne courante
   }
   //sauvegarder la matrice dans un fichier sous format "id_word id_patt:value "
   QFile outFile(matrixfile);
   if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
       qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
   QTextStream outsql(&outFile);
   outsql.setCodec("UTF-8");
   QMap<id,maLigne*>::iterator it;
   QMap<id,double>::iterator itl;
   for(it=DataMatrix.begin();it!=DataMatrix.end();++it)
   {
       outsql << sqlGetNomById(it.key()) << " ";
       for(itl=it.value()->begin();itl!=it.value()->end();++itl)
           outsql << itl.key() <<":"<<itl.value()<<" ";
       outsql<< "\n";
   }
   outFile.close();
   qDebug() << "Fin Matrice";
}
maMatrice AlgoSOMBDD::sqlGetDataMatrixXYArray(QString reqsql, int taille)
{
    maMatrice DataMatrix;
    QSqlQuery reqm(*BDD::DatabaseConnection) ;
    id ligne = 0 ; 	    // ligne corresponde à id_ngram
    int i = 0 ;
    //int nb_val = 0; // nombre de valeurs non null
    qDebug() << "Construction de la matrice en cours" <<endl;
    if(not reqm.exec(reqsql)) // On récupère i, j, valeur
    {
        qDebug() << reqm.lastError().text()<< endl;
        return {};
    }
    while(reqm.next())
    {
        ligne = reqm.value(0).toLongLong() ;
        DataMatrix.insert(ligne,new maLigne);
        QString myStr = reqm.value(1).toString();
        myStr.replace(QString("{"), QString(""));
        myStr.replace(QString("}"), QString(""));
        QStringList v = myStr.split(",");
        for(int s=0;s<v.length();s++)
        {
            DataMatrix[ligne]->insert(s,v[s].toFloat());
        }
        i++;
        // afficher dans l'interface graphique l'évolution de processus
        qDebug() << "Construction de la matrice en cours..." + QString::number(ligne + 1) + "/"+  QString::number(taille);
   }
   return DataMatrix;
}
void AlgoSOMBDD::setNameapp(QString napp)
{
    qDebug() << "Name APP: " << napp;
    nameapp=napp;
}
