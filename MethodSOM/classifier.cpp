#include "classifier.h"

using namespace std;

classifier::classifier(): QWidget()
{
    setFixedSize(700, 500);

    classifierBouton = new QPushButton("Classifier");
    synsetsBouton = new QPushButton("Insert Synsets");
    matrixtoclassify=new QPushButton("Matrix to Classify");

    QHBoxLayout *layoutBouton = new QHBoxLayout;

    // ajouter les boutons au layoutBouton
    layoutBouton->addStretch();
    layoutBouton->addWidget(classifierBouton);
    layoutBouton->addWidget(synsetsBouton);
    layoutBouton->addWidget(matrixtoclassify);


    // Extraction des vecteurs
    Seuil   = new QSpinBox;
    Seuil->setRange(0,30000);
    Seuil->setValue(0);
    Seuil->setMaximumWidth(80);
    QFormLayout *chargerLayout = new QFormLayout;
    NameApp = new QComboBox ;
    NameApp->setMaximumWidth (120);
    NameApp->addItems(BDD::nameapp);
    chargerLayout->addRow("Application-->Donnees d'entree du SOM:",NameApp);
    chargerLayout->addRow("Seuil:",Seuil);

    // fichiers résultats
    classesOut = new QLineEdit("../resultats/classes1.txt") ;
    motNeurone = new QLineEdit("../resultats/mot_neuronne1.txt") ;
    matrixfile= new QLineEdit("matrixtoclassify");
    QFormLayout *resultatLayout = new QFormLayout;
    resultatLayout->addRow("SOM---> calsses :",classesOut);
    resultatLayout->addRow("SOM---> mot_neurone :",motNeurone);
    resultatLayout->addRow("SOM-->MatrixToClassify:",matrixfile);

    resultat = new QGroupBox("Fichiers Resultat:");
    resultat->setLayout(resultatLayout);
    resultat->setEnabled(true);

    //paramètres SOM
    N_som = new QSpinBox;
    N_som->setRange(1,100);
    N_som->setValue(10);
    N_som->setMaximumWidth(50);

    M_som = new QSpinBox;
    M_som->setRange(1,100);
    M_som->setValue(10);
    M_som->setMaximumWidth(50);

    T_max = new QSpinBox;
    T_max->setRange(0,3000000);
    T_max->setValue(1000);
    T_max->setMaximumWidth(80);

    Topo = new QComboBox ;
    Topo->setMaximumWidth (120);
    QStringList topo ;
    topo << "hexagonal" << "huit_voisins" << "carre"  ;
    Topo->addItems(topo) ;

    Dist = new QComboBox ;
    Dist->setMaximumWidth (120);
    Dist->addItem("Cosinus");
    Dist->addItem("Manhattan");
    Dist->addItem("Euclidienne");

    Nb_th = new QSpinBox ;
    Nb_th->setRange(1,1024);
    Nb_th->setValue(2);
    Nb_th->setMaximumWidth(50);

    Nb_thAp = new QSpinBox ;
    Nb_thAp->setRange(1,10);
    Nb_thAp->setValue(2);
    Nb_thAp->setMaximumWidth(50);

    initialise = new QCheckBox("initialise SOM au centre de données") ;
    QHBoxLayout *layoutCheckBox = new QHBoxLayout;
    layoutCheckBox->addWidget(initialise);

    QFormLayout *somLayout = new QFormLayout;
    somLayout->addRow("Nsom :", N_som);
    somLayout->addRow("Msom :", M_som);
    somLayout->addRow("Tmax :", T_max);
    somLayout->addRow("Topologie :", Topo);
    somLayout->addRow("Distance :", Dist);
    somLayout->addRow("Nb_Threads pour classification :", Nb_th);
    somLayout->addRow("Nb_Threads pour l'apprentissage :", Nb_thAp);


    groupSom = new QGroupBox("Parametres du SOM:");
    groupSom->setLayout(somLayout);
    groupSom->setEnabled(true);


    // etatSOM
    QHBoxLayout *layoutLabel = new QHBoxLayout;
    etatSOM = new QLabel;
    layoutLabel->addWidget(etatSOM);

    QVBoxLayout *layoutPrincipal = new QVBoxLayout;


    layoutPrincipal->addLayout(chargerLayout);
    layoutPrincipal->addWidget(resultat);
    layoutPrincipal->addWidget(groupSom);
    layoutPrincipal->addLayout(layoutCheckBox);
    layoutPrincipal->addLayout(layoutBouton);
    layoutPrincipal->addLayout(layoutLabel);


    // ajouter layoutPrincipal a la fenetre
    setLayout(layoutPrincipal);

    //som = new Som(N_som->value(), M_som->value());
    som=new Som();
    connect(som, SIGNAL(etatChanged(QString)), this, SLOT(onEtatChanged(QString)));
    // Connexion des signaux et des slots
    connect(classifierBouton,SIGNAL(clicked()),this, SLOT(classer()));
    connect(synsetsBouton,SIGNAL(clicked()),this, SLOT(inserer()));
    connect(NameApp,SIGNAL(currentTextChanged(QString)),som->SOMBDD,SLOT(setNameapp(QString)));
    connect(matrixtoclassify,SIGNAL(clicked()),this,SLOT(creermatrixfile()));

}

void classifier::onEtatChanged(QString str)
{
    etatSOM->setText(str);
    etatSOM->setWordWrap(true) ;
    etatSOM->repaint() ;
}

void classifier::classer()
{
    QElapsedTimer time ;
    time.start() ;
    qDebug() << "Chronomètre relancé : " << time.restart() / (qint64) 60000 << " minutes" ;
    som->setparametersSOM(N_som->value(),M_som->value());
    som->sqlGetDataMatrix();
    //som->getDataMatrix("../data/dataMatrixGGDBsize5seuil100.txt");
    // normalisation des vecteurs d'entrées
    if (Dist->currentText() == "Manhattan")
        som->normaliseMtTousVecteurs();
    else
        som->normaliseEcTousVecteurs();

    if (initialise->isChecked())
        som->initialiseSOM();
    if (Dist->currentText() == "Manhattan")
    {
        som->apprentissage(T_max->value(), Topo->currentText(), &Som::distanceManhattanMth, Nb_thAp->value(), time);
        som->classificationMultiThread(&Som::distanceManhattanMth, Nb_th->value(), Nb_thAp->value(), time);
    }
    else if (Dist->currentText() == "Euclidienne")
    {
        som->apprentissage(T_max->value(), Topo->currentText(), &Som::distanceEuclidienneMth, Nb_thAp->value(), time);
        som->classificationMultiThread(&Som::distanceEuclidienneMth, Nb_th->value(), Nb_thAp->value(), time);
    }
    else
    {
        som->apprentissage(T_max->value(), Topo->currentText(), &Som::distanceCosinus, Nb_thAp->value(), time);
        som->classificationMultiThread(&Som::distanceCosinus, Nb_th->value(), Nb_thAp->value(), time);
    }
    qDebug() << "Imprime un fichier texte: id i j distance: " <<motNeurone->text();
    som->imprimeIdIJDist(motNeurone->text(), Seuil->value(), T_max->value(), Dist->currentText(), initialise->isChecked() ) ;
    qDebug() << "Imprimer les classes dans un fichier texte: " << classesOut->text();
    som->imprimeClasses(classesOut->text());
    
   QMessageBox::information(this, "Classification terminee", "consultez le fichier \"classes.txt\"!");
  
}
void classifier::inserer()
{
  QElapsedTimer time ;
  time.start() ;
  som->insertSynsets();
  QMessageBox::information(this, "Insertion terminee", "consultez BD");
}

void classifier::creermatrixfile()
{
    som->SOMBDD->sqlGetDataMatrixFile(matrixfile->text());
}

void classifier::on_vm_textChanged()
{
   groupSom->setEnabled(true);
}
