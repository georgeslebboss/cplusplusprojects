#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include "som.h"

class classifier : public QWidget
{
    Q_OBJECT

public:
    classifier();

private slots:
    void classer();
    void inserer();
    void creermatrixfile();
    void on_vm_textChanged();
    void onEtatChanged(QString);

    private:
    QPushButton *classifierBouton;
    QPushButton *synsetsBouton;
    QPushButton *matrixtoclassify;

    //QLineEdit *NameBDD;
    QComboBox *NameApp;
    QSpinBox *Seuil;
    QLineEdit *classesOut;
    QLineEdit *motNeurone;
    QCheckBox *initialise;
    QSpinBox *N_som;
    QSpinBox *M_som;
    QSpinBox *T_max;
    QSpinBox *Nb_th;
    QSpinBox *Nb_thAp;
    QComboBox *Topo;
    QComboBox *Dist;
    QGroupBox *groupSom;
    QGroupBox *resultat;

    QLineEdit  *matrixfile;
    // Etat de SOM
    QLabel *etatSOM;

    Som * som;
};


#endif // CLASSIFIER_H
