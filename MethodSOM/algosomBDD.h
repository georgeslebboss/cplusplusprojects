#ifndef ALGOSOMBDD_H
#define ALGOSOMBDD_H

#include <../fromInterface.h>

typedef QMap<id, double> maLigne ;
typedef QMap<id, maLigne*> maMatrice ;


class AlgoSOMBDD : public BDD
{
    Q_OBJECT
/********************************************************************************************/
public:
      AlgoSOMBDD();
      /*les requetes SQL utilises par AlgoSOM et CorpusSOM*/
      QString sqlGetNomById(id );
      bool sqlInsertSynsets(QString, id, qreal);
      int sqlGetTailleVecteur();
      maMatrice sqlGetDataMatrix(int );
      void sqlGetDataMatrixFile(QString );
private:
      QString nameapp;
      maMatrice sqlGetDataMatrixXYVal(QString , int );
      maMatrice sqlGetDataMatrixXYArray(QString, int);
public slots:
    void setNameapp(QString);
};
#endif // ALGOSOMBDD_H
