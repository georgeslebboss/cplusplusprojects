#ifndef BOXES_H
#define BOXES_H

#include <QObject>
#include <QVector>
#include <QString>

/**
 * @brief The Boxes class
 *
 * l'objectif de ce class est de permettre une echange des donnees entre les boxes
 *
 */
class Boxes : public QObject
{

  Q_OBJECT

  public:
    Boxes();
    ~Boxes();

    void setLanguage(QString);
    void setNormalizationParams(QVector<bool>);
    void setSegmenter(QString);
    void setLemmatizer(QString);

    QString getLanguage();
    QVector<bool> getNormalizationParams();
    QString getNormalizationParamsString();
    QString getFullParamsString();
    QString getSegmenter();
    QString getLemmatizer();

  protected:

    QString language;
    QVector<bool> normalizationParams;
    QString segmenter;
    QString lemmatizer;


};

#endif
// BOXES_H

