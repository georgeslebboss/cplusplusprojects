#include "boxes.h"

Boxes::Boxes() {}
Boxes::~Boxes() {}

void Boxes::setLanguage(QString p_language)
{
    language = p_language;
}
    
void Boxes::setNormalizationParams(QVector<bool> p_normalizationParams)
{
    normalizationParams = p_normalizationParams;
}

void Boxes::setSegmenter(QString p_segmenter)
{
    segmenter=p_segmenter;
}

void Boxes::setLemmatizer(QString p_lemmatizer)
{
    lemmatizer=p_lemmatizer;
}

QString Boxes::getSegmenter()
{
    return segmenter;
}

QString Boxes::getLemmatizer()
{
    return lemmatizer;
}

QString Boxes::getLanguage()
{
    return language;
}

QVector<bool> Boxes::getNormalizationParams()
{
    return normalizationParams;
}

QString Boxes::getNormalizationParamsString()
{
    QString s = "";

    if (normalizationParams.size() > 0)
    {
        for (int i=0; i<normalizationParams.size(); i++)
        {
            if (normalizationParams[i])
            {
                s = s + "1";
            } else
            {
                s = s + "0";
            }
        }
    }
    return s;
}
QString Boxes::getFullParamsString()
{
    QString s = "";

    if (normalizationParams.size() > 0)
    {
        for (int i=0; i<normalizationParams.size(); i++)
        {
            if (normalizationParams[i])
            {
                s = s + "1";
            } else
            {
                s = s + "0";
            }
        }
    }
    //add segmenter and lemmatizer params
    if (segmenter!="") s=s+"_"+segmenter;
    if (lemmatizer!="") s=s+"_"+lemmatizer;

    return s;
}
