#ifndef GRAPAMATRIX_H
#define GRAPAMATRIX_H
#include <QtWidgets>
#include "../fromInterface.h"

typedef QMap<id, float> grapaLine ;
typedef QMap<id, grapaLine*> grapaMatrixDense ; // la matrice sans 0
// l'élément d'une ligne dans la matrise sparse
struct line_matrix
{
    id idmot;                     // identifiant du mot
    QVector<float> valpoids; //les valeurs de pattern consecutif
} ;
typedef line_matrix grapaLineSparse ;
typedef QVector<grapaLineSparse*> grapaMatrixSparse;//matrice carree nbmots*nbpatterns contenant ts les patterns comme cols avec les 0s comme poids

class GraPaMatrix: public QObject
{
    Q_OBJECT
public:
    GraPaMatrix(int, QString, int, int, int, QString, QString) ;
    ~GraPaMatrix() {}
protected:

    QSqlDatabase *pdbConnection=BDD::DatabaseConnection;
    int CorpusID;                  //corpus id
    
    QString PreParams;             // params de pretraitement
    
    int SeuilMarque ;              // fréquence minimale pour définir une marque
    int JokerLength ;              // longueur maximale des mots consecutifs normaux dans un patron
    int SeuilPattern ;             // fréquence minimale pour conserver un patron

    float SumMatrix;               //somme totale de la matrice
    
    QString MatrixWeight ;             // poids de (w,p)
    QString MatrixReduction ;          // methode de reduction de matrice

    // Création
    QMap<id,float> IDMots;
    QMap<id,float> IDPattern;
    id PattFirstID;
    grapaMatrixDense  sqlGetDataMatrix(); //get la matrice dense
    void              sqlFillIDPattern(); //remplir IDPattern et get PattFirstID;
    grapaMatrixSparse sqlGetSparseMatrix(grapaMatrixDense);//get sparse matrix from dense matrix
    grapaMatrixSparse sqlGetSparseMatrixWeightPwp(grapaMatrixDense);//get sparse matrix for weight P(w,p) from dense matrix
    grapaMatrixSparse sqlGetSparseMatrixWeightPpGivenw(grapaMatrixDense);//get sparse matrix for weight P(w|p) from dense matrix
    grapaMatrixSparse sqlGetSparseMatrixWeightPpGivenwDividedPp(grapaMatrixDense);//get sparse matrix for weight P(w|p)/P(p) from dense matrix
    grapaMatrixSparse sqlGetSparseMatrixWeightPMI(grapaMatrixDense); //get sparse matrix for weight PMI=log2(P(w|p)/P(p))
    grapaMatrixSparse sqlGetSparseMatrixWeightPPMI(grapaMatrixDense); //get sparse matrix for weight PPMI=max(0,PMI)
    grapaMatrixSparse sqlGetSparseMatrixWeightTFIDF(grapaMatrixDense); //get sparse matrix for weight TF*IDF=MCount*log(#p/#pcontainsw)

} ;


#endif // GRAPAMATRIX_H
