cmake_minimum_required(VERSION 2.6)

find_package(Qt5Widgets REQUIRED)
find_package(Qt5Network REQUIRED)

set(SOURCES gpv_tab.cpp settings_box.cpp grapamatrix.cpp)
set(HEADERS gpv_tab.h settings_box.h grapamatrix.h)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

add_definitions(-Wall ${QT_DEFINITIONS} ${Qt5Widgets_DEFINITIONS})

include_directories(${Qt5Widgets_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})

qt5_wrap_cpp(HEADERS_MOC ${HEADERS})
qt5_wrap_ui(FORMS_MOC ${FORMS})
qt5_add_resources(RESOURCES_RCC ${RESOURCES})

add_library(grapaveclib SHARED ${SOURCES} ${HEADERS_MOC})
qt5_use_modules(grapaveclib Sql Widgets)
