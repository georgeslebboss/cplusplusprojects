#include "settings_box.h"

SettingsBox::SettingsBox()
: Box(tr("Settings Box"))
{
    qDebug() << "constructeur SettingsBox" ;

    QGridLayout * settingslayout = new QGridLayout ;
    setAlignment(Qt::AlignTop) ;
    setLayout(settingslayout) ;

    QGroupBox * preprocbox = new QGroupBox(tr("Preprocessing Settings")) ;;
    settingslayout->addWidget(preprocbox,0,0);
    QGridLayout * preproclayout = new QGridLayout ;
    preprocbox->setAlignment(Qt::AlignTop) ;
    preprocbox->setLayout(preproclayout) ;

    preproclayout->addWidget(new QLabel("Languages"), 0, 0) ;
    Languagebox = new QComboBox() ;
    connect(Languagebox, SIGNAL(currentIndexChanged(int)), this, SLOT(onLanguageChanged())) ;
    createArabicSection(preproclayout) ;                                // doit être fait avant sqlGetLanguage() à cause du connect
    SettingsBoxBDD * preprocbdd = new SettingsBoxBDD() ;
    Languagebox->setModel(preprocbdd->sqlGetLanguage()) ;                     // doit être fait après le connect
    preproclayout->addWidget(Languagebox, 0, 1) ;

    //segmenter section
    QLabel * qsegmenter = new QLabel("Segmenter");
    preproclayout->addWidget(qsegmenter, 9, 0);
    QComboBox * ar_segmenter = new QComboBox();
    ar_segmenter->addItem("");
    ar_segmenter->addItem("STANFORD SEGMENTER");
    ar_segmenter->addItem("HARRISMORPH");
    preproclayout->addWidget(ar_segmenter,9,1);
    connect
    (
      ar_segmenter,
      static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
      [=](int index){emit sigSegmenterModified(ar_segmenter->currentText()) ;}
    ) ;

    //stemming section
    QLabel * qstem = new QLabel("Lemmatizer");
    preproclayout->addWidget(qstem, 10, 0) ;
    QComboBox * stem = new QComboBox();
    stem->addItem("");
    stem->addItem("KHOJA Lemmatizer");
    preproclayout->addWidget(stem, 10, 1) ;
    connect
    (
      stem,
      static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
      [=](int index){emit sigLemmatizerModified(stem->currentText()) ;}
    ) ;

    if (Languagebox->itemText(Languagebox->currentIndex()) != "Arabic") hideArabicSection() ;

    //Partie de Pattern settings
    QGroupBox * patternbox = new QGroupBox(tr("Pattern Settings")) ;;
    settingslayout->addWidget(patternbox,0,2);
    QGridLayout * patternslayout = new QGridLayout ;
    patternbox->setAlignment(Qt::AlignTop) ;
    patternbox->setLayout(patternslayout) ;

    patternslayout->addWidget(new QLabel(tr("Marker threshold")), 0, 0) ;
    ThresholdBox = new QSpinBox ;
    ThresholdBox->setRange(1, 1000000) ;
    patternslayout->addWidget(ThresholdBox, 0, 1) ;
    connect
    (
       ThresholdBox,
       static_cast<void(QSpinBox::*)()>(&QSpinBox::editingFinished),
       [=](){emit sigMarkThresholdChanged(ThresholdBox->value());}
    );

    patternslayout->addWidget(new QLabel(tr("Joker Length")), 1, 0);
    JokerLengthBox = new QSpinBox ;
    JokerLengthBox->setRange(1, 1000000) ;
    patternslayout->addWidget(JokerLengthBox, 1, 1);
    connect
    (
       JokerLengthBox,
       static_cast<void(QSpinBox::*)()>(&QSpinBox::editingFinished),
       [=](){emit sigJokerLengthChanged(JokerLengthBox->value());}
    );

    patternslayout->addWidget(new QLabel(tr("Pattern threshold")), 2, 0);
    PatternThresholdBox = new QSpinBox ;
    PatternThresholdBox->setRange(1, 1000000) ;
    patternslayout->addWidget(PatternThresholdBox, 2, 1);
    connect
    (
       PatternThresholdBox,
       static_cast<void(QSpinBox::*)()>(&QSpinBox::editingFinished),
       [=](){emit sigPatternThresholdChanged(PatternThresholdBox->value());}
    );

    //Partie de generation de matrice settings
    QGroupBox * matrixbox = new QGroupBox(tr("Matrix Settings"));
    settingslayout->addWidget(matrixbox,0,3);
    QGridLayout * matrixlayout = new QGridLayout ;
    matrixbox->setAlignment(Qt::AlignTop) ;
    matrixbox->setLayout(matrixlayout) ;

    matrixlayout->addWidget(new QLabel("Weighting Factor"), 1, 0);
    MatrixWeightComboBox = new QComboBox();
    MatrixWeightComboBox->addItems({"#(w,p)", "P(w,p)", "P(p|w)","PMI","PPMI", "Mcount*idf", "MPpw*idf"});
    matrixlayout->addWidget(MatrixWeightComboBox, 1, 1);
    matrixlayout->setAlignment(Qt::AlignTop);
    connect
    (
      MatrixWeightComboBox,
      static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
      [=](int index){emit sigMatrixWeightModified(MatrixWeightComboBox->currentText()) ;}
    ) ;

    matrixlayout->addWidget(new QLabel("Matrix Reduction"), 2, 0);
    MatrixReductionComboBox = new QComboBox();
    MatrixReductionComboBox->addItems({"None", "SVD"});
    matrixlayout->addWidget(MatrixReductionComboBox, 2, 1);
    matrixlayout->setAlignment(Qt::AlignTop);
    connect
    (
      MatrixReductionComboBox,
      static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
      [=](int index){emit sigMatrixReductionModified(MatrixReductionComboBox->currentText()) ;}
    ) ;

    GenerateMatrixPushButton= new QPushButton("Generate Matrix");
    matrixlayout->addWidget(GenerateMatrixPushButton, 3, 0,1,2) ;
    matrixlayout->setAlignment(Qt::AlignTop);
    connect
    (
      GenerateMatrixPushButton,
      &QPushButton::clicked,
     [=] {emit sigGenerateMatrixPushButton();}
    );


}

void SettingsBox::onLanguageChanged()
{
    if (debug) qDebug() << "Preprocessing Box >> language changed to " << Languagebox->currentText();
    emit sigLanguageModified(Languagebox->currentText()) ;
     
    if(Languagebox->currentText() == "Arabic") showArabicSection();
    else hideArabicSection();
   
}

void SettingsBox::createArabicSection(QGridLayout * settingslayout)
{
  settingslayout->setAlignment(Qt::AlignTop);
  ArabicSectionTitle = new QLabel("For Arabic only") ;
  settingslayout->addWidget(ArabicSectionTitle, 1, 0) ;
  
  NormParams = new normparams ;
  
  NormParams->append("diacritics") ;
  NormParams->append("numbers") ;
  NormParams->append("foreign letters") ;
  NormParams->append("shadda and madda") ;
  NormParams->append("alef") ;
  NormParams->append("yeh") ;
  NormParams->append("heh") ;
  
  NormParamLines = new normparamlines ;
  NormParamValues = new normparamvalues ;

  for (int i = 0 ; i < NormParams->size() ; i++)
  {
    QLabel * label = new QLabel("remove " + NormParams->at(i)) ;
    QCheckBox * check = new QCheckBox() ;
    NormParamLines->insert(NormParams->at(i), qMakePair(label, check)) ;

    VarNormParamValues.insert(NormParams->at(i), false) ;
    emit sigVarNormParamModified(VarNormParamValues) ;

    connect
    (
      check,
      &QCheckBox::stateChanged,
     [=](int state){(VarNormParamValues)[NormParams->at(i)] = state ; emit sigVarNormParamModified(VarNormParamValues) ;}
    ) ;

    settingslayout->addWidget(label, i + 2, 0) ;
    settingslayout->addWidget(check, i + 2, 1) ;
  }
}

void SettingsBox::hideArabicSection()
{
  ArabicSectionTitle->hide() ;
  
  foreach (normparamline line, NormParamLines->values())
  {
    line.first->hide() ;
    line.second->hide() ;
  }
}

void SettingsBox::showArabicSection()
{
  ArabicSectionTitle->show() ;
  
  foreach (normparamline line, NormParamLines->values())
  {
    line.first->show() ;
    line.second->show() ;
  }
}

//============================================================= BDD ======================================

SettingsBoxBDD::SettingsBoxBDD() {}

/*requetes de SettingsBox*/
QSqlQueryModel * SettingsBoxBDD::sqlGetLanguage()
{
  qDebug() << "LoadLanguage de BDD: " << BDD::DatabaseConnection;
  QSqlQueryModel * languagesqlmodel = new QSqlQueryModel();
  languagesqlmodel->setQuery("SELECT name FROM language", * BDD::DatabaseConnection) ;
  return languagesqlmodel;
}
