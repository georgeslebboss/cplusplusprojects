#ifndef GPV_TAB_H
#define GPV_TAB_H

#include <QtWidgets>
#include "../fromInterface.h"
#include "settings_box.h"
#include "grapamatrix.h"


using namespace std;


//================================ COMPOSANTS DE LA FENETRE

class GraPaVecTab : public Tab
{
  Q_OBJECT

  public:
  
    GraPaVecTab() ;
    ~GraPaVecTab() ;

    
} ;

class GraPaVecParams : public Shared
{
  Q_OBJECT
  
  public:
    GraPaVecParams();
    ~GraPaVecParams();

    void setCorpusId(int);
    void setLanguage(QString) ;
    void setSegmenter(QString) ;
    void setLemmatizer(QString) ;
    void setNormParams(normparams *);
    void setNormParamValues(normparamvalues *);
    void setVarNormParamValues(normparamvalues);
    void setMarkThreshold(int);
    void setPatternThreshold(int);
    void setJokerLength(int);
    void setMatrixWeight(QString);
    void setMatrixReduction(QString);

    int getCorpusId();
    QString getLanguage() ;
    normparams * getNormParams();
    normparamvalues * getNormParamValues();
    normparamvalues getVarNormParamValues();
    QVector<bool> getNormalizationParams();
    QString getNormalizationParamsString();
    QString getFullParamsString();
    QString getSegmenter();
    QString getLemmatizer();
    int getMarkThreshold();
    int getPatternThreshold();
    int getJokerLength();
    QString getMatrixWeight();
    QString getMatrixReduction();

  protected:

    int CorpusId;
    QString Language;
    QVector<bool> NormalizationParams;
    normparamvalues * NormParamValues;
    normparamvalues VarNormParamValues;
    normparams * NormParams;
    QString Segmenter;
    QString Lemmatizer;
    int MarkThreshold;
    int PatternThreshold;
    int JokerLength;
    QString MatrixWeight;
    QString MatrixReduction;

} ;

class GraPaVecAppli : public Appli
{
  Q_OBJECT

  public:
    GraPaVecAppli() ;
  
    QSqlDatabase          * DbConnection=BDD::DatabaseConnection;
    GraPaMatrix           * grapamatrix;


  protected slots:

  protected:
} ;

#endif // GPV_TAB_H
