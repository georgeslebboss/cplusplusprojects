#include "grapamatrix.h"


// Constructeur de matrice mots*patterns
GraPaMatrix::GraPaMatrix(int corpusid,QString params, int seuilmarque, int jokerlength, int seuilpattern,QString mweight, QString mreduce )
  :CorpusID(corpusid),PreParams(params),SeuilMarque(seuilmarque),JokerLength(jokerlength), SeuilPattern(seuilpattern),MatrixWeight(mweight),MatrixReduction(mreduce)
{
    if (CorpusID==0 || PreParams=="0000000" || SeuilMarque==0 || JokerLength==0 || SeuilPattern==0)
        qDebug() << "tu dois choisir tous les params demandes, on pris les params par defaut!";
    qDebug() << "creer la matrice de Corpus " << corpusid << 
		" ayant params: " << params  <<"\n"
		" seuil marque: " << seuilmarque  <<"\n"
		" joker length: " << jokerlength  <<"\n"
		" seuil pattern: " << seuilpattern  <<"\n"
		" matrix wight: " << mweight  <<"\n"
        " matrix reduction: " << mreduce <<"\n";
    sqlFillIDPattern();
    grapaMatrixDense gpmdense=sqlGetDataMatrix();
    //grapaMatrixSparse msparse = sqlGetSparseMatrix(gpmdense); //tested ok
    //grapaMatrixSparse msparse =sqlGetSparseMatrixWeightPwp(gpmdense); //tested ok
    //grapaMatrixSparse msparse =sqlGetSparseMatrixWeightPpGivenw(gpmdense);//tested ok
    //grapaMatrixSparse msparse =sqlGetSparseMatrixWeightPpGivenwDividedPp(gpmdense);//tested ok
    grapaMatrixSparse msparse =sqlGetSparseMatrixWeightPMI(gpmdense);//tested ok
    QFile outFile("sparsematrixtoclassifyPMI.csv");
    if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
    QTextStream outsql(&outFile);
    outsql.setCodec("UTF-8");
    outsql << "mots/Patterns" << ";";
    for(int l=0;l<IDPattern.size();l++)
        outsql << l+PattFirstID << ";";
    outsql << "\n";
    qDebug() <<  msparse.size();
    for(int i=0;i<IDMots.size();i++)
    {
        outsql << msparse.at(i)->idmot << ";";
        for(int j=0; j<msparse.at(i)->valpoids.size();j++)
            outsql << msparse.at(i)->valpoids.at(j) << ";";
        outsql<< "\n";
    }
    outFile.close();

    qDebug() << "Matrice generee dans un fichier ligne:" << IDMots.size() << "* col:" << IDPattern.size() << " first:" << PattFirstID << " SUM total:" << SumMatrix  ;

    //print IDPattern for(auto e : IDPattern.keys())  qDebug() << ":::" << e << ":" << IDPattern.value(e) << '\n';
    //print IDMots    for(auto e : IDMots.keys())     qDebug() << ":::" << e << ":" << IDMots.value(e) << '\n';
}
grapaMatrixDense GraPaMatrix::sqlGetDataMatrix()
{
     grapaMatrixDense datamatrix;
     QSqlQuery reqm(*pdbConnection) ;
     id ligne = 0 ; 	    // ligne corresponde à id_mot
     id col ;
     float sumligne=0;
     float valeur;
     qDebug() << "Construction de la matrice en cours";
     QString reqsql=QString("select fk_form,fk_pattern,sum(val) as val from \"WORD_matrix\" where corpus_id=%1 and pt_params='%2' and smarque=%3 and jlentgh=%4 and spatt=%5 group by fk_form,fk_pattern order by fk_form,fk_pattern").arg(CorpusID).arg(PreParams).arg(SeuilMarque).arg(JokerLength).arg(SeuilPattern);
     //qDebug() << reqsql;
     if(not reqm.exec(reqsql)) // On récupère i, j, valeur
     {
         qDebug() << "ERREUR: " <<reqm.lastError().text()<< endl;
         return {};
     }
    while(reqm.next())
    {
        if (ligne != reqm.value(0).toFloat()) // if condition, On passe à ligne suivante dans la DataMatrix
        {
            ligne = reqm.value(0).toFloat() ;
            //qDebug() << "ligne :" <<ligne;
            datamatrix.insert(ligne, new grapaLine);
            IDMots.insert(ligne,0);
            sumligne=0;
        }
        col = reqm.value(1).toFloat() ;
        valeur =reqm.value(2).toFloat() ;
        sumligne += valeur;
        SumMatrix += valeur;
        IDPattern[col] +=valeur;
        IDMots[ligne]=sumligne;
        datamatrix[ligne]->insert(col, valeur); // insérer dans la ligne courante
    }
    //PattFirstID=IDPattern.firstKey();
    //sauvegarder la matrice dans un fichier sous format "id_word id_patt:value "
    QFile outFile("densematrixtoclassify.txt");
    if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
    QTextStream outsql(&outFile);
    outsql.setCodec("UTF-8");
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;
    for(it=datamatrix.begin();it!=datamatrix.end();++it)
    {
        outsql << it.key() << " ";
        //IDMots.append(it.key());
        for(itl=it.value()->begin();itl!=it.value()->end();++itl)
            outsql << itl.key() <<":"<<itl.value()<<" ";
        outsql<< "\n";
    }
    outFile.close();
    qDebug() << "matrice dense generee avec succes et sauvegarder dans le fichier text densematrixtoclassify";
    return datamatrix;
 }

void GraPaMatrix::sqlFillIDPattern()
{
    QSqlQuery req(*pdbConnection) ;
    if(not req.exec(QString("select id from \"WORD_pattern\" where corpus_id=%1 and pt_params='%2' and smarque=%3 and jlentgh=%4 and spatt=%5 order by id").arg(CorpusID).arg(PreParams).arg(SeuilMarque).arg(JokerLength).arg(SeuilPattern)))
    {
        qDebug() << "ERREUR: " <<req.lastError().text()<< endl;
        return;
    }
    while (req.next())
        IDPattern.insert(req.value(0).toInt(),0);
    PattFirstID=IDPattern.firstKey();
    qDebug() << "IDPattern genere avec succes de longueur:" << IDPattern.size() << " first:" << PattFirstID;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrix(grapaMatrixDense datamatrix)
{
    //Matrice des comptes #(w,p) w:word p:pattern
    //Mcount
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
            lsparse->valpoids.replace(itl.key()-PattFirstID,itl.value());//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrixWeightPwp(grapaMatrixDense datamatrix)
{
    //Matrice des P(w,p) w:word p:pattern
    //MPpw <- Mcount/sum(Mcount)
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
            lsparse->valpoids.replace(itl.key()-PattFirstID,itl.value()/SumMatrix);//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrixWeightPpGivenw(grapaMatrixDense datamatrix)
{
    //Matrice des P(p|w) correspond a normalisation lineaire en ligne
    //MPpGivenw <- MPpw *  1/rowSums(MPpw)
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
        {
            float rowsum= IDMots.value(it.key())/SumMatrix;
            lsparse->valpoids.replace(itl.key()-PattFirstID,(itl.value()/SumMatrix) * (1/rowsum));//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        }
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrixWeightPpGivenwDividedPp(grapaMatrixDense datamatrix)
{
    //Matrice des P(p|w)/P(p) = P(w|p)/P(w) = P(w,p)/(P(w)P(p))
    //Pw <- colSums(MPcw) //Pp <- rowSums(MPpw)
    //MPpGivenwDividedPp <- MPpGivenw %*%  diag(1/colSums(MPpw))
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
        {
            float colsum= IDPattern.value(itl.key())/SumMatrix;
            float rowsum= IDMots.value(it.key())/SumMatrix;
            lsparse->valpoids.replace(itl.key()-PattFirstID,(itl.value()/SumMatrix) / (rowsum*colsum));//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        }
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrixWeightPMI(grapaMatrixDense datamatrix)
{
    //Matrice des log2 de P(p|w)/P(p) = P(w|p)/P(w) = P(w,p)/(P(w)P(p))
    //Pw <- colSums(MPcw) //Pp <- rowSums(MPpw)
    //MPpGivenwDividedPp <- MPpGivenw %*%  diag(1/colSums(MPpw))
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
        {
            float colsum= IDPattern.value(itl.key())/SumMatrix;
            float rowsum= IDMots.value(it.key())/SumMatrix;
            lsparse->valpoids.replace(itl.key()-PattFirstID,std::log2((itl.value()/SumMatrix) / (rowsum*colsum)));//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        }
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}

grapaMatrixSparse GraPaMatrix::sqlGetSparseMatrixWeightPPMI(grapaMatrixDense datamatrix)
{
    // Matrice PPMI
    // max0 <- function(x){ifelse(x<0,0,x)}
    // PPMI <- max0(PMI)
    grapaMatrixSparse msparse=QVector<grapaLineSparse*>();
    qDebug() << datamatrix.size();
    QMap<id,grapaLine*>::iterator it;
    QMap<id,float>::iterator itl;

    for(it=datamatrix.begin();it!=datamatrix.end();it++) //idmot=it.key
    {
        grapaLineSparse *lsparse=new grapaLineSparse();
        lsparse->valpoids=QVector<float>(IDPattern.size(),0);
        lsparse->idmot=it.key();
        for(itl=it.value()->begin();itl!=it.value()->end();++itl) //idpattern=itl.key() ; val=itl.value();
        {
            float colsum= IDPattern.value(itl.key())/SumMatrix;
            float rowsum= IDMots.value(it.key())/SumMatrix;
            float valPPMI=std::log2((itl.value()/SumMatrix) / (rowsum*colsum));
            if (valPPMI<0)  valPPMI=0;
            lsparse->valpoids.replace(itl.key()-PattFirstID,valPPMI);//qDebug() << lsparse->idmot << ":" << lsparse->valpoids;
        }
        msparse.append(lsparse);
    }
    qDebug() << "matrice sparse generee avec succes et sauvegarder dans le fichier text sparsematrixtoclassify";
    return msparse;
}
