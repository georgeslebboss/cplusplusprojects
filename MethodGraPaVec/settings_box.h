#ifndef SETTINGSBOX_H
#define SETTINGSBOX_H
#include "QtWidgets"
#include "../fromInterface.h"


class SettingsBoxBDD : public BDD
{
  Q_OBJECT

public:
    SettingsBoxBDD();
    QSqlQueryModel * sqlGetLanguage();
} ;

//================================ COMPOSANTS DE LA FENETRE


typedef QVector<QString> normparams ;

typedef QMap<QString, bool> normparamvalues ;

typedef QPair<QLabel *, QCheckBox *> normparamline ;

typedef QMap<QString, normparamline> normparamlines ;


class SettingsBox : public Box
{
    Q_OBJECT
    
public:
  
    SettingsBox() ;

    bool debug = true;
    

protected:

    //settings de preprocessing

    normparams *      NormParams ;       // parameter strings
    normparamvalues * NormParamValues ;  // map with parameter strings and values (for exportation)
    normparamvalues   VarNormParamValues ;  // map with parameter strings and values (for exportation)
    normparamlines *  NormParamLines ;   // map with parameter strings, QLabels and QCheckBoxes

    QComboBox *    Languagebox ;         //combo contains all languages in DB
    QLabel  *       ArabicSectionTitle ;

    void            createArabicSection(QGridLayout *) ;
    void            hideArabicSection() ;
    void            showArabicSection() ;

    //Settings de pattern

    QSpinBox           * ThresholdBox ;
    QSpinBox           * JokerLengthBox ;
    QSpinBox           * PatternThresholdBox ;

    //settings de matrice
    QComboBox          * MatrixWeightComboBox;
    QComboBox          * MatrixReductionComboBox;
    QPushButton        * GenerateMatrixPushButton;
  
protected slots:
  
    void onLanguageChanged();
    
signals:

    void sigLanguageModified(QString) ;
    void sigNormParamModified(normparamvalues *) ;
    void sigVarNormParamModified(normparamvalues) ;
    void sigSegmenterModified(const QString& text) const ;
    void sigLemmatizerModified(const QString& text) const ;
    void sigMarkThresholdChanged(const int& val) const;
    void sigPatternThresholdChanged(const int val) const;
    void sigJokerLengthChanged(const int val) const;
    void sigMatrixWeightModified(const QString& text) const ;
    void sigMatrixReductionModified(const QString& text) const ;
    void sigGenerateMatrixPushButton();
} ;

#endif // SETTINGSBOX_H
