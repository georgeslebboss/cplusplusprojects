

// This box takes a preproc corpora to select from

#include "corpuspreprocselect_box.h"

CorpusPreprocSelectBox::CorpusPreprocSelectBox()
: Box("Select Corpus")
{
  DbConnection = BDD::DatabaseConnection; // if done in : DbConnection(BDD::DatabaseConnection, would it work or is it too early?

  QGridLayout * corpusselectboxlayout = new QGridLayout ;
  setAlignment(Qt::AlignTop) ;
  setLayout(corpusselectboxlayout);
  corpusselectboxlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));
  corpusselectboxlayout->setAlignment(Qt::AlignTop);

  corpusselectboxlayout->addWidget(new QLabel(tr("Corpus")), 0, 0);
  CorpusValue = new QComboBox;
  loadCorpora();

  CorpusValue->setCurrentIndex(-1);
  connect(CorpusValue, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {setCorpus(index);});
  corpusselectboxlayout->addWidget(CorpusValue, 0, 1);

  //bouton refresh
  QPushButton * refreshcorpora = new QPushButton(tr("Refresh"));
  corpusselectboxlayout->addWidget(refreshcorpora, 0, 3);
  connect(refreshcorpora, &QPushButton::clicked, [=]() {loadCorpora();});
}

/*avec preproc select
pareil que corpus + refresh
preproc + refresh
juste en dessous
avec autant de choix dans la selectbox que de Word_preproc avec corpus_id = id du corpus choisi
+ 1 pour nopreproc
le nom de preproc = nom de segmenteur + nom de lemmatiseur + options
sinon exactement pareil que corpus
on n'a pas besoin de preprocessingbox*/
QSqlQueryModel * CorpusPreprocSelectBox::getCorpusListSqlModel() {
    qDebug() << "getting preproc corpus list from Word_Segment: " << * DbConnection;

    QSqlQueryModel * CorpusListModel = new QSqlQueryModel();
    QString sql =
            " SELECT \"WORD_preproc\".idpreproc as idp , corpus.id as idc, "
            " corpus.nom_corpus  || '_' || \"WORD_preproc\".paramspreproc || '_' || \"WORD_preproc\".segmenter || '_' || \"WORD_preproc\".lemmatizer as namepreproc "
            " FROM \"WORD_preproc\", corpus, \"WORD_segment\" "
            " WHERE \"WORD_preproc\".fk_id_corpus = corpus.id AND  \"WORD_segment\".fk_idpreproc = \"WORD_preproc\".idpreproc "
            " group by \"WORD_preproc\".idpreproc,corpus.id"
            " UNION "
            " SELECT 0 as idp , corpus.id as idc, "
            " corpus.nom_corpus  || '_0000000__' as namepreproc "
            " FROM corpus, \"WORD_segment\" "
            " WHERE  \"WORD_segment\".fk_idpreproc =0 "
            " group by corpus.id";
    //qDebug() << sql ;
    CorpusListModel->setQuery(sql, * DbConnection);

    CorpusListModel->setHeaderData(0, Qt::Horizontal, tr("idpreproc"));
    CorpusListModel->setHeaderData(1, Qt::Horizontal, tr("idcorpus"));
    CorpusListModel->setHeaderData(2, Qt::Horizontal, tr("preprocname"));

    return CorpusListModel;
}

void CorpusPreprocSelectBox::loadCorpora()
{
    CorpusValue->setModel(getCorpusListSqlModel());
    CorpusValue->setModelColumn(2);
}

/*
QVector<MyMap> CorpusPreprocSelectBox::getCorpusDocumentsList(int corpusId) {
     QVector<MyMap> maplist;
     QSqlQuery req(* DbConnection);

     req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = %1").arg(corpusId));

     while (req.next())
         maplist.append(MyMap(req.value(0).toString(), req.value(1).toInt()));

     return maplist;

}
*/

void CorpusPreprocSelectBox::setCorpus(int index)
{
  CorpusId = 0;
  CorpusPreprocId=0;
  if (index >= 0)
  {
    QAbstractItemModel * corpusComboBoxModel = CorpusValue->model();
    CorpusPreprocId=corpusComboBoxModel->data(corpusComboBoxModel->index(index, 0)).toInt();
    CorpusId = corpusComboBoxModel->data(corpusComboBoxModel->index(index, 1)).toInt();
  }
  qDebug() << "Corpus est changé pour : " << CorpusId << CorpusValue->itemText(index);
  emit sigPreprocCorpusModified(CorpusPreprocId,CorpusId);
}
