
#ifndef CORPUSSELECTBOX_H
#define CORPUSSELECTBOX_H

#include "../BDD/bdd.h"
#include "../tabdialog.h"

#include <QtWidgets>

// == COMPOSANTS DE LA BOITE
// Cette boite permet de sélectionner parmi un ensemble de corpus
class CorpusSelectBox : public Box
{
    Q_OBJECT

  public:
    
    int CorpusId;
    QSqlDatabase * DbConnection = BDD::DatabaseConnection;
    
    QComboBox * CorpusValue;
    QSqlQueryModel * CorpusListModel;
    
    QLabel * Languagelabel;
    QLabel * Scriptlabel;
    
    QString Language;
    QString Script;

    // methods
    
    CorpusSelectBox();

    QString getLanguage();
    QString getScript();

    QSqlQueryModel * getCorpusListSqlModel();
    QSqlQueryModel * getCorpusPreprocListSqlModel();
    
    void setCorpus(int);

  signals:
    void sigCorpusModified(int);
    void sigLanguageModified(QString);
    void sigScriptModified(QString);

  public slots:

  protected slots:

  protected:
    void loadCorpora();
    QString sqlGetLanguage(int);
    QString sqlGetScript(int);
};

#endif // CORPUSSELECTBOX_H
