
#ifndef CORPUSPREPROCSELECT_BOX_H
#define CORPUSPREPROCSELECT_BOX_H

#include "../BDD/bdd.h"
#include "../tabdialog.h"

#include <QtWidgets>

// == COMPOSANTS DE LA BOITE
// Cette boite permet de sélectionner parmi un ensemble de corpus
class CorpusPreprocSelectBox : public Box
{
    Q_OBJECT

  public:
    
    int CorpusId;
    int CorpusPreprocId;
    QSqlDatabase * DbConnection = BDD::DatabaseConnection;
    
    QComboBox * CorpusValue;
    QSqlQueryModel * CorpusListModel;

    // methods
    CorpusPreprocSelectBox();

    QSqlQueryModel * getCorpusListSqlModel();
    //QVector<MyMap> getCorpusDocumentsList(int);

    void setCorpus(int);

  signals:
    void sigPreprocCorpusModified(int,int) ;

  public slots:

  protected slots:

  protected:
    void loadCorpora();

};

#endif // CORPUSPREPROCSELECT_BOX_H
