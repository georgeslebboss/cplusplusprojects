
// This box takes a corpora to select from

#include "corpusselect_box.h"

/*
Pour intégrer les corpus produits à différentes étapes du traitement

étape 1 : choix parmi les corpus bruts
étape 2 : choix parmi les corpus préprocessés pour un corpus brut donné
étape 3 : ?

Une variable globale, int Stage, qui serait mise à jour lors de la construction de l'objet CorpusSelectBox

Mais le signal renvoyé n'est pas le même, il est double : identifiant du corpus brut + identifiant du préproc

*/

CorpusSelectBox::CorpusSelectBox() 
: Box("Select Corpus")
{
  DbConnection = BDD::DatabaseConnection; // if done in : DbConnection(BDD::DatabaseConnection, would it work or is it too early?

  QGridLayout * corpusselectboxlayout = new QGridLayout ;
  setAlignment(Qt::AlignTop) ;
  setLayout(corpusselectboxlayout);
  corpusselectboxlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));
  corpusselectboxlayout->setAlignment(Qt::AlignTop);

  corpusselectboxlayout->addWidget(new QLabel(tr("Corpus")), 0, 0);
  CorpusValue = new QComboBox;
  loadCorpora();

  CorpusValue->setCurrentIndex(-1);
  connect(CorpusValue, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int index) {setCorpus(index);});
  corpusselectboxlayout->addWidget(CorpusValue, 0, 1);

  //bouton refresh
  QPushButton * refreshcorpora = new QPushButton(tr("Refresh"));
  corpusselectboxlayout->addWidget(refreshcorpora, 0, 3);
  connect(refreshcorpora, &QPushButton::clicked, [=]() {loadCorpora();});

  corpusselectboxlayout->addWidget(new QLabel(tr("Language")), 1, 0);
  
  Languagelabel = new QLabel("");
  Languagelabel->setFrameStyle(QFrame::Box);
  Languagelabel->setStyleSheet("QLabel {color : gray}");
  corpusselectboxlayout->addWidget(Languagelabel, 1, 1);

  corpusselectboxlayout->addWidget(new QLabel(tr("Script")), 2, 0);

  Scriptlabel = new QLabel("");
  Scriptlabel->setFrameStyle(QFrame::Box);
  Scriptlabel->setStyleSheet("QLabel {color : gray}");
  corpusselectboxlayout->addWidget(Scriptlabel, 2, 1);
}

void CorpusSelectBox::loadCorpora()
{
    CorpusValue->setModel(getCorpusListSqlModel());
    CorpusValue->setModelColumn(1);
}

// noter que les signaux autres que sigCorpusModified ne servent pas pour l'instant
void CorpusSelectBox::setCorpus(int index)
{
  CorpusId = 0;
  if (index >= 0)
  {
    QAbstractItemModel * corpusComboBoxModel = CorpusValue->model();
    CorpusId = corpusComboBoxModel->data(corpusComboBoxModel->index(index, 0)).toInt();
  }
  qDebug() << "Corpus changé : " << CorpusId << CorpusValue->itemText(index);
  Language = sqlGetLanguage(CorpusId);
  if (Languagelabel->text() != Language)
  {
    Languagelabel->setText(Language);
    emit sigLanguageModified(Language);
  }
  Script = sqlGetScript(CorpusId);
  if (Scriptlabel->text() != Script)
  {
    Scriptlabel->setText(Script);
    emit sigScriptModified(Script);
  }
  emit sigCorpusModified(CorpusId);
}

QString CorpusSelectBox::getLanguage()
{ return Language; }

QString CorpusSelectBox::getScript()
{ return Script; }

// ========================= Interactions base de données ==================================================


QSqlQueryModel * CorpusSelectBox::getCorpusListSqlModel()
{
    qDebug() << "getting corpus list from: " << * DbConnection;

    QSqlQueryModel * CorpusListModel = new QSqlQueryModel();
    CorpusListModel->setQuery("SELECT id, nom_corpus FROM corpus", * DbConnection);

    CorpusListModel->setHeaderData(0, Qt::Horizontal, tr("id"));
    CorpusListModel->setHeaderData(1, Qt::Horizontal, tr("nom_corpus"));

    return CorpusListModel;
}

// Version pour l'étape 2
/*avec preproc select
pareil que corpus + refresh
preproc + refresh
juste en dessous
avec autant de choix dans la selectbox que de Word_preproc avec corpus_id = id du corpus choisi
+ 1 pour nopreproc
le nom de preproc = nom de segmenteur + nom de lemmatiseur + options
sinon exactement pareil que corpus
*/

QSqlQueryModel * CorpusSelectBox::getCorpusPreprocListSqlModel()
{
  qDebug() << "getting preproc corpus list from Word_Segment: " << * DbConnection;

  QSqlQueryModel * CorpusListModel = new QSqlQueryModel();
  
  /*QString sql =
    " SELECT \"WORD_preproc\".idpreproc as idp , corpus.id as idc, "
    " corpus.nom_corpus  || '_' || \"WORD_preproc\".paramspreproc || '_' || \"WORD_preproc\".segmenter || '_' || \"WORD_preproc\".lemmatizer as namepreproc "
    " FROM \"WORD_preproc\", corpus, \"WORD_segment\" "
    " WHERE \"WORD_preproc\".fk_id_corpus = corpus.id AND  \"WORD_segment\".fk_idpreproc = \"WORD_preproc\".idpreproc "
    " group by \"WORD_preproc\".idpreproc,corpus.id"
    " UNION "
    " SELECT 0 as idp , corpus.id as idc, "
    " corpus.nom_corpus  || '_0000000__' as namepreproc "
    " FROM \"WORD_preproc\", corpus, \"WORD_segment\" "
    " WHERE \"WORD_preproc\".fk_id_corpus = corpus.id AND  \"WORD_segment\".fk_idpreproc =0 "
    " group by \"WORD_preproc\".idpreproc,corpus.id";*/
  QString sql =
      " SELECT \"WORD_preproc\".idpreproc as idp , corpus.id as idc, "
      " corpus.nom_corpus  || '_' || \"WORD_preproc\".paramspreproc || '_' || \"WORD_preproc\".segmenter || '_' || \"WORD_preproc\".lemmatizer as namepreproc "
      " FROM \"WORD_preproc\", corpus, \"WORD_segment\" "
      " WHERE \"WORD_preproc\".fk_id_corpus = corpus.id AND  \"WORD_segment\".fk_idpreproc = \"WORD_preproc\".idpreproc "
      " group by \"WORD_preproc\".idpreproc,corpus.id";
    
  CorpusListModel->setQuery(sql, * DbConnection);

  CorpusListModel->setHeaderData(0, Qt::Horizontal, tr("idpreproc"));
  CorpusListModel->setHeaderData(1, Qt::Horizontal, tr("idcorpus"));
  CorpusListModel->setHeaderData(2, Qt::Horizontal, tr("preprocname"));

    return CorpusListModel;
}

QString CorpusSelectBox::sqlGetLanguage(int corpusid)
{
  qDebug() << "getting value from " << * DbConnection;
  QString lang = "";
  if (corpusid > 0)
  {
    qDebug() << "get the corpus <" << corpusid << "> language";
    
    QSqlQuery qry(* BDD::DatabaseConnection);

    QString qcommand = QString(" SELECT language_corpus FROM \"corpus\" WHERE id = %1").arg(corpusid) ;
    if (qry.exec(qcommand))
    {
      qry.next();
      lang = qry.value(0).toString();
    }
    else qDebug() << qry.lastError();
  }
  return lang;
}


QString CorpusSelectBox::sqlGetScript(int corpusid)
{
  qDebug() << "getting value from " << * DbConnection;
  QString script = "";
  if (corpusid > 0)
  {
    qDebug() << "get the corpus <" << corpusid << "> script";
    
    QSqlQuery qry(* BDD::DatabaseConnection);

    QString qcommand = QString(" SELECT script_corpus FROM \"corpus\" WHERE id = %1").arg(corpusid) ;
    
    if (qry.exec(qcommand))
    {
      qry.next();
      script = qry.value(0).toString();
    }
    else qDebug() << qry.lastError();
  }
  return script;
}


/*
QVector<MyMap> CorpusSelectBox::getCorpusDocumentsList(int corpusId) {
     QVector<MyMap> maplist;
     QSqlQuery req(* DbConnection);

     req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = %1").arg(corpusId));

     while (req.next())
         maplist.append(MyMap(req.value(0).toString(), req.value(1).toInt()));

     return maplist;

}
*/
