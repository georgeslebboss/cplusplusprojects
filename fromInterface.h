#ifndef FROMINTERFACE_H
#define FROMINTERFACE_H

#include "Interface/Interface/Common.h"
#include "Interface/Interface/BDD/bdd.h"
#include "Interface/Interface/BDD/bdd_win.h"
#include "Interface/Interface/tabdialog.h"
#include "Interface/Interface/CorpusBox/corpusselect_box.h"
#include "Interface/Interface/CorpusBox/corpuspreprocselect_box.h"

#endif // FROMINTERFACE_H

