#include "tabgroup.h"
#include "fromInterface.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv) ;
  qDebug() << "launching main";
  
  BDDConnectionDialog * settingsdiag = new BDDConnectionDialog() ;
  QObject::connect(settingsdiag, SIGNAL(rejected()), qApp, SLOT(quit()));
  settingsdiag->run();
  
  if (BDD::DatabaseConnection)
  {
    MainWindow * win =  new MainWindow(QObject::tr("GraPaVec")) ;  // créer la fenêtre
    TabGroup * tabgroup = new TabGroup ;                           // créer le groupe
    
    qDebug() << "Main(): Initialisation des onglets" ;
                                                            // création des tabs
    //WindowNgrams * corpuschar = new WindowNgrams();
    WindowSelect * corpusselect = new WindowSelect() ;
    WordTab * corpusword = new WordTab() ;
    PatternTab * corpuspattern = new PatternTab();
    GraPaVecTab * grapavec= new GraPaVecTab();
    //WindowSom * corpussom = new WindowSom();
    classifier * algosomcreux = new classifier() ;
                                                            // concaténation des tabs
    //initialiseList(0,"CorpusChar",reinterpret_cast<QWidget*>(corpuschar));
    tabgroup->addTab(0, "CorpusSelect", corpusselect);
    tabgroup->addTab(1, "CorpusWord", corpusword);
    tabgroup->addTab(2, "CorpusPattern", corpuspattern);
    tabgroup->addTab(3,"GraPaVec",grapavec);
    tabgroup->addTab(4, "AlgoSOM", algosomcreux);
    //tabgroup->addTab(4, "CorpusSOM", corpussom);

    tabgroup->createTabs(win) ;                             // production de la fenêtre
    win->show();
  }
  else
  {
    qDebug() << "Database not connected";
    return -1;
  }
  return a.exec();
}
