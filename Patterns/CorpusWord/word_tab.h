#ifndef WORD_TAB_H
#define WORD_TAB_H

#include <QtWidgets>
//#include "synsets.h"
#include "../fromInterface.h"
#include "PreprocessingBox/preprocessing_box.h"
#include "word_box.h"

using namespace std;


//================================ COMPOSANTS DE LA FENETRE

class WordTab : public Tab
{
  Q_OBJECT

  public:
  
    WordTab() ;
    ~WordTab() ;
  //protected slots:

    
} ;

class WordParams : public Shared
{
  Q_OBJECT
  
  public:
    WordParams();
    ~WordParams();

    void setCorpusId(int);
    void setLanguage(QString);
    void setScript(QString);
    void setNormParams(normparams *);
    void setNormParamValues(normparamvalues *);
    void setVarNormParamValues(normparamvalues);
    void setSegmenter(QString) ;
    void setLemmatizer(QString) ;
    void setNormalizer(QString);

    int getCorpusId();
    QString getLanguage();
    QString getScript();
    normparams * getNormParams();
    normparamvalues * getNormParamValues();
    normparamvalues getVarNormParamValues();
    QVector<bool> getNormalizationParams();
    QString getNormalizationParamsString();
    QString getFullParamsString();

    QString getSegmenter();
    QString getLemmatizer();
    QString getNormalizer();

  protected:
    normparamvalues * NormParamValues;
    normparamvalues VarNormParamValues;
    normparams * NormParams;

    int CorpusId;
    QString Language;
    QString Script;
    QVector<bool> NormalizationParams;

    QString Segmenter;
    QString Lemmatizer;
    QString Normalizer;

} ;

class WordAppli : public Appli
{
  Q_OBJECT

  public:
    WordAppli() ;
  
    QSqlDatabase          * DbConnection=BDD::DatabaseConnection;
    QString paramsmessage;

    QElapsedTimer         Time ;
    static inline double  minutes(qint64 time) {return (double) time / (double) (qint64) 60000 ; }
} ;

#endif // WORD_TAB_H
