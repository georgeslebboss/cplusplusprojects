#ifndef SEGMENTS_H
#define SEGMENTS_H

#include "../fromInterface.h"
#include "wordonly.h"
#include "PreprocessingBox/arabicnormalizer.h"

// l'élément de l'arbre préfixe
// chaque élément est un caractère
// attention au BMP d'unicode pour d'autres langues
typedef qint32 val ;

// l'élément de l'arbre préfixe de segment
// chaque élément est un identifiant de mot
struct node_segment_only
{
    id              wordid;                 //identifiant de mot appartenant au segment
    val             occurrences;            // nombre d'occurences des segments
    bool            end;                    // fin de segment
                                            // inutile, on a déjà occurrences
    //id              identifiant;            // identifiant de chaque segment dans le corpus

    node_segment_only *     sons;
    node_segment_only *     bros;
} ;

typedef node_segment_only * arbresegmentonly ;

class Segments : public QObject
{
    Q_OBJECT

  public:

    Segments(){};
    Segments(arbrewordonly, int, QVector<MyMap>, QVector<id> *, QStringList *, QString);

    ~Segments();
    
    int ThreadNumber;
    QVector<MyMap> DocumentList;
    arbrewordonly Trie;
    QVector<int> * Form_Preprocform;
    QStringList * Preprocform;
    QString Path;
    
    static bool NOPREPROC;     //no preproc params
    static int IDCORPUS; //corpus
    static int IDPREPROC; //idpreproc if preprocparams
    static const int MaxSegment;

    void sqlWriteBatchSegment(int, arbresegmentonly, int idpreproc);

    QString createJsonTrie(arbresegmentonly , bool=false );
    QString createJsonNode(arbresegmentonly , bool );
    arbresegmentonly createNode(id, QVector<id>,bool = false, arbresegmentonly = NULL, arbresegmentonly = NULL) ;

    arbrewordonly createTries(bool = true, bool = true, bool = true);
    arbrewordonly createSuffixWordsTrie(arbrewordonly);

    void afficherArbre(arbrewordonly);
    void afficherArbre(arbresegmentonly);
    void generateWordsList(arbrewordonly, QString);
    void generateWord(arbrewordonly, QString);

    void printJsonSegments(arbresegmentonly, QTextStream &);

    QString reverseWord(QString);
    QString prepare(QString);

    // Nourredine
    int findWordInTable2() ;

    void insertinCsV(QVector<id>, int, int, QTextStream &);

private:

    //int NbSegments;
    bool Debug = false;
    //int TrieId;

    //bool saveJsonSegmentsTrie = true;

    QSqlDatabase * DBConnection = BDD::DatabaseConnection ;
    //QSqlDatabase  ThreadDBConnection;

    QTime Timer ;
    QList<QString> WordList ; //
    QList<int> WordOccList ;

    void printTokens(QString);
    void addTokens(QString, QTextStream &) ;
    

    id findWordInTrie(QString, arbrewordonly, int = 0);
    id findWordInTable(QString);




    /********************************************************/
    
    void insertTrie( QVector<id>, arbresegmentonly, int = 0) ;
    arbrewordonly assoc(QChar, arbrewordonly) ;
    arbresegmentonly assoc(int, arbresegmentonly) ;
    arbresegmentonly createBranch( QVector<id>, arbresegmentonly, int) ;

    QStringList cutText(QString) ;
    QStringList cutSegment(QString);

    //void segmentFile(int,int);//stanford word segmenter pour une liste des fichiers entre deb et fin pour le multithread

    //fcts segments
    void sqlWriteSegs(arbresegmentonly, QVector<id>  , int ,int, QTextStream &);
    void sqlWriteSeg(int , int , QVector<id> , int , QTextStream &);
    void sqlWriteSegsTest(arbresegmentonly, QVector<id> , int, int, QString);

    void printJsonNode(arbresegmentonly, QTextStream &);


signals:
    void sigsegfinished(node_segment_only *);
    void sigsegfinished();

public slots:
    void process();
};

#endif // SEGMENTS_H


