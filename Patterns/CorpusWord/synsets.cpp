#include "synsets.h"

using namespace std;

// Constructeur de synsets
Synsets::Synsets(QString lang,QVector<QString> lang_param, QSqlDatabase *conn)
{
   syn_lang=lang;
   syn_lang_param=lang_param;
   synBDD=conn;


   getAwnWordsInSynsets();
   setAwnwordslist();
   setAwnWordsSynset();
   setGraPaVecWordsInSynsets();
   assignSynsetsToClusters();
   afficherResultats();
}
//awn_synsetwords<synset,word_id> awn synset with list of word_id related to this synset
void Synsets::getAwnWordsInSynsets()
{
  QSqlQuery synreq(*synBDD);
  QString ligne="";
  QStringList templist {};
  //relier le synset aux listes de mots choisis
  qDebug() << "Début remplir la liste des awn sysnets";
  synreq.exec("SELECT synsetid, wordid FROM \"awn_word_work\" ORDER BY synsetid ASC;");
  while(synreq.next())
  {
      if (ligne!= synreq.value(0).toString())
      {
          if(ligne!="")
          {
              awn_synsetwords.insert(ligne,templist);
              templist.clear();
          }
          ligne=synreq.value(0).toString();
      }
      templist.append(synreq.value(1).toString());
  }
  awn_synsetwords.insert(ligne,templist);
  qDebug() << "fin remplir la liste des synsets: " << awn_synsetwords.size() << " synsets";
}
//mettre l'awn word dans une liste contenant (word_id,liste de formes)
void Synsets::setAwnwordslist()
{
    QSqlQuery synreq(*synBDD);
    QString wordsid="",motnorm;
    QStringList templist {};
    //ajouter tous les word_id et leurs valeurs dans la liste awn_words
    //synreq.exec("SELECT \"AWN_original_word\".word_id,\"AWN_original_word\".word_value,\"AWN_original_form\".word_value FROM \"AWN_original_word\" LEFT JOIN \"AWN_original_form\" ON \"AWN_original_word\".word_id=\"AWN_original_form\".word_id Order By \"AWN_original_word\".word_id ASC");
    synreq.exec("SELECT \"awn_word_work\".wordid,\"awn_word_work\".value_,\"awn_form\".value_ FROM \"awn_word_work\" LEFT JOIN \"awn_form\" ON \"awn_word_work\".wordid=\"awn_form\".wordid Order By \"awn_word_work\".wordid ASC");
    //synreq.exec("SELECT \"awn_word\".wordid,\"awn_word\".value_,\"awn_form\".value_ FROM \"awn_word\" LEFT JOIN \"awn_form\" ON \"awn_word\".wordid=\"awn_form\".wordid Order By \"awn_word\".wordid ASC");
    qDebug() << "Début remplir la liste des tous les mots reliés à un wordid";
    while (synreq.next())
    {
        if (wordsid!= synreq.value(0).toString())
        {
            if(wordsid!="")
                awn_words.insert(wordsid,templist);
            wordsid=synreq.value(0).toString();
            templist.clear();
        }
        motnorm=ArabicNormalizer::normalize(synreq.value(1).toString(),syn_lang_param[0],syn_lang_param[1],syn_lang_param[2],syn_lang_param[3],syn_lang_param[4],syn_lang_param[5],syn_lang_param[6]);
        if (!motnorm.isEmpty())
            templist.append(motnorm);
        if (!synreq.value(2).isNull())
        {
            motnorm=ArabicNormalizer::normalize(synreq.value(2).toString(),syn_lang_param[0],syn_lang_param[1],syn_lang_param[2],syn_lang_param[3],syn_lang_param[4],syn_lang_param[5],syn_lang_param[6]);
            //ajouter tous les valeurs de la meme famille comme root et broken plural de mot choisi dans la liste
            if (!templist.contains(motnorm))
                templist.append(motnorm);
        }
    }
    //ajouter la derniere ligne
    if(!templist.isEmpty())
        awn_words.insert(wordsid,templist);
    qDebug() << "fin remplir la liste des mots: " << awn_words.size() << " mots";
}
//mettre dans une liste (word_id,synset_id)
void Synsets::setAwnWordsSynset()
{
    QSqlQuery synreq(*synBDD);
    //relier le word_id au synset_id
    qDebug() << "Début remplir la liste des awn words sysnets";
    //synreq.exec("SELECT word_id, synset_id FROM \"AWN_original_word\" Order By word_id ASC ;");
    synreq.exec("SELECT wordid, synsetid FROM \"awn_word_work\" Order By wordid ASC ;");
    //synreq.exec("SELECT wordid, synsetid FROM \"awn_word\"  Order By wordid ASC ;");
    while(synreq.next())
        awn_words_synset.insert(synreq.value(0).toString(),synreq.value(1).toString());
    qDebug() << "fin remplir la liste des words synsets: " << awn_words_synset.size() ;
}
//mettre GraPaVec grapa_synset,liste des grapa_formes dans une liste
void Synsets::setGraPaVecWordsInSynsets()
{
    QSqlQuery synreq(*synBDD);
    QString ligne="";
    QStringList templist;
    qDebug() << "Début remplir la liste des GraPaVec Synsets";
    synreq.exec(" SELECT synset, form FROM \"WORD_synsets_1403\" ORDER BY synset ASC;");
    while(synreq.next())
    {
        if (ligne!= synreq.value(0).toString())
        {
            if(ligne!="")
            {
                GraPaVec_Synsetwords.insert(ligne,templist);
                templist.clear();
            }
            ligne=synreq.value(0).toString();
        }
        templist.append(synreq.value(1).toString());

    }
    if(!templist.isEmpty())
        GraPaVec_Synsetwords.insert(ligne,templist);
     qDebug() << "fin remplir la liste des GraPaVec synsets: " << GraPaVec_Synsetwords.size() << " synsets";
}
//mapper grapa_word to word_id correspondant
QStringList Synsets::assignFormToWordid(QString formword)
{
    //qDebug() << "Debut Assign GraPaVec form to word_id";
    QStringList wordidlist;
    QMap<QString,QStringList>::iterator it;
    for(it=awn_words.begin();it!=awn_words.end();++it)
    {
       if (it.value().contains(formword))
           wordidlist.append(it.key());
    }
    //qDebug() << formword << " est: " << wordidlist << " : " << wordidlist.size();
    return wordidlist;
}
//attribuer GraPaVec synset aux AWN synset
QStringList Synsets::AssignGraPaVecSynsetsToAwnSynsets(QString grapavecclass)
{
    qDebug() << "Debut Assign Synset to GraPaVecSynset " << grapavecclass;
    QStringList widlisttmp;
    QStringList synlist;
    QString motnorm;
    //int OOV=0;

    foreach (QString w, GraPaVec_Synsetwords[grapavecclass])
    {
        motnorm=ArabicNormalizer::PreprocStr(w,syn_lang,syn_lang_param);
        //motnorm=QString::fromStdWString(KhojaStemmer::stemWord(w.first.toStdWString()));
        widlisttmp=assignFormToWordid(motnorm);
        if (!widlisttmp.isEmpty())
            foreach(QString wid, widlisttmp)
                if (!synlist.contains(wid))
                    synlist.append(wid);
    }
    //synlist.append("OOV");
    //qDebug() << "la liste des mots de ce cluster" << grapavecclass << " est: " << synlist;
    //qDebug() << "la liste des mots OOV" << grapavecclass << " est: " << OOV << "/" << GraPaVec_Synsetwords[grapavecclass].size();
    return synlist;
}
QMap<QString,QStringList> Synsets::assignSynsetsToClusters()
{
    foreach (QString c, GraPaVec_Synsetwords.keys())
        synsetsclusters.insert(c,AssignGraPaVecSynsetsToAwnSynsets(c));
     return synsetsclusters;
}
void Synsets::afficherResultats()
{
    qDebug() << "Afficher dans le fichier";
    qreal nbIntersect;
    QString syn="";
    QFile outFile("..clusterstosyn.txt");
    if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
    {
         qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
         return;
     }
     QTextStream outsql(&outFile);
     outsql.setCodec("UTF-8");
     QMap<QString,QStringList>::iterator it;
     for(it=synsetsclusters.begin();it!=synsetsclusters.end();++it)
         foreach (QString v, it.value()) {
             syn=awn_words_synset[v];
             if (syn!="")
             {
                 nbIntersect=qreal(awn_synsetwords[syn].toSet().intersect(it.value().toSet()).size())/awn_synsetwords[syn].size();
                 outsql<< QString("%1;%2;%3\n").arg(it.key()).arg(syn).arg(nbIntersect);
             }
         }
    //outsql << output;
     outFile.close();
     qDebug() << "**************FIN****************";
}
