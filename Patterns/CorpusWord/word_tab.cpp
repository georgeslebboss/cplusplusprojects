#include "word_tab.h"

//========================================================= TAB ==========================================================

WordTab::WordTab()
{
  WordAppli * windowword = new WordAppli ;
  WordParams * wordparams = new WordParams ;
  initTab(windowword, wordparams) ;
  
  answerMessage("CorpusWordParams://") ;
}

WordTab::~WordTab()
{
}

//========================================================= SHARED ==========================================================

WordParams::WordParams() {}
WordParams::~WordParams() {}

void WordParams::setCorpusId(int id)
{ CorpusId = id; }

void WordParams::setLanguage(QString language)
{ Language = language; }

void WordParams::setScript(QString script)
{ Script = script; }

void WordParams::setNormParams(normparams * p_normalizationParams)
{ NormParams = p_normalizationParams; }

void WordParams::setNormParamValues(normparamvalues * params)
{ NormParamValues = params; }

void WordParams::setVarNormParamValues(normparamvalues params)
{ VarNormParamValues = params; }

void WordParams::setSegmenter(QString segmenter)
{ Segmenter = segmenter ; }

void WordParams::setLemmatizer(QString lemmatizer)
{ Lemmatizer = lemmatizer ; }

void WordParams::setNormalizer(QString normalizer)
{ Normalizer = normalizer ; }

int WordParams::getCorpusId()
{ return CorpusId; }

QString WordParams::getLanguage()
{ return Language; }

QString WordParams::getScript()
{ return Script; }


normparams * WordParams::getNormParams()
{ return NormParams; }

normparamvalues * WordParams::getNormParamValues()
{ return NormParamValues; }

normparamvalues WordParams::getVarNormParamValues()
{ return VarNormParamValues; }

QVector<bool> WordParams::getNormalizationParams()
{
    QVector<bool> varnormparams;

    if (VarNormParamValues.value("diacritics"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("numbers"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("foreign letters"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("shadda and madda"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("alef"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("yeh"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    if (VarNormParamValues.value("heh"))
        varnormparams.append(true);
    else
        varnormparams.append(false);

    NormalizationParams = varnormparams;
    return varnormparams;

}

QString WordParams::getNormalizationParamsString()
{
    QMapIterator<QString, bool> iter(VarNormParamValues);

    QString s = "";
    while(iter.hasNext()) {
        s = s + iter.key();
        if (iter.value()) {
            s = s + "1-";
        } else {
            s = s + "0-";
        }
        iter.next();
        qDebug() << iter.key() << " :: " << iter.value();
    }

    return s;
}

QString WordParams::getFullParamsString()
{
    QString s = "";
    QVector<bool> varnormparams=getNormalizationParams();
    if (varnormparams.size() > 0)
    {
        for (int i=0; i<varnormparams.size(); i++)
        {
            if (varnormparams[i]) s = s + "1";
            else s = s + "0";
        }
    }
    qDebug() << "params:" << s;
    /*add segmenter and lemmatizer params
    if (Segmenter!="") s=s+","+Segmenter;
    if (Lemmatizer!="") s=s+","+Lemmatizer;*/
    return s;
}

QString WordParams::getSegmenter()
{ return Segmenter; }

QString WordParams::getLemmatizer()
{ return Lemmatizer; }

QString WordParams::getNormalizer()
{ return Normalizer; }


//========================================================= APPLI ==========================================================

WordAppli::WordAppli()
//: MaxMarkers(800)
{ 
  //================================ FENETRAGE ====================================================
  QGridLayout * mainlayout = new QGridLayout;
  mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));

  //================================ PARTIE CORPUS ================================================

  /*CorpusBox * corpusbox = new CorpusBox() ;
  mainlayout->addWidget(corpusbox, 0, 0) ;
  corpusbox->App = this ;*/
  
  // if we need to share IdCorpus
  //SharedParams->setCorpusId(corpusbox->IdCorpus) ;
  PreprocessingBox * preprocbox = new PreprocessingBox(true, true);
  CorpusSelectBox * corpusbox = new CorpusSelectBox() ;
  corpusbox->setMinimumWidth(300);
  corpusbox->setAlignment(Qt::AlignTop);
  mainlayout->addWidget(corpusbox, 0, 0);
  corpusbox->App = this;
  connect
  (
    corpusbox,
    & CorpusSelectBox::sigCorpusModified,
    [=](int corpusid)
    {
      static_cast<WordParams *>(SharedParams)->setCorpusId(corpusid);
      static_cast<WordParams *>(SharedParams)->setLanguage(corpusbox->getLanguage());
      static_cast<WordParams *>(SharedParams)->setScript(corpusbox->getScript());
      preprocbox->setCorpusId(corpusid);
      preprocbox->setLanguage(corpusbox->getLanguage());
      //static_cast<WordParams *>(SharedParams)->setLemmatizer("KHOJA Lemmatizer");
      //static_cast<WordParams *>(SharedParams)->setNormalizer("IBM Normalizer");
      //static_cast<CwParams *>(SharedParams)->setLanguage(lang);
      static_cast<WordTab *>(this->parent())->answerMessage("CorpusWordParams:sigLemmatizerModified//lang:"+ static_cast<WordParams *>(SharedParams)->getLanguage() + "/Params:" +static_cast<WordParams *>(SharedParams)->getNormalizationParamsString()+ "/Segmenter:"+ static_cast<WordParams *>(SharedParams)->getSegmenter() + "/Stemmer:"+ static_cast<WordParams *>(SharedParams)->getLemmatizer() + "/Normalizer:"+ static_cast<WordParams *>(SharedParams)->getNormalizer());
    }
  );

  //================================ Pre-Processing ===============================================
  preprocbox->setMinimumWidth(300);
  preprocbox->setAlignment(Qt::AlignTop);
  mainlayout->addWidget(preprocbox, 0, 1) ;
  preprocbox->App = this ;
  
  connect
  (
    preprocbox,
   & PreprocessingBox::sigVarNormParamModified,
   [=](normparamvalues params)
    {
      static_cast<WordParams *>(SharedParams)->setVarNormParamValues(params);
      static_cast<WordTab *>(this->parent())->answerMessage("CorpusWordParams:sigLemmatizerModified//lang:"+ static_cast<WordParams *>(SharedParams)->getLanguage() + "/Params:" +static_cast<WordParams *>(SharedParams)->getNormalizationParamsString()+ "/Segmenter:"+ static_cast<WordParams *>(SharedParams)->getSegmenter() + "/Stemmer:"+ static_cast<WordParams *>(SharedParams)->getLemmatizer() + "/Normalizer:"+ static_cast<WordParams *>(SharedParams)->getNormalizer());
    }
  ) ;
  connect
  (
    preprocbox, 
    & PreprocessingBox::sigSegmenterModified,
    [=](QString seg) 
    {
      static_cast<WordParams *>(SharedParams)->setSegmenter(seg) ;
      static_cast<WordTab *>(this->parent())->answerMessage("CorpusWordParams:sigLemmatizerModified//lang:"+ static_cast<WordParams *>(SharedParams)->getLanguage() + "/Params:" +static_cast<WordParams *>(SharedParams)->getNormalizationParamsString()+ "/Segmenter:"+ static_cast<WordParams *>(SharedParams)->getSegmenter() + "/Stemmer:"+ static_cast<WordParams *>(SharedParams)->getLemmatizer() + "/Normalizer:"+ static_cast<WordParams *>(SharedParams)->getNormalizer());
    }
  ) ;

  connect
  (
    preprocbox, 
    & PreprocessingBox::sigLemmatizerModified,
    [=](QString lem) 
    {
      static_cast<WordParams *>(SharedParams)->setLemmatizer(lem) ;
      static_cast<WordTab *>(this->parent())->answerMessage("CorpusWordParams:sigLemmatizerModified//lang:"+ static_cast<WordParams *>(SharedParams)->getLanguage() + "/Params:" +static_cast<WordParams *>(SharedParams)->getNormalizationParamsString()+ "/Segmenter:"+ static_cast<WordParams *>(SharedParams)->getSegmenter() + "/Stemmer:"+ static_cast<WordParams *>(SharedParams)->getLemmatizer() + "/Normalizer:"+ static_cast<WordParams *>(SharedParams)->getNormalizer());
    }
  ) ;

  connect
  (
    preprocbox,
    & PreprocessingBox::sigNormalizerModified,
    [=](QString norm)
    {
      static_cast<WordParams *>(SharedParams)->setNormalizer(norm) ;
      static_cast<WordTab *>(this->parent())->answerMessage("CorpusWordParams:sigLemmatizerModified//lang:"+ static_cast<WordParams *>(SharedParams)->getLanguage() + "/Params:" +static_cast<WordParams *>(SharedParams)->getNormalizationParamsString()+ "/Segmenter:"+ static_cast<WordParams *>(SharedParams)->getSegmenter() + "/Stemmer:"+ static_cast<WordParams *>(SharedParams)->getLemmatizer() + "/Normalizer:"+ static_cast<WordParams *>(SharedParams)->getNormalizer());
    }
  ) ;

// pour accéder à la propriété depuis une autre boite : App->static_cast<WordParams *>ShareParams->getLanguage()
// ou alors définir les getters dans WordAppli

  //================================ PARTIE Word Box: Analyse des Mots ==========================================
  WordBox * wordbox = new WordBox(false);
  mainlayout->addWidget(wordbox, 0, 2);
  wordbox->App = this;
  // au moment où on insère les mots on leur transfère les paramètres choisis dans les autres box
  connect
  (
    wordbox,
    & WordBox::sigInsertWordSegmentClicked,
    [=](int caller)
    {
      wordbox->insertWordSegmentClicked
      (
        static_cast<WordParams *>(SharedParams)->getCorpusId(),
        static_cast<WordParams *>(SharedParams)->getLanguage(),
        static_cast<WordParams *>(SharedParams)->getScript(),
        static_cast<WordParams *>(SharedParams)->getNormalizationParams(),
        static_cast<WordParams *>(SharedParams)->getSegmenter(),
        static_cast<WordParams *>(SharedParams)->getLemmatizer(),
        static_cast<WordParams *>(SharedParams)->getNormalizer(),
        static_cast<WordParams *>(SharedParams)->getFullParamsString(),
        caller
      );
    }
  );

  setLayout(mainlayout);
  setWindowTitle("Corpus Analysis");
}
