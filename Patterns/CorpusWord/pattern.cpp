#include "pattern.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
//#include "../Interface/Interface/Common.h"
#include <locale>

using namespace std;

// =============================================== CONSTRUCTEURS =======================================
Patterns::Patterns(int corpusid, int preproccorpusid, int seuilmarque, int jokerlength, int seuilpattern, id jokerfirstid, id jokerid, QMap<id, val> * nopattern, int threadnumber, QVector<QVector<id> > seg_preprocform, bool isbigcorpus, id id0, id id1, QString path)
: CorpusID(corpusid), PreprocCorpusID(preproccorpusid), SeuilMarque(seuilmarque), JokerLength(jokerlength),  SeuilPattern(seuilpattern), JokerMinId(jokerfirstid), JokerId(jokerid), IsBigCorpus(isbigcorpus), Id0(id0), Id1(id1),Path(path), Nopattern(nopattern),NumThread(threadnumber),Segment_PreprocForms(seg_preprocform)
{
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: CorpusId set to " << CorpusID;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: PreprocCorpusId set to " << PreprocCorpusID;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: Non-marker first id set to " << JokerMinId;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: Marker Threshold set to " << SeuilMarque;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: JokerLength set to " << JokerLength;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: PatternThreshold set to " << SeuilPattern;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: Id0 set to " << Id0;
    if (Debug > 1) qDebug() << NumThread << ":" <<  "Patterns:: Id1 set to " << Id1;

    // arguments propres au thread
    if (Debug) qDebug() << NumThread << ":" <<  "Patterns:: Numthread set to " << NumThread;
}

//================================================= process ==================================
// appele par fonction generatePattern
// Segment_PreprocForms est un QVector<QVector<id> >
void Patterns::process()
{
  if (Debug) qDebug() << NumThread << ":" <<  "Patterns::process patterns nb of segments" << Segment_PreprocForms.size();
  
  Trie = createNode();
  
  if (Debug) qDebug() << NumThread << ":" <<  "Patterns::process root created";
  
  QList<QVector<id> > prepatterns;
  
  int number = Segment_PreprocForms.size();
  
  for (int i = 0; i < number; i++)
  {
    if (Debug) qDebug() << NumThread << ":" <<  "Patterns::process cutPrepatterns " << Segment_PreprocForms.at(0);
    prepatterns += cutPrepatterns(Segment_PreprocForms.takeFirst());
  }
  
  number = prepatterns.size();
  
  for (int i = 0; i < number; i++)
  {
    if (Debug) qDebug() << NumThread << ":" <<  "Patterns::process insert patterns " << prepatterns.at(0);
    insertPatterns(prepatterns.takeFirst());
  }
  if (IsBigCorpus)
  {
      if (Debug) qDebug() << NumThread << ":" <<  "------------BIG CORPUS debut insert jokers: " << NumThread << ":" << Path;
      //QString filepath = generateFilePath(Corpus, PreprocCorpus, MarkerThreshold->value(), JokerLength->value(), PatternThreshold->value(), "vectorsbig");
      //QString filepathpattern = "/tmp/" + QString::number(CorpusID) + "_" + getCorpusName(CorpusID).replace(" ", "_")+ "/patternstodb/" + NumThread + ".csv";
      QString filepathpattern = Path + "/patternstodb/" +QString::number(NumThread) + ".csv";
      if (Debug) qDebug() << NumThread << ":" <<  "generateFile : " << filepathpattern;
      QFile outfilepattern(filepathpattern);
      QTextStream streampattern(& outfilepattern);
      //outfilepattern.open(QIODevice::WriteOnly|QIODevice::Truncate) ;
      if (not outfilepattern.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
      {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfilepattern.fileName() << "en ecriture.";
        return;
      }
      //QString filepathvectors = "/tmp/" + QString::number(CorpusID) + "_" + getCorpusName(CorpusID).replace(" ", "_")+ "/vectorstodb/" + NumThread + ".csv";;
      QString filepathvectors = Path+ "/vectorstodb/" + QString::number(NumThread) + ".csv";;
      if (Debug) qDebug() << NumThread << ":" <<  "generateFile : " << filepathvectors;
      QFile outfilevectors(filepathvectors);
      QTextStream streamvectors(& outfilevectors);
      //outfilevectors.open(QIODevice::WriteOnly|QIODevice::Truncate) ;
      if (not outfilevectors.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
      {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfilevectors.fileName() << "en ecriture.";
        return;
      }

      insertJokers(Trie->sons,{},streampattern,streamvectors);
      if (Debug) qDebug() << NumThread << ":" <<  "fin insert jokers/ debut create vectors big: " << NumThread;
      streampattern.flush();
      streamvectors.flush();
      outfilepattern.close();
      outfilevectors.close();

      //createVectorsBig();
      if (Debug) qDebug() << NumThread << ":" <<  "fin create vectors big: " << NumThread;
      deleteTrie(Trie);
  }
  
  if (Debug) qDebug() << "Patterns::process patterns::emitfinishedPattern " << NumThread;
  emit finishedPattern(Trie);
  if (Debug) qDebug() << NumThread << ":" <<  "Patterns::process finishedPattern(Trie) emitted" << NumThread;
  emit finishedPattern();
}

/* 1ère phase : découpage des segments en prépatterns

Il faut couper les segments et produire des séquences de prépatterns (morceaux de segment correspondant à un pattern maximum). Les x situés à plus de JokerLength d'une marque sont stockés dans le pattern * ; il s'agit d'indiquer combien de fois x apparaît sans une marque dans son voisinage.

Si le segment ne contient que des m, fini

Si le segment commence par x, on recherche le m suivant
1) pas de m, on termine
2) distance <= JokerLength ; grouper JokerLength + m
3) sinon ; envoyer tout jusqu'à JokerLength + m au pattern * ; grouper JokerLength + m
puis appeler la fonction beginWithMarker.

Si la marque se trouve en position 6 du segment et que JokerLength = 3
on doit grouper 3, 4, 5 et 6 (JokerLength + 1 éléments) et assignJoker pour 0, 1, 2
6 = 7 éléments - 4 (pos + 1 - JokerLength - 1) = 3 à partir de 0

on ne devrait pas avoir de pattern vide. Par contre il peut y avoir des patterns avec un seul élément.

*/

QList<QVector<id> > Patterns::cutPrepatterns(QVector<id> segment)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::segment " << segment ;
  
  if (noJoker(segment)) {if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::NOJOKER" ; return QList<QVector<id> >();}
  
  QVector<id> prepattern = QVector<id>();
  QList<QVector<id> > prepatterns = QList<QVector<id> >();
  
  if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns hasJoker; JokerMinId " << JokerMinId << " JokerLength " << JokerLength;
  
  if (isJoker(segment.at(0)))                   // le segment ne commence pas par une marque
  {
    if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker " << segment.at(0) ;
    
    int position_next_marker = getNextMarker(segment, 1);
    if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker position_next_marker " << position_next_marker;

    if (position_next_marker < 0)               // il n'y a pas de prochaine marque, on a fini
    {
      if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker position_next_marker < 0 ; endPrepatterns";
      return endPrepatterns(prepattern, prepatterns, segment, 0);
    }
    
    if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker: position_next_marker >= 0";
    
    if (position_next_marker > JokerLength)     // la prochaine marque est trop loin
    {
      if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker: position_next_marker > JokerLength";
      
      assignJoker(segment.mid(0, position_next_marker - JokerLength - 1));
      prepattern = segment.mid(position_next_marker - JokerLength, JokerLength + 1);
      
      if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker calling beginWithMarker prepattern " << prepattern << " prepatterns " << prepatterns << " position " << position_next_marker;
      
      return beginWithMarker(prepattern, prepatterns, segment, position_next_marker + 1);
    }
    // la prochaine marque n'est pas trop loin
    if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::isJoker: position_next_marker <= JokerLength";
    prepattern = segment.mid(0, position_next_marker + 1);
    return beginWithMarker(prepattern, prepatterns, segment, position_next_marker + 1);
  }
  
  prepattern = segment.mid(0, 1);
  if (Debug > 1) qDebug() << NumThread << ":" <<  "cutPrepatterns::calling beginWithMarker prepattern " << prepattern << " prepatterns " << prepatterns;
  return beginWithMarker(prepattern, prepatterns, segment, 1);
}

// the segment has only markers ; they are not included (no information for vectors)
bool Patterns::noJoker(QVector<id> segment)
{ return ((* std::max_element(segment.constBegin(), segment.constEnd())) < JokerMinId); }

/* 
On part d'une marque et on recherche la suivante ;

1) il n'y a plus rien après la marque : on termine.

2) il n'y a pas de m suivant ; grouper m + JokerLength et envoyer le reste au pattern * ; fini, retourner le prépattern dans la liste ;

3) distance <= JokerLength ; grouper m + la suite jusqu'à la marque et continuer
  rechercher le m suivant (m au début)

4) distance <= 2 * JokerLength ; grouper m + JokerLength, fini pour ce pattern ; grouper JokerLength + m et continuer ; on renverra plusieurs prépatterns
  rechercher le m suivant

5) sinon ; grouper m + JokerLength, fini pour ce pattern ; affecter tout jusqu'à JokerLength + m au pattern * ; grouper JokerLength + m et continuer ; on renverra plusieurs prépatterns
  rechercher le m suivant
  
 (on ne tient pas compte de la possibilité d'une appartenance au pattern * pour tous ceux qui sont proches)
 (sinon on explose la proportion de pattern *)

*/

QList<QVector<id> > Patterns::beginWithMarker(QVector<id> prepattern, QList<QVector<id> > prepatterns, QVector<id> segment, int position)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:prepattern " << prepattern << " prepatterns " << prepatterns << " segment " << segment << " position " << position;
  if (position >= segment.size()) return prepatterns << prepattern;
  
  int position_next_marker = getNextMarker(segment, position);
  if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:position_next_marker " << position_next_marker;
  
  if (position_next_marker < 0)
  {
    prepattern += segment.mid(position, JokerLength);
    if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:case 1 position_next_marker < 0" << prepattern;
    assignJoker(segment.mid(position + JokerLength));
    return endPrepatterns(prepattern, prepatterns, segment, position + JokerLength - 1);
  }
  
  if (position_next_marker - position <= JokerLength)
  {
    prepattern += segment.mid(position, position_next_marker - position + 1);
    
    if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:case 2 position_next_marker - position <= JokerLength" << position_next_marker - position;

    if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:case 2:calling beginWithMarker prepattern " << prepattern << " prepatterns " << prepatterns << " segment " << segment << " position " << position_next_marker + 1;

    return beginWithMarker(prepattern, prepatterns, segment, position_next_marker + 1);
  }
  
  if (position_next_marker - position <= 2 * JokerLength)
  {
    prepattern += segment.mid(position, JokerLength);
    if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:case 3 position_next_marker - position <= 2 * JokerLength " << prepattern;
    prepatterns << prepattern;
    prepattern.clear();
    prepattern = segment.mid(position_next_marker - JokerLength, JokerLength + 1);

    if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:case 3:beginWithMarker prepattern = " << prepattern << " prepatterns = " << prepatterns << " segment " << segment << " position " << position_next_marker + 1;

    return beginWithMarker(prepattern, prepatterns, segment, position_next_marker + 1);
  }
  
  prepattern += segment.mid(position, JokerLength);
  
  if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:else " << prepattern;
  
  prepatterns << prepattern;
  assignJoker(segment.mid(position + JokerLength + 1, position_next_marker - JokerLength - 1));
  prepattern.clear();
  prepattern = segment.mid(position_next_marker - JokerLength, JokerLength + 1);

  if (Debug > 1) qDebug() << NumThread << ":" <<  "beginWithMarker:else:calling beginWithMarker prepattern " << prepattern << " prepatterns " << prepatterns << " segment " << segment << " position " << position_next_marker + 1;
  
  return beginWithMarker(prepattern, prepatterns, segment, position_next_marker + 1);
}

int Patterns::getNextMarker(QVector<id> segment, int position)
{
  while (position < segment.size())
  {
    if (not isJoker(segment.at(position))) return position;
    position++;
  }
  return -1;
}

// ending a segment with a lot of jokers at the end
// appelé par cutPrepatterns ou beginWithMarker
QList<QVector<id> > Patterns::endPrepatterns(QVector<id> prepattern, QList<QVector<id> > prepatterns, QVector<id> segment, int position)
{
  QMutex mutex;
  mutex.lock();
  assignJoker(segment.mid(position));
  mutex.unlock();
  if (Debug > 1) qDebug() << NumThread << ":" <<  "endPrepatterns<<<<<<<<<<<<" << prepattern;
  return prepatterns << prepattern;
}

// put all x of a (portion of) segment in pattern *
// on va simplement mettre à jour la QMap Nopattern qui réside dans le thread principal (dans PatternBox)
// inutile de compter ce pattern
void Patterns::assignJoker(QVector<id> segment)
{
  for (int i = 0; i < segment.size(); i++)
    (* Nopattern)[i]++;
}

/* 2ème phase : insertion

Une fois les pré-patterns constitués (il n'y a plus de pattern * à la racine), on va les éclater

m x x x m x x m m x# : à tous les x on affecte la sortie de ce pattern
 m x x x m x x m m#
 m x x x m x x m#
 m x x x m x x#
 m x x x m#
 m x x x#

x x x m x x m m x
 x x x m x x m m
 x x x m x x m
 x x x m x x
 x x x m

m x x m m x
 m x x m m
 m x x m
 m x x

x x m m x
 x x m m
 x x m

m m x
 m x

m x

*/ 

void Patterns::insertPatterns(QVector<id> prepattern)
{
  if (Debug) qDebug() << NumThread << ":" <<  "insertPatterns " << prepattern;

  if (not goodPattern(prepattern)) return;
  insertEndingPatterns(prepattern);
  insertPatterns(getNextPattern(prepattern));
}

// si un pattern n'est pas bon, les suivants ne sont pas bons
void Patterns::insertEndingPatterns(QVector<id> prepattern)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "insertEndingPatterns " << prepattern;
  
  if (not goodPattern(prepattern)) return;
  
  insertP(prepattern, Trie);
  insertEndingPatterns(getNextEndingPattern(prepattern));
}

// a good pattern should contain at least one m and one x
bool Patterns::goodPattern(QVector<id> segment)
{ 
  // for qDebug
  QString test = (not segment.isEmpty() and ((* std::max_element(segment.constBegin(), segment.constEnd())) >= JokerMinId) and ((* std::min_element(segment.constBegin(), segment.constEnd())) < JokerMinId)) ? "true" : "false";
  
  if (Debug > 1) qDebug() << NumThread << ":" <<  "goodPattern " << segment << " answer " << test;

  return
    not segment.isEmpty()
    and
    ((* std::max_element(segment.constBegin(), segment.constEnd())) >= JokerMinId)
    and
    ((* std::min_element(segment.constBegin(), segment.constEnd())) < JokerMinId);
}

// pattern suivant en enlevant le début
// on enlève soit m soit une suite de x
QVector<id> Patterns::getNextPattern(QVector<id> prepattern)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "getNextPattern " << prepattern;
  
  if (not isJoker(prepattern.at(0))) return prepattern.mid(1);
  for (int i = 0; i < prepattern.size(); i++)
    if (not isJoker(prepattern.at(i))) return prepattern.mid(i);
  prepattern.clear();
  return prepattern;
}

// pattern suivant en enlevant la fin
// on enlève soit m soit une suite de x
// si le dernier élément est une marque, on l'enlève
// sinon, on recherche à partir de la fin la première marque, et on ne garde que jusque-là
// ex. si le 5 ème élément est la dernière marque, on garde les 6 premiers éléments
QVector<id> Patterns::getNextEndingPattern(QVector<id> prepattern)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "getNextEndingPattern " << prepattern;
  if (not isJoker(prepattern.last())) return prepattern.mid(0, prepattern.size() - 1);
  for (int i = prepattern.size() - 1; i > 0 ; i--)
    if (not isJoker(prepattern.at(i))) return prepattern.mid(0, i + 1);
  prepattern.clear();
  return prepattern;
}

// insert into patterns
// les occurrences des patterns sont-elles utiles ?
// si le mot est un joker on l'ajoute au vecteur avec les jokers suivants
// on ne met les patterns dans l'arbre que si tout est fini
void Patterns::insertP(QVector<id> pattern, arbre_pattern tree, int position, QVector<id> jokers)
{
  if (position >= pattern.size()) return;

  arbre_pattern branch;
  if (isJoker(pattern.at(position))) branch = assoc(JokerId, tree->sons);
  else branch = assoc(pattern.at(position), tree->sons);
  
  if (branch)
  {
    //branch->occurrences++;        // utilité ?

    if (position == pattern.size() - 1)
    {
      branch->sortie++;
      if (branch->m_id == JokerId) jokers.append(pattern.at(position));
      addJokers(jokers, branch);
      return;
    }
    
    if (branch->m_id == JokerId) 
    {
      if(Debug > 1) qDebug() << "branch->m_id == JokerId";
      int size = getJokerSize(pattern, position);
      jokers.append(pattern.mid(position, size));
      if (position+size== pattern.size())
      {
          branch->sortie++;
          return;
      }
      insertP(pattern, branch, position + size, jokers);
    }
    else insertP(pattern, branch, position + 1, jokers);
    return;

  }

  branch = createBranch(pattern, tree, position, jokers);
  branch->bros = tree->sons;
  tree->sons   = branch;

  return ;
}


// si le noeud joker a déjà été créé, il ne faut pas en créer d'autres

arbre_pattern Patterns::createBranch(QVector<id> pattern, arbre_pattern tree, int position, QVector<id> jokers)
{
  if (Debug > 1) qDebug() << NumThread << ":" <<  "createBranch:" << pattern;
  if (position >= pattern.size()) return NULL ;
  if (position == pattern.size() - 1)
  {
    tree = createNode(pattern.at(position), true);
    if (isJoker(pattern.at(position))) jokers.append(pattern.at(position));
    addJokers(jokers, tree);
    return tree;
  }
  if (isJoker(pattern.at(position)))
  { 
    int size = getJokerSize(pattern, position);
    jokers.append(pattern.mid(position, size));
    if (position + size >= pattern.size())
    { 
      tree = createNode(pattern.at(position), true);
      addJokers(jokers, tree);
      return tree;
    }
    arbre_pattern branch = createNode(pattern.at(position), false, createBranch(pattern, tree, position + size, jokers));
    return branch;
  }
  return createNode(pattern.at(position), false, createBranch(pattern, tree, position + 1, jokers)) ;
}

// positions 2-3-4-(5) : x x x (m)
// position 2, last = 3, 4, 5 ; finit avec 5
// size = 3 (5-2)
int Patterns::getJokerSize(QVector<id> pattern, int position)
{
  int last = position + 1;
  while (last < pattern.size() and isJoker(pattern.at(last))) last++;
  return last - position;
}

// l'identifiant 0 c'est celui de la racine (pour le mot et pour le pattern)
arbre_pattern Patterns::createNode(id identifiant, bool end, arbre_pattern son, arbre_pattern bro)
{
  arbre_pattern p = new node_pattern;
  
  if (isJoker(identifiant)) p->m_id = JokerId;
  else p->m_id = identifiant;

  p->sons = son;
  p->bros = bro;
  //p->occurrences = 1;
  if (end) p->sortie = 1;
  if (Debug > 1) qDebug() << NumThread << ":" <<  "createNode: " << p->m_id << ":" << p->sortie;
  return p;
}

arbre_pattern Patterns::assoc(id m, arbre_pattern tree)
{
    if (not tree) return NULL;
    if (tree->m_id == m) return tree;
    return assoc(m, tree->bros);
}

// ajoute un segment de jokers en modifiant les occurrences
// il faut créer le noeud avant
// le premier élément du segment est dans la qMap => on incrémente les occurrences
// il n'est pas dans la qmap, on l'insère avec 1
// et on itère
// même pour le premier segment ajouté on fait comme ça, car il peut y avoir un mot répété dans un segment
// le livre du livre, une femme femme, un grand grand grand type
// le jokernode est le noeud de sortie du pattern (le préfixe valide)

void Patterns::addJokers(QVector<id> identifiants, arbre_pattern jokernode)
{
  for (int i = 0; i < identifiants.size(); i++) addJoker(identifiants.at(i), jokernode);
}

void Patterns::addJoker(id identifiant, arbre_pattern jokernode)
{
  if (not jokernode->jokers.contains(identifiant))
    jokernode->jokers.insert(identifiant, 1);
  else jokernode->jokers[identifiant]++;
}

bool Patterns::isJoker(id elt) { return elt >= JokerMinId || elt==Id0 || elt==Id1; }

void Patterns::insertJokers(arbre_pattern tree, QVector<id> identifiants,QTextStream &outpatterns, QTextStream &outvectors)
{
    if (not tree) return ;
    identifiants.append(tree->m_id);
    QString patternstr="";
    if (tree->sortie)
    {
        QMap<id, val>::iterator it = tree->jokers.begin();
        patternstr=convertPatterntoSet(identifiants);
        //tree->p_id=PatternId++;
        //outpatterns << tree->p_id << ";" << convertPatterntoSet(identifiants) << ";" << tree->occurrences << endl;
        outpatterns << patternstr << ";" << tree->sortie << endl;
        while (it != tree->jokers.end())
        {
          //outvectors << tree->p_id << ";" << it.key() << ";" << it.value() << endl;
          outvectors << patternstr << ";" << it.key() << ";" << it.value() << endl;
          ++it;
        }
        tree->jokers.clear();
    }
    insertJokers(tree->sons,identifiants,outpatterns,outvectors) ;
    identifiants.removeLast();
    insertJokers(tree->bros,identifiants,outpatterns,outvectors) ;
}
// libère la mémoire d'un trie
void Patterns::deleteTrie(arbre_pattern trie)
{
  if (not trie) return ;
  deleteTrie(trie->sons) ;
  deleteTrie(trie->bros) ;
  delete trie ;
}
QString Patterns::convertPatterntoSet(QVector<int> identifiants)
{
  QString pattern = "{";
  for(int i = 0 ; i < identifiants.size() - 1; i++)
    pattern += QString::number(identifiants[i]) + ",";
  pattern += QString::number(identifiants.last()) + "}";
  return pattern;
}
QString Patterns::getCorpusName(int corpusid)
{
    QSqlQuery req(* pdbConnection);

    req.exec(QString("select nom_corpus from corpus where id = %1").arg(corpusid));
    req.next();

    return req.value(0).toString();
}
