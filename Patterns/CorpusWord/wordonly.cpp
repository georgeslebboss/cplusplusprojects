#include "wordonly.h"
#include "thread"

QString WordOnly::Temp = TEMP;
QString WordOnly::W2V = WORD2VEC;
const int WordOnly::MaxWord = 30;
const int WordOnly::MaxArabic = 50;

// =============================================== CONSTRUCTEURS =======================================
// La construction de l'objet WordsOnly consiste à établir des paramètres


WordOnly::WordOnly(){}

//word multithread
WordOnly::WordOnly(QString script, int threadnumber, QVector<MyMap> doclist)
: Script(script), ThreadNumber(threadnumber), DocumentList(doclist) // ils vivent dans le thread principal
{
  if (Debug)  qDebug() << "WordOnly : threadnumber " << threadnumber << " number of docs " <<  doclist.size();
}

WordOnly::~WordOnly(){}


// ============ Méthodes utilisées pour accéder aux paramètres

//int WordOnly::getCorpusId() {return WordBox::Corpus;}


QString WordOnly::getSegmenter() {return Segmenter;}

QString WordOnly::getLemmatizer() {return Lemmatizer;}

int WordOnly::getTrieId() {return TrieId;}

// ================================================ Process ComputeWordTrie ==================================

// ===CONSTRUCTION DES TRIE ====

void WordOnly::process()
{
  arbrewordonly trie = createNode((QChar) '/', false) ;
  trie->prefix_occurrences = 0; // pour l'ajustement du calcul
  for (int i = 0 ; i < DocumentList.size() ; i++)
  {
    QString file = QUrl(DocumentList.at(i).first).toLocalFile() ;
    //if (Debug) qDebug() << "WordOnly::process : file to process " << file << " ThreadNumber: " << ThreadNumber ;
    addTokens(trie, file) ;
    //if (Debug) qDebug() << "WordOnly::process : file processed " << file << " ThreadNumber: " << ThreadNumber ;
  }
  //qDebug()<< "WordOnly::process : un trie construit";
  emit finished(trie);
  emit finished();
}

//======================================== LECTURE DES FICHIERS ===============================
// Méthode utilisée uniquement par process()
void WordOnly::addTokens(arbrewordonly Trie, QString file)
{
   //qDebug() << "WordOnly::addTokens : file " << file ;
  
  QFile inFile(file) ;
  if (not inFile.open(QIODevice::ReadOnly))
  {
    qDebug() << "WordOnly::addTokens : Could not open " + file + " for reading" ;
    return ;
  }
  QTextStream input(&inFile);
  input.setCodec("UTF-8");

  QStringList words = cutText(input.readAll());
  QString word;

  while (not words.isEmpty())
  {
    if ((word = prepare(words.first())) == "") words.removeAt(0);
    else
    {
      insertTrie(word, Trie);
      words.removeAt(0);
    }
  }
  return ;
}

//=================================== Découpage en mots

// uniquement appelée par addTokens
// Ceci est un hook parce que pour d'autres langues que l'arabe, la procédure sera plus complexe
// cas du tiret (dash) = en cas de coupure de mot, ou de mot composé
// cas du point (M., S.N.C.F., livre.txt)
// cas de l'apostrophe (c'est) en français / en anglais
// pour ces cas, vaut-il mieux les traiter en première analyse comme séparateurs (c'est ce qui est fait ici)
// ou prévoir un traitement plus subtil, en ne les traitant comme séparateurs que s'ils ne sont pas collés à droite pour le point
// c'est-à-dire, laisser S.N.C.F, livre.txt, comme mots, d'un côté, et M. => M, livre. => livre
// et pour l'apostrophe, séparateur toujours mais aujourd'hui
// à mettre sans doute dans Preprocessing

// p{L} is any letter (including some non letters according to ?)
// p{M} is a character modifier, as diacritics
// p{N} is any digits

// 

QStringList WordOnly::cutText(QString text)
{
  text.replace(QRegularExpression("[^\\p{L}\\p{M}\\p{N}]"), " ");
  return text.split(" ", QString::SkipEmptyParts);
}

// if (word.contains(QRegularExpression(QString("[^\\p{%1}]").arg(Corpus->Script)))) return "";

QString WordOnly::prepare(QString word)
{
  //if (word.contains(QRegularExpression(QString("[^\\p{%1}]").arg(Script))))      return "";
  if (word.length() <= MaxWord) return word;
  if (word.length() > MaxArabic) return ""; 
  return word;
}


//=============================== ComputeWordTrie: MOTS : sauvegarde dans la base de données ==========================

// Sauvegarde des mots eux-mêmes
// Il y a un problème avec wordtodb.csv
// comme toutes les instances de Grapavec vont utiliser le même nom de fichier, un bug est obligé
// il faudrait que chaque instance différencie son wordtodb.csv (nom de corpus, date, owner
// sinon on risque d'écraser le wordtodb.csv d'une autre appli en train de tourner
// pour cela on détruit le fichier à la fin de la fonction
void WordOnly::sqlWriteBatchWord(int corpusid, arbrewordonly trie, bool nopreproc)
{
  QSqlQuery req(* DBConnection) ;
  QString path ;
  Strsql = "" ;
  QFile outfile(QString("%1/wordtodb.csv").arg(WordOnly::Temp));
  path = QFileInfo(outfile).absoluteFilePath();
  if (not outfile.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
  {
    qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfile.fileName() << "en ecriture." << endl ;
    return;
  }
  QTextStream outsql(&outfile);
  outsql.setCodec("UTF-8");
  sqlWriteWords(trie->sons,"", corpusid) ;
  outsql << Strsql;
  outfile.close();
  QString sql;
  /*if (nopreproc)
  {
      //no preprocessing inserer les mots par ordre decroissant des occurences
      sql="CREATE TABLE if not exists \"WORD_tmp_form\" (form text, occs integer,  fk_corpus integer,  id bigint)";
      if (!req.exec(sql)) qDebug() << "sqlWriteBatchWord : Error= " << req.lastError().text() << " string : " << sql ;
      sql =QString("COPY \"WORD_tmp_form\" (id, form, occs, fk_corpus) from '%1' with delimiter ';' ENCODING 'UTF-8'").arg(path) ;
      if (!req.exec(sql)) qDebug() << "sqlWriteBatchWord : Error= " << req.lastError().text() << " string : " << sql ;
      req.exec("ALTER SEQUENCE \"WORD_form_id_seq\" RESTART WITH 1");
      sql = QString(" INSERT INTO \"WORD_form\"(form, occs, fk_corpus)"
                    " SELECT form, occs, fk_corpus "
                    " FROM \"WORD_tmp_form\" order by occs desc;");
      req.exec(sql);
      req.exec("DROP TABLE \"WORD_tmp_form\";");
  }
  else*/
  sql =QString("COPY \"WORD_form\" (id, form, occs, fk_corpus) from '%1' with delimiter ';' ENCODING 'UTF-8'").arg(path) ;
  //Timer.start();
  if (!req.exec(sql)) qDebug() << "sqlWriteBatchWord : Error= " << req.lastError().text() << " string : " << sql ;
  //qDebug()<< "Elapsed Time in Millisecond: " << Timer.elapsed();
  Strsql.clear();
  outfile.close();
  outfile.remove(); // vérifier si pas de problème
  if (nopreproc)
  {
      //ajouter la ligne de nopreproc dans word_preproc
      sql=QString("INSERT INTO \"WORD_preproc\"(paramspreproc, segmenter, lemmatizer, fk_id_corpus) VALUES ('0000000','','',%1)").arg(corpusid);
      if (!req.exec(sql))
      {
        qDebug() << "sqlWriteBatchWord : Database error = " << req.lastError().text() << " query : " << sql ;
        return;
      }
      int idpreproc = req.lastInsertId().toInt();
      if (Debug) qDebug() << "sqlWriteBatchWord : ligne de WORD_preproc créée et remplie avec id = " << idpreproc;
      //ajouter meme form au WORD_preprocform
      req.exec("ALTER SEQUENCE \"WORD_preprocform_idformpreproc_seq\" RESTART WITH 1");
      sql = QString(" INSERT INTO \"WORD_preprocform\"(preprocform, occs, fk_idpreproc)"
                    " SELECT form, occs, %1 "
                    " FROM \"WORD_form\" where fk_corpus=%2 order by occs desc;").arg(idpreproc).arg(corpusid);
      if (!req.exec(sql))
      {
        qDebug() << "sqlWriteBatchWord : Database error = " << req.lastError().text() << " query : " << sql ;
        return;
      }
      //remplir la relation
      sql=QString(" INSERT INTO \"WORD_form_preprocform\"(idform, idpreprocform, fk_idpreproc)"
                  " SELECT w.id, wp.idformpreproc, %1 "
                  " FROM \"WORD_form\" w, \"WORD_preprocform\" wp "
                  " WHERE w.form = wp.preprocform and wp.fk_idpreproc=%1 and w.fk_corpus=%2 ").arg(idpreproc).arg(corpusid);
      if (!req.exec(sql))
      {
        qDebug() << "sqlWriteBatchWord : Database error = " << req.lastError().text() << " query : " << sql ;
        return;
      }
  }
}

void WordOnly::sqlWriteWords(arbrewordonly tree, QString word, int corpus)
{
    if (not tree) return ;
    word.append(tree->letter) ;
    if (tree->occurrences)
        sqlWriteWord(word, tree->occurrences,tree->identifiant, corpus) ;
    sqlWriteWords(tree->sons, word, corpus) ;
    sqlWriteWords(tree->bros, word.left(word.size() - 1), corpus) ;
}

void WordOnly::sqlWriteWord(QString word, val occs, int idword, int corpus)
{
    Strsql += QString("%1;\%2;%3;%4\n").arg(idword).arg(word).arg(occs).arg(corpus) ;
//  if (Debug) qDebug() << "sqlWriteWord :" << Strsql;
}

//=============================== ComputePreprocFormsTrie: MOTS : sauvegarde dans la base de données 

// Sauvegarde des mots eux-mêmes
void WordOnly::sqlWriteBatchPreprocForm(int corpusid, arbrewordonly trie)
{
  QSqlQuery req (* DBConnection) ;
  QString path ;
  StrsqlPreproc = "" ;
  QFile outfile(QString("%1/preprocwordtodb.csv").arg(WordOnly::Temp));
  path = QFileInfo(outfile).absoluteFilePath();
  if (not outfile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
  {
    qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfile.fileName() << "en ecriture." << endl ;
    return;
  }
  QTextStream outsql(&outfile);
  outsql.setCodec("UTF-8");
  sqlWritePreprocForms(trie->sons,"", corpusid) ;
  outsql << StrsqlPreproc;
  outfile.close();
  QString sql
    =
     QString("COPY \"WORD_preprocform\" (preprocform, occs, prefixoccurences) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path) ;
  //Timer.start();
  if (!req.exec(sql)) qDebug() << "sqlWritePreprocForms : Error= " << req.lastError().text() << " string : " << sql ;
  //qDebug()<< "Elapsed Time in Millisecond: " << Timer.elapsed();
  StrsqlPreproc.clear();
  outfile.deleteLater();
}

void WordOnly::sqlWritePreprocForms(arbrewordonly tree, QString word, int corpus)
{
    if (not tree) return ;
    word.append(tree->letter) ;
    if (tree->occurrences)
        sqlWriteWord(word, tree->occurrences,tree->prefix_occurrences, corpus) ;
    sqlWriteWords(tree->sons, word, corpus) ;
    sqlWriteWords(tree->bros, word.left(word.size() - 1), corpus) ;
}

void WordOnly::sqlWritePreprocForm(QString word, val occs,  int prefixoccs)
{
    StrsqlPreproc += QString("%1;\%2;%3\n").arg(word).arg(occs).arg(prefixoccs) ;
  if (Debug) qDebug() << "sqlWritePreprocForm :" << StrsqlPreproc;
}


// ========================================== MOTS : fabrication de l'arbre ============================

void WordOnly::insertTrie(QString word, arbrewordonly tree, int position)
{
    if (position >= word.size()) return ;

    arbrewordonly branch = assoc(word.at(position), tree->sons);
    
    if (branch)
    {
        branch->prefix_occurrences++ ;
        if (position == word.size() - 1)
        {
            branch->occurrences++ ;
            return ;
        }

        insertTrie(word, branch, position + 1) ;
        return ;
    }
    branch = createBranch(word, tree, position) ;
    branch->bros    = tree->sons;
    tree->sons      = branch;
    return ;
}

arbrewordonly WordOnly::assoc(QChar c, arbrewordonly tree)
{
    if (not tree) return NULL ;
    if (tree->letter == c) return tree ;
    return assoc(c, tree->bros) ;
}

arbrewordonly WordOnly::createBranch(QString string, arbrewordonly tree, int position)
{
    if (position >= string.size()) return NULL ;
    if (position == string.size() - 1)
        return createNode(string.at(position), true, createBranch(string, tree, position + 1)) ;
    return createNode(string.at(position), false, createBranch(string, tree, position + 1)) ;
}

// Méthode utilisée par createTries (racine) et par createBranch (autres)

arbrewordonly WordOnly::createNode(QChar c, bool end, arbrewordonly son, arbrewordonly bro)
{
    arbrewordonly p = new node_word_only ;

    p->letter            = c ;
    p->occurrences       = 0 ;
    p->prefix_occurrences = 1 ;

    if (end) p->occurrences  = 1 ;
    
    p->sons = son ;
    p->bros = bro ;
    return p ;
}

// uniquement appelé par WordBox::computeSegtrie
// créer le wordtrie à partir du json
// copier d'abord le fichier en mémoire
arbrewordonly WordOnly::jsonToSegmentationTrie(QString jsonfilepath)
{ 
  * JsonCurrentIndex=0;
  QFile jsonfile(jsonfilepath);
  if (not jsonfile.open(QIODevice::ReadOnly|QIODevice::Text))
  {
    qDebug() << "WordOnly::jsonToSegmentationTrie : ERREUR: Impossible d'ouvrir le fichier" << jsonfile.fileName() << "en lecture";
    return NULL;
  }
  QTextStream in(&jsonfile);
  in.setCodec("UTF-8");
  if (Debug) qDebug() << "WordOnly::jsonToSegmentationTrie : reading jsonfile" << jsonfile.fileName();
  Json = in.readAll();
  if (Debug) qDebug() << "WordOnly::jsonToSegmentationTrie : jsonfile read" << jsonfile.fileName() << "json length: " << Json.length();
  if (Json.isEmpty())
  {
    if (Debug) qDebug() << "WordOnly::jsonToSegmentationTrie : jsonfile is Empty";
    return NULL;
  }
  return jsonToSegmentationTrieAux();
}

// fonction qui transforme le json en noeuds
arbrewordonly WordOnly::jsonToSegmentationTrieAux()
{
  QChar * read;
  if (not ((read = readJsonChar()) and (* read == '{')))
  {
    if (Debug) qDebug() << "Fin Anormale WordOnly::jsonToSegmentationTrieAux : Invalid beginning of json nest" << * JsonCurrentIndex;
    return NULL;                                                            // sortie anormale
  }
  if (not (read = readJsonChar()))
  {
    if (Debug) qDebug() << "Fin Anormale WordOnly::jsonToSegmentationTrieAux : No first node of json nest" << * JsonCurrentIndex;
    return NULL;                                                            // sortie anormale
  }
  if (* read == '}')    return NULL;                      // sortie normale
  unreadJsonChar();
  arbrewordonly node = createNodefromJson();
  node->bros = jsonToSegmentationTrieAux();
  node->sons = jsonToSegmentationTrieAux();
   if (not ((read = readJsonChar()) and (* read == '}')))
  {
    if (Debug) qDebug() << "Fin Anormale WordOnly::jsonToSegmentationTrieAux : No closing bracket in json node";
    return NULL;
  }
  return node;
}

arbrewordonly WordOnly::createNodefromJson()
{
  arbrewordonly node = new(node_word_only);
  QChar * read;
  node->letter = * readJsonChar();
  node->occurrences = readNumber();
  if (not ((read = readJsonChar()) and (* read == 'p')))
  { 
    if (Debug) qDebug() << "Fin Anormale WordOnly::createNodefromJson : No prefix_occurrences in json";
    return NULL;
  }
  node->prefix_occurrences = readNumber();
  if (not ((read = readJsonChar()) and (* read == 'i')))
  { 
    if (Debug) qDebug() << "Fin Anormale WordOnly::createNodefromJson : No identifiant in json";
    return NULL;
  }
  node->identifiant = readNumber();
  return node;
}

// lire un nombre
val WordOnly::readNumber()
{
  QString number = "";
  QChar * read;
  while ((read = readJsonChar()) and read->isDigit()) number.append(* read);
  unreadJsonChar();
  return number.toInt();
}


// lire un caractère dans la source et le supprimer de la source
QChar * WordOnly::readJsonChar()
{
  if ((* JsonCurrentIndex) >= Json.size())
  {
    JsonCurrentIndex = NULL;
    return NULL;
  }  
  QChar * next = new QChar;
  * next = Json[* JsonCurrentIndex];
  (* JsonCurrentIndex)++;
  return next;
}

// remettre le caractère dans la source
void WordOnly::unreadJsonChar()
{
  (* JsonCurrentIndex)--;
  return;
}

/*

Le json a la forme:

{<lettre><nombre>p<nombre>i<nombre>{...}{...}

*/ 

//========================Generate Word Json File======================================

void WordOnly::printJsonWords(arbrewordonly node, QTextStream & out)
{
  if (not node) return;
  out << "{";
  printJsonNode(node, out);
  if (node->bros) printJsonWords(node->bros, out);
  else out << "{}";
  if (node->sons) printJsonWords(node->sons, out);
  else out << "{}";
  out << "}";
}

void WordOnly::printJsonNode(arbrewordonly node, QTextStream & out)
{
    out << node->letter;
    out << node->occurrences;
    out << "p" << node->prefix_occurrences;
    out << "i" << node->identifiant;
}
