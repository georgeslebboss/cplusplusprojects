#ifndef WORDONLY_H
#define WORDONLY_H

#include "../fromInterface.h"

#include "PreprocessingBox/arabicnormalizer.h"

#include <iostream> // for debugging file process

#include "machine.h"

// l'élément de l'arbre préfixe
// chaque élément est un caractère
// attention au BMP d'unicode pour d'autres langues
typedef qint32 val ;

struct node_word_only
{
    int             identifiant = 0;
    QChar           letter;
    val             occurrences;            // nombre d'occurences des mots
    val             prefix_occurrences;     // nombre d'occurences des préfixes

    node_word_only *     sons;
    node_word_only *     bros;
} ;

typedef node_word_only * arbrewordonly ;

class WordOnly : public QObject
{
    Q_OBJECT

  public:

    WordOnly();
    WordOnly(QString, int, QVector<MyMap>);

    ~WordOnly();
    
    QString Script;
    int ThreadNumber;
    QVector<MyMap> DocumentList;
    
    static QString Temp;
    static QString W2V;
    static const int MaxWord;
    static const int MaxArabic;
    
    void setSegmenter(QString);
    void setLemmatizer(QString);

    QString getSegmenter();
    QString getLemmatizer();

    //QString createJsonTrie(arbrewordonly, bool=false);

    void printJsonWords(arbrewordonly , QTextStream &);

    //arbrewordonly createTries(bool = true, bool = true, bool = true, int = 1);

    /*arbrewordonly createSuffixWordsTrie(arbrewordonly);
    QString createJsonTrie(arbrewordonly, bool = false) ; // bool pour débugger
    QString createJsonNode(arbrewordonly, bool = false) ; // idem
    QString reverseWord(QString);
    void afficherArbre(arbrewordonly);
    void generateWordsList(arbrewordonly, QString);
    void generateWord(arbrewordonly, QString);*/



    arbrewordonly jsonToSegmentationTrie(QString);

    int getTrieId();
    int getCorpusId();


    static arbrewordonly createNode(QChar,bool = false, arbrewordonly = NULL, arbrewordonly = NULL) ;
    void sqlWriteBatch(int, arbrewordonly, int, QString);
    void sqlWriteBatchWord(int, arbrewordonly, bool);

    void sqlWriteBatchPreprocForm(int corpusid, arbrewordonly trie);

private:
    bool Debug = false ;
    int TrieId;
    QVector<bool> NormOptions ; // vecteur de normalisation orthographique
    QString Segmenter ;
    QString Lemmatizer ;
    QString Document;
    
    QString Json;               // chaîne json extraite du fichier ; utilisée par read_char
    int * JsonCurrentIndex = new int;

    bool saveJsonSegmentsTrie = false;

    QSqlDatabase * DBConnection = BDD::DatabaseConnection ;

    //QTime Timer ;
    QString Strsql ;
    QString StrsqlPreproc;

    void printJsonNode(arbrewordonly, QTextStream &);

    void sqlWritePreprocForms(arbrewordonly tree, QString word, int corpus);
    void sqlWritePreprocForm(QString word, val occs,  int prefixoccs);

    //QList<QString> WordList ; //
    //QList<int> WordOccList ;


    void addTokens(arbrewordonly,QString ) ;
    void insertTrie(QString, arbrewordonly, int = 0) ;
    arbrewordonly assoc(QChar, arbrewordonly) ;
    arbrewordonly createBranch(QString, arbrewordonly, int) ;    

    void sqlWriteWords(arbrewordonly, QString, int) ;
    void sqlWriteWord(QString, val,int, int);

    QStringList cutText(QString) ;
    QString prepare(QString);

    QString createJsonNode(arbrewordonly , bool);
    
    arbrewordonly jsonToSegmentationTrieAux();
    arbrewordonly createNodefromJson();
    val readNumber();
    QChar * readJsonChar();
    void unreadJsonChar();
    
    //void segmentFile(int,int);//stanford word segmenter pour une liste des fichiers entre deb et fin pour le multithread
signals:
    void finished(node_word_only *);
    void finished();
    void finishedpreproc(node_word_only *);
    void finishedpreproc();
public slots:
    void process() ;
};

#endif // WORDONLY_H



