#ifndef WORDS_H
#define WORDS_H

#include "../fromInterface.h"

#include "PreprocessingBox/arabicnormalizer.h"

// l'élément de l'arbre préfixe
// chaque élément est un caractère
// attention au BMP d'unicode pour d'autres langues
typedef qint32 val ;
struct node_word
{
    QChar           letter;
    val             occurrences;            // nombre d'occurences des mots
    val             prefix_occurrences;      // nombre d'occurences des préfixes

    bool            composite;              // le mot contient une ponctuation
                                            // pas sure si nous avons besoin de ca

    id              identifiant;            // identifiant de chaque mot dans le corpus

    node_word *     sons;
    node_word *     bros;
} ;

typedef node_word * arbreword ;

// l'élément de l'arbre préfixe de segment
// chaque élément est un identifiant de mot
struct node_segment
{
    id              wordid;                 //identifiant de mot appartenant au segment
    val             occurrences;            // nombre d'occurences des segments
    bool            end;                    // fin de segment
                                            // inutile, on a déjà occurrences
    id              identifiant;            // identifiant de chaque segment dans le corpus

    node_segment *     sons;
    node_segment *     bros;
} ;

typedef node_segment * arbresegment ;

class Words : public QObject
{
    Q_OBJECT

  public:

    Words();
    Words(int, QString, QVector<bool>);
    Words(int, QString, QVector<bool>, QString, QString);

    ~Words();

    void setCorpusId(int);
    void setDocList(QVector<MyMap>);
    void setLanguage(QString);
    void setLanguageParam(QVector<bool>);

    void setSegmenter(QString);
    void setLemmatizer(QString);

    int getCorpusId();
    QVector<MyMap> getDocList();
    QString getLanguage();
    QVector<bool> getLanguageParam();

    QString getSegmenter();
    QString getLemmatizer();

    arbreword createTries(bool = true, bool = true, bool = true);
    arbreword createSuffixWordsTrie(arbreword);

    QString createJsonTrie(arbreword, bool = false) ; // bool pour débugger
    QString createJsonTrie(arbresegment, bool = false) ; // idem
    QString createJsonNode(arbreword, bool = false) ; // idem
    QString createJsonNode(arbresegment, bool = false) ; // idem

    void afficherArbre(arbreword);
    void afficherArbre(arbresegment);

    void generateWordsList(arbreword, QString);
    void generateWord(arbreword, QString);

    void generateFile(int, QString, bool, QString, QString ) ;
    QString generateFilename(int, QString, bool) ;

    QString reverseWord(QString);

    int getTrieId();

private:
    static id Identifiant;
    int NbSegments;
    int InsertedSeg;
    bool Debug = false ;
    id InsertedWordId ;
    int CorpusId ;
    int TrieId;
    QString Options;
    QString Language ;
    QVector<MyMap> Documents ;
    QVector<bool> NormOptions ; // vecteur de normalisation orthographique
    QString Segmenter ;
    QString Lemmatizer ;

    bool saveJsonSegmentsTrie = false;

    QSqlDatabase * DbConnection = BDD::DatabaseConnection ;

    QTime Timer ;
    QString Strsql ;

    QList<QString> WordList ; //
    QList<int> WordOccList ;

    void addTokens(arbreword, arbresegment, QString, QTextStream &) ;

    void insertTrie(QString, arbreword, int = 0) ;
    void insertTrie(QTextStream &,id , QVector<id>, arbresegment, int = 0) ;
    arbreword assoc(QChar, arbreword) ;
    arbresegment assoc(id, arbresegment) ;
    arbreword createBranch(QString, arbreword, int) ;
    arbresegment createBranch(id, QVector<id>, arbresegment, int, QTextStream &) ;
    arbreword createNode(QChar,bool = false, arbreword = NULL, arbreword = NULL) ;
    arbresegment createNode(QTextStream &,id, id, QVector<id>,bool = false, arbresegment = NULL, arbresegment = NULL) ;

    void sqlWriteBatch(int, arbreword, int, arbresegment, QString);

    void sqlWriteBatchWord(int, arbreword, int, QString );
    void sqlWriteBatchSegment(int, int, arbresegment, QString );
    void sqlWriteWords(arbreword, QString, int, int, QString, bool = false) ;
    void sqlWriteWord(QString, val, int, int, int, QString, bool=false, bool = false);

    QStringList cutLine(QString) ;
    bool containsPunct(QString) ;

    void segmentFile(int,int);//stanford word segmenter pour une liste des fichiers entre deb et fin pour le multithread

    //fcts segments
    void sqlWriteSegs(arbresegment, QVector<id>  , int, int, QString, QTextStream & );
    void sqlWriteSeg(int , int , int , QVector<id> , int, QString, QTextStream &);
    //void sqlWriteSeg(int , int , int , QVector<int> , int, QString);
    void sqlWriteSegsTest(arbresegment, QVector<id> , int, int, QString);

    QVector<MyMap> getCorpusDocumentsList(int);
    QString getCorpusName(int param_CorpusId);

};

#endif // WORDS_H


