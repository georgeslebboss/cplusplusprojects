#ifndef WORDSPOS_H
#define WORDSPOS_H

#include "../Interface/Interface/Types.h"
#include "../fromInterface.h"
#include "../Interface/Interface/PreprocessingBox/arabicnormalizer.h"


// l'élément de l'arbre préfixe
struct node_word_POS
{
    QChar letter ;
    val occurrences ;
    bool composite ; // le mot contient une ponctuation
    QString POS;
    /* autre information utile ici */
    node_word_POS * sons ;
    node_word_POS * bros ;
} ;

typedef node_word_POS * arbrewordpos ;


class WordsPOS : public QObject
{
    Q_OBJECT
public:
    WordsPOS() ;
    ~WordsPOS();
private:
    QString strsql;
    QTime wordTimer;
    QSqlDatabase *wdbConnection=BDD::DatabaseConnection;
    QVector<MyMap> doclist=CorpusBox::DocList;
    void addTokens(arbrewordpos, QString);
    arbrewordpos create_node(QChar, QString, bool = false, arbrewordpos = NULL, arbrewordpos = NULL) ;
    void  insert(QStringRef,QString, arbrewordpos, int = 0);
    arbrewordpos create_branch(QStringRef , QString, arbrewordpos , int );
    arbrewordpos assoc(QChar, arbrewordpos);
    void sqlWriteBatch(int,arbrewordpos);
    void sqlWriteWords(arbrewordpos, QString, int, QSqlQuery, bool = false) ;
    void sqlWriteWord(QString, val, int,QString, QSqlQuery, bool=false, bool = false);
    int lastPunct(QStringRef) ;
    int firstPunct(QStringRef) ;
    QStringRef removePunctuation(QStringRef) ; 
    void segmentFile(int,int);//stanford word segmenter pour une liste des fichiers entre deb et fin pour le multithread
    void POSsegmentedFile(int,int);//stanford pos tagger pour une liste des fichiers entre deb et fin pour le multithread
} ;

#endif // WORDSPOS_H



