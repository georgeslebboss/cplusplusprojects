#include "WordsPOS.h"
#include "thread"

// Constructeur de l'arbre des Words

WordsPOS::WordsPOS()
{
    arbrewordpos Trie = create_node((QChar) '/', "", true) ;
    qDebug() << "Début de la construction de l'arbre préfixe" ;
    //multithreaded word segmenter
    if (QDir("../normalizedfiles").exists())
        QDir("../normalizedfiles").removeRecursively();
    if (QDir("../POSedfiles").exists())
        QDir("../POSedfiles").removeRecursively();
    QDir().mkdir("../normalizedfiles");
    QDir().mkdir("../POSedfiles");
    int debut, fin=0, nbth=4, l=doclist.size(),i;
    QVector <std::thread*> threads(nbth) ;
    for (i=0;i<nbth-1;i++)
    {
        debut=fin;
        fin+=l/nbth;
        threads[i] = new std::thread(&WordsPOS::segmentFile,this, debut, fin) ;
    }

    threads[i] = new std::thread(&WordsPOS::segmentFile,this, fin, l) ;
    for (i = 0; i < nbth; i++)
        threads[i]->join();
    fin=0;
    //multithreaded pos tagger
    for (i=0;i<nbth-1;i++)
    {
        debut=fin;
        fin+=l/nbth;
        threads[i] = new std::thread(&WordsPOS::POSsegmentedFile,this, debut, fin) ;
    }

    threads[i] = new std::thread(&WordsPOS::POSsegmentedFile,this, fin, l) ;
    for (i = 0; i < nbth; i++)
        threads[i]->join();
    //parse posed files
    for (int i=0;i<doclist.size();i++)
    {
        QString file=QUrl(doclist.at(i).first).toLocalFile();
        QString posedfile=QString("../POSedfiles/%1.posed").arg(QFileInfo(file).fileName());
        addTokens(Trie,posedfile);
    }
    qDebug()<< "Fin de la construction de l'arbre préfixe: " << wordTimer.elapsed();

    qDebug() << "Début de l'insertion dans la table WORD_form" ;
    sqlWriteBatch(CorpusBox::IdCorpus, Trie);
    strsql.clear();
    qDebug() << "Fin de l'insertion dans la table WORD_form" ;
}

WordsPOS::~WordsPOS(){}

arbrewordpos WordsPOS::create_node (QChar c,QString genre, bool end, arbrewordpos son, arbrewordpos bro)
{
    arbrewordpos p = new node_word_POS ;
    p->letter = c ;
    p->sons = son ;
    p->bros = bro ;
    if (end)
    {
        p->occurrences = 1 ;
        p->POS=genre;
    }
    else
        p->occurrences = 0 ;
    return p ;
}

/********************************************************************** Ajouter Words dans l'arbre **************************************/
void WordsPOS::addTokens(arbrewordpos Trie,QString file)
{
    QList<QList<QString> > dataSet;
    QList<QString> list;
    QString lastError = "";
    QFile inFile(file);
    if (!inFile.open(QIODevice::ReadOnly))
    {
        lastError = "Could not open "+file +" for reading";
        return;
    }
    QTextStream fread(&inFile);
    fread.setCodec("UTF-8");
    QString line;
    while(!fread.atEnd())
    {
        line = fread.readLine();
        QList<QString> record = line.split(QRegExp("\\s"));
        dataSet.append(record);
    }
    QString str ;
    for(int t=0;t<dataSet.length();t++)
    {
        list =dataSet[t];
        for(int t1=0;t1<list.length();t1++)
        {
            str=list[t1].split('/').first();
            QString genre=list[t1].split('/').last();
            //str=ArabicNormalizer::PreprocStr(str,language,language_param);
            insert(removePunctuation(& str),genre, Trie) ;
        }
    }
}
// ===================================================================== Fabrication de l'arbre ==========================================

//Cette fonction couvre 4 cas :
//Case 1:  le mot est fini (1er  if)
//Case 2:  le mot exactement est dans l'arbre, occurrences++ de la derniere lettre (2er  if)
//Case 3:  Une partie du mot est dans l'arbre (3ème if)
//Case 4:  autres cas (la suite) : la lettre n'est pas dans l'arbre, exemple
//         "abcd", "abef" and "abgh"
//         /  ----> root
//         |
//         a
//         |
//         b
//         |
//         c <=== e <=== g
//         |      |      |
//         d      f      h

void WordsPOS::insert(QStringRef word, QString genre, arbrewordpos tree, int position)
{
    arbrewordpos branch ;
    if (position >= word.size()) return ;
    branch = assoc(word.at(position), tree->sons) ;
    if (branch and (position == word.size() - 1))
    {
        branch->occurrences++ ;
        branch->POS=genre;
        return ;
    }
    if (branch)
    {
        insert(word, genre, branch, position + 1) ;
        return ;
    }
    branch = create_branch(word, genre, tree, position) ;
    branch->bros = tree->sons ;
    tree->sons = branch ;
    return ;
}

arbrewordpos WordsPOS::create_branch(QStringRef string, QString genre, arbrewordpos tree, int position)
{
    if (position >= string.size())
        return NULL ;
    if (position == string.size() - 1)
        return create_node(string.at(position),genre, true, create_branch(string,genre, tree, position + 1)) ;
    return create_node(string.at(position),genre, false, create_branch(string,genre, tree, position + 1)) ;
}

arbrewordpos WordsPOS::assoc(QChar c, arbrewordpos tree)
{
    if (not tree)
        return NULL ;
    if (tree->letter == c)
        return tree ;
    return assoc(c, tree->bros) ;
}

//==============================================ponctuation=======================================================================

// Dans cette méthode on provoque un deep-copy par mot
// Il vaudrait mieux soit renvoyer un QStringRef
// Soit modifier directement la QString avec argument QString & string

QStringRef WordsPOS::removePunctuation(QStringRef string)
{
    int punct = lastPunct(string) ;
    if (punct < string.size())
        string = string.left(punct + 1) ;
    punct = firstPunct(string) ;
    if (punct > 0)
        string = string.mid(punct) ;
    return string ;
}

// Renvoyer la position de la ponctuation qui ferme le mot

int WordsPOS::lastPunct(QStringRef string)
{
    int position ;
    for (position = string.size() - 1 ; position >= 0 ; position--)
        if (not string.at(position).isPunct())
            return position ;
    return position ;
}

// Renvoie la position du premier caractère qui n'est pas une ponctuation

int WordsPOS::firstPunct(QStringRef string)
{
    int position ;
    for (position = 0 ; position < string.size() ; position++)
        if (not string.at(position).isPunct())
            return position ;
    return position ;
}


//====================================================================écriture Word dans la base de données ==============================
void WordsPOS::sqlWriteBatch(int corpusid,arbrewordpos Trie)
{
    QSqlQuery req(*wdbConnection) ;
    QString path;
    strsql="";
    QFile outFile("wordtodb.csv");
    path=QFileInfo(outFile).absoluteFilePath();
    if (not outFile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
    {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outFile.fileName() << "en ecriture." << endl ;
        return;
    }
    QTextStream outsql(&outFile);
    outsql.setCodec("UTF-8");
    //req.prepare("INSERT INTO \"WORD_form\" (form, occs, composite, fk_corpus) VALUES (:word, :occs,  :composite, :corpus) ;");
    sqlWriteWords(Trie->sons, "", corpusid, req) ;
    sqlWriteWord("", 0, corpusid, "", req, false, true) ;
    outsql << strsql;
    outFile.close();
    QString sql=QString("COPY \"WORD_form\" (form, occs, fk_corpus,composite, genre) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path);
    wordTimer.start();
    if (!req.exec(sql))
        qDebug() << "Error= " << req.lastError().text();
    qDebug()<< "Elapsed Time in Millisecond: " << wordTimer.elapsed();
}

// composite est là pour gérer le cas des ponctuations en milieu de mot
// pour un post traitement éventuel
// apriori ce ne sera pas le cas de l'arabe (anglais ?) - en fait, si -, mais sûrement pour le français.
// les fils d'un composite sont composites
// mais pas ses frères

/* Pour le français :
 * 
 * Une lettre + ' ou (lors/quoi)qu + ' => découper.
 * Aujourd'hui => ne pas découper.
 * 
 * Comment faire ? Chaque langue a ses règles. Une base de règles ?
 * et que faire de passa-t-il, vient-il, venez-vous ?
 * 
 */

void WordsPOS::sqlWriteWords(arbrewordpos tree, QString word, int corpus, QSqlQuery req, bool composite)
{
    if (not tree)
        return ;
    word.append(tree->letter) ;
    if (tree->occurrences)
        sqlWriteWord(word, tree->occurrences, corpus, tree->POS,req, tree->letter.isPunct()) ;
    sqlWriteWords(tree->sons, word, corpus, req, tree->letter.isPunct()) ;
    sqlWriteWords(tree->bros, word.left(word.size() - 1), corpus, req, composite) ;
}

void WordsPOS::sqlWriteWord(QString word, val occs, int corpus, QString genre, QSqlQuery req, bool composite, bool flag)
{
    if (not flag)
        strsql +=QString("%1;%2;%3;%4;%5\n").arg(word).arg(occs).arg(corpus).arg(composite).arg(genre);
}

void WordsPOS::segmentFile(int deb,int fin)
{
    for (int i=deb;i<fin;i++)
    {
        QString file=QUrl(doclist.at(i).first).toLocalFile();
        //si stanford parser il faut parser le fichier segmente
        QString segmentedfile=QString("../normalizedfiles/%1.segmented").arg(QFileInfo(file).fileName());
        StanfordSegmenter::stanfordsegmentercmd(file,segmentedfile);
    }
}

void WordsPOS::POSsegmentedFile(int deb, int fin)
{
    for (int i=deb;i<fin;i++)
    {
        QString file=QUrl(doclist.at(i).first).toLocalFile();
        //si stanford parser il faut parser le fichier segmente
        QString segmentedfile=QString("../normalizedfiles/%1.segmented").arg(QFileInfo(file).fileName());
        QString posedfile=QString("../POSedfiles/%1.posed").arg(QFileInfo(file).fileName());
        StanfordTagger::stanfordtaggercmd(segmentedfile,posedfile);
    }
}

