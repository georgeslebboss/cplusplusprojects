#ifndef CP_TAB_H
#define CP_TAB_H

#include <QtWidgets>
//#include "synsets.h"
#include "../fromInterface.h"
#include "../Interface/Interface/CorpusBox/corpuspreprocselect_box.h"
#include "PreprocessingBox/preprocessing_box.h"
#include "pattern_box.h"
//#include "savedcorpus_box.h"

using namespace std;


//================================ COMPOSANTS DE LA FENETRE

class PatternTab : public Tab
{
  Q_OBJECT

  public:
  
    PatternTab() ;
    ~PatternTab() ;
  //protected slots:

    
} ;

class PatternParams : public Shared
{
  Q_OBJECT

  public:
    PatternParams();
    ~PatternParams();

    void setCorpusId(int);
    void setPreprocCorpusId(int);
    /*void setLanguage(QString) ;
    void setNormParams(normparams *);
    void setNormParamValues(normparamvalues *);
    void setVarNormParamValues(normparamvalues);
    void setSegmenter(QString) ;
    void setLemmatizer(QString) ;*/

    int getCorpusId();
    int getPreprocCorpusId();
    /*QString getLanguage() ;
    normparams * getNormParams();
    normparamvalues * getNormParamValues();
    normparamvalues getVarNormParamValues();
    QVector<bool> getNormalizationParams();
    QString getNormalizationParamsString();
    QString getFullParamsString();
    QString getSegmenter();
    QString getLemmatizer();*/

  protected:
     int CorpusId;
     int PreprocCorpusId;
    /*normparamvalues * NormParamValues;
    normparamvalues VarNormParamValues;
    normparams * NormParams;
    QString Language;
    QVector<bool> NormalizationParams;
    QString Segmenter;
    QString Lemmatizer;*/

} ;

class PatternAppli : public Appli
{
  Q_OBJECT

  public:
    PatternAppli() ;
  
    QSqlDatabase          * DbConnection=BDD::DatabaseConnection;
    QString paramsmessage;

    QElapsedTimer         Time ;
    static inline double  minutes(qint64 time) {return (double) time / (double) (qint64) 60000 ; }
} ;

#endif // CP_TAB_H
