#include "segmentonly.h"
#include "thread"

bool Segments::NOPREPROC = false;
int Segments::IDCORPUS;
int Segments::IDPREPROC=0;
const int Segments::MaxSegment=50;
// =============================================== CONSTRUCTEURS =======================================
// La construction de l'objet Segments consiste à établir des paramètres

Segments::Segments(arbrewordonly trie, int threadnumber, QVector<MyMap> doclist, QVector<id> *form_preprocform, QStringList * preprocform, QString path)
: ThreadNumber(threadnumber), DocumentList(doclist), Trie(trie), Form_Preprocform(form_preprocform), Preprocform(preprocform), Path(path)
{}

Segments::~Segments(){}

// ======================================= CONSTRUCTION DES TRIE ==================================

void Segments::process()
{
    // Phase 1 : génération des arbres
  //  if (Debug) qDebug() << "Segments::process : début de la construction d'un segtrie";
    
    // OPEN OUTPUTFILE HERE
  
  QFile outthreadfile(Path + QString("%1.csv").arg(QString::number(ThreadNumber)));
  if (not outthreadfile.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
  {
    qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outthreadfile.fileName() << "en ecriture.";
    return;
  }
  QTextStream outsql(&outthreadfile);
    
    QVector<int> root;
    //arbresegmentonly trieseg = createNode(0, root) ;

    for (int i = 0 ; i < DocumentList.size() ; i++)
    {
        QString file = QUrl(DocumentList.at(i).first).toLocalFile() ;
        if (Debug) qDebug() << file ;
        //if (Debug) qDebug() << "Segments::process : file to process : " << file << " ThreadNumber: " << ThreadNumber ;
        // pour Word2Vec et Glove
        //printTokens(file);
        addTokens(file, outsql) ;
        if (Debug) qDebug() << "Segments::process : file processed: " << file << " ThreadNumber: " << ThreadNumber ;
    }
    //qDebug()<< "Fin de la construction des arbres trie : " << Timer.elapsed();

    //CLOSE OUTPUTFILE HERE 
    outthreadfile.close();
    emit sigsegfinished();
}

//================================ Pour W2V et Glove

/*

Il faut :
- décommenter les appels à la base de données dans la fin de processSegments
- commenter les appels à la base de données pour Form_Preprocform idem
- décommenter l'appel à printTokens et commenter celui à addTokens dans process

*/


void Segments::printTokens(QString file)
{ 
  if (Debug) qDebug() << "Segments::printTokens";
  if (NOPREPROC)
  { 
    qDebug() << "Segments::printTokens : no preprocessing chosen, return";
    return;
  }
  QFile inFile(file) ;
  if (!inFile.open(QIODevice::ReadOnly))
  {
    qDebug() << "Segments::printTokens : Could not open " + file + " for reading" ;
    return ;
  }
  QFile outFile(Path + "/" + QFileInfo(inFile).fileName() + ".out") ;
  if (!outFile.open(QIODevice::ReadOnly))
  {
    qDebug() << "Segments::printTokens : Could not open " + QFileInfo(outFile).absoluteFilePath() + " for writing" ;
    return ;
  }
  QTextStream input(&inFile) ;
  //input.setCodec("UTF-8") ;
  QTextStream output(&outFile);

  QStringList segments ;
  QStringList segment ;
  QString word ;

  segments = cutText(input.readAll()) ; // donne une liste de segments

  while (not segments.empty())
  {
    segment = cutSegment(segments.takeFirst());
    if (Debug) qDebug() << "--------Segments::printTokens : segment:" << segment ;

    while (not segment.empty()) // traitement du segment
    {
      if ((word = prepare(segment.takeFirst())).isEmpty()) continue;
      word = prepare(segment.takeFirst()) ;
      word = (* Preprocform)[findWordInTable(word)];
      output << word << " ";
    }
  }
  outFile.close();
  inFile.close();
}

//********************************* EXTRACTION DES INFORMATIONS DE CHAQUE FICHIER *************************/

// Méthode utilisée uniquement par process()
void Segments::addTokens(QString file, QTextStream & out)
{
  //if (Debug) qDebug() << "Segments: add Tokens";
  QFile inFile(file) ;
  if (!inFile.open(QIODevice::ReadOnly))
  {
    qDebug() << "Segments::addTokens : Could not open " + file + " for reading" ;
    return ;
  }
  QTextStream input(&inFile) ;
  //input.setCodec("UTF-8") ;

  QStringList segments ;
  QStringList segment ;
  QString word ;
  QVector<int> output ;
  int length;
  int sum = 0;

  segments = cutText(input.readAll()) ; // donne une liste de segments

  if (Debug) qDebug() << "Segments::addTokens : file " << file << " read";
  while (not segments.empty())
  {
    segment = cutSegment(segments.takeFirst());
    length = segment.length();
    
    if (Debug) qDebug() << "Segments::addTokens : segment:" << segment ;
    //qDebug() << "Segments::addTokens : segment:" << segment ;

    while (not segment.empty()) // traitement du segment
    {
      if ((word = prepare(segment.takeFirst())).isEmpty()) continue;
      //int idw = (NOPREPROC ? findWordInTrie(word, Trie) : findWordInTable(word)) ;
      int idw = findWordInTable(word) ;
      sum += idw;
      //if (idw == 0) qDebug() << "Segments::addTokens : word " << word << "non trouvé ?";
      output.append(idw) ;
    }
    insertinCsV(output, length, sum, out);
    sum = 0;
    //insertTrie(output, trieseg);
    //if (Debug) qDebug() << "vecteur de segments:" << segment;
    output.clear();
    segment.clear();
  }
  inFile.close();
}

//============================================ Découpage en mots et segments utilisés par addTokens ========================

// n'est utilisée que par addTokens
// Ceci est un hook parce que pour d'autres langues que l'arabe, la procédure sera plus complexe
// en particulier le cas du tiret (dash) = en cas de coupure de mot, ou de mot composé
// à mettre sans doute dans Preprocessing

// il faut couper le texte : quand il y a plusieurs \n ou \r\n non séparés par des caractères (je ne compte pas les diacritiques ici comme caractères)
// Ajouter un test sur la longueur du segment ??

QStringList Segments::cutText(QString text)
{
  // coupe sur une séquence d'au moins deux LF / CRLF non séparés par des caractères 
  QStringList parts = text.split(QRegularExpression("\\r\\n[^\\p{L}\\p{N}]*?\\r\\n|\\n[^\\p{L}\\p{N}]*?\\n"), QString::SkipEmptyParts);
  
  // coupe ensuite suivant la ponctuation
  QStringList segments;
  while (not parts.empty())
    segments = segments + parts.takeFirst().split(QRegularExpression("(?!\u0022)\\p{Po}"), QString::SkipEmptyParts);
  return segments;
}

// Fonction pour découper en mots (ajouter un test sur longueur de segment
QStringList Segments::cutSegment(QString segment)
{
  segment.replace(QRegularExpression("[^\\p{L}\\p{M}\\p{N}]"), " ");
  QStringList segments=segment.split(" ", QString::SkipEmptyParts);
  if (segments.length() > Segments::MaxSegment) return {};
  return segments;
}

QString Segments::prepare(QString word)
{
  if (word.length() <= WordOnly::MaxWord) return word;
  if (word.length() > WordOnly::MaxArabic) return "";
  if (word.contains(QRegularExpression("[^\\p{Arabic}]"))) return "";
  return word;
}

//===================Find WordID pour communiquer l'identifiant du mot au traitement des segments=========

id Segments::findWordInTrie(QString word, arbrewordonly tree, int position)
{
    //if (Debug) qDebug() << "FindWordIntrie";
    if (not tree) return 0;
    arbrewordonly branch = assoc(word.at(position), tree->sons);
    if (not branch) return 0;
    if (position == word.size()-1) return branch->identifiant;
    return findWordInTrie(word, branch, position+1);
}

arbrewordonly Segments::assoc(QChar c, arbrewordonly tree)
{
  if (not tree) return NULL ;
  if (tree->letter == c) return tree ;
  return assoc(c, tree->bros) ;
}

id Segments::findWordInTable(QString word)
{
  id idw = findWordInTrie(word, Trie);
  QMutex mutex;
  mutex.lock();
  idw = (* Form_Preprocform)[idw-1];
  mutex.unlock();
  return idw;
}

// ========================================== Segments : fabrication de l'arbre ============================
/*
Delete from tempsegment;

insert into tempsegment values ('{2, 1, 4}', 3, 7);
insert into tempsegment values ('{5, 1, 1}', 3, 7);
insert into tempsegment values ('{2, 1, 4}', 3, 7);
insert into tempsegment values ('{1, 5, 1}', 3, 7);
insert into tempsegment values ('{1, 2}', 2, 3);
insert into tempsegment values ('{5, 3}', 2, 8);


SELECT nb, somme, count(*)
FROM tempsegment
GROUP BY nb, somme
HAVING count(*) > 1;

Drop table holdkey;

SELECT nb, somme, count(*)
INTO holdkey
FROM tempsegment
GROUP BY nb, somme
HAVING count(*) > 1;

Drop table holddups;

SELECT DISTINCT tempsegment.*
INTO holddups
FROM tempsegment, holdkey
WHERE tempsegment.nb = holdkey.nb
AND tempsegment.somme = holdkey.somme;

SELECT nb, somme, count(*)
FROM holddups
GROUP BY nb, somme;

DELETE FROM tempsegment
Using holdkey
WHERE tempsegment.nb = holdkey.nb
AND tempsegment.somme = holdkey.somme;

INSERT into tempsegment SELECT * FROM holddups;

*/

void Segments::insertinCsV(QVector<id> identifiants, int length, int sum, QTextStream & out)
{
  // here insert in the csv file out
  
  if (identifiants.size() < 1) return;
  QString segment = "{";
  for (int i = 0 ; i < identifiants.size()-1 ; i++)
  {
    segment += QString::number(identifiants[i]) + ",";
  }
  segment += QString::number(identifiants[identifiants.size()-1]) + "}";

  //structure de fichier/ligne: segment;length;sum
  out << segment << ";" << length << ";" << sum << endl;
}



//====================================== SEGMENTS : Fabrication de l'arbre ============================

void Segments::insertTrie(QVector<id> identifiant, arbresegmentonly tree, int position)
{
    if (position >= identifiant.size()) return ;
    
    arbresegmentonly branch = assoc(identifiant[position], tree->sons) ;

    if (branch and (position == identifiant.size() - 1))
    {
        branch->end = true;
        branch->occurrences++ ;
        return ;
    }
    if (branch)
    {
        branch->occurrences++ ;
        insertTrie(identifiant, branch, position + 1) ;
        return ;
    }
    branch = createBranch(identifiant, tree, position) ;
    branch->bros = tree->sons ;
    tree->sons = branch ;
    return ;
}

arbresegmentonly Segments::assoc(int m, arbresegmentonly tree)
{
    if (not tree) return NULL ;
    if (tree->wordid == m) return tree ;
    return assoc(m, tree->bros) ;
}

arbresegmentonly Segments::createBranch(QVector<id> identifiant, arbresegmentonly tree, int position)
{
    if (position > identifiant.size()-1) return NULL ;
    if (position == identifiant.size() - 1) return createNode(identifiant[position], identifiant, true);
    return createNode(identifiant[position], identifiant, false, createBranch(identifiant, tree, position + 1)) ;
}

arbresegmentonly Segments::createNode (id m_id, QVector<id> identifiant, bool s_end, arbresegmentonly son, arbresegmentonly bro)
{
    arbresegmentonly p = new node_segment_only ;
    p->wordid=m_id;
    p->sons = son ;
    p->bros = bro ;
    p->end=s_end;
    p->occurrences = 1 ;
    return p ;
}

//======================================= SEGMENTS : sauvegarde dans la base de données ====================

void Segments::sqlWriteBatchSegment(int corpusid, arbresegmentonly trieseg, int idpreproc)
{
    QSqlQuery req (* DBConnection) ;
    QString path ;
    QFile outsegfile(QString("%1/segtodb.csv").arg(WordOnly::Temp));
    path = QFileInfo(outsegfile).absoluteFilePath();
    if (not outsegfile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
    {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outsegfile.fileName() << "en ecriture." << endl ;
        return;
    }
    QTextStream outsql(&outsegfile);
    outsql.setCodec("UTF-8");
    QVector <int> identifiant={};
    sqlWriteSegs(trieseg->sons, identifiant, corpusid,idpreproc, outsql) ;
    //outsql << StrSql;
    outsegfile.close();
    QString sql
            =
            QString("COPY \"WORD_segment\" ( occs, segment, fk_idcorpus,fk_idpreproc) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path) ;
    Timer.start();
    if (!req.exec(sql)) qDebug() << "Error= " << req.lastError().text();
    if (Debug) qDebug()<< "Fin Sql Write Batch Segment Elapsed Time in Millisecond: " << Timer.elapsed();
    if(Debug) qDebug() << "ajout segement: " << sql;
    outsegfile.deleteLater();
}

// les segments sont écrits sous la forme {identifiant de mot, identifiant de mot...}
void Segments::sqlWriteSegs(arbresegmentonly tree, QVector<id> identifiants, int corpus,int idpreproc, QTextStream & out)
{
    if (not tree) return ;
    identifiants.append(tree->wordid) ;
    if (tree->end)
        sqlWriteSeg( tree->occurrences, corpus,identifiants,idpreproc, out) ;
    sqlWriteSegs(tree->sons, identifiants, corpus,idpreproc, out) ;
    identifiants.removeLast();
    sqlWriteSegs(tree->bros, identifiants, corpus,idpreproc, out) ;
}

void Segments::sqlWriteSeg( int occs, int corpus, QVector<id>  identifiant,int idpreproc, QTextStream & out)
{
    if (identifiant.size()==1) return;
    QString segment= "{";
    for(int i=0 ; i < identifiant.size()-1 ; i++)
    {
        segment += QString::number(identifiant[i]) + ",";
    }
    segment+=QString::number(identifiant[identifiant.size()-1])+"}";

    //QString str=QString("%1;%2;%3;%4;%5\n").arg(occs).arg(corpus).arg(segment).arg(trieid).arg(options);
    out << occs << ";" << segment << ";" << corpus << ";" << idpreproc << "\n";
    //QString str=QString("%1;%2;%3\n").arg(occs).arg(corpus).arg(segment);
    //StrSql +=str;
    //if (Debug) qDebug() << "sql insert segment" << str;
}

//==============================================================

void Segments::printJsonSegments(arbresegmentonly node, QTextStream & out)
{
  if (not node) return;
  out << "{ ";
  printJsonNode(node, out);
  if (node->bros)
  { 
    out << "\"b\": " ;
    printJsonSegments(node->bros, out);
    out << "," ;
  }
  else out << "\"b\": \"\",";
  if (node->sons)
  {
    out << "\"s\": ";
    printJsonSegments(node->sons, out);
  }
  else out << "\"s\": \"\"";
  out << " }";
}

void Segments::printJsonNode(arbresegmentonly node, QTextStream & out)
{ 
  out << "\"w\":" << node->wordid << "," ;
  out << "\"o\":" << node->occurrences << "," ;
  out << "\"e\":" << node->end << "," ;
}

void Segments::generateWordsList(arbrewordonly tree, QString word)
{
    if (not tree) return ;
    word.append(tree->letter) ;
    if (tree->occurrences) generateWord(tree, word) ;
    generateWordsList(tree->sons, word) ;
    generateWordsList(tree->bros, word.left(word.size() - 1)) ;
}

void Segments::generateWord(arbrewordonly tree, QString word)
{
    WordList.append(word);
    WordOccList.append(tree->occurrences);
}

/*****************************************************
tu vas parcourir word_form_preprocform sur idform  donc je t'ai créé
un index sur idform qui s'appelle idform_idx

tu vas parcourir word_preprocform sur les id,

si tu fais le test, sache que la seule façon que ça fonctionne avec
le grand corpus, c'est de faire insert segments avant de faire insert words
(il le fera lui-même)
donc pour tester, il faut d'abord vérifier que tout marche à partir d'une
base de données vierge avec juste le corpus par ex. standard
puis tu cliques directement insert segments, après avoir choisi le lemmatiseur
et le nombre de threads
et le corpus bien sur
je vais appeler mes nouvelles fcts où
tes nouvelles fonctions doivent remplacer findWordInTable
il sera quoi comme résultats attendue
il faut aussi virer cloneDatabase et tout ce qui concerne ThreadDBConnection
dans segmentonly.cpp
là je pense , je maitrise pas ,
dèja je comprends pas votre ecosysteme
l'accès au QVector et QStringList doit être protégé par mutex.lock(),
mutex.unlock() (avant, après
l'architecture de votre systeme
process dans segmentonly parcourt tous les fichiers et utilise addTokens pour
les lire
et les met dans un Trie, ça ça ne changera pas
sauf qu'on enlève le clonage de connexion
addTokens lit un mot et le convertit avec findWordIn...
 (Trie si NOPREPROC Table sinon)
c'est juste à ce point du code que tu interviens
tu reçois le mot lu dans le fichier
tu le convertis
et tu renvoie son identifiant (pour l'instant c tout ce qui compte)
(on laisse la QStringList des formes de côté
voilà ce que ta fonction qui va remplacer findWordInTable doit faire :
recevoir un mot et donner l'identifiant du mot préprocessé
pour ça, elle doit : trouver l'identifiant du mot = findWordInTrie
je vais créer une autre fct ou bien je vais ecrire à l'interieur de celle la
à partir de l'identifiant lire l'idprerpoc dans le QVector et le renvoyer
comme tu veux
le plus simple
mais tu supprimes complètement le code actuel (en commentaire)
la création du QVector doit avoir lieu à la fin de la fonction processSegment,
dans le else, juste avant le computeSegtrie
et après la création des index que je vais pousser et que tu pourras utiliser
pour la création du qvector
with index machintruc
donc c une fonction à mettre mais celle-ci est dans word_box.cpp

l'autre dans segmentonly.cpp
voilà je pousse et j'y vais
c'est simple pour l'insertion dans le code, deux points findWordInTable et
processSegments
c poussé
à ce soir
a ce soir
si au moins ça compile tes modifs c déjà ça
laisse la question de la QStringList de côté
c pour W2V mais s'il n'y a pas grapavec ça vaut pas le coup
ah et n'oublie pas QMutex mutex; mutex.lock() avant l'accès au QVector et
mutex.unlock() après
Envoyer un message
*********************************************************/

int Segments::findWordInTable2()
{
  //if (Debug) qDebug() << "FindWordIntrie";

  QVector<int> idtList ;
  QVector<QString> wordsList ;

 // QSqlQuery req(*DBConnection);
  // requête sql pour ramener la ligne avec la clé word
  //QString sql = "select id, form  from  \"WORD_form\" where id < 1000 " ;

  long long i = 0 ;

  /*if(not req.exec(sql))
   {
      qDebug() << "ERREUR: " <<req.lastError().text()<< endl;
      return {};
   }*/
   while(i < 8000000/*req.next()*/)
   {
       idtList.append(i/*req.value(0).toInt()*/) ;
       wordsList.append("booooooooooooonnnnjjjjjjjjooouuuur"/*req.value(1).toString()*/);
       fprintf(stderr, "\033[0GProcessed %lld ", i++) ;

    }
   int *p = idtList.data() ;
   QString *s = wordsList.data() ;

   for (i = 0; i < idtList.size(); i++)
       qDebug() <<s[i]<<"\t"<<p[i] ;
    return idtList.size() ;


}
