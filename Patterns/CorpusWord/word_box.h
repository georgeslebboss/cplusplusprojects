#ifndef WORDBOX_H
#define WORDBOX_H
#include <QtWidgets>
#include "word.h"
#include "wordonly.h"
#include "segmentonly.h"
#include  "../fromInterface.h"

#define WORDS           1
#define SEGMENTS        2
#define WORDS_SEGMENTS  3
#define WORD            false
#define SEG             true


class WordBox : public Box
{
    Q_OBJECT

public:
    WordBox(bool);
    QSqlDatabase           * DBConnection = BDD::DatabaseConnection;
    WordOnly               * Wordonly;
    Segments               * Segmentonly;

    bool WordTableFlag ;
    QString TrieResources = "Trie_resources/" ;
    QVector<id> Form_Preprocform;
    QStringList Preprocform;
    int IdPreproc=0;
    
    void insertWordSegmentClicked(int, QString, QString, QVector<bool>, QString, QString, QString, QString, int);
    void setWordsSharedParams(int, QVector<bool>, QString, QString, QString);
    
    id getWords(int, QString, int);
    id getSegments(int idpreproc);
    id getUniqueWordsCount();//count of unique word
    id getWordsCount();//count words not preprocessed
    id getUniquePreprocWordsCount(int );//count of uniq process words
    id getPreprocWordsCount(int );//count of preproc words
    int getIdPreproc();
    QTime myTimer;

    int NbThreads = 1;        //word multithreading
    
    QVector<MyMap> DocumentList;
    void getCorpusDocumentList(int);

    void deleteTrie(arbrewordonly);

protected:
    bool Debug = true;
    
    int Corpus;
    QVector<bool> Wordnormalizationparams;
    QString WordSegmenter;
    QString WordLemmatizer;
    QString WordFullparams;
    int InsertStage;  //insert word or segment or word & segment
    static int WordId;
    int TrieId;
    
    bool NOPREPROC = false;     //no preproc params
    bool WTINCORPUS = false;            //exists word trie in corpus
    bool STINCORPUS = false;            //segtrie exists in corpus
    bool STINPREPROC = false;            //segtrie exists in preproc

    QTableWidget            * WordsTable;
    
    QLabel *    StoreJsWtLabel;
    QCheckBox * StoreJsWt;
    QLabel *    StoreJsStLabel;
    QCheckBox * StoreJsSt;
    QPushButton * InsertWordsBtn;
    QPushButton * InsertSegmentsBtn;
    QPushButton * InsertWordsandSegmentsBtn;
    QLabel * ConvertFileLabel;
    QCheckBox * ConvertFile;



    static arbrewordonly Trie;
    static arbresegmentonly TrieSeg;
    
    void mergeTrie(arbrewordonly);
    void updateNode(arbrewordonly, arbrewordonly);
    void insertBros(arbrewordonly, arbrewordonly);
    void addSons(arbrewordonly, arbrewordonly);
    void pushBranch(arbrewordonly, arbrewordonly);
    arbrewordonly assoc(QChar, arbrewordonly);
    void updateWordId(arbrewordonly);
    void mergeTrie(arbresegmentonly);
    void updateNode(arbresegmentonly, arbresegmentonly);
    void insertBros(arbresegmentonly, arbresegmentonly);
    void addSons(arbresegmentonly, arbresegmentonly);
    void pushBranch(arbresegmentonly, arbresegmentonly);
    arbresegmentonly assoc(int, arbresegmentonly);

    QString generateFile(int, bool = WORD, bool = false);
    QString generateFilePath(int, bool = WORD, bool = false);
    QString getCorpusName(int);
    QString getCorpusScript(int);

    void processSegments(QString);
    void processWords(int, QString, QVector<bool>, QString, QString);
    void storeWords();
    void computeWordtrie(QString);
    void computeSegtrie();
    void computePreprocforms();//compute preproceesed forms with parameters idpreproc
    void storeSegments();
    
signals:

    void sigInsertWordSegmentClicked(int);
    void sigAllFinished();
    void sigSegAllFinished();
    void sigTrieFinished();
    void sigConvertFiles();

private slots:
    void onThreadNumberChanged(int); //word multithreading
    void onFinishedWork(node_word_only *)  ; //word multithreading
    void onSegFinishedWork() ; //word multithreading
    void onAllFinished();
    void onAllSegFinished();
};
#endif // WORDBOX_H
