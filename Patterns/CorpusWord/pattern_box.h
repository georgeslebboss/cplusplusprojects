#ifndef PATTERNBOX_H
#define PATTERNBOX_H
#include <QtWidgets>
#include "pattern.h"
#include "synsets.h"
#include "../fromInterface.h"
#include "machine.h"

using namespace std;

class PatternBox : public Box
{
    Q_OBJECT

public:
  PatternBox();
  ~PatternBox(){};
  
  QSqlDatabase          * dbConnection=BDD::DatabaseConnection;
  int Corpus;
  int PreprocCorpus;
  int CorpusType=0; //Trie only by default,small,medium,big
  int DebutThread;
  bool           bSaveEvalVectors;
  bool           bSavePatterns;
  bool           bSaveTrie;
  bool           bContinueGeneratePattern;
  static arbre_pattern TriePattern;
  QMap<id, val> NoPattern;
  QVector<QMap<QVector<id>,val>> SynonymPattern;
  
  QElapsedTimer         Time ;
  int ThreadNumber = 1 ;
  int ThreadcNumber;
  QString MatricePatterns = "MatricePatterns/" ;
  QString TMP="/tmp/";

  id MaxMarkers = 1000;    // nombre max de marques (indép. de la langue) ; pour l'instant ne sert que pour NbMarkers

  QMap<id, QMap<id, val> > Jokers;

  void    generatePattern() ;
  void    initNoPattern(id id0, id id1);

  
  static inline double  minutes(qint64 time) {return (double) time / (double) (qint64) 60000 ; }
  void setPatternsSharedParams(int, int);
  
  void nbMarkersEntered();
  void thresholdChanged(int);
  void validateMarkers();

signals:
    void            sigallfinishedPattern();
protected slots:
    void               onAllFinishedPattern();
    void               onFinishedWorkPattern(node_pattern *);
    void               onPatternCountButton();
    //void               onFinishedWorkPattern(node_pattern *, QMap<id, QMap<id, val> > *);
protected:
    QTime MyTimer;
    bool Debug = false;
    int * JsonCurrentIndex=new int;
    int Limit;//limit of chunck segment
        
    //Partie marques
    QLineEdit     * NbMarkers;            // nombre de marques voulu
    QSpinBox      * MarkerThreshold;      // ou seuil voulu
    QLabel        * Formsnb;              // nombre de mots autres que marques
    QTableWidget  * MarquesTable;
    QPushButton   * ValidateMarkers;
    
    void    createMarquesTable();
    int     populateMarkers(int);
    
    // résultats de cette partie
    int           MarkerMinFreq;          // fréquence minimale des marques
    id            JokerMinId;             // identifiant du premier mot qui n'est pas une marque
    int           JokerNb = 0;            // nombre de mots qui ne sont pas des marques
    
    // Partie patterns
    QSpinBox            * Threadbox;
    QSpinBox            * JokerLength;
    QSpinBox            * PatternThreshold ;
    QLabel              * NbPatterns;                // inutilisé
    QCheckBox           * SaveEvalVectors;
    QCheckBox           * SavePatterns;
    QCheckBox           * SaveTrie;
    QCheckBox           * ContinueGenratePatterns;
    QRadioButton        * RadioSmall;
    QRadioButton        * RadioMedium;
    QRadioButton        * RadioBig;
    QRadioButton        * RadioTrie;
    
    QPushButton         * PatternGenerationButton;
    QPushButton         * PatternCountButton;
    int CountPatterns ;            // nombre total de patrons
    void countPatterns(arbre_pattern, QTextStream &, QVector<id> = QVector<id>());
    void createTrie(arbre_pattern , int );
    
    //QVector<QVector<id> > Segment_Preprocform;

    void            printVectors(QMap<id, QMap<id, val> >, QTextStream &);
    void            printVectorsM(QTextStream &);
    void            printVectorsB(QTextStream &, int);
    QString         sqlGetWord(id);
    QVector<QVector<id> > sqlGetChunckSegment(int);
    void            printAdiosPatterns(arbre_pattern, QTextStream &,int, QVector<id> = QVector<id>());
    QString         convertPattern2String(QVector<int> );
    QString         convertPatternids2String(QVector <int>);

    
    int PatternId = 0;
    id JokerId = -1;
    QString JokerForm = "*";

    //QString strBigVectors;//sauvegarder les idpattern,jokers,occs fin de chque thread en cas de bigCorpus
    //QString strBigPatterns;//sauvegarder les big patterns dupliquees

//    void               getPatterns(int , QString , int , int , int);
    
    QString Json = "";
    //parametres de pattern
    int MarkerThresholdValue;
    int JokerLengthValue;
    int PatternThresholdValue;
    
    void mergeTrie(arbre_pattern);
    void addSons(arbre_pattern, arbre_pattern);

    void insertBros(arbre_pattern, arbre_pattern);
    arbre_pattern assoc (id, arbre_pattern);
    void pushBranch(arbre_pattern son, arbre_pattern bigtree);
    void mergeJokers(QMap<id, val>, QMap<id, val>);

    void createVectors();
    void createJokers(arbre_pattern, int);
    void updateMapJoker(QMap<id, val>, id, QMap<id, QMap<id, val> > &);

    void createVectorsM(); //create vectors for medium corpus
    void sqlWriteNoPattern(QTextStream &);
    void sqlWriteJokers(arbre_pattern, QTextStream &, int);

    void createVectorsBig();

    QString generateFile(int, int, int, int, int, QString);
    QString generateFilePath(int, int, int, int, int,QString);
    QString generateFileTempPath(int, int, int, int, int,QString);
    QString getCorpusName(int);
    void printJsonPatterns(arbre_pattern, QTextStream &);
    void printJsonPatternNode(arbre_pattern, QTextStream &);
    arbre_pattern jsonToSegmentationTrie(QString );
    arbre_pattern jsonToSegmentationTrieAux();
    arbre_pattern createNodefromJson();
    val readNumber();
    QChar * readJsonChar();
    void unreadJsonChar();
};
#endif // PATTERNBOX_H
