#ifndef SYNSETS_H
#define SYNSETS_H
#include <QtWidgets>
#include "../fromInterface.h"
#include "PreprocessingBox/arabicnormalizer.h"
#include<iostream>
typedef QMap<QString,QStringList> wordlist;
typedef QPair<QString,int> wordocc;

class Synsets: public QObject
{
  Q_OBJECT
public:
    Synsets (QString,QVector<QString>,QSqlDatabase *) ;
    QString syn_lang;
    QVector<QString> syn_lang_param;
    QSqlDatabase *synBDD;

    wordlist awn_words;
    //QMap<QString,QString> awn_words_synset;//(word_id, synset_id) assign a synset for each word_id
    QMap<QString,QString> awn_words_synset;//(word_id, synset_id) assign a synset for each word_id
    QMap<QString,QStringList> GraPaVec_Synsetwords;//awn synset with list of words related to this synset

    void setAwnwordslist(); //fill awn_words
    void setAwnWordsSynset();//assign for each word_id the proper synset
    void setGraPaVecWordsInSynsets();//fill GraPaVec_Synsetwords
    QStringList assignFormToWordid(QString);//assign each word in GraPaVec to a corresponding word(s)_id
    QStringList AssignGraPaVecSynsetsToAwnSynsets(QString);

    QMap<QString,QStringList> awn_synsetwords; //awn synset with list of word_id related to this synset
    void getAwnWordsInSynsets();//fill awn_sysnetwords
    QMap<QString, QStringList> assignSynsetsToClusters();//Assign synsets for all GraPaVec clusters
    QMap<QString,QStringList> synsetsclusters;//a afficher
    void afficherResultats();//afficher les resultats de mapping dans un fichier

} ;

#endif // SYNSETS_H
