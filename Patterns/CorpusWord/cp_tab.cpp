#include "cp_tab.h"

//========================================================= TAB =====================================================

PatternTab::PatternTab()
{
  PatternAppli * windowword = new PatternAppli ;
  PatternParams * wordparams = new PatternParams ;
  initTab(windowword, wordparams) ;
  
  answerMessage("CorpusPatternParams://") ;
}

PatternTab::~PatternTab()
{
}

//========================================================= SHARED ===================================================

PatternParams::PatternParams() {}
PatternParams::~PatternParams() {}

void PatternParams::setCorpusId(int id)
{ CorpusId = id; }

int PatternParams::getCorpusId()
{ return CorpusId; }

void PatternParams::setPreprocCorpusId(int id)
{ PreprocCorpusId = id; }

int PatternParams::getPreprocCorpusId()
{ return PreprocCorpusId; }

//========================================================= APPLI ====================================================

PatternAppli::PatternAppli()
{ 
  //================================ FENETRAGE ====================================================
  QGridLayout * mainlayout = new QGridLayout;
  mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));

  //================================ PARTIE CORPUS ================================================
  
  PatternBox * patternbox = new PatternBox();
  CorpusPreprocSelectBox * corpusbox = new CorpusPreprocSelectBox() ;
  corpusbox->setMinimumWidth(300);
  mainlayout->addWidget(corpusbox, 0, 0);
  corpusbox->App = this;
  connect
  (
    corpusbox,
    & CorpusPreprocSelectBox::sigPreprocCorpusModified,
    [=](int preproccorpusid, int corpusid)
    {
      static_cast<PatternParams *>(SharedParams)->setCorpusId(corpusid);
      static_cast<PatternParams *>(SharedParams)->setPreprocCorpusId(preproccorpusid);
      patternbox->setPatternsSharedParams(static_cast<PatternParams *>(SharedParams)->getCorpusId(),
                                          static_cast<PatternParams *>(SharedParams)->getPreprocCorpusId());

      static_cast<PatternTab *>(this->parent())->answerMessage("CorpusPatternParams://corpusid:"+QString::number(static_cast<PatternParams *>(SharedParams)->getCorpusId())+"/PreprocCorpus:"+ static_cast<PatternParams *>(SharedParams)->getPreprocCorpusId());
    }
  );


// pour accéder à la propriété depuis une autre boite : App->static_cast<PatternParams *>ShareParams->getLanguage()
// ou alors définir les getters dans PatternAppli

  //================================ PARTIE PatternBox: Extraction des patterns =====================================

  mainlayout->addWidget(patternbox,0,2);
  patternbox->App = this;
  setLayout(mainlayout);
  setWindowTitle("Pattern Generation");
}
