#include  "word_box.h"

arbrewordonly WordBox::Trie = NULL;
arbresegmentonly WordBox::TrieSeg = NULL;
int WordBox::WordId = 1;

WordBox::WordBox(bool wordtableflag)
: Box(tr("Analyse des mots et des segments")), WordTableFlag(wordtableflag)
{
    QGridLayout * wordlayout = new QGridLayout ;
    setAlignment(Qt::AlignTop) ;
    setLayout(wordlayout) ;

    QGroupBox * wordsettings = new QGroupBox(tr("Settings"));
    wordsettings->setAlignment(Qt::AlignTop);
    wordlayout->addWidget(wordsettings, 0, 0) ;
    QGridLayout * wordsettingslayout = new QGridLayout;
    wordsettings->setLayout(wordsettingslayout);

    wordsettingslayout->addWidget(new QLabel("Number of threads"), 0, 1) ;
    QSpinBox * threads = new QSpinBox() ;
    threads->setMinimum(1);
    threads->setMaximum(1000);
    wordsettingslayout->addWidget(threads, 0, 2);
    connect(threads,SIGNAL(valueChanged(int)),this,SLOT(onThreadNumberChanged(int)));
    
    StoreJsWtLabel = new QLabel("Store json trie of words");
    wordsettingslayout->addWidget(StoreJsWtLabel, 2, 0);
    StoreJsWt = new QCheckBox();
    StoreJsWt->setChecked(false);
    wordsettingslayout->addWidget(StoreJsWt, 2, 1);
    StoreJsWtLabel->hide();
    StoreJsWt->hide();
    StoreJsWt->setToolTip(tr("Check if insert segments does not follow insert words in the same session"));

    StoreJsStLabel = new QLabel("Store json trie of segments");
    wordsettingslayout->addWidget(StoreJsStLabel, 2, 2);
    StoreJsSt = new QCheckBox();
    StoreJsSt->setChecked(false);
    wordsettingslayout->addWidget(StoreJsSt, 2, 3);
    StoreJsStLabel->hide();
    StoreJsSt->hide();

    InsertWordsBtn = new QPushButton("Insert words") ;
    wordsettingslayout->addWidget(InsertWordsBtn, 4, 0) ;
    connect
    (
      InsertWordsBtn,
      &QPushButton::clicked,
      [=] { emit sigInsertWordSegmentClicked(WORDS); }
    );
    InsertWordsBtn->hide();
    
    InsertSegmentsBtn = new QPushButton("Insert segments") ;
    wordsettingslayout->addWidget(InsertSegmentsBtn, 4, 2) ;
    connect
    (
      InsertSegmentsBtn,
      &QPushButton::clicked,
     [=] {emit sigInsertWordSegmentClicked(SEGMENTS);}
    );
    InsertSegmentsBtn->hide();



    InsertWordsandSegmentsBtn = new QPushButton("Insert words and segments") ;
    wordsettingslayout->addWidget(InsertWordsandSegmentsBtn, 4, 1) ;
    connect
    (
      InsertWordsandSegmentsBtn,
      &QPushButton::clicked,
     [=] {emit sigInsertWordSegmentClicked(WORDS_SEGMENTS);}
    );
    
    ConvertFileLabel = new QLabel("Lemmatize and normalize files");
    wordsettingslayout->addWidget(ConvertFileLabel, 5, 2);
    ConvertFile = new QCheckBox();
    ConvertFile->setChecked(false);
    wordsettingslayout->addWidget(ConvertFile, 5, 3);

    //createWordsTable
    if (wordtableflag)
    {
      WordsTable = new QTableWidget(0, 2) ;
      WordsTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
      WordsTable->setHorizontalHeaderLabels(QStringList() << tr("Words") << tr("Frequency")) ;
      WordsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
      WordsTable->verticalHeader()->hide() ;
      WordsTable->setShowGrid(false) ;
      WordsTable->setContextMenuPolicy(Qt::CustomContextMenu);
      wordlayout->addWidget(WordsTable, 3,0) ;
    }

  threads->setValue(2);
}

// Slot déclenché par le choix du nombre de threads
void WordBox::onThreadNumberChanged(int val)
{ NbThreads = val ;
  if (val > 1) 
  {
    StoreJsWtLabel->show();
    StoreJsWt->show();
    StoreJsWt->setChecked(true);
    StoreJsStLabel->show();
    StoreJsSt->setChecked(true);
    StoreJsSt->show();
    InsertWordsandSegmentsBtn->hide();
    InsertWordsBtn->show();
    InsertSegmentsBtn->show();
  }
  else
  {
    StoreJsWtLabel->hide();
    StoreJsWt->hide();
    StoreJsWt->setChecked(false);
    StoreJsStLabel->hide();
    StoreJsSt->setChecked(false);
    StoreJsSt->hide();
    InsertWordsandSegmentsBtn->show();
    InsertSegmentsBtn->hide();
    InsertWordsBtn->hide();
  }
}

//========================================= RÉCUPÉRATION PARAMÈTRES ET AIGUILLAGE =================================

// slot appelé par word_tab sur la réception du signal sigInsertWordSegmentClicked (envoyé par insert...)
// word tab transfère les parametres nécessaires (identité du corpus, paramètres de preprocessing)
void WordBox::insertWordSegmentClicked(int corpus, QString language, QString script, QVector<bool> normalizationparams, QString segmenter, QString lemmatizer, QString normalizer, QString fullparamsstring, int caller)
{
  //insert normalizer in fullparamsstring
  if (normalizer=="IBM Normalizer")
     fullparamsstring="IBM Normalizer";
  else if (normalizer=="Latin Normalizer")
      fullparamsstring="Latin Normalizer";
  setWordsSharedParams(corpus, normalizationparams, segmenter, lemmatizer, fullparamsstring);
  InsertStage = caller ;
  myTimer.start();
  
  if (InsertStage == WORDS_SEGMENTS) // monothread (non maintenu)
  {
    if (Debug) qDebug() << "Insert word and segment on monothread";
    Words * word = new Words(corpus, language, normalizationparams, segmenter, lemmatizer);
    word->createTries(StoreJsWt->isChecked()) ;
    //int i= getWords(corpus,fullparamsstring, word->getTrieId());
    int j= getSegments(Corpus);
    static_cast<Tab *>(this->parent()->parent())->answerMessage("Segments inserted: "+QString::number(j));
  }
  else
  {
    //recuperer les documents du corpus
    getCorpusDocumentList(corpus) ;
    if (Debug) qDebug() << "insertWordSegmentClicked : doclist size " << DocumentList.size() ;

    if (InsertStage == WORDS) processWords(corpus, script, normalizationparams, segmenter, lemmatizer);
    else if (InsertStage == SEGMENTS) processSegments(script);
  }
}

// appelé uniquement par insertWordSegmentClicked
void WordBox::getCorpusDocumentList(int corpusid)
{
    QVector<MyMap> maplist;
    QSqlQuery req(* BDD::DatabaseConnection);
    req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = %1").arg(corpusid));

    while (req.next()) maplist.append(MyMap(req.value(0).toString(), req.value(1).toInt()));
    DocumentList = maplist;
}

// appelé uniquememt par InsertWordsandSegmentsBtn
// récupération des paramètres de word tab et instanciation des flags NOPREPROC et WTINCORPUS
void WordBox::setWordsSharedParams(int corpus, QVector<bool> normalizationparams, QString segmenter, QString lemmatizer, QString fullparamsstring)
{
  // lire et preciser les parametres captés dans word_tab
  Corpus = corpus;
  Wordnormalizationparams = normalizationparams;
  WordSegmenter = segmenter;
  WordLemmatizer = lemmatizer;
  WordFullparams = fullparamsstring;
  //si rien des parametres de pretraitement choisis NOPREPROC=true
  if (WordFullparams=="0000000" and WordSegmenter=="" and WordLemmatizer=="") NOPREPROC = true;
  QSqlQuery query(* DBConnection);
  //tester si word trie exite deja dans corpus
  QString sql = QString("SELECT fk_wt_id FROM corpus where id=%1").arg(corpus);
  query.exec(sql);
  if (query.next()) WTINCORPUS = not query.value(0).isNull();
  if (Debug) qDebug() << "setWordsSharedParams : word trie exists in corpus:" << WTINCORPUS;
}

//=============================================== INSERTION DES MOTS ==================================================

// appelé par insertWordSegmentClicked
// récupère les paramètres dans la base de données puis choisit la fonction à exécuter
// normalement Trie exists = WTINCORPUS = word_forms filled
void WordBox::processWords(int corpus, QString script, QVector<bool> normalizationparams, QString segmenter, QString lemmatizer)
{
  if (Debug) qDebug() << "processWords : Insert Word Only in multithread";

  // vérifier si wordtrie existe dans corpus
  if (Debug) qDebug() << "processWords : is trie registered in corpus? " << WTINCORPUS ;
  if (Debug) qDebug() << "processWords : does Trie exists? " << (not (not Trie)) ;
  if (Debug) qDebug() << "processWords : NOPREPROC? " << NOPREPROC ;
  
  if (WTINCORPUS and NOPREPROC)
  {
    if (Debug) qDebug() << "processWords : no preprocessing, nothing more to do" ;
    return;
  }
  connect(this, &WordBox::sigTrieFinished, [=]{if (not NOPREPROC) computePreprocforms();});
  // s’il n’existe pas appeler la fonction computeWordtrie
  if (not WTINCORPUS) computeWordtrie(script);
  else emit sigTrieFinished();
}

//========================================== Création du trie des mots
// appelé par processWords ou par processSegments quand le trie des mots n'existe pas
// remplit la table WORD_form et crée le jsonfile des mots
// lance les threads sur les mots
void WordBox::computeWordtrie(QString script)
{
  if (Debug) qDebug() << "computeWordtrie";
  int chuncksize = DocumentList.size() / NbThreads;
  if (Debug) qDebug() << "computeWordtrie : chuncksize " << chuncksize ;
  
  if (Debug) qDebug() << "computeWordtrie : Début de la construction des arbres word trie";

  for (int i = 0 ; i < NbThreads - 1 ; i++)
  {
    QThread * thread = new QThread ;
    Wordonly = new WordOnly(script, i, DocumentList.mid(i * chuncksize, chuncksize));
    Wordonly->moveToThread(thread) ;
    connect(thread, SIGNAL(started()), Wordonly, SLOT(process()));
    connect(Wordonly, SIGNAL(finished(node_word_only *)), this, SLOT(onFinishedWork(node_word_only *)));
    connect(Wordonly, SIGNAL(finished()), thread, SLOT(quit()));
    connect(Wordonly, SIGNAL(finished()), Wordonly, SLOT(deleteLater())) ;
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start() ;
  }
  QThread * thread = new QThread ;
  Wordonly = new WordOnly(script, NbThreads - 1, DocumentList.mid((NbThreads - 1) * chuncksize));
  Wordonly->moveToThread(thread);
  connect(thread, SIGNAL(started()), Wordonly, SLOT(process()));
  connect(Wordonly, SIGNAL(finished(node_word_only *)), this, SLOT(onFinishedWork(node_word_only *)));
  connect(Wordonly, SIGNAL(finished()), thread, SLOT(quit()));
  connect(Wordonly, SIGNAL(finished()), Wordonly, SLOT(deleteLater())) ;
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
  thread->start() ;
  
  connect(this, SIGNAL(sigAllFinished()), this, SLOT(onAllFinished()));
}

// slot déclenché quand un thread a fini pour merger son trie avec le trie principal
void WordBox::onFinishedWork(node_word_only * trie)
{
  //if (Debug) qDebug() << "onFinishedWork";
  static int finishedthreadnb = 0;
  finishedthreadnb++;
  //if (Debug) qDebug() << "onFinishedWork: " << finishedthreadnb;

  QMutex mutex;
  //if (Debug) qDebug() << "onFinishedWork: Debut merge:" << finishedthreadnb;
  mutex.lock();
  mergeTrie(trie);
  //if (Debug) qDebug() << "onFinishedWork: Fin merge:" << finishedthreadnb;
  mutex.unlock();
  if (finishedthreadnb == NbThreads) emit sigAllFinished();
}

// slot déclenché quand tous les threads sont finis pour terminer le travail
void WordBox::onAllFinished()
{
  //if (Debug) qDebug() << "on AllFinished: Fin ComputeWordTrie";
  updateWordId(Trie);
  //if (Debug) qDebug() << "on AllFinished : wordid updated";
  storeWords();
}

// appelé uniquement par onAllFinished
//update wordid after all merge
void WordBox::updateWordId(arbrewordonly trie)
{
    if (not trie) return;
    if (trie->occurrences) trie->identifiant = WordId++;
    updateWordId(trie->bros);
    updateWordId(trie->sons);
}

// appelé uniquememt par computeWordtrie
// Si StoreJsWt n'est pas coché (défaut coché), ça veut dire que l'utilisateur pense effectuer tout le traitement dans la même session
// dans tous les cas on remplit la table WORD_form
// et par défaut on met json word trie dans un fichier tmp et ds WORD_trie, avec mise à jour de corpus

void WordBox::storeWords()
{
  Wordonly = new WordOnly();

  if (Debug)  qDebug() << "storeWords : Début de l'insertion dans la table WORD_form" ;
  Wordonly->sqlWriteBatchWord(Corpus, WordBox::Trie, NOPREPROC);
  if (Debug)  qDebug() << "storeWords : Fin de l'insertion dans la table WORD_form ";

  if (StoreJsWt->isChecked())
  {
    // print word json trie file into build
    QString trienameword = generateFile(Corpus, WORD);
    if (Debug) qDebug() << "storeWords : Impression du json file " << trienameword;
  
    // insert json trie file name
    QFile outfile(trienameword);
    QString path = QFileInfo(outfile).absoluteFilePath();

    QSqlQuery req(* DBConnection);
    QString query;
    query = QString("INSERT INTO \"WORD_trie\"(wt_name) VALUES ('%1');").arg(path); // insert file json name into word_trie
    req.exec(query);
    
    // mise à jour de corpus
    TrieId = req.lastInsertId().toInt();    
    query = QString("UPDATE corpus SET fk_wt_id=%1 WHERE id=%2 ").arg(TrieId).arg(Corpus);
    if (not req.exec(query)) qDebug() << "storeWords : Database Error " << BDD::DatabaseConnection->lastError().text();
  }
  
  static_cast<Tab *>(this->parent()->parent())->answerMessage("number of words:" + QString::number(getWordsCount()) + "/number of unique words:"+ QString::number(getUniqueWordsCount()));
  if (Debug) qDebug() << "storeWords : Fin insertion des mots non normalisés-->Durée de l'insertion : " << QString::number(myTimer.elapsed());

  emit sigTrieFinished();
}

/* OBSOLETE PART BECAUSE JSONFILE TOO BIG TO FIT INTO TABLE
    // copier le contenu du fichier wordtrie dans une table temporaire (wordTrieTemp)
    query="create table if not exists WordTrietemp(jsonvalues json)";
    req.exec(query);
    query="delete from WordTrietemp";
    req.exec(query);
    query=QString("copy WordTrietemp from '%1'").arg(path);
    qDebug() << "storeWords : copy query " << query;
    req.exec(query);
  
    // insérer dans la table finale
    query = "insert into \"WORD_trie\" (wt_name, wt_json) values ('" + trienameword + "', (select jsonvalues from WordTrietemp as jsonval) )";
    qDebug() << "storeWords : insert query " << query;
    if (! req.exec(query))
    { 
      qDebug() << "storeWords : Database Error " << BDD::DatabaseConnection->lastError().text();
      return;
    }
*/


//===================================== Merge word tries
// Let tree be the thread tree, and BigTree the global tree
// this function just updates the root and calls mergeTries
void WordBox::mergeTrie(arbrewordonly tree)
{
  if (not Trie)
  {
    Trie = tree;
    return;
  }
  addSons(tree, Trie);
}

// graft a trie to bigtree and compute new values
// cross recursive

// bigtree has no sons : add sons of tomerge
// bigtree has sons : insert sons of tomerge in sons of bigtree
void WordBox::addSons(arbrewordonly tomerge, arbrewordonly bigtree)
{
    if (not tomerge) return;
    //qDebug() << "addSons ; tomerge " << tomerge->letter << " bigtree " << bigtree->letter;
    updateNode(bigtree, tomerge);
    if (bigtree->sons) insertBros(tomerge->sons, bigtree);
    else bigtree->sons = tomerge->sons;
}

// bro is not member of bigtree->sons => recurse on bro->bros then push bro in front of bigtree->sons
// else recurse on sons and bros (order should not count here)
void WordBox::insertBros(arbrewordonly bro, arbrewordonly bigtree)
{
    if (not bro) return;

    //qDebug() << "insertBros ; bro " << bro->letter << " bigtree " << bigtree->letter;
    arbrewordonly branch;
    if (not (branch = assoc(bro->letter, bigtree->sons)))
    {
        insertBros(bro->bros, bigtree);
        pushBranch(bro, bigtree);
        return;
    }
    addSons(bro, branch);
    insertBros(bro->bros, bigtree);
}

void WordBox::updateNode(arbrewordonly bigtree, arbrewordonly tree)
{
  bigtree->prefix_occurrences += tree->prefix_occurrences;
  bigtree->occurrences += tree->occurrences;
}

arbrewordonly WordBox::assoc(QChar c, arbrewordonly tree)
{
    if (not tree) return NULL ;
    if (tree->letter == c) return tree ;
    return assoc(c, tree->bros) ;
}

// add a new son in front of the list of sons
// we will lose all bros
void WordBox::pushBranch(arbrewordonly son, arbrewordonly bigtree)
{
    if (not son) return;
    arbrewordonly bros = bigtree->sons;
    bigtree->sons = son;
    son->bros = bros;
}

//========================================== PREPROCESSING DES MOTS ========================================

// slot correspondant au signal sigTrieFinished, lancé par processWords ou par storeWords
// dans cette version il n'y a que la lemmatisation (incluant normalisation ?)

void WordBox::computePreprocforms()
{
    QTime time;
    time.start();
    if (Debug) qDebug() << "computePreprocforms";
    
    QSqlQuery query(* DBConnection);
    QString sql;
    
    // si preproc existe deja return ;
    IdPreproc = getIdPreproc();
    if (IdPreproc > 0) return;

    // inserer les valeurs de pretraitement dans la table WORD_preproc
    sql = QString("INSERT INTO \"WORD_preproc\"(paramspreproc, segmenter, lemmatizer,fk_id_corpus) VALUES ('%1', '%2', '%3',%4)").arg(WordFullparams).arg(WordSegmenter).arg(WordLemmatizer).arg(Corpus);
    if (!query.exec(sql))
    {
      qDebug() << "computePreprocforms : Database error = " << query.lastError().text() << " query : " << sql ;
      return;
    }
    IdPreproc = query.lastInsertId().toInt();
    
    if (Debug) qDebug() << "computePreprocforms : ligne de WORD_preproc créée et remplie avec id = " << IdPreproc;
    //2) pour chaque forme, calculer la forme préprocessée avec WORD_preproc
    //remplir les tables WORD_preprocform (avec addition des occs), WORD_relation

    // créer répertoires avec les bonnes permissions (pas de permission sur path, seulement les sous-répertoires)
    QProcess * process = new QProcess;
    QString path = WordOnly::Temp + "/" + QString::number(Corpus) + "_" + getCorpusName(Corpus).replace(" ", "_");

    if (Debug) qDebug() << "computePreprocforms : suppression éventuelle et création du répertoire " << path;
   
    QDir dir(path);
    if (dir.exists(path)) dir.removeRecursively();
    dir.mkpath(path);
    
    process->start("sh",QStringList() << "-c" << QString(" chmod 777 '%1'").arg(path));
    qDebug() << "Process Exit code: " << process->exitCode();
    process->waitForFinished(-1);
    
    QString pathin = path + "/lemma";
    if (Debug) qDebug() << "computePreprocforms : création des répertoires " << pathin << " et " << pathin + "/ids";
    dir.mkpath(pathin + "/ids");

    process->start("sh", QStringList() << "-c" << QString(" chmod 777 '%1'").arg(pathin));
    qDebug() << "Process Exit code: " << process->exitCode();
    process->waitForFinished(-1);

    process->start("sh", QStringList() << "-c" << QString(" chmod 777 '%1/ids'").arg(pathin));
    qDebug() << "Process Exit code: " << process->exitCode();
    process->waitForFinished(-1);

    if (Debug) qDebug() << "computePreprocforms : génération de " << NbThreads << " fichiers dans " << pathin << " et " << pathin + "/ids";

    // table découpée en NbThreads parties, de chaque partie on fait un fichier .ids (identifiants) et un fichier .forms (formes)
    for (int numthread = 0; numthread < NbThreads; numthread++)
    {
      QString nomfile;
      if (numthread < 10) nomfile = "00" + QString::number(numthread);
      else if (numthread < 100) nomfile = "0" + QString::number(numthread);
      else nomfile = QString::number(numthread);

      sql = QString("copy (SELECT id from (select form, id, occs, fk_corpus, ntile(%1) over(ORDER BY id) as chunk FROM \"WORD_form\") as chunkwordfrom where fk_corpus=%2 and chunk=%3) TO  '%4/ids/%5.ids' ").arg(NbThreads).arg(Corpus).arg(numthread+1).arg(pathin).arg(nomfile);
      if (Debug) qDebug() << "computePreprocforms : génération des fichiers *.ids avec " << sql;
      query.exec(sql);
      
      sql = QString("copy (SELECT form from (select form, id, occs, fk_corpus, ntile(%1) over(ORDER BY id) as chunk FROM \"WORD_form\") as chunkwordfrom where fk_corpus=%2 and chunk=%3)  to  '%4/%5.forms'").arg(NbThreads).arg(Corpus).arg(numthread+1).arg(pathin).arg(nomfile);
      if (Debug) qDebug() << "computePreprocforms : génération des fichiers *.forms avec " << sql;
      query.exec(sql);
    }
    
    process->start("sh",QStringList() << "-c" << QString(" chmod -R 777 '%1'").arg(pathin));
    qDebug() << "Process Exit code: " << process->exitCode();
    process->waitForFinished(-1);
    
    QString pathout = path + "/lemma_out";
    dir.mkpath(pathout);

    //ajouter ici les pretraitements desires
    if (WordFullparams=="Latin Normalizer")
    {
        //normalizer de script latin
        // Lancer normalizer : multithread prend le fichier i.forms et génère i.out ; il doit générer un mot par ligne
        if (Debug) qDebug() << "computePreprocforms : normalizerLatin multithread prend le folder des fichiers i.forms et génère les fichiers i.out dans repertoire " << pathout ;
        ArabicNormalizer::normalizeLatincmd(QString("%1/").arg(pathin),QString("%2/").arg(pathout), NbThreads);
    }

    else if (WordFullparams=="IBM Normalizer" && WordLemmatizer=="")
    {
        // Lancer normalizer : multithread prend le fichier i.forms et génère i.out ; il doit générer un mot par ligne
        if (Debug) qDebug() << "computePreprocforms : normalizerIBM multithread prend le folder des fichiers i.forms et génère les fichiers i.out dans repertoire " << pathout ;
        ArabicNormalizer::normalizecmd(QString("%1/").arg(pathin),QString("%2/").arg(pathout), NbThreads);
    }
    else
    {
        // Lancer Khoja : multithread prend le fichier i.forms et génère i.out ; il doit générer un mot par ligne
        if (Debug) qDebug() << "computePreprocforms : Khoja multithread prend le folder des fichiers i.forms et génère les fichiers i.out dans repertoire " << pathout ;
        KhojaStemmer::khojastemmercmd(QString("%1/").arg(pathin),QString("%2/").arg(pathout), NbThreads);
    }
    
    //***************************************************

    // shell : on fusionne i.ids avec i.out ligne par ligne :
    // chaque ligne de i.fin contient : id dans word_form tab + forme préprocessée
    for (int numthread = 0; numthread < NbThreads; numthread++)
    {
        QString nomfile;
        if (numthread < 10) nomfile= "00" + QString::number(numthread);
        else if (numthread < 100) nomfile = "0" + QString::number(numthread);
        else nomfile = QString::number(numthread);
        
        if (Debug) qDebug() << "computePreprocforms : paste  i.ids i.out > i.fin  (delimited with tab) " << NbThreads;

        process->start("sh", QStringList()   << "-c" << QString ("paste '%2/ids/%1.ids' '%3/%1.forms.out' > '%3/%1.fin' " ).arg(nomfile).arg(pathin).arg(pathout));
        process->waitForFinished(-1);
    }

    if (Debug) qDebug() << "computePreprocforms :fusionner tous les fichiers  i.fin dans l’ordre pour avoir un seul fichier out.fin";
    
    process->start("sh", QStringList()   << "-c" << QString ("ls -1 %1/*.fin | sort | while read fn ; do cat \"$fn\" >> %1/out.fin ; done").arg(pathout));
    process->waitForFinished(-1);
    
    //==========================Insertion dans la base WORD_preprocform et WORD_form_preprocform====================
    //==TABLE "WORD_preprocform" (idformpreproc serial NOT NULL,preprocform text,occs integer,prefixoccurences integer
    //==TABLE "WORD_form_preprocform"(idform integer,idpreprocform integer,idcorpus integer,idpreproc integer    )
    //==out.fin contient id de WORD_form et formes lemmatisees
    if (Debug) qDebug() << "computePreprocforms : Debut d'insertion dans la table temporaire tempout du fichier out.fin";
    // table temporaire : id_formebase formepreproc id_formepreproc << id_formebase et formepreproc de out.fin
    
    process->start("sh",QStringList() << "-c" << QString(" chmod -R 777 '%1'").arg(pathout));
    qDebug() << "Process Exit code: " << process->exitCode();
    process->waitForFinished(-1);
    sql = "create table if not exists tempout (idformebase integer, formepreproc text, idformepreproc integer)";
    query.exec(sql);
    sql = "delete from tempout";
    query.exec(sql);
    sql=QString("copy tempout (idformebase , formepreproc) from '%1/out.fin' using delimiters E'\t' ").arg(pathout);
    if (Debug)  qDebug() << "computePreprocforms: créer table temporaire << idformebase, formepreproc de out.fin avec " << sql;
    if(!query.exec(sql))
    {
        qDebug() << "computePreprocforms : Database error= " << query.lastError().text() << " string : " << sql ;
        return;
    }

    //on trie par formepreproc
    //on génère un identifiant par formepreproc
    //on fait un join word_form pour avoir les occurrences
    //6) on remplit la table word_preprocform avec formepreproc et id_formpreproc et occurrences (par la jointure en additionnant les occurrences  où id_formpreproc est le même)
    query.exec("ALTER SEQUENCE \"WORD_preprocform_idformpreproc_seq\" RESTART WITH 1");
    sql = QString(" INSERT INTO \"WORD_preprocform\"( preprocform, occs, fk_idpreproc)"
                  " SELECT tempout.formepreproc, sum(w.occs) as sumocc,%1 "
                  " FROM \"WORD_form\" w, tempout "
                  " WHERE w.id = tempout.idformebase and w.fk_corpus=%2 group by tempout.formepreproc order by sumocc desc").arg(IdPreproc).arg(Corpus);
    qDebug() << "computePreprocforms : inserer WORD_preprocform query " << sql ;
    if (not query.exec(sql))
    {
        qDebug() << "computePreprocforms : Database error= " << query.lastError().text() << " query " << sql ;
        return;
    }
    //7) on remplit la table word_form_preprocform avec id_formebase et id_formepreproc
    sql=QString(" INSERT INTO \"WORD_form_preprocform\"(idform, idpreprocform,fk_idpreproc)"
                " SELECT tempout.idformebase, wp.idformpreproc, %1 "
                " FROM tempout, \"WORD_preprocform\" wp "
                " WHERE tempout.formepreproc = wp.preprocform and wp.fk_idpreproc=%1").arg(IdPreproc);
    if(not query.exec(sql))
    {
        qDebug() << "computePreprocforms : Database error= " << query.lastError().text() << " query " << sql ;
        return;
    }
    qDebug() << "computePreprocforms : inserer WORD_form_preprocform query " << sql ;

    if (Debug) qDebug() << "computePreprocforms : Fin insertion -->Durée de l'insertion des mots lemmatisees : " << QString::number(time.elapsed());
    static_cast<Tab *>(this->parent()->parent())->answerMessage("number of preprocessed words:" + QString::number(getPreprocWordsCount(IdPreproc)) + "/number of unique preprocessed words:"+ QString::number(getUniquePreprocWordsCount(IdPreproc)));
    //Tout ça peut se faire dans une stored procedure.
    //(détruire le fichier out.fin avant)
    // QFile file(QString("%1/out.fin").arg(pathout));
    // file.remove();
}

//================================================ INSERTION DES SEGMENTS ==========================================

/*
Table des segments pour preproc

avoir dans la table la colonne de Word_preproc

Si Word_preproc = 0 alors il s'agit de la totalité des segments

Donc attention, pas de valeur 0 dans Word_preproc

si c'est word_segment preproc on ne le met pas dans corpus
*/

// et peut-être vérifier s'il y a eu un changement de paramètres
// un flag après all finished



// appelé par insertWordSegmentClicked
// récupère les paramètres dans la base de données puis choisit la fonction à exécuter
// peut faire appel à insert words
// noter un problème de synchronisation si NOPREPROC avec computeWordtrie
// le connect est là pour ça ; mais n'est-il pas installé trop tard ?
void WordBox::processSegments(QString script)
{
  if (Debug) qDebug() <<"processSegments : Insert Segment Only in multithread";

  // Première partie : tester la présence de Segtrie dans le corpus
  if (NOPREPROC)
  {
    // tester si segment trie existe déjà dans corpus
    QSqlQuery query(* DBConnection);
    QString sql = QString("SELECT fk_st_id FROM corpus where id=%1").arg(Corpus);
    query.exec(sql);
    if (query.next()) STINCORPUS = not query.value(0).isNull();
    if (Debug) qDebug() << "processSegments : segment trie exists in corpus? " << STINCORPUS;
    if (STINCORPUS) return;
  }
  else
  {  
    QSqlQuery query(* DBConnection);
    QString sql = QString("SELECT st_id FROM \"WORD_preproc\" where id=%1").arg(getIdPreproc());
    query.exec(sql);
    if (query.next()) STINPREPROC = not query.value(0).isNull();
    if (Debug) qDebug() << "processSegments : segment trie exists in preproc? " << STINPREPROC;
    if (STINPREPROC) return;
  }
  
  // Segtrie n'est pas dans le corpus ni plain ni préprocessé
  
  // les segments ont besoin d'un wordtrie dans tous les cas
  connect(this, &WordBox::sigTrieFinished, [=]{if (not TrieSeg) computeSegtrie();});

  // créer wordtrie ; ça déclenchera la suite ;
  if (not (WTINCORPUS or Trie))
  {
    if (Debug) qDebug() << "processSegments : no wordtrie, run computeWordtrie";
    computeWordtrie(script);
    if(Debug) qDebug() << "processSegments : : wordtrie created";
  }
  else emit sigTrieFinished();
}

//========================================== Création du trie des segments
void WordBox::computeSegtrie() // transmettre word_preproc
{ 
  qDebug() << "++++++++++++++++++computeSegtrie :";
  IdPreproc=getIdPreproc();
  //if (not NOPREPROC)
  //{
    // vérifier l'existence de la table preprocform dans Word_preproc sinon la créer
    // requête pour indexer la table preprocform where fk_word_preproc = word_preproc_id sur les formes
    if (IdPreproc == 0)
    {
      if (Debug) qDebug() << "computeSegtrie : no nopreproc no Word_preproc, run computePreprocforms";
      computePreprocforms();
      if(Debug) qDebug() << "computeSegtrie : tables preproc, preprocform and from_preprocform created";
    }

    qDebug() << "computeSegtrie : IdPreproc" << IdPreproc;
    QString sql;
    
    Form_Preprocform.reserve(SIZE); // see machine.h
    QSqlQuery query(* DBConnection);
    query.exec("CREATE INDEX idform_idx ON \"WORD_form_preprocform\" (idform)");
    query.exec("CREATE INDEX idpreprocform_idx ON \"WORD_form_preprocform\" (idpreprocform)");
    
    // sélectionner tous les idform de word_form_preprocform
    if (not query.exec(sql = QString("SELECT \"WORD_form_preprocform\".idpreprocform FROM \"WORD_form\", \"WORD_form_preprocform\", \"WORD_preprocform\" WHERE \"WORD_form_preprocform\".idform = \"WORD_form\".id AND \"WORD_form_preprocform\".idpreprocform = \"WORD_preprocform\".idformpreproc AND \"WORD_form_preprocform\".fk_idpreproc =\"WORD_preprocform\".fk_idpreproc AND \"WORD_preprocform\".fk_idpreproc=%1 AND \"WORD_form\".fk_corpus=%2 order by \"WORD_form_preprocform\".idform;").arg(IdPreproc).arg(Corpus)))
    
    qDebug() << "computeSegtrie : Database error = " << query.lastError().text();
    // if (Debug) qDebug() << "computeSegtrie : query " << sql ;

    while (query.next()) Form_Preprocform.append(query.value(0).toString().toLong());
    //qDebug() <<"________________" <<  Form_Preprocform;

    //for (int j=0; j<3; j++) qDebug()  << "computeSegtrie : Database value of Form_Preprocform: " << Form_Preprocform.at(j);
    
    // pour Word2Vec et Glove
    // Preprocform.reserve(200000);
    // query.exec("CREATE UNIQUE INDEX idformpreproc_idx ON \"WORD_preprocform\" (idformpreproc)");
    // QStringList preprocforms;
    // if (not query.exec(sql = QString("SELECT \"WORD_preprocform\".preprocform FROM \"WORD_form\", \"WORD_form_preprocform\", \"WORD_preprocform\" WHERE \"WORD_form_preprocform\".idform = \"WORD_form\".id AND \"WORD_form_preprocform\".idpreprocform = \"WORD_preprocform\".idformpreproc AND \"WORD_preprocform\".fk_idpreproc=%1 AND \"WORD_form\".fk_corpus=%2 order by \"WORD_form_preprocform\".idform;").arg(IdPreproc).arg(Corpus)))

    // while (query.next()) Preprocform.append(query.value(0).toString());
  //}
  
  // mettre des valeurs aux paramètres statiques de Segments
  Segments::NOPREPROC = NOPREPROC;
  Segments::IDCORPUS = Corpus;
  Segments::IDPREPROC = IdPreproc;
  //recuperer le trie de mots s'il n'existe pas charger la du fichier
  if (not Trie)
  {
      QSqlQuery query(* DBConnection);
      if (not query.exec(QString("SELECT \"WORD_trie\".wt_name FROM \"WORD_trie\",corpus  WHERE \"WORD_trie\".\"wt_id\" = corpus.\"fk_wt_id\" AND corpus.id =%1;").arg(Corpus)))
          qDebug() << "computeSegtrie : Database error = " << query.lastError().text();
      query.next();
      WordOnly *word=new WordOnly();
      Trie =word->jsonToSegmentationTrie(query.value(0).toString());
  }
  
  //QString path = WordOnly::Temp + "/segtodb/";
  
  // Pour Word2Vec et Glove
  // QString path = WordOnly::W2V + "/";

  QString path = WordOnly::Temp + "/" + QString::number(Corpus) + "_" + getCorpusName(Corpus).replace(" ", "_")+ "/segtodb/";

  if (Debug) qDebug() << "computeSegtrie : suppression éventuelle et création du répertoire " << path;

  QDir dir(path);
  if (dir.exists(path)) dir.removeRecursively();
  dir.mkpath(path);

  // calculer le segtrie avec la méthode actuelle en multithread (threads créés ici)

  int chuncksize = DocumentList.size() / NbThreads;
  if (Debug) qDebug() << "computeSegtrie : chuncksize " << chuncksize;
  
  if (Debug) qDebug() << "computeSegtrie : Début de la construction des arbres segtrie";

  for (int i = 0 ; i < NbThreads - 1 ; i++)
  {
    QThread * thread = new QThread ;
    Segmentonly = new Segments(Trie, i, DocumentList.mid(i * chuncksize, chuncksize), &Form_Preprocform, &Preprocform, path);
    Segmentonly->moveToThread(thread) ;
    connect(thread, SIGNAL(started()), Segmentonly, SLOT(process()));
    connect(Segmentonly, SIGNAL(sigsegfinished()), Segmentonly, SLOT(deleteLater())) ;
    connect(Segmentonly, SIGNAL(sigsegfinished()), thread, SLOT(quit()));
    connect(Segmentonly, SIGNAL(sigsegfinished()), this, SLOT(onSegFinishedWork()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start() ;
  }
  QThread * thread = new QThread ;
  Segmentonly = new Segments(Trie, NbThreads - 1, DocumentList.mid((NbThreads - 1) * chuncksize), &Form_Preprocform, &Preprocform, path);
  Segmentonly->moveToThread(thread) ;
  connect(thread, SIGNAL(started()), Segmentonly, SLOT(process()));
  connect(Segmentonly, SIGNAL(sigsegfinished()), Segmentonly, SLOT(deleteLater())) ;
  connect(Segmentonly, SIGNAL(sigsegfinished()), thread, SLOT(quit()));
  connect(Segmentonly, SIGNAL(sigsegfinished()), this, SLOT(onSegFinishedWork()));
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
  thread->start() ;
  
  connect(this, SIGNAL(sigSegAllFinished()), this, SLOT(onAllSegFinished()));
}

// slot déclenché quand un thread a fini
void WordBox::onSegFinishedWork()
{
  static int finishedthreadnb = 0;
  finishedthreadnb++;
  if (Debug) qDebug() << "onSegFinishedWork : " << finishedthreadnb;
/*
    QMutex mutex;
    if (Debug) qDebug() << "onSegFinishedWork : Debut merge " << finishedthreadnb;
    mutex.lock();
    mergeTrie(trie);
    if (Debug) qDebug() << "onSegFinishedWork : Fin merge " << finishedthreadnb;
    mutex.unlock();
*/
  if (finishedthreadnb == NbThreads) emit sigSegAllFinished();
}

// slot déclenché quand tous les threads sont finis pour terminer le travail
void WordBox::onAllSegFinished()
{
  // we should free the Trie here
  if (Trie) deleteTrie(Trie);
  if (Debug) qDebug() << "onAllSegFinished : Fin de generation des fichiers de segments en multithread";
  storeSegments();
}

// libère la mémoire d'un trie
void WordBox::deleteTrie(arbrewordonly trie)
{
  if (not trie) return ;
  deleteTrie(trie->sons) ;
  deleteTrie(trie->bros) ;
  delete trie ;
}

void WordBox::storeSegments()
{
  /*if (StoreJsSt->isChecked())
  {

    Segmentonly = new Segments();

    if (Debug) qDebug() << "storeSegments : Debut de l'insertion dans la table Word_segment, corpus " << Corpus << "/Idpreproc:" << IdPreproc ;
    Segmentonly->sqlWriteBatchSegment(Corpus, TrieSeg, IdPreproc);
    if (Debug) qDebug() << "storeSegments : Fin de l'insertion dans la table Word_segment";

    // print seg json trie file
    QString trienameseg = generateFile(Corpus, SEG);
    if (Debug) qDebug() << "storeSegments : Impression du segment json file " << trienameseg;
  
    // copy from json trie file into build
    QFile outfile(trienameseg);
    QString path = QFileInfo(outfile).absoluteFilePath();
 
    QSqlQuery req(* DBConnection);
    QString query;
    query = QString("INSERT INTO \"WORD_trieseg\"(st_name) VALUES ('%1');").arg(path); // ADELLE : verify
    req.exec(query);
    
    // mise à jour de corpus
    TrieId = req.lastInsertId().toInt();    
    query = QString("UPDATE corpus SET fk_st_id=%1 WHERE id=%2 ").arg(TrieId).arg(Corpus);
    if (not req.exec(query)) qDebug() << "storeSegments : Database Error " << BDD::DatabaseConnection->lastError().text();

}*/
  // ici lire les fichiers csv pour fabriquer la table des segments
      QProcess *process = new QProcess();
      QString corpuspath=WordOnly::Temp + "/" + QString::number(Corpus) + "_" + getCorpusName(Corpus).replace(" ", "_");
      QString path = corpuspath+ "/segtodb";
      if (Debug) qDebug() << "storeSegments :fusionner tous les fichiers " << path << "/*.csv pour avoir un seul fichier segstodb.csv";

      process->start("sh", QStringList()   << "-c" << QString ("ls -1 %1/*.csv | while read fn ; do cat \"$fn\" >> %1/segstodb.csv ; done").arg(path));
      process->waitForFinished(-1);
      qDebug() << "Process Exit code: " << process->exitCode();

      QSqlQuery query(*DBConnection);
      QString sql;
      //creation du table temporaire pour supprimer les duplications
      sql = "create table if not exists tempdupout (dupseg integer[], duplen integer, dupsum integer)";
      query.exec(sql);
      sql = "delete from tempdupout";
      query.exec(sql);
      sql=QString("copy tempdupout from '%1/segstodb.csv' using delimiters ';' ").arg(path);
      if (Debug)  qDebug() << "storeSegments: créer table temporaire du fichier " << path << "/segstodb.csv avec " << sql;
      if(!query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " string : " << sql ;
          return;
      }

      process->start("sh",QStringList() << "-c" << QString(" chmod -R 777 '%1'").arg(path));
      qDebug() << "Process Exit code: " << process->exitCode();
      process->waitForFinished(-1);
      //on sauvegarde dans un fichier dictinct (segment,len,sum,count over meme len et sum)
      //on aura les segments distincts non dupliques
      sql = QString(" copy (select distinct dupseg, count , %1,%2 from"
                    " (SELECT *, count(*) OVER (PARTITION BY duplen,dupsum) AS count FROM tempdupout) "
                    " as tempdupoutWithCount) to '%3/nondupseg.csv';").arg(Corpus).arg(IdPreproc).arg(path);
      qDebug() << "storeSegments : insererles segments non dupliques dans " << path << "/nondupseg.csv " << sql ;
      if (not query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " query " << sql ;
          return;
      }
      //on remplit la table word_segment avec les segments distincts de nondupseg.csv et fk_idcorpus,fk_idpreproc
      sql=QString("COPY \"WORD_segment\" ( segment,occs, fk_idcorpus,fk_idpreproc) from '%1/nondupseg.csv'").arg(path);
      if(not query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " query " << sql ;
          return;
      }
      qDebug() << "storeSegments : inserer WORD_segment " << sql ;

      //Nettoyer WORD_segment de segments ne contenant que 0
      query.exec("select idformpreproc from \"WORD_preprocform\" where trim(preprocform)='0'");
      if (query.next())
          if (Debug) qDebug() << "id of 0 :" << query.value(0).toString();
      int idx=query.value(0).toInt();
      sql=QString("delete from \"WORD_segment\" where '%1' = ALL(segment)").arg(idx);
      if(not query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " query " << sql ;
          return;
      }
      qDebug() << "storeSegments : Nettoyer WORD_segment de segments ne contenant que 0 " << sql;

      //Nettoyer WORD_segment de segments ne contenant que 1
      query.exec("select idformpreproc from \"WORD_preprocform\" where trim(preprocform)='1'");
      if (query.next())
          if (Debug) qDebug() << "id of 1 :" << query.value(0).toString();
      int id1=query.value(0).toInt();
      sql=QString("delete from \"WORD_segment\" where '%1' = ALL(segment)").arg(id1);
      if(not query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " query " << sql ;
          return;
      }
      qDebug() << "storeSegments : Nettoyer WORD_segment de segments ne contenant que 1 " << sql;

      //Nettoyer WORD_segment de segments ne contenant que 1 et 0
      sql=QString("delete from \"WORD_segment\" where '{%1, %2}' @> (segment)").arg(idx).arg(id1);
      if(not query.exec(sql))
      {
          qDebug() << "storeSegments : Database error= " << query.lastError().text() << " query " << sql ;
          return;
      }
      qDebug() << "storeSegments : Nettoyer WORD_segment de segments ne contenant que 1 et x " << sql;
 
  int nbsegments = getSegments(IdPreproc);
  static_cast<Tab *>(this->parent()->parent())->answerMessage("number of segments inserted: "+QString::number(nbsegments));
  if (Debug) qDebug() << "Fin de l'insertion dans la table Word_segment";
  //delete all tmp seg files
  process->start("sh", QStringList()   << "-c" << QString ("rm -r %1").arg(corpuspath));
  qDebug() << "Process Exit code: " << process->exitCode();
  process->waitForFinished(-1);
  process->deleteLater();
  if (Debug) qDebug()<< "Fin de la construction de tous les arbres trie en:" << QString::number(myTimer.elapsed());
}

//===========================Merge Segment Trie=======================================
// Let tree be the thread tree, and BigTree the global tree
// this function just updates the root and calls mergeTries
void WordBox::mergeTrie(arbresegmentonly tree)
{
  if (not TrieSeg)
  {
    TrieSeg = tree;
    return;
  }
  addSons(tree, TrieSeg);
}

// graft a trie to bigtree and compute new values
// cross recursive

// bigtree has no sons : add sons of tomerge
// bigtree has sons : insert sons of tomerge in sons of bigtree
void WordBox::addSons(arbresegmentonly tomerge, arbresegmentonly bigtree)
{
    if (not tomerge) return;
    //qDebug() << "addSons ; tomerge " << tomerge->letter << " bigtree " << bigtree->letter;
    updateNode(bigtree, tomerge);
    if (bigtree->sons) insertBros(tomerge->sons, bigtree);
    else bigtree->sons = tomerge->sons;
}

// bro is not member of bigtree->sons => recurse on bro->bros then push bro in front of bigtree->sons
// else recurse on sons and bros (order should not count here)
void WordBox::insertBros(arbresegmentonly bro, arbresegmentonly bigtree)
{
    if (not bro) return;

    //qDebug() << "insertBros ; bro " << bro->letter << " bigtree " << bigtree->letter;
    arbresegmentonly branch;
    if (not (branch = assoc(bro->wordid, bigtree->sons)))
    {
        insertBros(bro->bros, bigtree);
        pushBranch(bro, bigtree);
        return;
    }
    addSons(bro, branch);
    insertBros(bro->bros, bigtree);
}

void WordBox::updateNode(arbresegmentonly bigtree, arbresegmentonly tree)
{
  bigtree->occurrences += tree->occurrences;
}

arbresegmentonly WordBox::assoc(int m, arbresegmentonly tree)
{
    if (not tree) return NULL ;

    if (tree->wordid == m)
    {
        tree->occurrences++;
        return tree ;
    }
    return assoc(m, tree->bros) ;
}

// add a new son in front of the list of sons
// we will lose all bros
void WordBox::pushBranch(arbresegmentonly son, arbresegmentonly bigtree)
{
    if (not son) return;
    arbresegmentonly bros = bigtree->sons;
    bigtree->sons = son;
    son->bros = bros;
}

//========================== get word and segments count===============================
// should be modified
id WordBox::getWords(int corpus, QString options, int word_trie_id = 0)
{
    if (Debug) qDebug() << "WordBox::getWords";
    QSqlQuery req(* DBConnection) ;
    if (WordTableFlag)
    {
        WordsTable->clear();
        WordsTable->setRowCount(0);
        if (word_trie_id > 0)
            req.exec(QString("select * from \"WORD_form\" where  fk_corpus=%1 and wt_params='%2' and wt_id=%3").arg(corpus).arg(options).arg(word_trie_id)) ;
        else
            req.exec(QString("select * from \"WORD_form\" where  fk_corpus=%1 and wt_params='%2'").arg(corpus).arg(options)) ;

        int row;
        while (req.next())
        {
            QTableWidgetItem * Word = new QTableWidgetItem(req.value(0).toString()) ;
            QTableWidgetItem * frequency = new QTableWidgetItem((req.value(1)).toString()) ;
            row = WordsTable->rowCount();
            WordsTable->insertRow(row);
            WordsTable->setItem(row, 0, Word);
            WordsTable->setItem(row, 1, frequency);
        }
        if (Debug) qDebug() << "number of words is :" << row+1;
        return row+1;
    }
    else
    {
        req.exec(QString("select count(*) from \"WORD_form\" where  fk_corpus=%1 and wt_params='%2'").arg(corpus).arg(options)) ;
        if (req.next()) qDebug() << "number of words is :" << req.value(0).toString();
        return req.value(0).toInt();
    }

}

id WordBox::getUniqueWordsCount()
{
    if (Debug) qDebug() << "getUniqueWordsCount:: for Corpus " << Corpus;
    QSqlQuery req(* DBConnection) ;
    req.exec(QString("SELECT count(*) FROM \"WORD_form\" where fk_corpus=%1;").arg(Corpus));
    if (req.next()) qDebug() << "number of unique words is :" << req.value(0).toString();
    return req.value(0).toInt();
}

id WordBox::getWordsCount()
{
    if (Debug) qDebug() << "getWordsCount:: for Corpus " << Corpus;
    QSqlQuery req(* DBConnection) ;
    req.exec(QString("SELECT Sum(occs) FROM \"WORD_form\" where fk_corpus=%1;").arg(Corpus));
    if (req.next()) qDebug() << "number of words is :" << req.value(0).toString();
    return req.value(0).toInt();
}

id WordBox::getUniquePreprocWordsCount(int idpreproc)
{
    if (Debug) qDebug() << "getUniqueWordsCount:: for Corpus " << Corpus;
    QSqlQuery req(* DBConnection) ;
    req.exec(QString("SELECT count(idformpreproc) FROM \"WORD_preprocform\" where fk_idpreproc=%1;").arg(idpreproc));
    if (req.next()) qDebug() << "number of unique words is :" << req.value(0).toString();
    return req.value(0).toInt();
}

id WordBox::getPreprocWordsCount(int idpreproc)
{
    if (Debug) qDebug() << "getUniqueWordsCount:: for Corpus " << Corpus;
    QSqlQuery req(* DBConnection) ;
    req.exec(QString("SELECT sum(occs) FROM \"WORD_preprocform\" where fk_idpreproc=%1;").arg(idpreproc));
    if (req.next()) qDebug() << "number of unique words is :" << req.value(0).toString();
    return req.value(0).toInt();
}

id WordBox::getSegments(int idpreproc)
{
    if (Debug) qDebug() << "getSegments:" << idpreproc;
    QSqlQuery req(*DBConnection) ;
    req.exec(QString("select count(*) from \"WORD_segment\" where  fk_idpreproc=%1;").arg(idpreproc)) ;
    if (req.next())
        if (Debug) qDebug() << "number of segments is :" << req.value(0).toString();
    return req.value(0).toInt();
}

// =============================== Impression des fichiers temporaires

// imprime le json dans un fichier
// type = WORD ou SEG
QString WordBox::generateFile(int corpusid, bool type, bool inverse)
{
  QString filepath = generateFilePath(corpusid, type, inverse);
  if (Debug) qDebug() << "generateFile : " << filepath;
  QFile outfile(filepath);
  outfile.open(QIODevice::WriteOnly|QIODevice::Truncate) ;

  QTextStream stream(&outfile) ;
  if (type) Segmentonly->printJsonSegments(TrieSeg, stream);
  else Wordonly->printJsonWords(Trie, stream);
  
  outfile.setPermissions(QFileDevice::ReadOther|QFileDevice::WriteOther|QFileDevice::ReadGroup|QFileDevice::WriteGroup|QFileDevice::ReadUser|QFileDevice::ReadOwner|QFileDevice::WriteOwner);
  outfile.close();
  return filepath;
}

QString WordBox::generateFilePath(int corpusid, bool type, bool inverse)
{
  QString path = TrieResources + QString::number(corpusid) + "_" + getCorpusName(corpusid).replace(" ", "_") + "/";

  if (not QDir().exists(path)) QDir().mkpath(path);
  
  QFile file(QString(path + (inverse ? "inverse_" : "") + QDate::currentDate().toString().replace(" ", "_") + (type ? ".seg" : ".word") + "trie")) ;
  return QFileInfo(file).absoluteFilePath();
}

QString WordBox::getCorpusName(int corpusid)
{
    QSqlQuery req(* DBConnection);

    req.exec(QString("select nom_corpus from corpus where id = %1").arg(corpusid));
    req.next();

    return req.value(0).toString();
}

QString WordBox::getCorpusScript(int corpusid)
{
    QSqlQuery req(* DBConnection);

    req.exec(QString("select script_corpus from corpus where id = %1").arg(corpusid));
    req.next();

    return req.value(0).toString();
}

int WordBox::getIdPreproc()
{
    QSqlQuery query(* DBConnection);
    int idpreproc = 0;
    //si preproc existe deja return
    query.exec(QString("SELECT idpreproc FROM \"WORD_preproc\" where paramspreproc='%1' and segmenter='%2' and  lemmatizer='%3' and fk_id_corpus=%4;").arg(WordFullparams).arg(WordSegmenter).arg(WordLemmatizer).arg(Corpus));
    if(query.next())
    {
        idpreproc = query.value(0).toInt();
        qDebug() << "ComputePreprocForms: Preproc existe dans la base:" << QString::number(idpreproc);
    }
    return idpreproc;
}
