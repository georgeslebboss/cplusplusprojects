#ifndef PATTERNS_H
#define PATTERNS_H
#include <QtWidgets>
#include "../fromInterface.h"
#include "PreprocessingBox/arabicnormalizer.h"
#include<iostream>

/*typedef struct jokerelement
{
    int  key ;  //id du mot normal
    int  segmentIndex; //identifiant du segment contenant ce mot normal
    int position; //position de ce mot normal dans le pattern
} JokerElement;*/

// l'élément de l'arbre préfixe des patrons
struct node_pattern
{
    id m_id;                          // identifiant de la marque ou JokerId
    int sortie = 0;                   // nombre de sorties par ce noeud
    //val occurrences;                // nombre de patterns comprenant ce noeud on n'a plus besoin c calcule dans sortie
    QMap<id, val> jokers;             // identifiants et occurrences des jokers
    int p_id;                         // identifiant du pattern, servira après le merge
    node_pattern * sons ;
    node_pattern * bros ;
} ;

typedef node_pattern * arbre_pattern ;

class Patterns: public QObject
{
    Q_OBJECT

public:
    Patterns(int,int, int, int, int, id, id, QMap<id, val> *, int, QVector<QVector<id> >, bool, id, id, QString);
    ~Patterns() {}
    
    // variables héritées de PatternBox
    int CorpusID;
    int PreprocCorpusID;
    int SeuilMarque ;           // fréquence minimale pour définir une marque
    int JokerLength ;
    int SeuilPattern ;          // fréquence minimale pour conserver un patron
    int JokerMinId;             // identifiant du premier mot qui n'est pas une marque
    id  JokerId;                // l'identifiant du joker
    bool IsBigCorpus;
    id Id0; //identifinat des mots etrangers normalises
    id Id1; //identifiant des nombres
    QString Path;
    QMap<id, val> * Nopattern;  // va contenir le nombre de fois que id est loin de tout pattern

    int NumThread;
    QVector<QVector<id> > Segment_PreprocForms;
    
    QSqlDatabase * pdbConnection=BDD::DatabaseConnection;
    
    arbre_pattern Trie;

    // première phase
    QList<QVector<id> > cutPrepatterns(QVector<id>);
    QList<QVector<id> > beginWithMarker(QVector<id>, QList<QVector<id> >, QVector<id>, int = 0);
    int getNextMarker(QVector<id>, int);
    QList<QVector<id> > endPrepatterns(QVector<id>, QList<QVector<id> >, QVector<id>, int);
    void assignJoker(QVector<id>);
    bool noJoker(QVector<id>);
    
    // deuxième phase
    void insertPatterns(QVector<id>);
    void insertEndingPatterns(QVector<id>);
    bool goodPattern(QVector<id>);
    QVector<id> getNextPattern(QVector<id>);
    QVector<id> getNextEndingPattern(QVector<id>);
    void insertP(QVector<id>, arbre_pattern, int = 0, QVector<id> = QVector<id>());
    arbre_pattern createBranch(QVector<id>, arbre_pattern, int, QVector<id>);
    int getJokerSize(QVector<id>, int);
    
    // l'identifiant de la racine est 0 pour le mot et le pattern
    arbre_pattern createNode(id = 0, bool = false, arbre_pattern = NULL, arbre_pattern = NULL);
    arbre_pattern createJokerNode(QVector<int>, bool, int = 0, int = -1, arbre_pattern = NULL, arbre_pattern = NULL);
    
    void addJokers(QVector<id>, arbre_pattern);
    void addJoker(id, arbre_pattern);
    
    // bas niveau
    bool isJoker(id);


protected:
    int Debug = 0;
    //int PatternId=1;
       
    arbre_pattern assoc(id, arbre_pattern);
    void insertJokers(arbre_pattern , QVector<id>, QTextStream &, QTextStream &);
    void deleteTrie(arbre_pattern);
    QString         convertPatterntoSet(QVector<int> );
    QString getCorpusName(int);


public slots:
    void process() ;
signals:
    void finishedPattern(node_pattern *);
    void finishedPattern(node_pattern *, QMap<id, QMap<id, val> > *);
    void finishedPattern();

} ;

#endif // PATTERNS_H
