#include "word.h"
#include "thread"
int Words::Identifiant=1;
// =============================================== CONSTRUCTEURS =======================================
// La construction de l'objet Words consiste à établir des paramètres


Words::Words(){}

Words::Words(int corpusid, QString language, QVector<bool> norm_options)
{
    setCorpusId(corpusid);
    setDocList(getCorpusDocumentsList(corpusid));
    setLanguage(language);
    setLanguageParam(norm_options);
}

Words::Words
(int corpusid, QString language, QVector<bool> norm_options, QString segmenter, QString lemmatizer)
{
    qDebug() << "Words";
    setCorpusId(corpusid);
    setDocList(getCorpusDocumentsList(corpusid));
    setLanguage(language);
    setLanguageParam(norm_options);
    setSegmenter(segmenter);
    setLemmatizer(lemmatizer);
}

Words::~Words(){}


// ======== Méthodes utilisées par le constructeur pour modifier les paramètres

void Words::setCorpusId(int corpusid)
{
    CorpusId = corpusid;
    if (Debug) qDebug() << "Words:: CorpusId set to " << CorpusId;
}

void Words::setDocList(QVector<MyMap> documents) {Documents = documents ;}

void Words::setLanguage(QString language)
{
    Language = language;
    if (Debug) qDebug() << "Words:: language set to " << Language;
}

void Words::setLanguageParam(QVector<bool> norm_options) {NormOptions = norm_options ;}

void Words::setSegmenter(QString segmenter)
{
    Segmenter = segmenter;
    if (Debug) qDebug() << "Words:: segmenter set to " << segmenter;
}

void Words::setLemmatizer(QString lemmatizer)
{
    Lemmatizer = lemmatizer;
    if (Debug) qDebug() << "Words:: stemmer set to " << lemmatizer;
}

// ============ Méthodes utilisées pour accéder aux paramètres

int Words::getCorpusId() {return CorpusId;}

QString Words::getLanguage() {return Language ;}

QVector<bool> Words::getLanguageParam() {return NormOptions ;}

QString Words::getSegmenter() {return Segmenter;}

QString Words::getLemmatizer() {return Lemmatizer;}


int Words::getTrieId() {return TrieId;}

// utilisée par createTries()

QVector<MyMap> Words::getDocList() {return Documents ;}


// ======================================= CONSTRUCTION DES TRIE ==================================

// Méthode utilisée uniquement par WordBox::insertWordSegmentClicked() dans word_box.cpp
// La construction en parallèle des deux arbres Trie permet de gagner du temps
// (évite de relire deux fois les fichiers)

arbreword Words::createTries(bool savejsontrie, bool savewords, bool exportjsontrietofile)
{
    Timer.start();
    // trie ID est calculer pour le sauvegarder dans les deux tables word_form et word_trie
    QSqlQuery querygettrieid(* DbConnection);
    QString querygettrieid_sql = "SELECT MAX(trieid) + 1 AS trie_id FROM ((SELECT wt_id AS trieid FROM \"WORD_form\" ORDER BY wt_id DESC LIMIT 1) UNION (SELECT wt_id AS trieid FROM \"WORD_trie\" ORDER BY wt_id DESC LIMIT 1)) AS TMP_TABLE";
    querygettrieid.exec(querygettrieid_sql);
    querygettrieid.first();
    TrieId = querygettrieid.value(0).toInt();
    if (TrieId == 0)
        TrieId = 1;
    qDebug() << "trie_id: " << TrieId;
    // sauvegarder les parametres de pretraitement afin d'ajouter dans BDD
    //QString options; // = "0000000" ;
    if (NormOptions.size() > 0)
    {
       for (int c = 0 ; c < NormOptions.size() ; c++)
       {
          if (NormOptions[c]) Options = Options + "1" ;
          else Options = Options + "0" ;
       }
    }
    //ajouter segmenter et lemmatizer aux options
    //add segmenter and lemmatizer params
    if (Segmenter!="") Options=Options+","+Segmenter;
    if (Lemmatizer!="") Options=Options+","+Lemmatizer;

  qDebug() << "Début de la construction des arbres trie";

  arbreword trie = createNode((QChar) '/', false) ;
  trie->prefix_occurrences = 0; // pour l'ajustement du calcule

  QString path ;
  QFile outfile("/tmp/segmentsinput.csv");
  path = QFileInfo(outfile).absoluteFilePath();
  if (not outfile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
  {
    qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfile.fileName() << "en ecriture." << endl ;
  }
  QTextStream outsql(&outfile);
  outsql.setCodec("UTF-8");

  QVector<id> root;
  arbresegment trieseg = createNode(outsql,0,0,root) ;

  QVector<MyMap> doclist = getDocList() ;
  qDebug() << "doclist size:" << doclist.size() ;

  // Phase 1 : génération des arbres
  // c'est ici que devrait s'insérer le multithread pour diminuer le temps d'exécution
  NbSegments=0;

  for (int i = 0 ; i <doclist.size() ; i++)
  {
    QString file = QUrl(doclist.at(i).first).toLocalFile() ;
    addTokens(trie, trieseg, file, outsql) ;
    if (Debug) qDebug() << "file: " << file << " - nb: " << i ;
  }

  outfile.close();

  qDebug() << "nb de segments:" << NbSegments;
  qDebug()<< "Fin de la construction des arbres trie : ";

  // for Debug
  // afficherArbre(trie);

  // Phase 2 : écriture dans la base de données
  // les mots sont sauvés dans la base de données (pour recherche des marques + identifiants)
  // les arbres sont sauvés sous format json (avec les identifiants pour les mots)
  // le if (savewords) est destiné à gagner du temps pendant la phase de débug
  // La fonction copy est utilisée pour accélerer les opérations
  
/*
ATTENTION : les fichiers à copier doivent être dans la même partition que le serveur PostgresQL
si par hasard /tmp n'est pas dans la même partition que le serveur, il y aura une erreur à la lecture du fichier

Pour ne pas avoir à donner à l'owner de la bdd le trait superuser, il faudra :
  
1. Définir une fonction possédée par postgres lors de l'initialisation de la base de données

CREATE FUNCTION import_segments(file TEXT)
RETURNS VOID
  AS $BODY$
    COPY WORD_segment (id, occs, fk_corpus, segment, wt_id, wt_params) FROM 'file' USING delimiters ';' with ENCODING 'UTF-8' ;
  $BODY$;

LANGUAGE SQL VOLATILE SECURITY DEFINER
SET search_path = public, pg_temp;

Donner les droits au propriétaire de la base de données (fabriquer une catégorie d'utilisateurs ?)


1. initDB est à modifier :

 si la base n'existe pas,
   demander "êtes vous administrateur du serveur PostgresQL ?"
   si oui,
     demander le mot de passe
     créer la base
     proposer la création des fonctions d'importation
     "Pour accélérer le traitement, vous pouvez faire exécuter une partie des opérations par le serveur PostgresQL ; le souhaitez-vous ?"
     Ajouter : "attention, si votre répertoire /tmp ne se trouve pas sur la même partition que $PSQLPATH$ ce choix provoquera une erreur en lecture"
       si acceptées, les créer et mettre le flag à true
   sinon, dire "demandez à votre administrateur la création de la base de données ... avec ... comme propriétaire et revenez"

 si elle existe, 
   demander "êtes vous administrateur du serveur PostgresQL ?"
   si oui,
     proposer la création des fonctions d'importation
      si acceptées,
        demander le mot de passe
        mettre le flag à true
        continuer avec le script actuel
      sinon, mettre le flag à false

      
2. Dans le code, mettre au choix en fonction du flag


if flag
  SELECT import_segments(path+file)
else 
  insert avec batch

*/

  if (savewords)
  {
    qDebug() << "Début de l'insertion dans la table WORD_form" ;
    sqlWriteBatchWord(CorpusId, trie, TrieId, Options);
    qDebug() << "Fin de l'insertion dans la table WORD_form" ;
    qDebug() << "Début de l'insertion dans la table WORD_segment" ;
    //sqlWriteBatchSegment(CorpusId, TrieId, trieseg, Options);
    QString sql
      =
        QString("COPY \"WORD_segment\" (id, occs, fk_corpus,segment, wt_id, wt_params) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path) ;
   // Timer.start();
    QSqlQuery req (* DbConnection) ;
    if (!req.exec(sql)) qDebug() << "Error= " << req.lastError().text();
    qDebug() << "Fin de l'insertion dans la table WORD_segment" ;
  }



  if (savejsontrie)
  {
     QString json = createJsonTrie(trie) ;
     QString jsonseg = "";
     if (saveJsonSegmentsTrie)
         createJsonTrie(trieseg, true);
     // pour débugger :
     // QString json = createJsonTrie(trie, true) ;

     // pour débugger, génère un fichier avec le même contenu
     if (exportjsontrietofile)
     {
         generateFile(CorpusId, Options, false, json, ".wordtrie") ;
         if (saveJsonSegmentsTrie)
             generateFile(CorpusId, Options, false, jsonseg, ".arbreword");
     }
//***** here to change options by Options aand also trieid by
     QString triename =  generateFilename(CorpusId, Options, false) + QDate::currentDate().toString() + ".wordtrie" ;
     QString trienameseg = "";
     if (saveJsonSegmentsTrie)
         trienameseg = generateFilename(CorpusId, Options, false) + QDate::currentDate().toString() + ".arbreword" ;
     QSqlQuery req(* DbConnection);
     QString query;
     query = "insert into \"WORD_trie\" (wt_id, wt_name, wt_params, wt_json, corpus_id) values ('" + QString::number(TrieId) + "', '" + triename + "', '" + Options + "', '" + json + "', " + QString::number(CorpusId) + ")";
     //qDebug() << query;
     bool result = req.exec(query);
     if (! result)
       qDebug() <<"Database Error " << BDD::DatabaseConnection->lastError().text();
     if (saveJsonSegmentsTrie) {
         query= "insert into \"WORD_trieseg\" (st_id, st_name, st_params, st_json, corpus_id) values ('" + QString::number(TrieId) + "', '" + trienameseg + "', '" + Options + "', '" + jsonseg + "', " + QString::number(CorpusId) + ")";
         //qDebug() << query;
         result = req.exec(query);
         if (! result)
           qDebug() <<"Database Error " << BDD::DatabaseConnection->lastError().text();
     }
     // test de l'arbre suffixe
     arbreword suffixtrie = createSuffixWordsTrie(trie);
     json = createJsonTrie(suffixtrie);
     // pour débugger :
     // json = createJsonTrie(suffixtrie, true) ;

     qDebug() << "trie_id: " << TrieId << " " << QString::number(TrieId);

     // sauvegarder suffixtrie dans la base de donnee
     QSqlQuery querysavesuffixtrie(* DbConnection);
     QString querysavesuffixtrie_sql = "UPDATE \"WORD_trie\" set wt_json_inverse = '" + json + "' WHERE wt_id = " + QString::number(TrieId);
     bool savesuffixtrie = querysavesuffixtrie.exec(querysavesuffixtrie_sql);
     //qDebug() << querysavesuffixtrie_sql;
     if (!savesuffixtrie)
         qDebug() << "Database Error (querysavesuffixtrie) " << BDD::DatabaseConnection->lastError().text();

     // pour débugger, génère un fichier avec le contenu
     if (exportjsontrietofile)
         generateFile(CorpusId, Options, true, json, ".wordtrie") ;
     // fin du test // until here *****
  }

  // !important example
  /*
  qDebug() << "Recostruction de l'arbre";
  qDebug() << s.replace("\n", " ");
  arbreword T = createTrieFromJson(s);
  afficherArbre(T);*/
  qDebug()<< "Fin Elapsed Time in Millisecond: " << Timer.elapsed();
  return trie ;
}

/********************************* EXTRACTION DES INFORMATIONS DE CHAQUE FICHIER *************************/

// Méthode utilisée uniquement par createTries()

void Words::addTokens(arbreword Trie, arbresegment trieseg, QString file, QTextStream &outsql)
{
  QFile inFile(file) ;
  if (!inFile.open(QIODevice::ReadOnly))
  {
    qDebug() << "Could not open " + file + " for reading" ;
    return ;
  }
  QTextStream fread(&inFile) ;
  fread.setCodec("UTF-8") ;

  QStringList line ;
  QVector<id> segment ; // to make sure with adel ***** QVector<int> segment ;
  QString word ;

  while (! fread.atEnd())
  {
    line = cutLine(fread.readLine()) ;
    // cas où il n'y a pas de mot dans la ligne
    if ((line.length() == 1) and (containsPunct(word)))
    {
        //qDebug() << "line length 1" ;
        //insertTrie(segment, trieseg) ;
        segment.clear() ;
    }
    //qDebug() << "line:" << line ;
    else while (not line.empty())
    {
        word=line.takeFirst();
      //qDebug() << "tester contains punct:" << containsPunct(".");
      if (containsPunct(word))
      {
        //qDebug() << "contains punct:";
        if (segment.size()>2)
        {
            NbSegments++;
            insertTrie(outsql,NbSegments,segment, trieseg) ;
        }
        segment.clear() ;
      }
      word = ArabicNormalizer::PreprocStr(word, Language, NormOptions, Segmenter, Lemmatizer) ;
      //qDebug() << "word after preproc:" << word;
      if (word!="")
      {
          insertTrie(word, Trie) ;
          Trie->prefix_occurrences++ ;
          segment.append(InsertedWordId) ;
      }
    }
  }
    return ;
}

//============================================ Découpage en mots et segments utilisés par addTokens ========================

// Fonction pour découper en mots
// n'est utilisée que par addTokens
// Ceci est un hook parce que pour d'autres langues que l'arabe, la procédure sera plus complexe
// en particulier le cas du tiret (dash) = en cas de coupure de mot, ou de mot composé
// à mettre sans doute dans Preprocessing
// Curieux : ne fonctionne pas avec QRegularExpression contrairement à la doc officielle de QString.split

QStringList Words::cutLine(QString line) {return line.split(QRegExp("\\b"));}

// Fonction qui teste s'il y a une ponctuation

bool Words::containsPunct(QString string) {return string.contains(QRegularExpression("\\p{P}")) ;}

// composite est là pour gérer le cas des ponctuations en milieu de mot
// pour un post traitement éventuel
// apriori ce ne sera pas le cas de l'arabe (anglais ?) - en fait, si -, mais sûrement pour le français.
// les fils d'un composite sont composites
// mais pas ses frères

/* Pour le français :
 *
 * Une lettre + ' ou (lors/quoi)qu + ' => découper.
 * Aujourd'hui => ne pas découper.
 *
 * Comment faire ? Chaque langue a ses règles. Une base de règles ?
 * et que faire de passa-t-il, vient-il, venez-vous ?
 *
 */

// ========================================== MOTS : fabrication de l'arbre ============================

//Cette fonction couvre 4 cas :
//Case 1:  le mot est fini (1er  if)
//Case 2:  le mot exactement est dans l'arbre, occurrences++ de la derniere lettre (2er  if)
//Case 3:  Une partie du mot est dans l'arbre (3ème if)
//Case 4:  autres cas (la suite) : la lettre n'est pas dans l'arbre, exemple
//         "abcd", "abef" and "abgh"
//         /  ----> root
//         |
//         a
//         |
//         b
//         |
//         c <=== e <=== g
//         |      |      |
//         d      f      h

// InsertedWordId est pour communiquer l'identifiant du mot au traitement des segments

void Words::insertTrie(QString word, arbreword tree, int position)
{
    arbreword branch ;
    if (position >= word.size()) return ;

    branch = assoc(word.at(position), tree->sons);
    if (branch)
    {
        branch->prefix_occurrences++ ;
        if (position == word.size() - 1)
        {
            branch->occurrences++ ;
            if (branch->identifiant==0)
                branch->identifiant=Words::Identifiant++;
            InsertedWordId = branch->identifiant ;
            return ;
        }

        insertTrie(word, branch, position + 1) ;
        return ;
    }
    branch = createBranch(word, tree, position) ;
    branch->bros    = tree->sons;
    tree->sons      = branch;

    return ;
}

arbreword Words::assoc(QChar c, arbreword tree)
{
    if (not tree) return NULL ;
    if (tree->letter == c) return tree ;
    return assoc(c, tree->bros) ;
}

arbreword Words::createBranch(QString string, arbreword tree, int position)
{
    if (position >= string.size()) return NULL ;
    if (position == string.size() - 1)
        return createNode(string.at(position), true, createBranch(string, tree, position + 1)) ;
    return createNode(string.at(position), false, createBranch(string, tree, position + 1)) ;
}

// Méthode utilisée par createTries (racine) et par createBranch (autres)
// InsertedWordId est pour communiquer l'identifiant du mot au traitement des segments

arbreword Words::createNode(QChar c, bool end, arbreword son, arbreword bro)
{
    //static int identifiant = 0 ;
    arbreword p = new node_word ;

    p->letter            = c ;
    p->occurrences       = 0 ;
    p->prefix_occurrences = 1 ;
    p->identifiant       = 0 ;  // 0 signifie que ce n'est pas un mot

    if (end)
    {
        p->occurrences  = 1 ;
        InsertedWordId = p->identifiant = Words::Identifiant++ ;
    }
    p->sons = son ;
    p->bros = bro ;
    return p ;
}

//================================================ MOTS : sauvegarde dans la base de données ==========================

// Sauvegarde des mots eux-mêmes
//void Words::sqlWriteBatch(int corpusid, arbreword trie, int trie_id, arbresegment trieseg, QString options)
void Words::sqlWriteBatchWord(int corpusid, arbreword trie, int trie_id, QString options)
{
  QSqlQuery req (* DbConnection) ;
  QString path ;
  Strsql = "" ;
  QFile outfile("/tmp/wordtodb.csv");
  path = QFileInfo(outfile).absoluteFilePath();
  if (not outfile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
  {
    qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outfile.fileName() << "en ecriture." << endl ;
    return;
  }
  QTextStream outsql(&outfile);
  outsql.setCodec("UTF-8");
  sqlWriteWords(trie->sons, "", corpusid, trie_id ,options ,false) ;
  outsql << Strsql;
  outfile.close();
  QString sql
    =
      QString("COPY \"WORD_form\" (id, form, occs, fk_corpus,composite, wt_id, wt_params) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path) ;
  //Timer.start();
  if (!req.exec(sql)) qDebug() << "Error= " << req.lastError().text();
 // qDebug()<< "Elapsed Time in Millisecond: " << Timer.elapsed();
  Strsql.clear();
  outfile.deleteLater();

}
void Words::sqlWriteBatchSegment(int corpusid, int trie_id, arbresegment trieseg, QString options)
{
    arbresegment arbreword=trieseg;
    QSqlQuery req (* DbConnection) ;
    QString path ;
    Strsql = "" ;
    QFile outsegfile("/tmp/segtodb.csv");
    path = QFileInfo(outsegfile).absoluteFilePath();
    if (not outsegfile.open(QIODevice::ReadWrite|QIODevice::Text | QIODevice::Truncate))
    {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier" << outsegfile.fileName() << "en ecriture." << endl ;
        return;
    }
    //sqlWriteSegs(trieseg->sons, {}, corpusid, trie_id,options) ;
    QTextStream outsegsql(&outsegfile);
    outsegsql.setCodec("UTF-8");
    //outsegsql << Strsql;
    QVector <id> identifiant={};
    sqlWriteSegs(arbreword->sons, identifiant, corpusid, trie_id,options, outsegsql) ;
    outsegfile.close();
    outsegfile.deleteLater();
    QString sql
            =
            QString("COPY \"WORD_segment\" (id, occs, fk_corpus,segment, wt_id, wt_params) from '%1' using delimiters ';' with ENCODING 'UTF-8'").arg(path) ;
    //Timer.start();
    if (!req.exec(sql)) qDebug() << "Error= " << req.lastError().text();
    //qDebug()<< "Fin Sql Write Batch Segment Elapsed Time in Millisecond: " << Timer.elapsed();
    Strsql.clear();
}
void Words::sqlWriteWords(arbreword tree, QString word, int corpus, int trie_id, QString options , bool composite)
{
    if (not tree) return ;
    word.append(tree->letter) ;
    if (tree->occurrences)
        sqlWriteWord(word, tree->occurrences, tree->identifiant, corpus, trie_id, options , tree->letter.isPunct()) ;
    sqlWriteWords(tree->sons, word, corpus, trie_id, options , tree->letter.isPunct()) ;
    sqlWriteWords(tree->bros, word.left(word.size() - 1), corpus, trie_id, options , composite) ;
}

void Words::sqlWriteWord(QString word, val occs, int idword, int corpus, int trie_id, QString options , bool composite, bool flag)
{
  if (not flag)
    Strsql +=QString("%1;%2;%3;%4;%5;%6;%7\n").arg(idword).arg(word).arg(occs).arg(corpus).arg(composite).arg(trie_id).arg(options) ;
  //qDebug() << Strsql;
}

// Crée la version json de l'arbre trie
QString Words::createJsonTrie(arbreword trie, bool Debug)
{
    QString s = createJsonNode(trie, Debug) ;
    if (trie->bros)
    {
        s = s +  "\"bros\": ";
        if (Debug) qDebug() << "\"bros\": " ;
        s = s + createJsonTrie(trie->bros) ;
        s = s + ",";
        if (Debug) qDebug() << "," ;
    }
    else
    {
        s = s + "\"bros\": \"\",";
        if (Debug) qDebug() << "\"bros\": \"\",";
    }
    if (trie->sons)
    {
        s = s + "\"sons\": " ;
        if (Debug) qDebug() << "\"sons\": " ;
        s = s + createJsonTrie(trie->sons) ;
    }
    else
    {
        s = s + "\"sons\": \"\"";
        if (Debug) qDebug() << "\"sons\": \"\"" ;
    }
    s = s + "\n}\n";
    if (Debug) qDebug() << "}" ;
    return s ;
}

// Crée la version json du noeud

QString Words::createJsonNode(arbreword trie, bool Debug)
{
    QString s = "{\n" ;
    s = s + "\"letter\": \"" + trie->letter + "\",\n";
    s = s + "\"occurrences\": \"" + QString::number(trie->occurrences) + "\",\n";
    s = s + "\"prefix_occurrences\": \"" + QString::number(trie->prefix_occurrences) + "\",\n";
    s = s + "\"identifiant\": \"" + QString::number(trie->identifiant) + "\",\n";
    if (Debug)
    {
        qDebug() << "{ \"letter\": " << trie->letter << ", ";
        qDebug() << "\"occurrences\": \"" << trie->occurrences << "\",\n";
        qDebug() << "\"prefix_occurrences\": \"" << trie->prefix_occurrences << "\",\n";
        qDebug() << "\"identifiant\": \"" << trie->identifiant << "\",\n";
    }
    return s ;
}

// Pour débugger ; inutilisé maintenant

void Words::afficherArbre(arbreword node) {

    if (node)
    {
        qDebug()  << node->letter;
        qDebug()  << node->occurrences;
    }

    if (not node->sons and node->bros)
    {
        afficherArbre(node->bros);
    }

    if (node->sons) {
        afficherArbre(node->sons);
        if (node->bros) {
            afficherArbre(node->bros);
        }

    }

}


//====================================== SEGMENTS : Fabrication de l'arbre ============================

void Words::insertTrie(QTextStream & out,id idseg, QVector<id> identifiant, arbresegment tree, int position)
{
    arbresegment branch= new node_segment ;
    if (position >= identifiant.size()) return ;
    branch = assoc(identifiant[position], tree->sons) ;

    if (branch && (position == identifiant.size() - 1))
    {
        branch->end=true;
        branch->occurrences++ ;
        if (branch->identifiant==0)
        {
            branch->identifiant=idseg;
            sqlWriteSeg(idseg, branch->occurrences, CorpusId, identifiant, TrieId, Options, out);
        }

        return ;
    }
    if (branch)
    {
        branch->occurrences++ ;
        insertTrie(out,idseg, identifiant, branch, position + 1) ;
        return ;
    }
    branch = createBranch(idseg, identifiant, tree, position,out) ;
    branch->bros = tree->sons ;
    tree->sons = branch ;
    return ;
}

arbresegment Words::assoc(id m, arbresegment tree)
{
    if (not tree) return NULL ;

    if (tree->wordid == m)
    {
        tree->occurrences++;
        return tree ;
    }
    return assoc(m, tree->bros) ;
}

arbresegment Words::createBranch(id idseg, QVector<id> identifiant, arbresegment tree, int position, QTextStream & out)
{
    if (position > identifiant.size()-1) return NULL ;
    if (position == identifiant.size() - 1)
        return createNode(out,idseg,identifiant[position],identifiant,true);
    return createNode(out,idseg,identifiant[position],identifiant,false, createBranch(idseg,identifiant, tree, position + 1,out)) ;
}

arbresegment Words::createNode (QTextStream & out,id idseg,id m_id, QVector<id> identifiant, bool s_end, arbresegment son, arbresegment bro)
{
    // ***** static int segidentifiant = 0;
    arbresegment p = new node_segment ;
    p->wordid=m_id;
    p->sons = son ;
    p->bros = bro ;
    p->end=s_end;
    p->occurrences = 1 ;
    if (s_end)
    {
        p->identifiant = idseg ;
        sqlWriteSeg(idseg, p->occurrences, CorpusId, identifiant, TrieId, Options, out);
    }
    else
    {
        p->identifiant = 0;
    }

    return p ;
}

//======================================= SEGMENTS : sauvegarde dans la base de données ====================

// les segments sont écrits sous la forme {identifiant de mot, identifiant de mot...}
void Words::sqlWriteSegs(arbresegment tree, QVector<id> identifiants, int corpus, int trieid, QString options, QTextStream &out)
{
    if (not tree) return ;
    identifiants.append(tree->wordid) ;
    if (tree->end)
        sqlWriteSeg(tree->identifiant, tree->occurrences, corpus,identifiants,trieid, options, out) ;
    sqlWriteSegs(tree->sons, identifiants, corpus,trieid, options, out) ;
    identifiants.removeLast();
    sqlWriteSegs(tree->bros, identifiants, corpus,trieid, options, out) ;
}

void Words::sqlWriteSeg(id idseg, int occs, int corpus, QVector<id>  identifiant, int trieid, QString options, QTextStream &out)
{
    if (identifiant.size()==1) return;
    QString segment= "{";
    for(int i=0 ; i < identifiant.size()-1 ; i++)
    {
        segment += QString::number(identifiant[i]) + ",";
    }
    segment+=QString::number(identifiant[identifiant.size()-1])+"}";

    QString str=QString("%1;%2;%3;%4;%5;%6\n").arg(idseg).arg(occs).arg(corpus).arg(segment).arg(trieid).arg(options);
    out << str;
    //qDebug() << "InsertedSeg:" << idseg << "/" << NbSegments;
}

// création Json pour les arbres de segments :

QString Words::createJsonTrie(arbresegment trie, bool Debug)
{
    QString s = createJsonNode(trie, Debug) ;
    if (trie->bros)
    {
        s = s +  "\"bros\": ";
        if (Debug) qDebug() << "\"bros\": " ;
        s = s + createJsonTrie(trie->bros) ;
        s = s + ",";
        if (Debug) qDebug() << "," ;
    }
    else
    {
        s = s + "\"bros\": \"\",";
        if (Debug) qDebug() << "\"bros\": \"\",";
    }
    if (trie->sons)
    {
        s = s + "\"sons\": " ;
        if (Debug) qDebug() << "\"sons\": " ;
        s = s + createJsonTrie(trie->sons) ;
    }
    else
    {
        s = s + "\"sons\": \"\"";
        if (Debug) qDebug() << "\"sons\": \"\"" ;
    }
    s = s + "\n}\n";
    if (Debug) qDebug() << "}" ;
    return s ;
}

// idem pour le segment
QString Words::createJsonNode(arbresegment trie, bool Debug)
{
    QString s = "{\n" ;
    s = s + "\"wordid\": \"" + QString::number(trie->wordid) + "\",\n";
    s = s + "\"occurrences\": \"" + QString::number(trie->occurrences) + "\",\n";
    s = s + "\"identifiant\": \"" + QString::number(trie->identifiant) + "\",\n";
    s = s + "\"end\": \"" + QString::number(trie->end) + "\",\n";

    if (Debug)
    {
        qDebug() << " { \"wordid\": " << trie->wordid << ", ";
        qDebug() << "\"occurrences\": \"" << trie->occurrences << "\",\n";
        qDebug() << "\"identifiant\": \"" << trie->identifiant << "\",\n";
    }
    return s ;
}

void Words::afficherArbre(arbresegment node){
    if (node)
    {
        qDebug()  << node->identifiant << node->wordid << node->occurrences << node->end;
    }

    if (not node->sons and node->bros)
    {
        afficherArbre(node->bros);
    }

    if (node->sons) {
        afficherArbre(node->sons);
        if (node->bros) {
            afficherArbre(node->bros);
        }

    }

}

/*********************** Fonctions Corpus ****************************/

QVector<MyMap> Words::getCorpusDocumentsList(int corpusid)
{
    QVector<MyMap> maplist;
    QSqlQuery req(* DbConnection);

    req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = %1").arg(corpusid));

    while (req.next())
        maplist.append(MyMap(req.value(0).toString(), req.value(1).toInt()));

    return maplist;
}


QString Words::getCorpusName(int corpusid)
{
    QSqlQuery req(* DbConnection);

    req.exec(QString("select nom_corpus from corpus where id = %1").arg(corpusid));
    req.next();

    return req.value(0).toString();
}

arbreword Words::createSuffixWordsTrie(arbreword trie)
{
    int i, j;
    QString str;

    generateWordsList(trie->sons, "");

    arbreword suffixtrie = createNode((QChar) '/', false) ;
    if (WordList.size() > 0)
    {
        for (i = 0 ; i < WordList.size() ; i++)
            for (j = 0 ; j< WordOccList[i] ; j++)
            {
                str = reverseWord(WordList[i]);
                //qDebug() << "insert " << str;
                insertTrie(str, suffixtrie);
            }
    }

    return suffixtrie;

}

void Words::generateWordsList(arbreword tree, QString word)
{
    if (not tree)
        return ;
    word.append(tree->letter) ;
    if (tree->occurrences)
        generateWord(tree, word) ;
    generateWordsList(tree->sons, word) ;
    generateWordsList(tree->bros, word.left(word.size() - 1)) ;
}

void Words::generateWord(arbreword tree, QString word)
{
    WordList.append(word);
    WordOccList.append(tree->occurrences);
}

QString Words::reverseWord(QString word)
{
    QString reverseword;

    for (QString::iterator it = word.begin(); it != word.end(); ++it)
        reverseword.push_front(*it);

    return reverseword;
}

// ====================================== pour débugger

// imprime le json dans un fichier
// si on a besoin du nom du fichier, on peut le renvoyer

void Words::generateFile(int corpusid, QString options, bool inverse, QString contents, QString extension)
{
    QString filename =  generateFilename(corpusid, options, inverse) + QDate::currentDate().toString() + extension;
    QFile outfile(filename) ;
    //outfile.open(QIODevice::ReadWrite | QIODevice::WriteOnly | QIODevice::Append) ;
    outfile.open(QIODevice::WriteOnly) ;
    QTextStream stream(&outfile) ;
    stream << contents ;
    outfile.close() ;
}

// génère la base du nom du fichier contenant le trie

QString Words::generateFilename(int corpusid, QString options, bool inverse)
{
    QString filename = QString::number(corpusid) + "_" + getCorpusName(corpusid).replace(" ", "-") + "_" + options + "_" ;
    if (inverse) filename = filename + "inverse_" ;
    return filename ;
}
