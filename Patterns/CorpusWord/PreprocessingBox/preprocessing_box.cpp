#include "preprocessing_box.h"

PreprocessingBox::PreprocessingBox(bool segmenterflag, bool stemflag)
: Box(tr("Preprocessing Corpus")), SegmenterFlag(segmenterflag), StemFlag(stemflag)
{
    qDebug() << "constructeur PreprocessingBox" ;

    QGridLayout * preprocessinglayout = new QGridLayout ;
    setAlignment(Qt::AlignTop) ;
    setLayout(preprocessinglayout) ;

    createArabicSection(preprocessinglayout) ;

    if (Language != "Arabic") hideArabicSection() ;
}

void PreprocessingBox::setCorpusId(int corpusid)
{ CorpusId = corpusid; }

void PreprocessingBox::setLanguage(QString language)
{ Language = language;
  if (debug) qDebug() << "Preprocessing Box >> language changed to " << Language;
    
  if (Language == "Arabic") showArabicSection();
  else hideArabicSection();  
}

QString PreprocessingBox::getLanguage()
{ return Language; }

void PreprocessingBox::createArabicSection(QGridLayout * preprocessinglayout)
{
  preprocessinglayout->setAlignment(Qt::AlignTop);
  ArabicSectionTitle = new QLabel("For Arabic only") ;
  preprocessinglayout->addWidget(ArabicSectionTitle, 1, 0) ;
  
  NormParams = new normparams ;
  
  NormParams->append("diacritics") ;
  NormParams->append("numbers") ;
  NormParams->append("foreign letters") ;
  NormParams->append("shadda and madda") ;
  NormParams->append("alef") ;
  NormParams->append("yeh") ;
  NormParams->append("heh") ;
  
  NormParamLines = new normparamlines ;
  NormParamValues = new normparamvalues ;

  for (int i = 0 ; i < NormParams->size() ; i++)
  {
    QLabel * label = new QLabel("remove " + NormParams->at(i)) ;
    QCheckBox * check = new QCheckBox() ;
    NormParamLines->insert(NormParams->at(i), qMakePair(label, check)) ;

    //NormParamValues->insert(NormParams->at(i), false) ;
    //emit sigNormParamModified(NormParamValues) ;
    VarNormParamValues.insert(NormParams->at(i), false) ;
    emit sigVarNormParamModified(VarNormParamValues) ;

    connect
    (
      check,
      &QCheckBox::stateChanged,
     [=](int state){(VarNormParamValues)[NormParams->at(i)] = state ; emit sigVarNormParamModified(VarNormParamValues) ;}
    ) ;

    preprocessinglayout->addWidget(label, i + 2, 0) ;
    preprocessinglayout->addWidget(check, i + 2, 1) ;
  }
  //segmenter section
  if (SegmenterFlag)
  {
      qSegmenter = new QLabel("Segmenter");
      preprocessinglayout->addWidget(qSegmenter, 10, 0);
      arSegmenter = new QComboBox();
      arSegmenter->addItem("");
      arSegmenter->addItem("STANFORD SEGMENTER");
      arSegmenter->addItem("HARRISMORPH");
      preprocessinglayout->addWidget(arSegmenter,10,1);
      connect
      (
        arSegmenter,
        static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        [=](int index){emit sigSegmenterModified(arSegmenter->currentText()) ;}
      ) ;
  }

  //stemming section
  if (StemFlag)
  {
      qStem = new QLabel("Lemmatizer");
      preprocessinglayout->addWidget(qStem, 11, 0) ;
      arStem = new QComboBox();
      arStem->addItem("");
      arStem->addItem("KHOJA Lemmatizer");
      preprocessinglayout->addWidget(arStem, 11, 1) ;
      //to put khoja lemmatizer as default selection
      //stem->setCurrentIndex(1);
      //emit sigLemmatizerModified(stem->currentText()) ;
      connect
      (
        arStem,
        static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        [=](int index){emit sigLemmatizerModified(arStem->currentText()) ;}
      ) ;
  }
  //
  qNormalizer = new QLabel("Normalizer");
  preprocessinglayout->addWidget(qNormalizer, 12, 0);
  Normalizer = new QComboBox();
  Normalizer->addItem("");
  //to put arabic normalizer as default selection
  if (Language=="Arabic") Normalizer->addItem("IBM Normalizer");
  else Normalizer->addItem("Latin Normalizer");
  Normalizer->setCurrentIndex(1);
  emit sigNormalizerModified(Normalizer->currentText()) ;
  preprocessinglayout->addWidget(Normalizer,12,1);
  connect
  (
    Normalizer,
    static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    [=](int index){emit sigNormalizerModified(Normalizer->currentText()) ;}
  ) ;
}

void PreprocessingBox::hideArabicSection()
{
  ArabicSectionTitle->hide() ;
  
  foreach (normparamline line, NormParamLines->values())
  {
    line.first->hide() ;
    line.second->hide() ;
  }
  ArabicSectionTitle->hide();
  qSegmenter->hide();
  arSegmenter->hide();
  qStem->hide();
  arStem->hide();
  if (Normalizer->itemText(1)=="IBM Normalizer")
      Normalizer->addItem("Latin Normalizer");
  Normalizer->setCurrentText("Latin Normalizer");
  emit sigNormalizerModified(Normalizer->currentText()) ;
}

void PreprocessingBox::showArabicSection()
{
  ArabicSectionTitle->show() ;
  
  foreach (normparamline line, NormParamLines->values())
  {
    line.first->show() ;
    line.second->show() ;
  }
  ArabicSectionTitle->show() ;
  qSegmenter->show();
  arSegmenter->show();
  qStem->show();
  arStem->show();
  if (Normalizer->itemText(1)=="Latin Normalizer")
      Normalizer->addItem("IBM Normalizer");
  Normalizer->setCurrentText("IBM Normalizer");
  emit sigNormalizerModified(Normalizer->currentText()) ;
}
