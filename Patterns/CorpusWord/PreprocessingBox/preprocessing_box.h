#ifndef PREPROCESSINGBOX_H
#define PREPROCESSINGBOX_H
#include "QtWidgets"
#include "../../fromInterface.h"


//================================ COMPOSANTS DE LA FENETRE


typedef QVector<QString> normparams ;

typedef QMap<QString, bool> normparamvalues ;

typedef QPair<QLabel *, QCheckBox *> normparamline ;

typedef QMap<QString, normparamline> normparamlines ;


class PreprocessingBox : public Box
{
    Q_OBJECT
    
public:
  
    PreprocessingBox(bool, bool) ;

    normparams *      NormParams ;       // parameter strings
    normparamvalues * NormParamValues ;  // map with parameter strings and values (for exportation)
    normparamvalues   VarNormParamValues ;  // map with parameter strings and values (for exportation)
    normparamlines *  NormParamLines ;   // map with parameter strings, QLabels and QCheckBoxes

    bool debug = true;

    void            setCorpusId(int);
    void            setLanguage(QString);
    QString         getLanguage();

protected:
  
    bool SegmenterFlag ;
    bool StemFlag ;

    int CorpusId = 0;
  
    QString Language;
  
    QLabel  *       ArabicSectionTitle ;
    QLabel  *        qSegmenter;
    QComboBox *     arSegmenter;
    QLabel *        qStem;
    QComboBox * arStem;
    QLabel * qNormalizer;
    QComboBox * Normalizer;
    
    void            createArabicSection(QGridLayout *) ;
    void            hideArabicSection() ;
    void            showArabicSection() ;
  
protected slots:
    
signals:

    void sigNormParamModified(normparamvalues *) ;
    void sigVarNormParamModified(normparamvalues) ;
    void sigSegmenterModified(const QString& text) const ;
    void sigLemmatizerModified(const QString& text) const ;
    void sigNormalizerModified(const QString& text) const ;

} ;

#endif // PREPROCESSINGBOX_H
