#include "arabicnormalizer.h"
#include <iostream>
#include <sstream>
#include <cstdio>
#include <QtSql>
#include <QRegExp>
#include <string>
using namespace std;
/********************************************ARABICNORMALIZER**************************************/
void ArabicNormalizer::normalizeLatincmd(QString infile, QString outfile,int nbthread)
{
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultiThreadNormalizerLatin.jar %1 %2 %3" ).arg(infile).arg(outfile).arg(nbthread));
    qDebug() << "normalizecmd: java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultiThreadNormalizerLatin.jar " << infile << " " << outfile << " " << nbthread;
    process->waitForFinished(-1);
    return;
}
void ArabicNormalizer::normalizecmd(QString infile, QString outfile,int nbthread)
{
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultiThreadNormalizer.jar %1 %2 %3" ).arg(infile).arg(outfile).arg(nbthread));
    qDebug() << "normalizecmd: java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultiThreadNormalizer.jar " << infile << " " << outfile << " " << nbthread;
    process->waitForFinished(-1);
    return;
}

QString ArabicNormalizer::normalize(QString s, bool remove_diacritics, bool remove_shadda, bool normalize_alef, bool normalize_yeh, bool normalize_heh)
{
    //ALEF_MADDA:ALEF_HAMZA_ABOVE:ALEF_HAMZA_BELOW: replaced by ALEF
    if (normalize_alef)
        s.replace(QRegExp("[\u0622\u0623\u0625]"),"\u0627");
    //DOTLESS_YEH: replaced by YEH
    if (normalize_yeh)
        s.replace(QRegExp("\u0649"),"\u064a");
    //TEH_MARBUTA: replaced by HEH
    if (normalize_heh)
        s.replace(QRegExp("\u0629"),"\u0647");
    //SHADDA:TATWEEL: removed
    if (remove_shadda)
        s.remove(QRegExp("[\u0651,\u0640]"));
    //KASRATAN:DAMMATAN:FATHATAN:FATHA:DAMMA:KASRA:SUKUN: removed
    if (remove_diacritics)
        s.remove(QRegExp("[\u064b,\u064c,\u064d,\u064e,\u064f,\u0650,\u0652]"));
    return s;
}

// surcharge introduite par Gilles pour segmenteur ; il y a un booléen supplémentaire (à la fin)
// replace with the same number of character : no change
// remove : one position less
// replace with more characters : repeat the position (no case here)
// replace with fewer characters : keep only the first position

// The QVector of preprocessingparams is a bad idea : one change and all goes down

QPair<QString, QVector<int> > ArabicNormalizer::normalize(QString s, QVector<bool> preprocessingparams, bool keep_position)
{
  QString originalword = s;
  int position = 0;           // index on originalword
  QVector<int> positions;
  int i = 0;                  // index on positions
  while (i < originalword.size()) { positions[i] = i; i++; }
  
  //KASRATAN:DAMMATAN:FATHATAN:FATHA:DAMMA:KASRA:SUKUN: removed
  if (preprocessingparams[0])
  {
    while ((position = originalword.indexOf(QRegExp("[\u064b,\u064c,\u064d,\u064e,\u064f,\u0650,\u0652]"), position)) >= 0)
      positions.remove(position++);
    s.remove(QRegExp("[\u064b,\u064c,\u064d,\u064e,\u064f,\u0650,\u0652]"));
  }
  //remove digits
  if (preprocessingparams[1])
  {
    while ((position = originalword.indexOf(QRegExp("\\d+"), position) >= 0)) positions.remove(position++);
    s.remove(QRegExp("\\d+"));
  }
  //remove foreign characters
  if (preprocessingparams[2])
  {
    while ((position = originalword.indexOf(QRegExp("[^\u0621-\u063a,\u0640-\u0655,\\d+]"), position)) >= 0) positions.remove(position++);
    s.remove(QRegExp("[^\u0621-\u063a,\u0640-\u0655,\\d+]"));
  }
  //SHADDA:TATWEEL: removed
  if (preprocessingparams[3])
  {
    while ((position = originalword.indexOf(QRegExp("[\u0651,\u0640]"), position)) >= 0) positions.remove(position++);
    s.remove(QRegExp("[\u0651,\u0640]"));
  }
  //ALEF_MADDA:ALEF_HAMZA_ABOVE:ALEF_HAMZA_BELOW: replaced by ALEF
  if (preprocessingparams[4]) s.replace(QRegExp("[\u0622\u0623\u0625]"),"\u0627");
  //DOTLESS_YEH: replaced by YEH
  if (preprocessingparams[5]) s.replace(QRegExp("\u0649"),"\u064a");
  //TEH_MARBUTA: replaced by HEH
  if (preprocessingparams[6]) s.replace(QRegExp("\u0629"),"\u0647");

  return qMakePair(s, positions);
}

// surcharge introduite par Youssef ? avec deux booléens de plus (au milieu) pour enlever les nombres et les caractères non arabes
QString ArabicNormalizer::normalize(QString s, bool remove_diacritics, bool remove_digits, bool remove_non_arabic, bool remove_shadda, bool normalize_alef, bool normalize_yeh, bool normalize_heh)
{
    if (remove_non_arabic && remove_digits)
        s=ArabicNormalizer::isArabicWord(ArabicNormalizer::RemoveDigitWord(s));
    else if (remove_non_arabic)
        s=ArabicNormalizer::isArabicWord(s);
    else if (remove_digits)
        s=ArabicNormalizer::RemoveDigitWord(s);
    if (s.length()>0)
        return ArabicNormalizer::normalize(s,remove_diacritics,remove_shadda,normalize_alef,normalize_yeh,normalize_heh);
    return s;
}

QString ArabicNormalizer::isArabicWord(QString s)
{
    s.remove(QRegExp("[^\u0621-\u063a,\u0640-\u0655,\\d+]"));
    return s;
}

QString ArabicNormalizer::RemoveDigitWord(QString s)
{
    s.remove(QRegExp("\\d+"));
    return s;
}

/*QString ArabicNormalizer::PreprocStr(QString str, QString lang, QVector<bool> lang_param, QString stemmer="")
{
    if(lang=="Arabic")
    {
          str=ArabicNormalizer::normalize(str,lang_param[0],lang_param[1],lang_param[2],lang_param[3],lang_param[4],lang_param[5],lang_param[6]);
          if(stemmer=="KHOJA STEMMER")
          {
              if (KhojaStemmer::staticFiles.size()==0)
                  KhojaStemmer::readInStaticFiles("../CorpusPatterns/CorpusWord/PreprocessingBox/KhojaStemmerFiles/");
              str=QString::fromStdWString(KhojaStemmer::stemWord(str.toStdWString()));
          }
    }
    return str;
}*/

QString Segmenter::harrismorphsegmentercmd(QString mot)
{
    //@youssef a faire
    return mot;
}

QString Segmenter::stanfordsegmentercmd(QString mot)
{
    /*ArabicTokenizer supports various orthographic normalization options that can be configured
    in ArabicSegmenter using the -orthoOptions flag. The argument to -orthoOptions is a comma-separated list of
    normalization options. The following options are supported:

      useUTF8Ellipsis   : Replaces sequences of three or more full stops with \u2026
      normArDigits      : Convert Arabic digits to ASCII equivalents
      normArPunc        : Convert Arabic punctuation to ASCII equivalents
      normAlif          : Change all alif forms to bare alif
      normYa            : Map ya to alif maqsura
      removeDiacritics  : Strip all diacritics
      removeTatweel     : Strip tatweel elongation character
      removeQuranChars  : Remove diacritics that appear in the Quran
      removeProMarker   : Remove the ATB null pronoun marker
      removeSegMarker   : Remove the ATB clitic segmentation marker
      removeMorphMarker : Remove the ATB morpheme boundary markers
      removeLengthening : Replace sequences of three identical characters with one
      atbEscaping       : Replace left/right parentheses with ATB escape characters
     */
    //qDebug() << "mot a segmenter: " << mot;
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("echo %1|   tee infile.txt | java -cp ../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-segmenter-2015-12-09/stanford-segmenter-3.6.0.jar:../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-segmenter-2015-12-09/slf4j-api.jar:../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-segmenter-2015-12-09/slf4j-simple.jar  edu.stanford.nlp.international.arabic.process.ArabicSegmenter -loadClassifier ../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-segmenter-2015-12-09/data/arabic-segmenter-atb+bn+arztrain.ser.gz -orthoOptions removeMorphMarker,removeProMarker -textFile infile.txt" ).arg(mot));
    process->waitForFinished();
    QString motseg= process->readAll();
    //QList<QString> motseg=motaseg.split(QRegExp("\\s"));
    //qDebug() << "mot segmente: " << motseg;

    return motseg;
}

QString ArabicNormalizer::PreprocStr(QString str, QString lang, QVector<bool> lang_param, QString segmenter, QString lemmatizer)
{
    bool donormalize, dosegment, dostem;
    //même si on normalise, on utilise la forme dénormalisée pour le lemme,
    //et la forme normalisée pour la segmentation (si c'est la meilleure)
    donormalize=lang_param.contains(true);
    dosegment=segmenter!="";
    dostem=lemmatizer!="";
    if (!donormalize && !dosegment && !dostem) //pas de pretraitement
        return str;
    if (donormalize && !dosegment && !dostem) //normaliser slt
        return ArabicNormalizer::normalize(str,lang_param[0],lang_param[1],lang_param[2],lang_param[3],lang_param[4],lang_param[5],lang_param[6]);
    if (dosegment && !dostem) //il faut normaliser puis segmenter puis si pas de normalisation denormaliser
    {
        QString strnormseg;//ce que je fais c normaliser si normaliser slt
        if (donormalize) str= ArabicNormalizer::normalize(str,lang_param[0],lang_param[1],lang_param[2],lang_param[3],lang_param[4],lang_param[5],lang_param[6]);
        if (segmenter=="STANFORD SEGMENTER")
            strnormseg=Segmenter::stanfordsegmentercmd(str);
        if (segmenter=="HARRISMORPH")
            strnormseg=Segmenter::harrismorphsegmentercmd(str);
        //if(!donormalize)         //denormaliser??        //QList<QString> motseg=strnormseg.split(QRegExp("\\s"));
        return strnormseg;
    }
    if (!dosegment && dostem)
    {
        if (donormalize) str= ArabicNormalizer::normalize(str,lang_param[0],lang_param[1],lang_param[2],lang_param[3],lang_param[4],lang_param[5],lang_param[6]);
        if(lemmatizer=="KHOJA Lemmatizer")
        {
            if (KhojaStemmer::staticFiles.size()==0)
                KhojaStemmer::readInStaticFiles("../CorpusPatterns/CorpusWord/PreprocessingBox/KhojaStemmerFiles/");
            return QString::fromStdWString(KhojaStemmer::stemWord(str.toStdWString()));
        }
    }
    //3 to do:normaliser, segmenter, denormaliser, lemmatiser
    if (donormalize && dosegment && dostem)
    {
        QString strnormseg,strreturn,strpart;
        str= ArabicNormalizer::normalize(str,lang_param[0],lang_param[1],lang_param[2],lang_param[3],lang_param[4],lang_param[5],lang_param[6]);
        if (segmenter=="STANFORD SEGMENTER")
            strnormseg=Segmenter::stanfordsegmentercmd(str);
        if (segmenter=="HARRISMORPH")
            strnormseg=Segmenter::harrismorphsegmentercmd(str);
        //pas denormaliser
        QList<QString> strseg=strnormseg.split(QRegExp("\\s"));;
        foreach (strpart, strseg)
            if (strpart != "")
                if(lemmatizer=="KHOJA Lemmatizer")
                {
                    if (KhojaStemmer::staticFiles.size()==0)
                        KhojaStemmer::readInStaticFiles("../CorpusPatterns/CorpusWord/PreprocessingBox/KhojaStemmerFiles/");
                    strreturn =strreturn + " " + QString::fromStdWString(KhojaStemmer::stemWord(strpart.toStdWString()));
                }
        return strreturn;
    }

    return str;
}

/*******************************KhojaStemmer*************************************************/
QVector<QVector<wstring> > KhojaStemmer::staticFiles;
bool KhojaStemmer::rootFound = false;
bool KhojaStemmer::patternFound = false;
bool KhojaStemmer::stopwordFound = false;
bool KhojaStemmer::strangeWordFound = false;
bool KhojaStemmer::rootNotFound = false;
bool KhojaStemmer::fromSuffixes = false;
bool KhojaStemmer::addVectorFromFile ( QString path, QString fileName )
{
    QVector<wstring> tmp;
    QTextStream flux;
    QFile infile;
    infile.setFileName(QString("%1/%2").arg(path).arg(fileName)) ;

    if (not infile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "impossible to open the file " << fileName ;
        return false;
    }
      flux.setDevice(& infile) ;
      //addTokens(flux) ;
      QString str ;
        while (not flux.atEnd())
        {
            flux >> str;
            //tmp.push_back(str.toUtf8().constData());
            tmp.push_back(str.toStdWString());
        }
      infile.close() ;
      KhojaStemmer::staticFiles.push_back(tmp);
      return true;
}
void KhojaStemmer::readInStaticFiles ( QString pathToStemmerFiles)
{
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "definite_article.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "duplicate.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "first_waw.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "first_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_alif.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_hamza.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_maksoura.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "mid_waw.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "mid_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles, "prefixes.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles ,"punctuation.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles, "quad_roots.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "stopwords.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles ,"suffixes.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "tri_patt.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "tri_roots.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "diacritics.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "strange.txt" ))
        cout << "";
}

// check that the word is a strange word
bool KhojaStemmer::checkStrangeWords(wstring currentWord)
{
    KhojaStemmer::strangeWordFound=find(KhojaStemmer::staticFiles.at(18).begin(),KhojaStemmer::staticFiles.at(18).end(),currentWord)!= KhojaStemmer::staticFiles.at(18).end();
    return KhojaStemmer::strangeWordFound;
}
// check that the word is a stopword
bool KhojaStemmer::checkStopwords(wstring currentWord)
{
    KhojaStemmer::stopwordFound=find(KhojaStemmer::staticFiles.at(13).begin(),KhojaStemmer::staticFiles.at(13).end(),currentWord)!= KhojaStemmer::staticFiles.at(13).end();
    return KhojaStemmer::stopwordFound;
}
// handle duplicate letters in the word
wstring KhojaStemmer::duplicate ( wstring word )
{
     // check if a letter was duplicated
    if (find(KhojaStemmer::staticFiles.at(1).begin(),KhojaStemmer::staticFiles.at(1).end(),word)!= KhojaStemmer::staticFiles.at(1).end())
    {
        // if so, then return the deleted duplicate letter
        word = word + word.substr(1);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
    }
    return word;
}
// check if the last letter of the word is a weak letter
wstring KhojaStemmer::lastWeak(wstring word)
{
    wstring stemWord(L"");
    // check if the last letter was an alif
    if (find(KhojaStemmer::staticFiles.at(4).begin(),KhojaStemmer::staticFiles.at(4).end(),word)!= KhojaStemmer::staticFiles.at(4).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0627");
        word=stemWord;
        stemWord.clear();
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        return word;
    }
    // check if the last letter was an hamza
    if (find(KhojaStemmer::staticFiles.at(5).begin(),KhojaStemmer::staticFiles.at(5).end(),word)!= KhojaStemmer::staticFiles.at(5).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0623");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was an maksoura
    if (find(KhojaStemmer::staticFiles.at(6).begin(),KhojaStemmer::staticFiles.at(6).end(),word)!= KhojaStemmer::staticFiles.at(6).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0649");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was an yah
    if (find(KhojaStemmer::staticFiles.at(7).begin(),KhojaStemmer::staticFiles.at(7).end(),word)!= KhojaStemmer::staticFiles.at(7).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u064a");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// check if the first letter is a weak letter
wstring KhojaStemmer::firstWeak(wstring word)
{
    wstring stemWord(L"");
    // check if the firs letter was a waw
    if (find(KhojaStemmer::staticFiles.at(2).begin(),KhojaStemmer::staticFiles.at(2).end(),word)!= KhojaStemmer::staticFiles.at(2).end())
    {
        stemWord.append(L"\u0648");
        stemWord.append(word);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was a yah
    if (find(KhojaStemmer::staticFiles.at(3).begin(),KhojaStemmer::staticFiles.at(3).end(),word)!= KhojaStemmer::staticFiles.at(3).end())
    {
        stemWord.append(L"\u064a");
        stemWord.append(word);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// check if the middle letter of the root is weak
wstring KhojaStemmer::middleWeak(wstring word)
{
    wstring stemWord(L"");
     // check if the middle letter is a waw
    if (find(KhojaStemmer::staticFiles.at(8).begin(),KhojaStemmer::staticFiles.at(8).end(),word)!= KhojaStemmer::staticFiles.at(8).end())
    {
        // return the waw to the word
        stemWord.append(word.substr(0,1));
        stemWord.append(L"\u0648");
        stemWord.append(word.substr(1));
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the middle letter is a yah
    if (find(KhojaStemmer::staticFiles.at(9).begin(),KhojaStemmer::staticFiles.at(9).end(),word)!= KhojaStemmer::staticFiles.at(9).end())
    {
        //return the yah to the word
        stemWord.append(word.substr(0,1));
        stemWord.append(L"\u064a");
        stemWord.append(word.substr(1));
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// if the word consists of two letters
wstring KhojaStemmer::isTwoLetters ( wstring word )
{
    // if the word consists of two letters, then this could be either
    // - because it is a root consisting of two letters (though I can't think of any!)
    // - because a letter was deleted as it is duplicated or a weak middle or last letter.
    word = KhojaStemmer::duplicate ( word );
    // check if the last letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::lastWeak ( word );
    // check if the first letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::firstWeak ( word );
    // check if the middle letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::middleWeak ( word );
    return word;
}
// if the word consists of three letters
wstring KhojaStemmer::isThreeLetters ( wstring word )
{
    wstring modifiedWord( word );
    wstring root = L"";
    // if the first letter is a 'ا', 'ؤ'  or 'ئ'
   // then change it to a 'أ'
    if ( word.length() > 0 )
    {
        if ( word[0] == L'\u0627' || word[0] == L'\u0624' || word[0] == L'\u0626' )
        {
            modifiedWord.clear();
            modifiedWord.append( L"\u0623" );
            modifiedWord.append( word.substr(1) );
            root = modifiedWord;
        }
        // if the last letter is a weak letter or a hamza
        // then remove it and check for last weak letters
        if ( word[2] == L'\u0648' || word[2] == L'\u064a' || word[2] == L'\u0627' ||
             word[2] == L'\u0649' || word[2] == L'\u0621' || word[2] == L'\u0626' )
        {
            root = word.substr( 0, 2 );
            root = KhojaStemmer::lastWeak( root );
            if ( KhojaStemmer::rootFound )
            {
                return root;
            }
        }
        // if the second letter is a weak letter or a hamza
        // then remove it
        if ( word[1] == L'\u0648' || word[1] == L'\u064a' || word[1] == L'\u0627' || word[1] == L'\u0626' )
        {
            root = word.substr( 0, 1 );
            root = root + word.substr( 2 );
            root = KhojaStemmer::middleWeak( root );
            if ( KhojaStemmer::rootFound )
                return root;
        }
        // if the second letter has a hamza, and it's not on a alif
        // then it must be returned to the alif
        if ( word[1] == L'\u0624' || word[1] == L'\u0626' )
        {
            if ( word[2] == L'\u0645' || word[2] == L'\u0632' || word[2] == L'\u0631' )
            {
                root = word.substr( 0, 1 );
                root = root + L"\u0627";
                root = root+ word.substr( 2 );
            }
            else
            {
                root = word.substr( 0, 1 );
                root = root + L"\u0623";
                root = root + word.substr( 2 );
            }
        }
        // if the last letter is a shadda, remove it and
        // duplicate the last letter
        if ( word[2] == L'\u0651')
        {
            root = word.substr( 0, 1 );
            //????ADELLE??//
            root = root + word.substr(1, 1);
        }
    }
    // if word is a root, then rootFound is true
    if ( root.length() == 0 )
    {
        if (find(KhojaStemmer::staticFiles.at(16).begin(),KhojaStemmer::staticFiles.at(16).end(),word)!= KhojaStemmer::staticFiles.at(16).end())
        {
            KhojaStemmer::rootFound = true;
            return word;
        }
    }
    // check for the root that we just derived
    else if (find(KhojaStemmer::staticFiles.at(16).begin(),KhojaStemmer::staticFiles.at(16).end(),root)!= KhojaStemmer::staticFiles.at(16).end())
    {
        KhojaStemmer::rootFound = true;
        return root;
    }
    return word;
}
// if the word has four letters
void KhojaStemmer::isFourLetters(wstring word)
{
    if (find(KhojaStemmer::staticFiles.at(12).begin(),KhojaStemmer::staticFiles.at(12).end(),word)!= KhojaStemmer::staticFiles.at(12).end())
        KhojaStemmer::rootFound=true;
}
// check if the word matches any of the patterns
wstring KhojaStemmer::checkPatterns(wstring word)
{
    wstring root(L"");
    // if the first letter is a hamza, change it to an alif
    if ( word.length() > 0 )
        if ( word[0] == L'\u0623' || word[0] == L'\u0625' || word[0] == L'\u0622' )
        {
            root.append(L"\u0627" );
            root.append ( word.substr(1 ) );
            word = root;
        }
    // try and find a pattern that matches the word
    QVector<wstring> patterns=KhojaStemmer::staticFiles.at(15);
    std::size_t numberSameLetters = 0;
    wstring pattern = L"";
    wstring modifiedWord = L"";
    // for every pattern
    for( int i = 0; i < patterns.size(); i++ )
    {
        pattern = patterns[i];
        root.clear();
        // if the length of the words are the same
        if ( pattern.length() == word.length() )
        {
            numberSameLetters = 0;
            // find out how many letters are the same at the same index
            // so long as they're not a fa, ain, or lam
            for ( std::size_t j = 0; j < word.length(); j++ )
                if ( pattern[j] == word[j]  && pattern[j] != L'\u0641' && pattern[j] != L'\u0639' && pattern[j] != L'\u0644')
                    numberSameLetters ++;
            // test to see if the word matches the pattern ЧнксЧ
            if ( word.length() == 6 && word[3] == word[5] && numberSameLetters == 2 )
            {
                root.append ( word.substr(1,1) );
                root.append ( word.substr(2,1) );
                root.append ( word.substr(3,1 ) );
                modifiedWord = root;
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
                if ( KhojaStemmer::rootFound )
                    return modifiedWord;
                else
                    root.clear();
            }
            // if the word matches the pattern, get the root
            if ( word.length() - 3 <= numberSameLetters )
            {
                // derive the root from the word by matching it with the pattern
                for ( std::size_t j = 0; j < word.length(); j++ )
                    if (pattern[j] == L'\u0641' || pattern[j] == L'\u0639' || pattern[j] == L'\u0644')
                        root.append ( word.substr( j,1 ) );
                modifiedWord = root;
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
                if ( KhojaStemmer::rootFound )
                {
                    word = modifiedWord;
                    return word;
                }
            }
        }
    }
    return word;
}
// METHOD CHECKFORSUFFIXES
wstring KhojaStemmer::checkForSuffixes ( wstring word )
{
    wstring suffix = L"";
    wstring modifiedWord = word;
    QVector<wstring> suffixes =KhojaStemmer::staticFiles.at( 14 );
    KhojaStemmer::fromSuffixes= true;
    // for every suffix in the list
    for (int i = 0; i < suffixes.size(); i++ )
    {
        suffix = suffixes[i];
        // if the suffix was found
        //if( suffix.regionMatches ( 0, modifiedWord, modifiedWord.length() - suffix.length(), suffix.length() ) )
        if (modifiedWord.length()>suffix.length() && modifiedWord.compare(modifiedWord.length()-suffix.length(),suffix.length(),suffix)== 0)
        {
            modifiedWord = modifiedWord.substr( 0, modifiedWord.length() - suffix.length() );
            // check to see if the word is a stopword
            if ( KhojaStemmer::checkStopwords ( modifiedWord ) )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
            {
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            }
            else if ( modifiedWord.length() == 3 )
            {
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            }
            else if ( modifiedWord.length() == 4 )
            {
                KhojaStemmer::isFourLetters ( modifiedWord );
            }
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length( ) > 2 )
            {
                modifiedWord = KhojaStemmer::checkPatterns( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
        }
    }
    KhojaStemmer::fromSuffixes = false;
    return word;
}

// check and remove any prefixes
wstring KhojaStemmer::checkForPrefixes ( wstring word )
{
    wstring prefix = L"";
    wstring modifiedWord = word;
    QVector<wstring> prefixes = KhojaStemmer::staticFiles.at( 10 );
    // for every prefix in the list
    for ( int i = 0; i < prefixes.size(); i++ )
    {
        prefix = prefixes[i];
        // if the prefix was found
        //if ( prefix.regionMatches ( 0, modifiedWord, 0, prefix.length() ) )
        if (prefix==modifiedWord.substr(0,prefix.length()))
        {
            modifiedWord = modifiedWord.substr( prefix.length() );
            // check to see if the word is a stopword
            if ( KhojaStemmer::checkStopwords( modifiedWord ) )
                return modifiedWord;
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            else if ( modifiedWord.length() == 4 )
                KhojaStemmer::isFourLetters ( modifiedWord );
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
                modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
            // if the root STILL hasn't been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound && !KhojaStemmer::fromSuffixes)
            {
                // check for suffixes
                modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
                return modifiedWord;
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                return modifiedWord;
            }
        }
    }
    return word;
}
// check and remove the definite article
wstring KhojaStemmer::checkDefiniteArticle ( wstring word )
{
    // looking through the vector of definite articles
    // search through each definite article, and try and
    // find a match
    wstring definiteArticle = L"";
    wstring modifiedWord = L"";
    QVector<wstring> definiteArticles = KhojaStemmer::staticFiles.at( 0 );

    // for every definite article in the list
    for ( int i = 0; i < definiteArticles.size(); i++ )
    {
        definiteArticle = definiteArticles[i];
        // if the definite article was found
        //if ( definiteArticle.regionMatches ( 0, word, 0, definiteArticle.length() ) )
        if (word.length() > definiteArticle.length() && definiteArticle==word.substr(0,definiteArticle.length()))
        {
            // remove the definite article
            modifiedWord = word.substr(definiteArticle.length(), word.length() - definiteArticle.length());
            // check to see if the word is a stopword
            if (KhojaStemmer::checkStopwords ( modifiedWord ) )
                return modifiedWord;
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            else if ( modifiedWord.length() == 4 )
                KhojaStemmer::isFourLetters ( modifiedWord );
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
                modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
            // if the root STILL hasnt' been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                // check for suffixes
                modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
            }
            // if the root STILL hasn't been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                // check for prefixes
                modifiedWord = KhojaStemmer::checkForPrefixes ( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
                return modifiedWord;
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                return modifiedWord;
            }
        }
    }
    if ( modifiedWord.length() > 3 )
        return modifiedWord;
    return word;
}
// check and remove the special prefix (waw)
wstring KhojaStemmer::checkPrefixWaw ( wstring word )
{
    wstring modifiedWord = L"";
   // if ( word.length() > 3 && word.at( 0 ) == '\u0648' )
    if ( word.length() > 3 && word[0] == L'\u0648' )
    {
        modifiedWord = word.substr(1 );
        // check to see if the word is a stopword
        if ( KhojaStemmer::checkStopwords ( modifiedWord ) )
            return modifiedWord;
        // check to see if the word is a root of three or four letters
        // if the word has only two letters, test to see if one was removed
        if ( modifiedWord.length() == 2 )
            modifiedWord = KhojaStemmer::isTwoLetters( modifiedWord );
        else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
            modifiedWord = KhojaStemmer::isThreeLetters( modifiedWord );
        else if ( modifiedWord.length() == 4 )
            KhojaStemmer::isFourLetters ( modifiedWord );
        // if the root hasn't been found, check for patterns
        if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
            modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
        // if the root STILL hasnt' been found
        if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            // check for suffixes
            modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
        }
        // iIf the root STILL hasn't been found
        if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            // check for prefixes
            modifiedWord = KhojaStemmer::checkForPrefixes ( modifiedWord );
        }
        if ( KhojaStemmer::stopwordFound )
            return modifiedWord;
        if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            return modifiedWord;
        }
    }
    return word;
}
wstring KhojaStemmer::stemWord ( wstring word )
{
    // check if the word consists of two letters
    // and find it's root
    if ( word.length() == 2 )
        word = KhojaStemmer::isTwoLetters ( word );
    // if the word consists of three letters
    if( word.length() == 3 && !KhojaStemmer::rootFound )
        // check if it's a root
        word = KhojaStemmer::isThreeLetters ( word );
    // if the word consists of four letters
    if( word.length() == 4 )
        // check if it's a root
        KhojaStemmer::isFourLetters ( word );
    // if the root hasn't yet been found
    if( !KhojaStemmer::rootFound )
    {
        // check if the word is a pattern
        word = KhojaStemmer::checkPatterns ( word );
    }
    // if the root still hasn't been found
    if ( !KhojaStemmer::rootFound )
    {
        // check for a definite article, and remove it
        word = KhojaStemmer::checkDefiniteArticle ( word );
    }
    // if the root still hasn't been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for the prefix waw
        word = KhojaStemmer::checkPrefixWaw ( word );
    }
    // if the root STILL hasnt' been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for suffixes
        word = KhojaStemmer::checkForSuffixes ( word );
    }
    // if the root STILL hasn't been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for prefixes
        word = KhojaStemmer::checkForPrefixes ( word );
    }
    KhojaStemmer::rootFound = false;
    KhojaStemmer::patternFound = false;
    KhojaStemmer::stopwordFound = false;
    KhojaStemmer::strangeWordFound = false;
    KhojaStemmer::rootNotFound = false;
    KhojaStemmer::fromSuffixes = false;
    return word;
}

/*QString KhojaStemmer::khojastemmercmd(QString mot)
{
    //qDebug() << "mot a lemmatiser: " << mot;
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("echo %1|   tee infile.txt | java -cp ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line.jar infile.txt" ).arg(mot));
    //java -jar khoja-stemmer-command-line.jar test-in.txt test-out.txt
    process->waitForFinished();
    QString motlem= process->readAll();
    qDebug() << "lemma:" << motlem;
    return motlem;
}

void KhojaStemmer::khojastemmercmd(QString infile, QString outfile)
{
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("cd ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line; java -jar khoja-stemmer-command-line.jar %1 %2" ).arg(infile).arg(outfile));
    qDebug() << "khojastemmercmd: java -jar ../../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line.jar " << infile << " " << outfile;
    process->waitForFinished();
    return;
}*/

void KhojaStemmer::khojastemmercmd(QString infile, QString outfile,int nbthread)
{
    QProcess *process = new QProcess;
    process->start("sh", QStringList()   << "-c" << QString ("java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultithreadKhoja.jar %1 %2 %3" ).arg(infile).arg(outfile).arg(nbthread));
    qDebug() << "khojastemmercmd: java -jar ../CorpusPatterns/CorpusWord/PreprocessingBox/khoja-stemmer-command-line/MultithreadKhoja.jar " << infile << " " << outfile << " " << nbthread;
    process->waitForFinished(-1);
    return;
}

/**********************BuckWalter Encoding/Decoding******************************************/
QVector<QPair<QString,QString>> BuckWalterCoder::buckwaltermapping;
bool BuckWalterCoder::addVectorFromFile()
{
    QList<QByteArray> tmp;
    QFile infile("../Interface/PreprocessingBox/BuckWalterFiles/BuckwalterCodes.txt");
    if (not infile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier buckwalter en lecture." << endl ;
        return false;
    }
        while (not infile.atEnd())
        {
            QByteArray line= infile.readLine();
            tmp=line.split('\t');
            BuckWalterCoder::buckwaltermapping.append(QPair<QString,QString>(tmp[0],tmp[1]));
        }
      infile.close() ;
      for(int i=0;i<BuckWalterCoder::buckwaltermapping.size();i++)
          qDebug() << BuckWalterCoder::buckwaltermapping[i].first <<";"<< BuckWalterCoder::buckwaltermapping[i].second;
      return true;
}
/*
void StanfordSegmenter::stanfordsegmentercmd(QString infile, QString outfile)
{
    ostringstream out;
    string cmd;
    out << "java -cp ../Interface/PreprocessingBox/stanford-segmenter-2015-12-09/stanford-segmenter-3.6.0.jar:../Interface/PreprocessingBox/stanford-segmenter-2015-12-09/slf4j-api.jar:../Interface/PreprocessingBox/stanford-segmenter-2015-12-09/slf4j-simple.jar  edu.stanford.nlp.international.arabic.process.ArabicSegmenter -loadClassifier ../Interface/PreprocessingBox/stanford-segmenter-2015-12-09/data/arabic-segmenter-atb+bn+arztrain.ser.gz -orthoOptions removeMorphMarker,removeProMarker  -textFile " << infile.toUtf8().constData() << " > " << outfile.toUtf8().constData();
    cmd=out.str();
    system(cmd.c_str());
}*/


void StanfordTagger::stanfordtaggercmd(QString infile, QString outfile)
{
    ostringstream out;
    string cmd;

    out << "java -cp ../Interface/PreprocessingBox/stanford-postagger-full-2015-12-09/stanford-postagger.jar:../Interface/PreprocessingBox/stanford-postagger-full-2015-12-09/lib/* edu.stanford.nlp.tagger.maxent.MaxentTagger -model ../Interface/PreprocessingBox/stanford-postagger-full-2015-12-09/models/arabic.tagger -textFile " << infile.toUtf8().constData() << " > " << outfile.toUtf8().constData();
    //out << "java -cp ../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-postagger-full-2015-12-09/stanford-postagger.jar:../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-postagger-full-2015-12-09/lib/* edu.stanford.nlp.tagger.maxent.MaxentTagger -model ../CorpusPatterns/CorpusWord/PreprocessingBox/stanford-postagger-full-2015-12-09/models/arabic.tagger -textFile " << infile.toUtf8().constData() << " > " << outfile.toUtf8().constData();

    cmd=out.str();
    system(cmd.c_str());
}
